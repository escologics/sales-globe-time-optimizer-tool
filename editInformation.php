<?php
include('includes/top.php');

if(Input::exists())
{
    if($_POST['editUser'] == 'editManager'){
        DB::getInstance()->update('users', Input::post('user_id'), array(
                'first_name' => Input::post('first_name'),
                'last_name' => Input::post('last_name'),
                'company'   => Input::post('company'),
                'email' => Input::post('email'),
                'level' => Input::post('level'),
         ));
        
       
         Redirect::to(Input::post('current_page').'?user_id='.Input::post('user_id'));
    
    } elseif($_POST['editUser'] == 'editsalesr'){
         DB::getInstance()->update('users', Input::post('user_id'), array(
                'first_name' => Input::post('first_name'),
                'last_name' => Input::post('last_name'),
                'company'   => Input::post('company'),
                'email' => Input::post('email'),
                'level' => Input::post('level'),
                'assigned_to' => Input::post('assigned_to'),
         ));
        
         Redirect::to(Input::post('current_page'));
    }
    
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
