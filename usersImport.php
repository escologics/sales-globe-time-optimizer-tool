<?php
$page_title = 'Users Import';
include('includes/top.php');
if (!Session::get('login')) {
    Redirect::to('index.php');
}
if (Session::get('level') == 3 || Session::get('level') == 4) {
    Redirect::to('viewMyday.php');
}
include('includes/header.php');

$error_manager = '';
$success_manaer = '';
$error_salesrep = '';
$success_salesrep = '';
if (isset($_POST['managers_import_submit'])) {
    $allowed = array('csv');
    $filename = $_FILES['managers_import_file']['name'];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    if (is_uploaded_file($_FILES['managers_import_file']['tmp_name'])) {
        if (!in_array($ext, $allowed)) {
            $error_manager .= '<p class="error_messages">File Type is Invalid.</p>';
        } else {
            $handle = fopen($_FILES['managers_import_file']['tmp_name'], "r");
            $i = 0;
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($i++ > 0) {

                    $error_manager_count = 0;
                    if ($data[0] != '' && $data[1] != '' && $data[2] != '' && $data[3] != '' && $data[4] != '') {
                        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $data[0])) {
                            $error_manager .= '<p class="error_messages">' . $data[0] . ' Please insert valid First Name</p>';
                            $error_manager_count = 1;
                        }

                        if (filter_var($data[3], FILTER_VALIDATE_EMAIL) === false) {
                            $error_manager .= '<p class="error_messages">' . $data[3] . ' Please insert valid Email Address</p>';
                            $error_manager_count = 1;
                        }
                        if ($data[5] == '') {
                            $error_manager .= '<p class="error_messages">' . $data[5] . ' Please insert valid Sales Team ID</p>';
                            $error_manager_count = 1;
                        }
                        if (User::checkByUsername($data[2]) != 0) {
                            $error_manager .= '<p class="error_messages">' . $data[2] . ' Username Already Exist</p>';
                            $error_manager_count = 1;
                        }
                        if (User::checkByEmail($data[3], 0) != 0) {
                            $error_manager .= '<p class="error_messages">' . $data[3] . ' Email Address Already Exist</p>';
                            $error_manager_count = 1;
                        }
                        if ($error_manager_count == 1) {
                            $error_manager .='<hr/>';
                        }

                        if ($error_manager_count == 0) {
                            $import_data_manager = array(
                                'first_name' => $data[0],
                                'last_name' => $data[1],
                                'username' => $data[2],
                                'email' => $data[3],
                                'password' => md5($data[4]),                          
                                'created_user' => date('Y-m-d'),
                                'level' => 3,
                                'status' => 1
                            );
                            $last_insert_id = DB::getInstance()->insert('users', $import_data_manager);

                            $user_sales_teams_ids = explode(',', trim($data[5]));
                            if (!empty($user_sales_teams_ids)) {
                                foreach ($user_sales_teams_ids as $user_sales_teams_id) {
                                    DB::getInstance()->insert('user_sales_team', array(
                                        'sales_team_id' => $user_sales_teams_id,
                                        'user_id' => $last_insert_id
                                    ));
                                }
                            }

                            $success_manaer .= '<p class="success_messages">' . $data[0] . ' ' . $data[1] . ' Manager Successfully Inserted</p><hr/>';
                            continue;
                        }
                    }
                }
            }
            fclose($handle);
        }
    } else {
        $error_manager .= '<p class="error_messages">Please select csv file.</p>';
    }
}

if (isset($_POST['salesrep_import_submit'])) {
    $allowed = array('csv');
    $filename = $_FILES['salesrep_import_file']['name'];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    if (is_uploaded_file($_FILES['salesrep_import_file']['tmp_name'])) {
        if (!in_array($ext, $allowed)) {
            $error_salesrep .= '<p class="error_messages">File Type is Invalid.</p>';
        } else {
            $handle = fopen($_FILES['salesrep_import_file']['tmp_name'], "r");
            $i = 0;
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                if ($i++ > 0) {
                    $error_salesrep_count = 0;
                    if ($data[0] != '' && $data[1] != '' && $data[2] != '' && $data[3] != '' && $data[4] != '') {

                        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $data[0])) {
                            $error_salesrep .= '<p class="error_messages">' . $data[0] . ' Please insert a valid First Name</p>';
                            $error_salesrep_count = 1;
                        }

                        if (filter_var($data[3], FILTER_VALIDATE_EMAIL) === false) {
                            $error_salesrep .= '<p class="error_messages">' . $data[3] . ' Please insert a valid Email Address</p>';
                            $error_salesrep_count = 1;
                        }
                        if (!preg_match("/^[[:digit:]]+$/", $data[5])) {
                            $error_salesrep .= '<p class="error_messages">' . $data[5] . ' Please insert a valid Sales Team ID</p>';
                            $error_salesrep_count = 1;
                        }
                        if (!preg_match("/^[[:digit:]]+$/", $data[6])) {
                            $error_salesrep .= '<p class="error_messages">' . $data[6] . ' Please insert a valid Title ID</p>';
                            $error_salesrep_count = 1;
                        }
                        if (User::checkByUsername($data[2]) != 0) {
                            $error_salesrep .= '<p class="error_messages">' . $data[2] . ' Username Already Exist</p>';
                            $error_salesrep_count = 1;
                        }
                        if (User::checkByEmail($data[3], 0) != 0) {
                            $error_salesrep .= '<p class="error_messages">' . $data[3] . ' Email Address Already Exist</p>';
                            $error_salesrep_count = 1;
                        }
                        if ($error_salesrep_count == 1) {
                            $error_salesrep .='<hr/>';
                        }

                        if ($error_salesrep_count == 0) {
                            $import_data_manager = array(
                                'first_name' => $data[0],
                                'last_name' => $data[1],
                                'username' => $data[2],
                                'email' => $data[3],
                                'password' => md5($data[4]),
                                'industry_id' => $data[5],
                                'designation' => $data[6],
                                'created_user' => date('Y-m-d'),
                                'level' => 4,
                                'status' => 1
                            );
                            DB::getInstance()->insert('users', $import_data_manager);
                            $success_salesrep .= '<p class="success_messages">' . $data[0] . ' ' . $data[1] . ' Sales Representative Successfully Inserted</p><hr/>';
                            continue;
                        }
                    }
                }
            }
            fclose($handle);
        }
    } else {
        $error_salesrep .= '<p class="error_messages">Please select csv file.</p>';
    }
}
?>

<section class="createManager clearfix">
    <div class="tabss createTab">
        <ul class="clearfix tabsNavigationn">
            <li><a href="#tab_manager" class="<?php echo isset($_POST['managers_import_submit']) ? 'selected' : '' ?>">Managers</a></li>
            <li><a href="#tab_sales" class="<?php echo isset($_POST['salesrep_import_submit']) ? 'selected' : '' ?>">Sales Representatives</a></li>
        </ul><!-- ( TABS LINK END ) -->
        <div class="tabs_container clearfix">
            <div id="tab_manager" class="createTabContentt" style="<?php echo isset($_POST['managers_import_submit']) ? 'display: block;' : 'display: none;' ?>">
                <?php if (!empty($error_manager)) : ?>
                    <?php echo $error_manager; ?>
                <?php endif; ?>
                <?php if (!empty($success_manaer)) : ?>
                    <?php echo $success_manaer; ?>
                <?php endif; ?>
                <form action="" class="clearfix" id="myform" method="POST" enctype="multipart/form-data">
                    <div class="width_240 floatLeft">
                        <p>
                            <label for="Select File" class="req">Select CSV File</label>
                            <input type="file" tabindex="1" name="managers_import_file">
                        </p>
                        <p>                            
                            <input type="submit" tabindex="2" name="managers_import_submit" value="Import">
                        </p>                      
                    </div>                   
                </form>
            </div><!-- ( TAB 1 END ) -->
            <div id="tab_sales" class="createTabContentt" <?php echo isset($_POST['salesrep_import_submit']) ? 'style="display: block;"' : 'style="display: none;"' ?>>
                <form action="" class="clearfix" id="myform" method="POST" enctype="multipart/form-data">
                    <?php if (!empty($error_salesrep)) : ?>
                        <?php echo $error_salesrep; ?>
                    <?php endif; ?>
                    <?php if (!empty($success_salesrep)) : ?>
                        <?php echo $success_salesrep; ?>
                    <?php endif; ?>
                    <div class="width_240 floatLeft">
                        <p>
                            <label for="Select File" class="req">Select CSV File</label>
                            <input type="file" tabindex="1" name="salesrep_import_file">
                        </p>
                        <p>                            
                            <input type="submit" tabindex="2" name="salesrep_import_submit" value="Import">
                        </p>                      
                    </div>                   
                </form>
            </div><!-- ( TAB 2 END ) -->
        </div><!-- ( TABS CONTAINER END ) -->
    </div><!-- ( TABS END ) -->
</section>
<?php if (isset($_POST)) { ?>
    <script>
        $(function () {
            var tabsContainers = $('div.tabss > div > div');
            $('div.tabss ul.tabsNavigationn a').click(function () {
                tabsContainers.hide();
                tabsContainers.filter(this.hash).show();
                $('div.tabss ul.tabsNavigationn a').removeClass('selected');
                $(this).addClass('selected');
                return false;
            });
        });
    </script>
<?php } else { ?>
    <script>
        $(function () {
            var tabsContainers = $('div.tabss > div > div');
            tabsContainers.hide();
            $('div.tabss ul.tabsNavigationn a').click(function () {
                tabsContainers.hide();
                tabsContainers.filter(this.hash).show();
                $('div.tabss ul.tabsNavigationn a').removeClass('selected');
                $(this).addClass('selected');
                return false;
            });
        });
    </script>
<?php } ?>
<?php include('includes/footer.php'); ?>