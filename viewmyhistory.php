<?php
$page_title = 'View My History';
include('includes/top.php');
if (!Session::get('login')) {
    Redirect::to('index.php');
}
include('includes/header.php');

if (isset($_POST['go']) == 'Go') {
    $user_id = Session::get('user_id');
    $start_date = $_POST['start_date'];
    $start_date = date("Y-m-d", strtotime($start_date));
    $end_date = $_POST['end_date'];
    $end_date = date("Y-m-d", strtotime($end_date));
    $date_array = explode('-', $end_date);
    $month = $date_array[0];
    $date = $date_array[1];
    $year = $date_array[2];
    $err = '';
    if ($start_date == "" || $end_date == "") {
        $err = "Please select date range.<br><br>";
    }
    if ($err == '') {
        $totalHours = Reporting::getTotalHoursByDateRangeAndUserId($start_date, $end_date, $user_id);
        $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndUserId($start_date, $end_date, $user_id);
        if (empty($meetingHistoryData)) {
            $success_m = "<p style='color: #e80702; font-weight: bold;'>No records were found.</p>";
        }
    } else {
        $success_m = "<p style='color: #e80702; font-weight: bold;'>" . $err . "</p>";
    }
}

if (isset($_POST['go_menual']) == 'Go') {
    $user_id = Session::get('user_id');
    $start_date = $_POST['m_strat_date'];
    $start_date = date("Y-m-d", strtotime($start_date));
    $end_date = $_POST['m_end_date'];
    $end_date = date("Y-m-d", strtotime($end_date));
    $err = '';
    if ($_POST['m_strat_month'] == "" || $_POST['m_strat_day'] == "" || $_POST['m_strat_year'] == "" || $_POST['m_end_month'] == "" || $_POST['m_end_day'] == "" || $_POST['m_end_year'] == "") {
        $err = "Please select date range.<br><br>";
    }
    if ($err == '') {
       $totalHours = Reporting::getTotalHoursByDateRangeAndUserId($start_date, $end_date, $user_id);
        $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndUserId($start_date, $end_date, $user_id);
        if (empty($meetingHistoryData)) {
            $success_m = "<p style='color: #e80702; font-weight: bold;'>No records were found.</p>";
        }
    } else {
        $success_m = "<p style='color: #e80702; font-weight: bold;'>" . $err . "</p>";
    }
}

if (isset($_POST['go_days']) == 'Go') {
    $user_id = Session::get('user_id');
    $value = $_POST['days_select'];
    $err = '';
    if ($err == '') {
        $totalHours = Reporting::getTotalHoursByDaysAndUserId($value, $user_id);
        $meetingHistoryData = Reporting::getMeetingHistoryByDaysAndUserId($value, $user_id);
        $success_m_days = '';
        if (empty($meetingHistoryData)) {
            $success_m_days = "<p style='color: #e80702; font-weight: bold;'>No records were found.</p>";
        }
    } else {
        $success_m_days = "<p style='color: #e80702; font-weight: bold;'>" . $err . "</p>";
    }
}
?>
<script>
    $(document).ready(function () {
        $("#calender_image_viewhistory1").datepicker({
            showOn: "both",
            buttonImage: "assets/images/calendar.jpg",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            yearRange: "-3:+0",
            onSelect: function (selectedDate, obj) {
                var date_array = selectedDate.split("/");
                var month = date_array[0];
                var days = date_array[1];
                var year = date_array[2];
                $('#m_strat_month').val(month);
                $('#m_strat_day').val(days);
                $('#m_strat_year').val(year);
                $('#m_strat_date').val(selectedDate);
            }
        });

        $("#calender_image_viewhistory2").datepicker({
            showOn: "both",
            buttonImage: "assets/images/calendar.jpg",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            yearRange: "-3:+0",
            onSelect: function (selectedDate, obj) {
                var date_array = selectedDate.split("/");
                var month = date_array[0];
                var days = date_array[1];
                var year = date_array[2];
                $('#m_end_month').val(month);
                $('#m_end_day').val(days);
                $('#m_end_year').val(year);
                $('#m_end_date').val(selectedDate);
            }
        });

        $(".m_date_check").on('click', function () {
            if ($(this).is(':checked')) {
                $('#pickerSelcet').hide();
                $('#menualSelcet').show();
                $('#start_date').attr('disabled', true);
                $('#end_date').attr('disabled', true);
                $('#m_strat_month').attr('disabled', false);
                $('#m_strat_day').attr('disabled', false);
                $('#m_strat_year').attr('disabled', false);
                $('#m_end_month').attr('disabled', false);
                $('#m_end_day').attr('disabled', false);
                $('#m_end_year').attr('disabled', false);
            } else {
                $('#pickerSelcet').show();
                $('#menualSelcet').hide();
                $('#start_date').attr('disabled', false);
                $('#end_date').attr('disabled', false);
                $('#m_strat_month').attr('disabled', true);
                $('#m_strat_day').attr('disabled', true);
                $('#m_strat_year').attr('disabled', true);
                $('#m_end_month').attr('disabled', true);
                $('#m_end_day').attr('disabled', true);
                $('#m_end_year').attr('disabled', true);

            }
        });

        $(".m_date").change(function () {
            if ($(this).attr('id') == 'm_strat_month') {
                if ($(this).val() > 12) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }
            } else if ($(this).attr('id') == 'm_strat_day') {
                if ($(this).val() > daysInMonth($('#m_strat_month').val(), $('#m_strat_year').val())) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }
            } else if ($(this).attr('id') == 'm_strat_year') {
                if ($(this).val() > 2015) {
                    $(this).val('2015');
                } else if ($(this).val() < 2012) {
                    $(this).val('2015');
                }
            } else if ($(this).attr('id') == 'm_end_month') {
                if ($(this).val() > 12) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }
            } else if ($(this).attr('id') == 'm_end_day') {
                if ($(this).val() > daysInMonth($('#m_end_month').val(), $('#m_end_year').val())) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }
            } else if ($(this).attr('id') == 'm_end_year') {
                if ($(this).val() > 2015) {
                    $(this).val('2015');
                } else if ($(this).val() < 2012) {
                    $(this).val('2015');
                }
            }
            var m_strat_date = ($('#m_strat_month').val() + '/' + $('#m_strat_day').val() + '/' + $('#m_strat_year').val());
            $('#m_strat_date').val(m_strat_date);
            var m_end_date = ($('#m_end_month').val() + '/' + $('#m_end_day').val() + '/' + $('#m_end_year').val());
            $('#m_end_date').val(m_end_date);
        });
    });
    function daysInMonth(month, year) {
        return new Date(year, month, 0).getDate();
    }
</script>
<style>
    img.ui-datepicker-trigger {
        margin: -5px 0px 0px 0px;
    }
</style>
<section class="mainContent viewMyHistory clearfix">
    <div class="filterBox clearfix">
        <h2>Select a Date Range</h2>
        <form action="" name="ViewUnsavedForm" id="ViewUnsavedForm" method="POST" class="clearfix">
            <?php if (!empty($success_m)) {
                ?>
                <p style="color:green; font-weight: bold;"><?php echo $success_m; ?></p>
            <?php } ?>
            <div class="grid_5 dateArea clearfix marginNone">
                <p class="dateIcon" style="width: 160px;">                   
                    <input type="number" name="m_strat_month" id="m_strat_month" class="m_date" placeholder="mm" value="<?php echo $_POST['m_strat_month']; ?>" style="width: 38px;">
                    <input type="number" name="m_strat_day" id="m_strat_day" class="m_date" placeholder="dd" value="<?php echo $_POST['m_strat_day']; ?>" style="width: 30px;">
                    <input type="number" name="m_strat_year" id="m_strat_year" class="m_date" placeholder="yy" value="<?php echo $_POST['m_strat_year'] != "" ? $_POST['m_strat_year'] : '2015'; ?>" style="width: 47px;">
                    <input type="hidden" name="m_strat_date" id="m_strat_date" value="<?php echo $_POST['m_strat_date'] ?>">
                    <input type="hidden" id="calender_image_viewhistory1"  class="datepicker_view_my_history"/>                    
                </p>
                <label>to</label>
                <p class="dateIcon" style="width: 160px;">
                    <input type="number" name="m_end_month" id="m_end_month" class="m_date" placeholder="mm" value="<?php echo $_POST['m_end_month']; ?>" style="width: 38px;">
                    <input type="number" name="m_end_day" id="m_end_day" class="m_date" placeholder="dd" value="<?php echo $_POST['m_end_day']; ?>" style="width: 30px;">
                    <input type="number" name="m_end_year" id="m_end_year" class="m_date" placeholder="yy" value="<?php echo $_POST['m_end_year'] != "" ? $_POST['m_end_year'] : '2015'; ?>" style="width: 47px;">                    
                    <input type="hidden" name="m_end_date" id="m_end_date" value="<?php echo $_POST['m_end_date'] ?>">
                    <input type="hidden" id="calender_image_viewhistory2"  class="datepicker_view_myhistory"/>
                </p>
                <input type="submit" name="go_menual" value="Go" class="redBtn goBTN" id="menualSelcet">
            </div>
        </form>
        <form action="" name="historybydays" id="historybydays" method="POST" class="clearfix">
            <?php if (!empty($success_m_days)) {
                ?>
                <p style="color:green; font-weight: bold;"><?php echo $success_m_days; ?></p>
            <?php } ?>
            <div class="grid_5 dateArea clearfix marginNone">
                <select name="days_select">
                    <option value="7" <?php echo ($_POST['days_select']) == 7 ? 'selected="selected"' : ''; ?>>Last 7 Days</option>
                    <option value="30" <?php echo ($_POST['days_select']) == '30' ? 'selected="selected"' : ''; ?>>Last 30 Days</option>
                    <option value="60" <?php echo ($_POST['days_select']) == '60' ? 'selected="selected"' : ''; ?>>Last 60 Days</option>
                    <option value="90" <?php echo ($_POST['days_select']) == '90' ? 'selected="selected"' : ''; ?>>Last 90 Days</option>
                    <option value="6m" <?php echo ($_POST['days_select']) == '6m' ? 'selected="selected"' : ''; ?>>Last 6 Months</option>
                    <option value="12m" <?php echo ($_POST['days_select']) == '12m' ? 'selected="selected"' : ''; ?>>Last 12 Months</option>
                </select>
                <input type="submit" name="go_days" value="Go" class="redBtn goBTN">
            </div>
        </form>
        <?php if (isset($totalHours) && $totalHours != 0) {             
            ?>          
            <p class="viewhistoryTopcontent">Total Hours & Minutes For Selected Date Range: <?php echo m2h("i:s", $totalHours); ?></p>
        <?php } ?>
    </div>

    <?php if ($meetingHistoryData) {
        ?>
        <div class="tableSec summryTable clearfix">
            <div class="summryrow clearfix">
                <div class="summryindent clearfix">
                    <div class="summryTableHead clearfix">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr class="mainTitle">
                                    <th width="20%" align="left" class="tdCol-1"><span>Activity Summary</span></th>
                                    <th width="8%"  align="center" class="tdCol-2"><span>Time Spent (hh:mm)</span></th>
                                    <th width="8%"  align="center" class="tdCol-2"><span>% to Total Time</span></th>                                    
                                    <th width="8%"  align="center" class="tdCol-2"><span>Difficulty Level</span></th>      
                                </tr>
                            </thead>
                        </table>
                    </div>
                <?php } ?>

                <?php
                if ($meetingHistoryData) {
                    foreach ($meetingHistoryData as $meeting) {
                        if (isset($_POST['go_days'])) {
                            $ActivityTypedata = Reporting::getActivityTypeHoursbyDays($value, $user_id, $meeting['meeting_category']);
                            $ActivityTypedatacomments = Reporting::getActivityTypeCommentsbyDays($value, $user_id, $meeting['meeting_category']);
                        } else {
                            $ActivityTypedata = Reporting::getActivityTypeHours($start_date, $end_date, $user_id, $meeting['meeting_category']);
                            $ActivityTypedatacomments = Reporting::getActivityTypeComments($start_date, $end_date, $user_id, $meeting['meeting_category']);
                        }

                        $rate_count = 0;
                        foreach ($ActivityTypedata as $row_count) {
                            $rate_count += ($row_count['Rate']);
                        }
                        ?>
                        <div class="percentageTable clearfix">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <thead>                                  
                                    <tr class="subTitle">                                      
                                        <th width="20%" align="left" class="tdCol-1"><span><?php echo $meeting['name']; ?></span></th>
                                        <th align="center" class="tdCol-2"><span><?php echo m2h("i:s", $meeting['hours']); ?></span></th>
                                        <th align="center" class="tdCol-2"><span><?php echo round($meeting['hours'] / $totalHours * 100, 2); ?>%</span></th>
                                        <th align="center" class="tdCol-2"><span><?php echo number_format($rate_count / sizeof($ActivityTypedata), 2); ?></span></th>     
                                    </tr>
                                </thead>
                            </table>                           
                            <div class="grid_12a leftTableInfo">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <?php
                                        $other_fill = '';
                                        $other_value_fill = '';
                                        foreach ($ActivityTypedata as $activity_type) {
                                            $activity_type_id[] = $activity_type['id'];
                                            $activeper = $activity_type['hours'] / $totalHours * 100;
                                            ?>
                                            <tr>                                                                        
                                                <td class="tdCol-1 noBorderTop"><?php echo $activity_type['name']; ?></td>
                                                <td class="tdCol-2 noBorderTop" align="center"><?php echo m2h("i:s", $activity_type['hours']); ?></td>
                                                <td class="tdCol-2 noBorderTop" align="center"><?php echo round($activity_type['hours'] / $totalHours * 100, 2); ?>%</td>
                                                <td align="center" class="tdCol-2"><span><?php echo number_format($activity_type['Rate'], 2); ?></span></td>                                                                                                                                                                         
                                                <td class="tdCol-3 noBorderTop tdNone"></td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                        ?>                                     
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <?php
                    }
                }
                ?>                          
            </div>
        </div>
</section>
<?php
include('includes/footer.php');
