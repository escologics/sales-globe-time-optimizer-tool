<?php
include('includes/top.php'); ?>
<link rel="stylesheet" href="assets/css/drop/chosen.css"/>
<?php
$category_ids = $_POST['category'];
$user_id_check = Session::get('user_id');
$meeting_date_check = $_POST['meeting_date_check'];
$meeting_id_check = Meeting::getMeetingIdByDateAndUserId($meeting_date_check, $user_id_check);
$totalHours = Meeting::getTotalTimeByDateAndUserId($meeting_date_check, $user_id_check);
$html = '';
$submited_activity = '';
$counter = 0;
$cn = 0;
$html .= '<input type="hidden" id="checkAddanotheractivity" value="'.$meeting_date_check.'"/>';
foreach ($category_ids as $category_id) {
    $activity_types = Meeting::getActivityTypeByMeetingCategoryId($category_id['category']);
    $max = count($activity_types);
    $i = 0;

    $html .= ' <table border="0" cellspacing="0" cellpadding="0">';
    if ($counter <= 0) {
        $html .= '<thead>
                        <tr>
                            <th>Activity</th>
                            <th>Hours & Minutes</th>
                            <th>Sub-Activity</th>   
                            <th style="line-height: 15px; text-align: center;">Difficulty of Completing Activity  <a href="#" data-toggle="tooltip" title="Assess the difficulty that systems, time constraints, resource availability, or personal knowledge impose on your ability to complete this activity. Please add details in Comment box."><img src="assets/images/info-icon.png"/></a></th> 
                            <th>Comment</th> 
                        </tr>
                    </thead>';
    }
    $html .= '<tbody id="">';
    for ($i = 0; $i < $max; $i++) {

        $html .= '<tr>';
        

        if ($i < 1) {
            $html .= '<td style="width: 15%;"><strong style="text-align: center;">' . Meeting::getMeetingCategoryNameById($category_id['category']) . '</strong></td>';
        } elseif ($i < 2) {
            $html .= '<td rowspan="15" class="light"></td>';
        }
        $m = '';
        if (!empty($activity_types[$i]['name'])) {
            if (!empty($meeting_date_check)) {
                $submited_activity = Meeting::GetMeeting($activity_types[$i]['id'], $meeting_date_check, $meeting_id_check, $user_id_check);
            }
            $html .= '<td align="center" style="width: 10%;">';

            if ($submited_activity['activity_type'] == $activity_types [$i]['id']) {
                $html .= '<input type="checkbox"  name="activity_type" class="activity_typeclass" onclick="enabledCheckbox(this);" value="' . $activity_types [$i]['id'] . '" data-rowid="' . $cn . '" checked="checked">';
            } else {
                $html .= '<input type="checkbox"  name="activity_type" class="activity_typeclass" onclick="enabledCheckbox(this);" value="' . $activity_types [$i]['id'] . '" data-rowid="' . $cn . '">';
            }

            if ($submited_activity['activity_type'] == $activity_types [$i]['id']) {
                $actualtime = gmdate("i:s", $submited_activity['hours']);
                $houra_array = explode(':', $actualtime);
                $hr = ltrim($houra_array[0], '0');
                $mn = ltrim($houra_array[1], '0');

                $html .= '<div class="timeFields">                    
                    <div>                    
                    <select data-placeholder="Hours" id="hours" class="chosen-select hours' . $cn . '">
                        <option value=""></option>';
                for ($x = 0; $x <= 12; $x++) {
                    if ($x == $hr) {
                        $selected_hr = 'selected="selected"';
                    } else {
                        $selected_hr = '';
                    }
                    $html .= '<option value="' . $x . '" ' . $selected_hr . '>' . $x . '</option>';
                }
                $html .= '</select>
                    </div>
                    <div>                    
                    <select data-placeholder="Minutes" id="minutes" class="chosen-select minutes' . $cn . '">
                        <option value=""></option>';
                for ($x = 0; $x <= 60; $x++) {
                    if ($x == $mn) {
                        $selected_mn = 'selected="selected"';
                    } else {
                        $selected_mn = '';
                    }
                    $html .= '<option value="' . $x . '" ' . $selected_mn . '>' . $x . '</option>';
                }
                $html .= '</select>
                    </div>
                    </div>';
            } else {
                $html .= '<div class="timeFields" style="display:none;">                    
                    <div>                    
                    <select data-placeholder="Hours" id="hours" class="chosen-select hours' . $cn . '" onchange="checkhours_viewMyDay(this)">
                        <option value=""></option>';
                for ($x = 0; $x <= 12; $x++) {
                    $html .= '<option value="' . $x . '">' . $x . '</option>';
                }
                $html .= '</select>
                    </div>
                    <div>                    
                    <select data-placeholder="Minutes" id="minutes" class="chosen-select minutes' . $cn . ' "onchange="checkhours_viewMyDay(this)">
                        <option value=""></option>';
                for ($x = 0; $x <= 60; $x++) {
                    $html .= '<option value="' . $x . '">' . $x . '</option>';
                }
                $html .= '</select>
                    </div>
                    </div>';
            }

            $html .= '<input  type="hidden" name="activity_category" class="activity_category' . $cn . '" value="' . $category_id['category'] . '" />
                  </td>';
            $html .= '<td>                    
                    <label for="">' . $activity_types [$i]['name'] . '</label>
                  </td>';

            if ($submited_activity['activity_type'] == $activity_types [$i]['id']) {

                if ($submited_activity['activity_rate'] == 1) {
                    $v1 = 'checked';
                } else {
                    $v1 = '';
                }
                if ($submited_activity['activity_rate'] == 2) {
                    $v2 = 'checked';
                } else {
                    $v2 = '';
                }
                if ($submited_activity['activity_rate'] == 3) {
                    $v3 = 'checked';
                } else {
                    $v3 = '';
                }
                if ($submited_activity['activity_rate'] == 4) {
                    $v4 = 'checked';
                } else {
                    $v4 = '';
                }
                if ($submited_activity['activity_rate'] == 5) {
                    $v5 = 'checked';
                } else {
                    $v5 = '';
                }

                $html .= '<td class="light" style="width: 16%;">
                    <div class="ratehideall">
                        <div class="activityrating rating' . $cn . '">
                            <input type="radio" name="activity_rate" ' . $v1 . ' class="ratingAll rating ' . $v1 . '" value="1" />
                            <input type="radio" name="activity_rate" ' . $v2 . ' class="ratingAll rating ' . $v2 . '" value="2" />
                            <input type="radio" name="activity_rate" ' . $v3 . ' class="ratingAll rating ' . $v3 . '" value="3" />
                            <input type="radio" name="activity_rate" ' . $v4 . ' class="ratingAll rating ' . $v4 . '" value="4" />
                            <input type="radio" name="activity_rate" ' . $v5 . ' class="ratingAll rating ' . $v5 . '" value="5" />
                        </div>
                    </div>
                    <span class="messagerateHideall" style="display:none;">Rate This Activity\'s Difficulty</span>
                    </td>';
            } else {
                $html .= '<td class="light" style="width: 16%;">
                    <div class="ratehideall" style="display:none;">
                        <div class="activityrating rating' . $cn . '">
                            <input type="radio" name="activity_rate" class="ratingAll rating" value="1" />
                            <input type="radio" name="activity_rate" class="ratingAll rating" value="2" />
                            <input type="radio" name="activity_rate" class="ratingAll rating" value="3" />
                            <input type="radio" name="activity_rate" class="ratingAll rating" value="4" />
                            <input type="radio" name="activity_rate" class="ratingAll rating" value="5" />
                        </div>
                    </div>
                    <span class="messagerateHideall">Rate This Activity\'s Difficulty</span>
                    </td>';
            }
            if ($submited_activity['activity_type'] == $activity_types [$i]['id']) {
                $html .= '<td  class="light" style="width: 30%;">
                    <textarea class="activity_comment comment' . $cn . '" name="activity_comment">' . $submited_activity['activity_comment'] . '</textarea>
                  </td>';
            } else {
                $html .= '<td  class="light" style="width: 30%;">
                    <textarea class="activity_comment comment' . $cn . '" name="activity_comment" disabled="disabled"></textarea>
                  </td>';
            }
        } else {
            $html .= '<td></td>';
        }
        $html .= '</tr>';
        $cn++;
    }
    $html .= '</tbody>';
    if ($counter > 1) {
        if ($meeting_date_check != 0) {
            $html .= '<tfoot>
                        <tr>
                            <td><strong>Total Time (hours/minutes)</strong>Total of All Categories</td>
                            <td id="totalhours"><span id="remaningTime">' . gmdate("i:s", $totalHours) . '</span><span style="display:none;">1440</span></td>
                    <input type="hidden" id="remaningTimehidden" value="' . $totalHours . '"/>
                    <input type="hidden" id="remaningTimecalc" value="' . $totalHours . '"/>
                    <td colspan="5"></td>                           
                    </tr>
                    </tfoot>';
        } else {
            $html .= '<tfoot>
                        <tr>
                            <td><strong>Total Time (hours/minutes)</strong>Total of All Categories</td>
                            <td id="totalhours"><span id="remaningTime">0</span><span style="display:none;">1440</span></td>
                    <input type="hidden" id="remaningTimehidden"/>
                    <input type="hidden" id="remaningTimecalc" value=""/>
                    <td colspan="5"></td>                           
                    </tr>
                    </tfoot>';
        }
    }
    $html .= '</table>';
    $counter++;
}
echo $html;
?>

<script>
    $('.activity_typeclass').on('change', function () {
        checkhours_viewMyDay(this);
    });
    function enabledCheckbox(obj) {
        if ($(obj).is(':checked')) {
            $(obj).parents('tr').find('.ratehideall').show();
            $(obj).parents('tr').find('.messagerateHideall').hide();
            $(obj).parents('tr').find('.timeFields').show();
            $(obj).parents('tr').find('.activity_comment').removeAttr('disabled');

        } else {
            $(obj).parents('tr').find('.ratehideall').hide();
            $(obj).parents('tr').find('.messagerateHideall').show();
            $(obj).parents('tr').find('.timeFields').hide();
            $(obj).parents('tr').find('.activity_comment').val('');
            $(obj).parents('tr').find('.activity_comment').attr("disabled", true);
            if ($(obj).parents('tr').find('.checked').length > 0) {

            } else {
                $(obj).parents('tr').find('.ratingAll').removeAttr('checked');
                $(obj).parents('tr').find('.star').removeClass('fullStar');
            }
        }
    }
</script>

<script src="assets/javascripts/drop/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
    $(".chosen-select").chosen({width: "80%"});
</script>