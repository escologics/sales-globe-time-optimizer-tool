<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function getMd5($password) {
    if (isset($password) && !empty($password)) {
        return md5($password);
    }
}

function generatePassword() {
    $array = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&**()';
    $s = '';
    for ($i = 1; $i < 8; $i++) {
        // echo strlen($array)."<br>";
        // echo rand(0, strlen($array) -1)."<br>";
        $password .= $array[rand(0, strlen($array) - 1)];
    }

    return $password;
}

function curPageURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["PHP_SELF"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["PHP_SELF"];
    }
    return $pageURL;
}

function getPage() {
    $fileName = explode('.', basename($_SERVER['PHP_SELF']));


    return $fileName[0];
}

//function time_format($time) {
//
//    if ($time > 59) {
//        $actualtime = ($time / 60);
//        $houra_array = explode('.', $actualtime);
//        $minutes = '0.' . $houra_array[1];
//        $hr = '0' . floor($time / 60);
//        if ($hr >= '10') {
//            $hr = floor($time / 60);
//        } else {
//            $hr = '0' . floor($time / 60);
//        }
//        $min = ($minutes * 60);
//        if (floor($min) < 10) {
//            $finaltime = $hr . ':' . ($time == 60 ? '00' : '0' . floor($min));
//        } else {
//            $finaltime = $hr . ':' . ($time == 60 ? '00' : floor($min));
//        }
//    } else {
//        $finaltime = '00:' . $time;
//    }
//
//    return $finaltime;
//}

function m2h($format, $mins) {
    if ($mins < 0) {
        $min = Abs($mins);
    } else {
        $min = $mins;
    }
    $H = Floor($min / 60);
    $M = ($min - ($H * 60)) / 100;
    $hours = $H + $M;
    if ($mins < 0) {
        $hours = $hours * (-1);
    }

    $expl = explode(".", $hours);
    $H = $expl[0];
    if (empty($expl[1])) {
        $expl[1] = 00;
    }

    $M = $expl[1];
    if (strlen($M) < 2) {
        $M = $M . 0;
    }

    $hours = ($H < 10 ? '0'.$H : $H) . ":" . $M;

    return $hours;
}

function time_format_export($time) {

//    if ($time > 59) {
//        $actualtime = ($time / 60);
//        $houra_array = explode('.', $actualtime);
//        $minutes = '0.' . $houra_array[1];
//        $hr = '0' . floor($time / 60);
//        if ($hr >= '10') {
//            $hr = floor($time / 60);
//        } else {
//            $hr = '0' . floor($time / 60);
//        }
//        $min = ($minutes * 60);
//        if (floor($min) < 10) {
//            $finaltime = '( '.$hr . ':' . ($time == 60 ? '00' : '0' . floor($min)).' )';
//        } else {
//            $finaltime = '( '.$hr . ':' . ($time == 60 ? '00' : floor($min)).' )';
//        }
//    } else {
//        $finaltime = '( 00:' . $time.' )';
//    }
    $finaltime = '( ' . gmdate("i:s", $time) . ' )';

    return $finaltime;
}

function convert($hour, $minutes) {
    $data = $hour * 60 + $minutes;
    return $data;
}

?>
