<?php

session_start();

$GLOBALS['config'] = array(
    'mysql' => array(
        'host' => 'localhost',
        'username' => 'root',
        'password' => '',
        'db' => 'optimizer'
    ),
    'path' => array(
    ),
    'remember' => array(
        'cookie_name' => 'hash',
        'cookie_expire' => 604800
    ),
    'session' => array(
        'session_name' => 'user'
    ),
    'configuration' => array(
        'baseUrl' => 'http://localhost/salesglobetot/',
    )
);

$site_url = $GLOBALS['config']['configuration']['baseUrl'];
error_reporting(0);
$minutes = 240; //Set logout time in minutes    
$time = time(); //Set logout time in minutes    
if (!isset($_SESSION['time'])) {
    $_SESSION['time'] = time();
} elseif ($time - $_SESSION['time'] > $minutes * 60) {
    session_destroy();
    header('location:login.php'); //redirect user to a login page or any page to which we want to redirect.
}
/*
 * All class will be auto loaded from classes folder
 */

require_once ('classes/Archive.php');
require_once ('classes/Config.php');
require_once ('classes/DB.php');
require_once ('classes/Input.php');
require_once ('classes/Meeting.php');
require_once ('classes/Redirect.php');
require_once ('classes/Reporting.php');
require_once ('classes/Session.php');
require_once ('classes/User.php');
require_once ('classes/Validate.php');


// includes function.php file
require_once ('functions/functions.php');
