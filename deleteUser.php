<?php

include('includes/top.php');

if(Input::post('user_type') == 'manager')
{
    $result = Archive::getSalesRepresentativesIdsUnderManagerByManagerId(Input::post('user_id'));
    $person_name = User::getFullNameByUserId(Input::post('user_id'));
    $company_name = User::getCompanyByUserId(Input::post('user_id'));
    $email = User::getEmailByUserId(Input::post('user_id'));


    $under_people = json_encode($result);

    // print_r($under_people); exit;
    DB::getInstance()->insertJson('archive', array(
        'action' => 'deleted',
        'entry_id' => Input::post('user_id'),
        'date_added' => date('Y-m-d', time()),
        'under_people' => mysql_real_escape_string($under_people),
        'previous_position' => "Manager",
        'current_position' => "",
        'person_name' => mysql_real_escape_string($person_name),
        'company' => mysql_real_escape_string($company_name),
        'email' => $email
    ));



    //$result =  User::getUsersByAssignedId(Input::post('user_id'));
    $update = DB::getInstance()->updateByAssigned('users', Input::post('user_id'), array(
        'assigned_to' => Input::post('assigned_to')
            ));

    if($update)
    {

        $delete = DB::getInstance()->update('users', Input::post('user_id'), array(
		'status' => 0
		));
    }

    if($delete)
    {
        Redirect::to(Input::post('current_page') . '?deleted=manager');
    }
}
elseif(Input::post('user_type') == 'salesrep')
{
    $person_name = User::getFullNameByUserId(Input::post('user_id'));
    $company_name = User::getCompanyByUserId(Input::post('user_id'));
    $email = User::getEmailByUserId(Input::post('user_id'));
	
	 $delete = DB::getInstance()->update('users', Input::post('user_id'), array(
		'status' => 0
		));
   // $delete = DB::getInstance()->delete('users', Input::post('user_id'));

    if($delete)
    {

        DB::getInstance()->insertJson('archive', array(
            'action' => 'deleted',
            'entry_id' => Input::post('user_id'),
            'date_added' => date('Y-m-d', time()),
            'under_people' => '',
            'previous_position' => "Sales Representative",
            'current_position' => "",
            'person_name' => mysql_real_escape_string($person_name),
            'company' => mysql_real_escape_string($company_name),
            'email' => $email
        ));
        Redirect::to(Input::post('current_page') . '?deleted=salesrep');
    }
}
?>
