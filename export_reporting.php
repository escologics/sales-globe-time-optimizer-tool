<?php

error_reporting(0);
require_once 'core/init.php';
$filename = '';
$table = '';

if (isset($_POST['manager_export']) == 'Export Activity Level') {
    $manager_ids = $_POST['manager_id'];
    $start_date = $_POST['start_date_m'];
    $start_date = date("Y-m-d", strtotime($start_date));
    $end_date = $_POST['end_date_m'];
    $end_date = date("Y-m-d", strtotime($end_date));

    if (!empty($manager_ids)) {
        if (($_POST['m_strat_month'] != "" && $_POST['m_strat_day'] != "" && $_POST['m_strat_year'] != "" && $_POST['m_end_month'] != "" && $_POST['m_end_day'] != "" && $_POST['m_end_year'] != "") || (isset($_POST['days_select']))) {
            $table .= '<table width="1200" border="1">';
            $total_hours_export_manager = 0;
            foreach ($manager_ids as $manager_id) {

                if (isset($_POST['days_select'])) {

                    $totalHours = Reporting::getTotalHoursByDaysAndUserId($_POST['days_select'], $manager_id);
                    $meetingHistoryData = Reporting::getMeetingHistoryByDaysAndUserId($_POST['days_select'], $manager_id);
                    $user_full_name = User::getFullNameByUserId($manager_id);
                    $info = User::getAllInformationByUserId($manager_id);
                    $total_hours_export_manager += $totalHours;
                    $filename = 'Managers [' . $_POST['days_select'] . ']';
                } else {

                    $totalHours = Reporting::getTotalHoursByDateRangeAndUserId($start_date, $end_date, $manager_id);
                    $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndUserId($start_date, $end_date, $manager_id);
                    $user_full_name = User::getFullNameByUserId($manager_id);
                    $info = User::getAllInformationByUserId($manager_id);
                    $total_hours_export_manager += $totalHours;
                    $filename = 'Managers [' . $start_date . ' To ' . $end_date . ']';
                }




                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color: white; padding: 15px 10px; text-align: center;" bgcolor="black" colspan="7">[' . $user_full_name . ']</td>                                                                                               
        </tr>';

                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Name</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Sales Team</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Email Address</td>                                        
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Username</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px;" bgcolor="#808080">User Since</td>  
                                                                                                   
        </tr>';

                $table .= '<tr>
        <td style="padding: 15px 10px; font-size: 20px;" bgcolor="#fff"><b>' . $info['first_name'] . ' ' . $info['last_name'] . '</b></td>';
                $manager_industries = User::getIndustriesByUserId($info['id']);
                $table .= '<td style="padding: 15px 10px; font-size: 20px;" bgcolor="#fff">';
                foreach ($manager_industries as $manager_industry) {
                    $table .= '<b>' . User::getIndustryByUserId($manager_industry['sales_team_id']) . '</b><br/>';
                }
                $table .= '</td>';
                $table .= '<td style="padding: 15px 10px; font-size: 20px;" bgcolor="#fff"><b>' . $info['email'] . '</b></td>
        <td style="padding: 15px 10px; font-size: 20px;" bgcolor="#fff"><b>' . $info['username'] . '</b></td>  
        <td style="padding: 15px 10px; font-size: 20px;" bgcolor="#fff"><b>' . date('m-d-Y', strtotime($info['created_user'])) . '</b></td>          
        </tr>';

                if ($meetingHistoryData) {
                    $totalpercntt = 0;

                    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Activity and Type Summary</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Hours</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Percent to Total</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Difficulty Level</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Comments</td>                                                    
        </tr>';

                    foreach ($meetingHistoryData as $meeting) {

                        $totalpercntt += round($meeting['hours'] / $totalHours * 100, 2);
                        if (isset($_POST['days_select'])) {
                            $ActivityTypedata = Reporting::getActivityTypeHoursForExportDataonlyforsalesandmanager($_POST['days_select'], $info['id'], $meeting['meeting_category']);
                        } else {
                            $ActivityTypedata = Reporting::getActivityTypeHoursBydaterange($start_date, $end_date, $info['id'], $meeting['meeting_category']);
                        }
                        //$ActivityTypedata = Reporting::getActivityTypeHoursForExportData($info['id'], $meeting['meeting_category']);
                        //$ActivityTypedatacomments = Reporting::getActivityTypeCommentsForExportData($info['id'], $meeting['meeting_category']);
                        $max = count($ActivityTypedata);
                        $rate_count_rating = 0;
                        foreach ($ActivityTypedata as $row_count_rating) {
                            $rate_count_rating += ($row_count_rating['Rate']);
                        }

                        $table .= '<tr>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . $meeting['name'] . '</td>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . m2h("i:s", $meeting['hours']) . '</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . round($meeting['hours'] / $totalHours * 100, 2) . '%</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . number_format($rate_count_rating / sizeof($ActivityTypedata), 2) . '</td>              
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>                        
        </tr>';

                        for ($i = 0; $i < $max; $i++) {
                            $table .= '<tr>';
                            $table .= ' <td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['name'] . '</td>';
                            if (isset($ActivityTypedata[$i]['hours'])) {
                                $table .= '<td style="padding: 15px 10px;">' . m2h("i:s", $ActivityTypedata[$i]['hours']) . '</td>';
                                $table .= '<td style="padding: 15px 10px;">' . round($ActivityTypedata[$i]['hours'] / $totalHours * 100, 2) . '%</td>';
                                $table .= '<td style="padding: 15px 10px;">' . number_format($ActivityTypedata[$i]['Rate'], 2) . '</td>';

//                                $table .= '<td style="padding: 15px 10px;">';
//                                foreach ($ActivityTypedatacomments as $ActivityTypedatacomment) {
//                                    if ($ActivityTypedata[$i]['id'] == $ActivityTypedatacomment['Activity_type']) {
//                                        $table .= '<span class="comments_span">' . $ActivityTypedatacomment['Comments'] . '</span><br/>';
//                                    }
//                                }
//                                $table .= '</td>';
                                $table .= '<td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['Comments'] . '</td>';
                            } else {
                                $table .= '<td style="padding: 15px 10px;"></td>';
                            }
                            $table .= ' </tr>';
                        }
                    }

                    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">TOTAL HOURS FOR DATE PERIOD</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . m2h("i:s", $totalHours) . '</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . $totalpercntt . '%</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        
        </tr>';
                } else {
                    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="red" colspan="7">No Activity Data Record Found</td>                                                                                               
        </tr>';
                }

                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="white" colspan="7"></td>                                                                                               
        </tr>';
            }

            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center; color:white;" bgcolor="#589340" colspan="7">Total # of Hours (' . m2h("i:s", $total_hours_export_manager) . ')</td>                                                                                               
        </tr>';
            $table .= '</table>';
            header('Content-type: application/vnd.xls');
            header('Content-Disposition: attachment; filename="' . $filename . '.xls"');

            print $table;
        }
    }
}

if (isset($_POST['representative_export']) == 'Export Activity Level') {
    $representative_ids = $_POST['representative_id'];
    $start_date = $_POST['start_date_s'];
    $start_date = date("Y-m-d", strtotime($start_date));
    $end_date = $_POST['end_date_s'];
    $end_date = date("Y-m-d", strtotime($end_date));

    if (!empty($representative_ids)) {
        $total_hours_export_salesrep = 0;
        if (($_POST['m_strat_month_s'] != "" && $_POST['m_strat_day_s'] != "" && $_POST['m_strat_year_s'] != "" && $_POST['m_end_month_s'] != "" && $_POST['m_end_day_s'] != "" && $_POST['m_end_year_s'] != "") || (isset($_POST['days_select_s']))) {
            $table .= '<table width="1500" border="1">';
            $table .= '<tr>
                        <td style="font-weight:bold; border-left:1px solid #fff; color:white; text-align: center; font-size: 20px;" bgcolor="black">User Name</td>
                        <td style="font-weight:bold; border-left:1px solid #fff; color:white; text-align: center; font-size: 20px;" bgcolor="black">Team Name</td>
                        <td style="font-weight:bold; border-left:1px solid #fff; color:white; text-align: center; font-size: 20px;" bgcolor="black">Manager Name</td>                                        
                        <td style="font-weight:bold; border-left:1px solid #fff; color:white; text-align: center; font-size: 20px;" bgcolor="black">Activity Heading</td> 
                        <td style="font-weight:bold; border-left:1px solid #fff; color:white; text-align: center; font-size: 20px;" bgcolor="black">Sub-activity title</td> 
                        <td style="font-weight:bold; border-left:1px solid #fff; color:white; text-align: center; font-size: 20px;" bgcolor="black">Entry Date</td> 
                        <td style="font-weight:bold; border-left:1px solid #fff; color:white; text-align: center; font-size: 20px;" bgcolor="black">Sub-activity Difficulty</td> 
                        <td style="font-weight:bold; border-left:1px solid #fff; color:white; text-align: center; font-size: 20px;" bgcolor="black">Comments</td> 
                        </tr>';
            foreach ($representative_ids as $representative_id) {
                if (isset($_POST['days_select_s'])) {
                    $totalHours = Reporting::getTotalHoursByDaysAndUserId($_POST['days_select_s'], $representative_id);
                    $meetingHistoryData = Reporting::getMeetingHistoryByDaysAndUserId($_POST['days_select_s'], $representative_id);
                    $user_full_name = User::getFullNameByUserId($representative_id);
                    $info = User::getAllInformationByUserId($representative_id);
                    $total_hours_export_salesrep += $totalHours;
                    $filename = 'Sales Representative [' . $_POST['days_select_s'] . ']';
                } else {
                    $totalHours = Reporting::getTotalHoursByDateRangeAndUserId($start_date, $end_date, $representative_id);
                    $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndUserId($start_date, $end_date, $representative_id);
                    $user_full_name = User::getFullNameByUserId($representative_id);
                    $info = User::getAllInformationByUserId($representative_id);
                    $total_hours_export_salesrep += $totalHours;
                    $filename = 'Sales Representative [' . $start_date . ' To ' . $end_date . ']';
                }

                $sales_team_name = User::getIndustryByUserId($info['industry_id']);
                $title = User::getDesignationByUserId($info['designation']);
                $assign_manager = User::getFullNameByUserId($info['assigned_to']);
                if ($meetingHistoryData) {
                    $totalpercntt = 0;
                    foreach ($meetingHistoryData as $meeting) {
                        $totalpercntt += round($meeting['hours'] / $totalHours * 100, 2);
                        if (isset($_POST['days_select_s'])) {
                            $ActivityTypedata = Reporting::getActivityTypeHoursForExportDataonlyforsalesandmanager($_POST['days_select_s'], $info['id'], $meeting['meeting_category']);
                        } else {
                            $ActivityTypedata = Reporting::getActivityTypeHoursBydaterange($start_date, $end_date, $info['id'], $meeting['meeting_category']);
                        }
                        $max = count($ActivityTypedata);
                        $rate_count_rating = 0;
                        foreach ($ActivityTypedata as $row_count_rating) {
                            $rate_count_rating += ($row_count_rating['Rate']);
                        }

                        for ($i = 0; $i < $max; $i++) {
                            $table .= '<tr>';
                            $table .= ' <td>' . $info['username'] . '</td>';
                            $table .= ' <td>' . $sales_team_name . '</td>';
                            $table .= ' <td>' . $assign_manager . '</td>';
                            $table .= ' <td>' . $meeting['name'] . '</td>';
                            $table .= ' <td>' . $ActivityTypedata[$i]['name'] . '</td>';
                            if (isset($ActivityTypedata[$i]['hours'])) {
                                $table .= '<td>' . $ActivityTypedata[$i]['sub_activity_added_date'] . '</td>';
                                $table .= '<td>' . number_format($ActivityTypedata[$i]['Rate']) . '</td>';
                                $table .= '<td>' . $ActivityTypedata[$i]['Comments'] . '</td>';
                            } else {
                                $table .= '<td></td>';
                            }
                            $table .= ' </tr>';
                        }
                    }
                } else {
                    $table .= '<tr>
        <td>'.$info['username'].'</td>                                                                                               
        <td>' . $sales_team_name . '</td>                                                                                            
        <td>' . $assign_manager . '</td>                                                                                            
        <td>No Record Found</td>                                                                                            
        <td>No Record Found</td>                                                                                            
        <td>&nbsp;</td>                                                                                            
        <td>&nbsp;</td>                                                                                            
        <td>&nbsp;</td>                                                                                            
        </tr>';
                }
            }
            $table .= '</table>';
            header('Content-type: application/vnd.xls');
            header('Content-Disposition: attachment; filename="' . $filename . '.xls"');
            print $table;
        }
    }
}

if (isset($_POST['sales_team_export'])) {
    $sales_team_id = $_POST['sales_team'];
    $sales_team_name = User::getIndustryById($sales_team_id);
    $get_info_manager = User::getAllInformationBySalesIdForManager($sales_team_id, 3);
    $get_info_sales = User::getAllInformationBySalesId($sales_team_id, 4);
    $filename = 'Export-(Users) [' . $sales_team_name['name'] . ']';
    if (!empty($get_info_manager) || !empty($get_info_sales)) {
        $table .= '<table width="1200" border="1">';

        if (!empty($get_info_manager)) {
            $total_hours_export_manager_salesteam = 0;
            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color: white; padding: 15px 10px; text-align: center;" bgcolor="black" colspan="8">MANAGERS</td>                                                                                               
        </tr>';

            foreach ($get_info_manager as $info) {
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Name</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Sales Team</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Email Address</td>                                        
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Username</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">User Since</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">&nbsp</td>                                                                                             
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">&nbsp</td>                                                                                             
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">&nbsp</td>                                                                                             
        </tr>';
                $sales_team_name = User::getIndustryByUserId($sales_team_id);
                $totalHours = Reporting::getTotalHoursByUserIdForexportData($info['id']);
                $total_hours_export_manager_salesteam += $totalHours;
                $meetingHistoryData = Reporting::getMeetingHistoryUserIdforExportData($info['id']);
                $table .= '<tr>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['first_name'] . ' ' . $info['last_name'] . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $sales_team_name . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['email'] . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['username'] . '</b></td>  
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . date('m-d-Y', strtotime($info['created_user'])) . '</b></td>          
        <td style="padding: 15px 10px;" bgcolor="#fff">&nbsp</td>                       
        <td style="padding: 15px 10px;" bgcolor="#fff">&nbsp</td>                       
        <td style="padding: 15px 10px;" bgcolor="#fff">&nbsp</td>                       
        </tr>';

                if (!empty($meetingHistoryData)) {
                    $totalpercntt = 0;
                    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Activity and Type Summary</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Hours</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Percent to Total</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Difficulty Level</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Comments</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                            
        </tr>';

                    foreach ($meetingHistoryData as $meeting) {
                        $totalpercntt += round($meeting['hours'] / $totalHours * 100, 2);
                        $ActivityTypedata = Reporting::getActivityTypeHoursForExportData($info['id'], $meeting['meeting_category']);
                        //$ActivityTypedatacomments = Reporting::getActivityTypeCommentsForExportData($info['id'], $meeting['meeting_category']);
                        $max = count($ActivityTypedata);
                        $rate_count_rating = 0;
                        foreach ($ActivityTypedata as $row_count_rating) {
                            $rate_count_rating += ($row_count_rating['Rate']);
                        }

                        $table .= '<tr>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . $meeting['name'] . '</td>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . m2h("i:s", $meeting['hours']) . '</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . round($meeting['hours'] / $totalHours * 100, 2) . '%</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . number_format($rate_count_rating / sizeof($ActivityTypedata), 2) . '</td>              
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        </tr>';

                        for ($i = 0; $i < $max; $i++) {
                            $table .= '<tr>';
                            $table .= ' <td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['name'] . '</td>';
                            if (isset($ActivityTypedata[$i]['hours'])) {
                                $table .= '<td style="padding: 15px 10px;">' . m2h("i:s", $ActivityTypedata[$i]['hours']) . '</td>';
                                $table .= '<td style="padding: 15px 10px;">' . round($ActivityTypedata[$i]['hours'] / $totalHours * 100, 2) . '%</td>';
                                $table .= '<td style="padding: 15px 10px;">' . number_format($ActivityTypedata[$i]['Rate'], 2) . '</td>';
//                                $table .= '<td style="padding: 15px 10px;">';
//                                foreach ($ActivityTypedatacomments as $ActivityTypedatacomment) {
//                                    if ($ActivityTypedata[$i]['id'] == $ActivityTypedatacomment['Activity_type']) {
//                                        $table .= '<span class="comments_span">' . $ActivityTypedatacomment['Comments'] . '</span><br/>';
//                                    }
//                                }
//                                $table .= '</td>';
                                $table .= '<td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['Comments'] . '</td>';
                                $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                                $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                                $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                            } else {
                                $table .= '<td style="padding: 15px 10px;"></td>';
                            }
                            $table .= ' </tr>';
                        }
                    }
                    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">TOTAL HOURS FOR DATE PERIOD</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . m2h("i:s", $totalHours) . '</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . $totalpercntt . '%</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        </tr>';
                } else {
                    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="red" colspan="8">No Activity Data Record Found</td>                                                                                               
        </tr>';
                }
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="white" colspan="8"></td>                                                                                               
        </tr>';
            }
            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center; color:white;" bgcolor="#589340" colspan="8">Total # of Hours For Managers (' . m2h("i:s", $total_hours_export_manager_salesteam) . ')</td>                                                                                               
        </tr>';
            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="white" colspan="8"></td>                                                                                               
        </tr>';
        }
        if (!empty($get_info_sales)) {
            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color: white; padding: 15px 10px; text-align: center;" bgcolor="black" colspan="8">SALES REPRESENTATIVES</td>                                                                                               
        </tr>';
            $total_hours_export_salesrep_salesteam = 0;
            foreach ($get_info_sales as $info) {
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Name</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Sales Team</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Email Address</td>                                        
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Username</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">&nbsp</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Title</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Assigned Manager</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">User Since</td> 
        </tr>';

                $sales_team_name = User::getIndustryByUserId($info['industry_id']);
                $title = User::getDesignationByUserId($info['designation']);
                $assign_manager = User::getFullNameByUserId($info['assigned_to']);
                $totalHours = Reporting::getTotalHoursByUserIdForexportData($info['id']);
                $total_hours_export_salesrep_salesteam += $totalHours;
                $meetingHistoryData = Reporting::getMeetingHistoryUserIdforExportData($info['id']);
                $table .= '<tr>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['first_name'] . ' ' . $info['last_name'] . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $sales_team_name . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['email'] . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['username'] . '</b></td>     
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff">&nbsp</td>     
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $title . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $assign_manager . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . date('m-d-Y', strtotime($info['created_user'])) . '</b></td>
        </tr>';
                if (!empty($meetingHistoryData)) {
                    $totalpercnt = 0;
                    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Activity and Type Summary</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Hours</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Percent to Total</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Difficulty Level</td>                                               
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Comments</td>                                               
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                               
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                               
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                               
        </tr>';
                    foreach ($meetingHistoryData as $meeting) {
                        $totalpercnt += round($meeting['hours'] / $totalHours * 100, 2);

                        $ActivityTypedata = Reporting::getActivityTypeHoursForExportData($info['id'], $meeting['meeting_category']);
                        //$ActivityTypedatacomments = Reporting::getActivityTypeCommentsForExportData($info['id'], $meeting['meeting_category']);
                        $max = count($ActivityTypedata);
                        $rate_count_rating = 0;
                        foreach ($ActivityTypedata as $row_count_rating) {
                            $rate_count_rating += ($row_count_rating['Rate']);
                        }

                        $table .= '<tr>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . $meeting['name'] . '</td>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . m2h("i:s", $meeting['hours']) . '</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . round($meeting['hours'] / $totalHours * 100, 2) . '%</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . number_format($rate_count_rating / sizeof($ActivityTypedata), 2) . '</td>              
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>              
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>              
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>              
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>              
        </tr>';

                        for ($i = 0; $i < $max; $i++) {
                            $table .= '<tr>';
                            $table .= ' <td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['name'] . '</td>';
                            if (isset($ActivityTypedata[$i]['hours'])) {
                                $table .= '<td style="padding: 15px 10px;">' . m2h("i:s", $ActivityTypedata[$i]['hours']) . '</td>';
                                $table .= '<td style="padding: 15px 10px;">' . round($ActivityTypedata[$i]['hours'] / $totalHours * 100, 2) . '%</td>';
                                $table .= '<td style="padding: 15px 10px;">' . number_format($ActivityTypedata[$i]['Rate'], 2) . '</td>';
//                                $table .= '<td style="padding: 15px 10px;">';
//                                foreach ($ActivityTypedatacomments as $ActivityTypedatacomment) {
//                                    if ($ActivityTypedata[$i]['id'] == $ActivityTypedatacomment['Activity_type']) {
//                                        $table .= '<span class="comments_span">' . $ActivityTypedatacomment['Comments'] . '</span><br/>';
//                                    }
//                                }
//                                $table .= '</td>';
                                $table .= '<td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['Comments'] . '</td>';
                                $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                                $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                                $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                            } else {
                                $table .= '<td style="padding: 15px 10px;"></td>';
                            }
                            $table .= ' </tr>';
                        }
                    }
                    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">TOTAL HOURS FOR DATE PERIOD</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . m2h("i:s", $totalHours) . '</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . $totalpercnt . '%</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        </tr>';
                } else {
                    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="red" colspan="8">No Activity Data Record Found</td>                                                                                               
        </tr>';
                }
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="white" colspan="8"></td>                                                                                               
        </tr>';
            }
            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center; color:white;" bgcolor="#589340" colspan="8">Total # of Hours For Sales Representatives (' . m2h("i:s", $total_hours_export_salesrep_salesteam) . ')</td>                                                                                               
        </tr>';
            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="white" colspan="8"></td>                                                                                               
        </tr>';
        }
        $table .= '</table>';
        header('Content-type: application/vnd.xls');
        header('Content-Disposition: attachment; filename="' . $filename . '.xls"');

        print $table;
    } else {
        $table .= 'No Record Found';
    }
}

if (isset($_POST['all_users_export'])) {

    $all_users_start_date = $_POST['all_users_start_date'];
    $all_users_start_date = date("Y-m-d", strtotime($all_users_start_date));

    $all_users_end_date = $_POST['all_users_end_date'];
    $all_users_end_date = date("Y-m-d", strtotime($all_users_end_date));

    if (isset($_POST['days_select_all_user'])) {
        $all_managers = User::getAllManagersByDay($_POST['days_select_all_user']);
        $all_salesrepresentatives = User::getAllSalesRepresentativeByDay($_POST['days_select_all_user']);
        $filename = 'All Users [' . $_POST['days_select_all_user'] . ']';
    } else {
        $all_managers = User::getAllManagersByDateRange($all_users_start_date, $all_users_end_date);
        $all_salesrepresentatives = User::getAllSalesRepresentativeByDateRange($all_users_start_date, $all_users_end_date);
        $filename = 'All Users [' . $all_users_start_date . ' To ' . $all_users_end_date . ']';
    }


    $table .= '<table width="1200" border="1">';

    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color: white; padding: 15px 10px; text-align: center;" bgcolor="black" colspan="7">All Managers</td>                                                                                               
        </tr>';

    if (!empty($all_managers)) {

        $total_hours_export_manager_allUser = 0;
        foreach ($all_managers as $info) {
            //$sales_team_name = User::getIndustryByUserId($info['industry_id']);
            $totalHours = Reporting::getTotalHoursByUserIdForexportData($info['id']);
            $total_hours_export_manager_allUser += $totalHours;
            $meetingHistoryData = Reporting::getMeetingHistoryUserIdforExportData($info['id']);
            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Name</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Sales Team</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Email Address</td>                                        
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Username</td>                                         
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">User Since</td>                                        
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">&nbsp</td>                                        
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">&nbsp</td>                                        
        </tr>';
            $table .= '<tr>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['first_name'] . ' ' . $info['last_name'] . '</b></td>';
            $manager_industries = User::getIndustriesByUserId($info['id']);
            $table .= '<td style="padding: 15px 10px; font-size: 20px;" bgcolor="#fff">';
            foreach ($manager_industries as $manager_industry) {
                $table .= '<b>' . User::getIndustryByUserId($manager_industry['sales_team_id']) . '</b><br/>';
            }
            $table .= '</td>';

            $table .='<td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['email'] . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['username'] . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . date('m-d-Y', strtotime($info['created_user'])) . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff">&nbsp</td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff">&nbsp</td>
        </tr>';
            if (!empty($meetingHistoryData)) {
                $totalpercntt = 0;
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Activity and Type Summary</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Hours</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Percent to Total</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Difficulty Level</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Comments</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                            
        </tr>';

                foreach ($meetingHistoryData as $meeting) {
                    $totalpercntt += round($meeting['hours'] / $totalHours * 100, 2);
                    $ActivityTypedata = Reporting::getActivityTypeHoursForExportData($info['id'], $meeting['meeting_category']);
                    //$ActivityTypedatacomments = Reporting::getActivityTypeCommentsForExportData($info['id'], $meeting['meeting_category']);
                    $max = count($ActivityTypedata);
                    $rate_count_rating = 0;
                    foreach ($ActivityTypedata as $row_count_rating) {
                        $rate_count_rating += ($row_count_rating['Rate']);
                    }

                    $table .= '<tr>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . $meeting['name'] . '</td>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . m2h("i:s", $meeting['hours']) . '</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . round($meeting['hours'] / $totalHours * 100, 2) . '%</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . number_format($rate_count_rating / sizeof($ActivityTypedata), 2) . '</td>              
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        </tr>';

                    for ($i = 0; $i < $max; $i++) {
                        $table .= '<tr>';
                        $table .= ' <td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['name'] . '</td>';
                        if (isset($ActivityTypedata[$i]['hours'])) {
                            $table .= '<td style="padding: 15px 10px;">' . m2h("i:s", $ActivityTypedata[$i]['hours']) . '</td>';
                            $table .= '<td style="padding: 15px 10px;">' . round($ActivityTypedata[$i]['hours'] / $totalHours * 100, 2) . '%</td>';
                            $table .= '<td style="padding: 15px 10px;">' . number_format($ActivityTypedata[$i]['Rate'], 2) . '</td>';
//                            $table .= '<td style="padding: 15px 10px;">';
//                            foreach ($ActivityTypedatacomments as $ActivityTypedatacomment) {
//                                if ($ActivityTypedata[$i]['id'] == $ActivityTypedatacomment['Activity_type']) {
//                                    $table .= '<span class="comments_span">' . $ActivityTypedatacomment['Comments'] . '</span><br/>';
//                                }
//                            }
//                            $table .= '</td>';
                            $table .= '<td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['Comments'] . '</td>';
                            $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                            $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                        } else {
                            $table .= '<td style="padding: 15px 10px;"></td>';
                        }
                        $table .= ' </tr>';
                    }
                }
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">TOTAL HOURS FOR DATE PERIOD</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . m2h("i:s", $totalHours) . '</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . $totalpercntt . '%</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        </tr>';
            } else {
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="red" colspan="7">No Activity Data Record Found</td>                                                                                               
        </tr>';
            }
            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="white" colspan="7"></td>                                                                                               
        </tr>';
        }
        $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center; color:white;" bgcolor="#589340" colspan="8">Total # of Hours For Managers ' . m2h("i:s", $total_hours_export_manager_allUser) . '</td>                                                                                               
        </tr>';
        $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="white" colspan="8"></td>                                                                                               
        </tr>';
    } else {
        $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="red" colspan="7">No Record Found</td>                                                                                               
        </tr>';
    }

    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color: white; padding: 15px 10px; text-align: center;" bgcolor="black" colspan="7">All Sales Representatives</td>
        </tr>';

    if (!empty($all_salesrepresentatives)) {
        $total_hours_export_salesrep_AllUser = 0;
        foreach ($all_salesrepresentatives as $info) {
            $title = User::getDesignationByUserId($info['designation']);
            $assign_manager = User::getFullNameByUserId($info['assigned_to']);
            $totalHours = Reporting::getTotalHoursByUserIdForexportData($info['id']);
            $total_hours_export_salesrep_AllUser += $totalHours;
            $meetingHistoryData = Reporting::getMeetingHistoryUserIdforExportData($info['id']);
            $sales_team_name = User::getIndustryByUserId($info['industry_id']);
            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Name</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Sales Team</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Email Address</td>                                        
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Username</td>
         <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Title</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Assigned Manager</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">User Since</td>
        </tr>';

            $table .= '<tr>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['first_name'] . ' ' . $info['last_name'] . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $sales_team_name . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['email'] . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['username'] . '</b></td>     
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $title . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $assign_manager . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . date('m-d-Y', strtotime($info['created_user'])) . '</b></td>              
        </tr>';

            if (!empty($meetingHistoryData)) {
                $totalpercntt = 0;
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Activity and Type Summary</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Hours</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Percent to Total</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Difficulty Level</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Comments</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                            
        </tr>';

                foreach ($meetingHistoryData as $meeting) {
                    $totalpercntt += round($meeting['hours'] / $totalHours * 100, 2);
                    $ActivityTypedata = Reporting::getActivityTypeHoursForExportData($info['id'], $meeting['meeting_category']);
                    //$ActivityTypedatacomments = Reporting::getActivityTypeCommentsForExportData($info['id'], $meeting['meeting_category']);
                    $max = count($ActivityTypedata);
                    $rate_count_rating = 0;
                    foreach ($ActivityTypedata as $row_count_rating) {
                        $rate_count_rating += ($row_count_rating['Rate']);
                    }

                    $table .= '<tr>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . $meeting['name'] . '</td>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . m2h("i:s", $meeting['hours']) . '</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . round($meeting['hours'] / $totalHours * 100, 2) . '%</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . number_format($rate_count_rating / sizeof($ActivityTypedata), 2) . '</td>              
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        </tr>';

                    for ($i = 0; $i < $max; $i++) {
                        $table .= '<tr>';
                        $table .= ' <td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['name'] . '</td>';
                        if (isset($ActivityTypedata[$i]['hours'])) {
                            $table .= '<td style="padding: 15px 10px;">' . m2h("i:s", $ActivityTypedata[$i]['hours']) . '</td>';
                            $table .= '<td style="padding: 15px 10px;">' . round($ActivityTypedata[$i]['hours'] / $totalHours * 100, 2) . '%</td>';
                            $table .= '<td style="padding: 15px 10px;">' . number_format($ActivityTypedata[$i]['Rate'], 2) . '</td>';
//                            $table .= '<td style="padding: 15px 10px;">';
//                            foreach ($ActivityTypedatacomments as $ActivityTypedatacomment) {
//                                if ($ActivityTypedata[$i]['id'] == $ActivityTypedatacomment['Activity_type']) {
//                                    $table .= '<span class="comments_span">' . $ActivityTypedatacomment['Comments'] . '</span><br/>';
//                                }
//                            }
//                            $table .= '</td>';
                            $table .= '<td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['Comments'] . '</td>';
                            $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                            $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                        } else {
                            $table .= '<td style="padding: 15px 10px;"></td>';
                        }
                        $table .= ' </tr>';
                    }
                }
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">TOTAL HOURS FOR DATE PERIOD</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . m2h("i:s", $totalHours) . '</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . $totalpercntt . '%</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        </tr>';
            } else {
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="red" colspan="7">No Activity Data Record Found</td>                                                                                               
        </tr>';
            }
            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="white" colspan="7"></td>                                                                                               
        </tr>';
        }

        $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center; color:white;" bgcolor="#589340" colspan="8">Total # of Hours For Sales Representatives ' . m2h("i:s", $total_hours_export_salesrep_AllUser) . '</td>                                                                                               
        </tr>';
        $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="white" colspan="8"></td>                                                                                               
        </tr>';
    } else {
        $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="red" colspan="7">No Record Found</td>                                                                                               
        </tr>';
    }
    $table .= '</table>';
    header('Content-type: application/vnd.xls');
    header('Content-Disposition: attachment; filename="' . $filename . '.xls"');
    print $table;
}

if (isset($_POST['representative_export_manager']) == 'Export Activity Level') {
    $representative_ids = $_POST['manager_id'];
    $start_date = $_POST['start_date'];
    $start_date = date("Y-m-d", strtotime($start_date));
    $end_date = $_POST['end_date'];
    $end_date = date("Y-m-d", strtotime($end_date));

    if (!empty($representative_ids)) {
        $total_hours_export_salesrep = 0;
        if (($_POST['m_strat_month'] != "" && $_POST['m_strat_day'] != "" && $_POST['m_strat_year'] != "" && $_POST['m_end_month'] != "" && $_POST['m_end_day'] != "" && $_POST['m_end_year'] != "") || (isset($_POST['days_select']))) {
            $table .= '<table width="1200" border="1">';
            foreach ($representative_ids as $representative_id) {

                if (isset($_POST['days_select'])) {
                    $totalHours = Reporting::getTotalHoursByDaysAndUserId($_POST['days_select'], $representative_id);
                    $meetingHistoryData = Reporting::getMeetingHistoryByDaysAndUserId($_POST['days_select'], $representative_id);
                    $user_full_name = User::getFullNameByUserId($representative_id);
                    $info = User::getAllInformationByUserId($representative_id);
                    $total_hours_export_salesrep += $totalHours;
                    $filename = 'Sales Representative [' . $_POST['days_select'] . ']';
                } else {
                    $totalHours = Reporting::getTotalHoursByDateRangeAndUserId($start_date, $end_date, $representative_id);
                    $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndUserId($start_date, $end_date, $representative_id);
                    $user_full_name = User::getFullNameByUserId($representative_id);
                    $info = User::getAllInformationByUserId($representative_id);
                    $total_hours_export_salesrep += $totalHours;
                    $filename = 'Sales Representative [' . $start_date . ' To ' . $end_date . ']';
                }

                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color: white; padding: 15px 10px; text-align: center;" bgcolor="black" colspan="8">[' . $user_full_name . ']</td>                                                                                               
        </tr>';

                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Name</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Sales Team</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Email Address</td>                                        
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Username</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">&nbsp</td>           
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Title</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Assigned Manager</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px;" bgcolor="#808080">User Since</td>  
        </tr>';
                $sales_team_name = User::getIndustryByUserId($info['industry_id']);
                $title = User::getDesignationByUserId($info['designation']);
                $assign_manager = User::getFullNameByUserId($info['assigned_to']);

                $table .= '<tr>
        <td style="padding: 15px 10px; font-size: 20px;" bgcolor="#fff"><b>' . $info['first_name'] . ' ' . $info['last_name'] . '</b></td>
        <td style="padding: 15px 10px; font-size: 20px;" bgcolor="#fff"><b>' . $sales_team_name . '</b></td>
        <td style="padding: 15px 10px; font-size: 20px;" bgcolor="#fff"><b>' . $info['email'] . '</b></td>
        <td style="padding: 15px 10px; font-size: 20px;" bgcolor="#fff"><b>' . $info['username'] . '</b></td>        
        <td style="padding: 15px 10px; font-size: 20px;" bgcolor="#fff"><b>&nbsp</b></td>        
        <td style="padding: 15px 10px; font-size: 20px;" bgcolor="#fff"><b>' . $title . '</b></td>
        <td style="padding: 15px 10px; font-size: 20px;" bgcolor="#fff"><b>' . $assign_manager . '</b></td>
        <td style="padding: 15px 10px; font-size: 20px;" bgcolor="#fff"><b>' . date('m-d-Y', strtotime($info['created_user'])) . '</b></td>
                             
        </tr>';

                if ($meetingHistoryData) {
                    $totalpercntt = 0;
                    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Activity and Type Summary</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Hours</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Percent to Total</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Difficulty Level</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Comments</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                            
                                                    
        </tr>';

                    foreach ($meetingHistoryData as $meeting) {
                        $totalpercntt += round($meeting['hours'] / $totalHours * 100, 2);
                        $ActivityTypedata = Reporting::getActivityTypeHoursForExportData($info['id'], $meeting['meeting_category']);
                        //$ActivityTypedatacomments = Reporting::getActivityTypeCommentsForExportData($info['id'], $meeting['meeting_category']);
                        $max = count($ActivityTypedata);
                        $rate_count_rating = 0;
                        foreach ($ActivityTypedata as $row_count_rating) {
                            $rate_count_rating += ($row_count_rating['Rate']);
                        }

                        $table .= '<tr>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . $meeting['name'] . '</td>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . m2h("i:s", $meeting['hours']) . '</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . round($meeting['hours'] / $totalHours * 100, 2) . '%</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . number_format($rate_count_rating / sizeof($ActivityTypedata), 2) . '</td>              
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>     
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>     
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>     
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>     
                
        </tr>';

                        for ($i = 0; $i < $max; $i++) {
                            $table .= '<tr>';
                            $table .= ' <td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['name'] . '</td>';
                            if (isset($ActivityTypedata[$i]['hours'])) {
                                $table .= '<td style="padding: 15px 10px;">' . m2h("i:s", $ActivityTypedata[$i]['hours']) . '</td>';
                                $table .= '<td style="padding: 15px 10px;">' . round($ActivityTypedata[$i]['hours'] / $totalHours * 100, 2) . '%</td>';
                                $table .= '<td style="padding: 15px 10px;">' . number_format($ActivityTypedata[$i]['Rate'], 2) . '</td>';
//                                $table .= '<td style="padding: 15px 10px;">';
//                                foreach ($ActivityTypedatacomments as $ActivityTypedatacomment) {
//                                    if ($ActivityTypedata[$i]['id'] == $ActivityTypedatacomment['Activity_type']) {
//                                        $table .= '<span class="comments_span">' . $ActivityTypedatacomment['Comments'] . '</span><br/>';
//                                    }
//                                }
//                                $table .= '</td>';
                                $table .= '<td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['Comments'] . '</td>';
                                $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                                $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                                $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                            } else {
                                $table .= '<td style="padding: 15px 10px;"></td>';
                            }
                            $table .= ' </tr>';
                        }
                    }

                    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">TOTAL HOURS FOR DATE PERIOD</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . m2h("i:s", $totalHours) . '</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . $totalpercntt . '%</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        
        </tr>';
                } else {
                    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="red" colspan="8">No Activity Data Record Found</td>                                                                                               
        </tr>';
                }

                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="white" colspan="8"></td>                                                                                               
        </tr>';
            }

            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center; color:white;" bgcolor="#589340" colspan="8">Total # of Hours ' . m2h("i:s", $total_hours_export_salesrep) . '</td>                                                                                               
        </tr>';
            $table .= '</table>';
            header('Content-type: application/vnd.xls');
            header('Content-Disposition: attachment; filename="' . $filename . '.xls"');

            print $table;
        }
    }
}

if (isset($_POST['user_rotation_export'])) {


    $rotation_id = $_POST['rotation'];
    $rotation_fetch_data = User::getRotationById($rotation_id);
    $all_managers = User::getAllusersByRotation($rotation_id, 3);
    $all_salesrepresentatives = User::getAllusersByRotation($rotation_id, 4);
    $filename = 'Campaigns [' . $rotation_fetch_data['rotation_title'] . ']';



    $table .= '<table width="1200" border="1">';

    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color: white; padding: 15px 10px; text-align: center;" bgcolor="black" colspan="7">All Managers</td>                                                                                               
        </tr>';

    if (!empty($all_managers)) {

        $total_hours_export_manager_allUser = 0;
        foreach ($all_managers as $info) {

            $sales_team_name = User::getIndustryByUserId($info['industry_id']);
            $totalHours = Reporting::getTotalHoursByDateRangeAndUserId($rotation_fetch_data['start_date'], $rotation_fetch_data['end_date'], $info['roation_user_id']);
            $total_hours_export_manager_allUser += $totalHours;
            $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndUserId($rotation_fetch_data['start_date'], $rotation_fetch_data['end_date'], $info['roation_user_id']);
            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Name</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Sales Team</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Email Address</td>                                        
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Username</td>                                         
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">User Since</td>                                        
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">&nbsp</td>                                        
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">&nbsp</td>                                        
        </tr>';
            $table .= '<tr>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['first_name'] . ' ' . $info['last_name'] . '</b></td>';
            $manager_industries = User::getIndustriesByUserId($info['id']);
            $table .= '<td style="padding: 15px 10px; font-size: 20px;" bgcolor="#fff">';
            foreach ($manager_industries as $manager_industry) {
                $table .= '<b>' . User::getIndustryByUserId($manager_industry['sales_team_id']) . '</b><br/>';
            }
            $table .= '</td>';
            $table .= '<td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['email'] . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['username'] . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . date('m-d-Y', strtotime($info['created_user'])) . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff">&nbsp</td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff">&nbsp</td>
        </tr>';
            if (!empty($meetingHistoryData)) {
                $totalpercntt = 0;
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Activity and Type Summary</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Hours</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Percent to Total</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Difficulty Level</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Comments</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                            
        </tr>';

                foreach ($meetingHistoryData as $meeting) {
                    $totalpercntt += round($meeting['hours'] / $totalHours * 100, 2);
                    $ActivityTypedata = Reporting::getActivityTypeHoursForExportData($info['roation_user_id'], $meeting['meeting_category']);
                    //$ActivityTypedatacomments = Reporting::getActivityTypeCommentsForExportData($info['id'], $meeting['meeting_category']);
                    $max = count($ActivityTypedata);
                    $rate_count_rating = 0;
                    foreach ($ActivityTypedata as $row_count_rating) {
                        $rate_count_rating += ($row_count_rating['Rate']);
                    }

                    $table .= '<tr>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . $meeting['name'] . '</td>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . m2h("i:s", $meeting['hours']) . '</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . round($meeting['hours'] / $totalHours * 100, 2) . '%</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . number_format($rate_count_rating / sizeof($ActivityTypedata), 2) . '</td>              
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        </tr>';

                    for ($i = 0; $i < $max; $i++) {
                        $table .= '<tr>';
                        $table .= ' <td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['name'] . '</td>';
                        if (isset($ActivityTypedata[$i]['hours'])) {
                            $table .= '<td style="padding: 15px 10px;">' . m2h("i:s", $ActivityTypedata[$i]['hours']) . '</td>';
                            $table .= '<td style="padding: 15px 10px;">' . round($ActivityTypedata[$i]['hours'] / $totalHours * 100, 2) . '%</td>';
                            $table .= '<td style="padding: 15px 10px;">' . number_format($ActivityTypedata[$i]['Rate'], 2) . '</td>';
//                            $table .= '<td style="padding: 15px 10px;">';
//                            foreach ($ActivityTypedatacomments as $ActivityTypedatacomment) {
//                                if ($ActivityTypedata[$i]['id'] == $ActivityTypedatacomment['Activity_type']) {
//                                    $table .= '<span class="comments_span">' . $ActivityTypedatacomment['Comments'] . '</span><br/>';
//                                }
//                            }
//                            $table .= '</td>';
                            $table .= '<td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['Comments'] . '</td>';
                            $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                            $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                        } else {
                            $table .= '<td style="padding: 15px 10px;"></td>';
                        }
                        $table .= ' </tr>';
                    }
                }
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">TOTAL HOURS FOR DATE PERIOD</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . m2h("i:s", $totalHours) . '</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . $totalpercntt . '%</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        </tr>';
            } else {
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="red" colspan="7">No Activity Data Record Found</td>                                                                                               
        </tr>';
            }
            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="white" colspan="7"></td>                                                                                               
        </tr>';
        }
        $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center; color:white;" bgcolor="#589340" colspan="8">Total # of Hours For Managers ' . m2h("i:s", $total_hours_export_manager_allUser) . '</td>                                                                                               
        </tr>';
        $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="white" colspan="8"></td>                                                                                               
        </tr>';
    } else {
        $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="red" colspan="7">No Record Found</td>                                                                                               
        </tr>';
    }

    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color: white; padding: 15px 10px; text-align: center;" bgcolor="black" colspan="7">All Sales Representatives</td>
        </tr>';

    if (!empty($all_salesrepresentatives)) {
        $total_hours_export_salesrep_AllUser = 0;
        foreach ($all_salesrepresentatives as $info) {
            $sales_team_name_sales = User::getIndustryByUserId($info['industry_id']);
            $title = User::getDesignationByUserId($info['designation']);
            $assign_manager = User::getFullNameByUserId($info['assigned_to']);
            $totalHours = Reporting::getTotalHoursByDateRangeAndUserId($rotation_fetch_data['start_date'], $rotation_fetch_data['end_date'], $info['roation_user_id']);
            $total_hours_export_salesrep_AllUser += $totalHours;
            $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndUserId($rotation_fetch_data['start_date'], $rotation_fetch_data['end_date'], $info['roation_user_id']);
            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Name</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Sales Team</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Email Address</td>                                        
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Username</td>
         <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Title</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">Assigned Manager</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; color:white; padding: 15px 10px; font-size: 20px;" bgcolor="#808080">User Since</td>
        </tr>';

            $table .= '<tr>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['first_name'] . ' ' . $info['last_name'] . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $sales_team_name_sales . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['email'] . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $info['username'] . '</b></td>     
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $title . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . $assign_manager . '</b></td>
        <td style="padding: 15px 10px; font-size: 18px;" bgcolor="#fff"><b>' . date('m-d-Y', strtotime($info['created_user'])) . '</b></td>              
        </tr>';

            if (!empty($meetingHistoryData)) {
                $totalpercntt = 0;
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Activity and Type Summary</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Hours</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Percent to Total</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Difficulty Level</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Comments</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                            
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp</td>                                            
        </tr>';

                foreach ($meetingHistoryData as $meeting) {
                    $totalpercntt += round($meeting['hours'] / $totalHours * 100, 2);
                    $ActivityTypedata = Reporting::getActivityTypeHoursForExportData($info['roation_user_id'], $meeting['meeting_category']);
                    //$ActivityTypedatacomments = Reporting::getActivityTypeCommentsForExportData($info['id'], $meeting['meeting_category']);
                    $max = count($ActivityTypedata);
                    $rate_count_rating = 0;
                    foreach ($ActivityTypedata as $row_count_rating) {
                        $rate_count_rating += ($row_count_rating['Rate']);
                    }

                    $table .= '<tr>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . $meeting['name'] . '</td>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . m2h("i:s", $meeting['hours']) . '</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . round($meeting['hours'] / $totalHours * 100, 2) . '%</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . number_format($rate_count_rating / sizeof($ActivityTypedata), 2) . '</td>              
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">&nbsp</td>        
        </tr>';

                    for ($i = 0; $i < $max; $i++) {
                        $table .= '<tr>';
                        $table .= ' <td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['name'] . '</td>';
                        if (isset($ActivityTypedata[$i]['hours'])) {
                            $table .= '<td style="padding: 15px 10px;">' . m2h("i:s", $ActivityTypedata[$i]['hours']) . '</td>';
                            $table .= '<td style="padding: 15px 10px;">' . round($ActivityTypedata[$i]['hours'] / $totalHours * 100, 2) . '%</td>';
                            $table .= '<td style="padding: 15px 10px;">' . number_format($ActivityTypedata[$i]['Rate'], 2) . '</td>';
//                            $table .= '<td style="padding: 15px 10px;">';
//                            foreach ($ActivityTypedatacomments as $ActivityTypedatacomment) {
//                                if ($ActivityTypedata[$i]['id'] == $ActivityTypedatacomment['Activity_type']) {
//                                    $table .= '<span class="comments_span">' . $ActivityTypedatacomment['Comments'] . '</span><br/>';
//                                }
//                            }
//                            $table .= '</td>';
                            $table .= '<td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['Comments'] . '</td>';
                            $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                            $table .= '<td style="padding: 15px 10px;">&nbsp</td>';
                        } else {
                            $table .= '<td style="padding: 15px 10px;"></td>';
                        }
                        $table .= ' </tr>';
                    }
                }
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">TOTAL HOURS FOR DATE PERIOD</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . m2h("i:s", $totalHours) . '</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . $totalpercntt . '%</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        </tr>';
            } else {
                $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="red" colspan="7">No Activity Data Record Found</td>                                                                                               
        </tr>';
            }
            $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="white" colspan="7"></td>                                                                                               
        </tr>';
        }

        $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center; color:white;" bgcolor="#589340" colspan="8">Total # of Hours For Sales Representatives ' . m2h("i:s", $total_hours_export_salesrep_AllUser) . '</td>                                                                                               
        </tr>';
        $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="white" colspan="8"></td>                                                                                               
        </tr>';
    } else {
        $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px; text-align: center;" bgcolor="red" colspan="7">No Record Found</td>                                                                                               
        </tr>';
    }
    $table .= '</table>';
    header('Content-type: application/vnd.xls');
    header('Content-Disposition: attachment; filename="' . $filename . '.xls"');
    print $table;
}