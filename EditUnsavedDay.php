<?php
error_reporting(0);
$page_title = 'Edit Unsaved Day';
include('includes/top.php');
if (!Session::get('login')) {
    Redirect::to('index.php');
}
$id = $_GET['id'];
$meetingDetail = Meeting::getMeetingDetailsById($id);
$meeting_id = $meetingDetail['meeting_id'];
$total_hours_meeting = Meeting::getTotalHoursofMeeting($meeting_id);
$created_by = Meeting::getUserIdbyMeetingId($meetingDetail['meeting_id']);
$meeting_category_id = Meeting::getMeetingCategoryById($id);
$activity_types = Meeting::getActivityTypeByMeetingCategoryId($meeting_category_id);
$max = max(count($activity_types), count($account_types), count($customer_status), count($product_service), count($contact_mode));
$actualtime = gmdate("i:s", $meetingDetail['hours']);
$houra_array = explode(':', $actualtime);
$hr = ltrim($houra_array[0], '0');
$mn = ltrim($houra_array[1], '0');
$tot_hours = 0;
$html = '';
$i = 0;
for ($i = 0; $i < $max; $i++) {
    $html .= '<tr>';
    if ($i < 1) {
        $tot_hours+=$meetingDetail['hours'];
        $html .= '<td style="width: 15%;"><strong style="text-align: center;">' . Meeting::getMeetingCategoryNameById($meetingDetail['meeting_category']) . '</strong></td>
                  <td align="center" style="width: 10%;">';
        $html .= '<input  type="hidden" name="activity_category" id="activity_category" value="' . $meeting_category_id . '" />';
        $html .= '<input type="hidden" id="previousHours" value="' . $hr . '"/>';
        $html .= '<input type="hidden" id="previousMinutes" value="' . $mn . '"/>';
        $html .= '<div class="timeFields">                    
                    <div>                    
                    <select data-placeholder="Hours" name="hours" id="user_hours" class="chosen-select hours' . $cn . '" onchange="checkhoursEdit();">
                        <option value=""></option>';
        for ($x = 0; $x <= 12; $x++) {
            if ($x == $hr) {
                $selected_hr = 'selected="selected"';
            } else {
                $selected_hr = '';
            }
            $html .= '<option value="' . $x . '" ' . $selected_hr . '>' . $x . '</option>';
        }
        $html .= '</select>
                    </div>
                    <div>                    
                    <select data-placeholder="Minutes" id="user_minutes" name="minutes" class="chosen-select minutes' . $cn . '" onchange="checkhoursEdit();">
                        <option value=""></option>';
        for ($x = 0; $x <= 60; $x++) {
            if ($x == $mn) {
                $selected_mn = 'selected="selected"';
            } else {
                $selected_mn = '';
            }
            $html .= '<option value="' . $x . '" ' . $selected_mn . '>' . $x . '</option>';
        }
        $html .= '</select>
                    </div>
                    </div>';
        $html .='</td>';
    } elseif ($i < 2) {
        $html .= '<td rowspan="15" class="light"></td>
                  <td rowspan="15" align="center" class="light"></td>';
    }
    if (!empty($activity_types[$i]['name'])) {
        if ($meetingDetail['activity_type'] == $activity_types[$i]['id']) {
            $html .= '<td><input type="radio" name="activity_type" checked  value="' . $activity_types[$i]['id'] . '" onClick="removeOneditradio();"> <label for="">' . $activity_types[$i]['name'] . '</label></td>';
        } else {
            $html .= '<td><input type="radio" class="activity_typeclass" onclick = "enabledCheckbox(this);"  name="activity_type"  value="' . $activity_types[$i]['id'] . '" onClick="showingRatingedit(' . $activity_types [$i]['id'] . ');"> <label for="">' . $activity_types[$i]['name'] . '</label></td>';
        }
    } else {
        $html .= '<td></td>';
    }
    if ($meetingDetail['activity_rate'] == 1) {
        $v1 = 'checked';
    } else {
        $v1 = '';
    }
    if ($meetingDetail['activity_rate'] == 2) {
        $v2 = 'checked';
    } else {
        $v2 = '';
    }
    if ($meetingDetail['activity_rate'] == 3) {
        $v3 = 'checked';
    } else {
        $v3 = '';
    }
    if ($meetingDetail['activity_rate'] == 4) {
        $v4 = 'checked';
    } else {
        $v4 = '';
    }
    if ($meetingDetail['activity_rate'] == 5) {
        $v5 = 'checked';
    } else {
        $v5 = '';
    }
    if ($meetingDetail['activity_type'] == $activity_types[$i]['id']) {
        $html .= '<td class="light" style="width: 16%;"><div class="activityrating activityratingstaredit">
            <input type="radio" name="activity_rate" ' . $v1 . ' class="rating ratingedit" value="1" />
            <input type="radio" name="activity_rate" ' . $v2 . '  class="rating ratingedit" value="2" />
            <input type="radio" name="activity_rate" ' . $v3 . '  class="rating ratingedit" value="3" />
            <input type="radio" name="activity_rate" ' . $v4 . '  class="rating ratingedit" value="4" />
            <input type="radio" name="activity_rate" ' . $v5 . '  class="rating ratingedit" value="5" />
            </div></td>';
    } else {
        $html .= '<td   class="light" style="width: 16%;"><div class="ratehideall rateHide' . $activity_types [$i]['id'] . '" style="display:none;"><div class="activityrating activityratingFullstar">
            <input type="radio" name="activity_rate" class="ratingAll rating" value="1" />
            <input type="radio" name="activity_rate" class="ratingAll rating" value="2" />
            <input type="radio" name="activity_rate" class="ratingAll rating" value="3" />
            <input type="radio" name="activity_rate" class="ratingAll rating" value="4" />
            <input type="radio" name="activity_rate" class="ratingAll rating" value="5" />
            </div></div><span><input type="checkbox" id="' . $activity_types [$i]['id'] . '" class="showratingstar' . $activity_types [$i]['id'] . ' ratingShow" name="showratingstar" onClick="showingRating(' . $activity_types [$i]['id'] . ');" disabled="disabled" style="display:none;"/></span><span class="messagerateHideall messagerateHide' . $activity_types [$i]['id'] . '">Rate This Activity\'s Difficulty</span></td>';
    }
    if ($meetingDetail['activity_type'] == $activity_types[$i]['id']) {
        $html .= '<td  class="light" style="width: 30%;"><textarea id="activity_comment" class="activity_commentvalue activity_commentvalueedit activity_comment' . $activity_types [$i]['id'] . '" name="activity_comment">' . $meetingDetail['activity_comment'] . '</textarea></td>';
    } else {
        $html .= '<td  class="light" style="width: 30%;"><textarea class="activity_comment' . $activity_types [$i]['id'] . ' activity_commentall activity_commentvalue" name="activity_comment" disabled="disabled"></textarea></td>';
    }
    $html .= '</tr>';
}
include('includes/header.php');
?>
<link rel="stylesheet" href="assets/css/drop/chosen.css"/>
<script src="assets/javascripts/drop/chosen.jquery.js" type="text/javascript"></script>
<style>
    body .ui-tooltip {font-size: 12px;}
</style>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<section class="mainContent viewMyDay clearfix">
    <div class="tableSec clearfix">
        <input type="hidden" id="meeting_date" value="<?php echo $meetingDetail['added_date']; ?>" >
        <input type="hidden" id="created_by" value="<?php echo $created_by; ?>" >
        <input type="hidden" id="current_meeting_hours" value="<?php echo $meetingDetail['hours'] ?>" />
        <input type="hidden" id="meeting_detail_id" value="<?php echo $_GET['id']; ?>">
        <input type="hidden" id='total_hours_meeting' value="<?php echo number_format((float) $total_hours_meeting, 2, '.', ''); ?>" />
        <?php
        $meeting_hours_seperate = $total_hours_meeting - $meetingDetail['hours'];
        ?>
        <input type="hidden" id="meeting_hours_seperate" value="<?php echo $meeting_hours_seperate; ?>" />
        <div class="tableScroll clearfix">
            <table border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th>Category </th>
                        <th>Hours</th>
                        <th>Activity Type</th>
                        <th style="line-height: 15px; text-align: center;">Difficulty of Completing Activity  <a href="#" data-toggle="tooltip" title="Assess the difficulty that systems, time constraints, resource availability, or personal knowledge impose on your ability to complete this activity. Please add details in Comment box."><img src="assets/images/info-icon.png"/></a></th> 
                        <th>Comments</th>
                    </tr>
                </thead>
                <tbody><?php echo $html; ?></tbody>
                <tfoot>
                    <tr>
                        <td><strong>Total Time (hours/minutes)</strong>Total of All Categories</td>
                        <td id="totalhours">
                            <?php if (Session::get('level') != 1) { ?>                           
                                <?php $totalHours = Meeting::getTotalTimeByDateAndUserId($meetingDetail['added_date'], Session::get('user_id')); ?>
                                <span id="remaningTimeedit"><?php echo m2h("i:s", $totalHours); ?></span><span style="display:none;">1440</span>
                                <input type="hidden" id="remaningTimehiddenedit" value="<?php echo $totalHours; ?>"/>
                                <input type="hidden" id="remaningTimehiddeneditcalc" value="<?php echo $totalHours; ?>"/>
                            <?php } ?>
                            <?php if (Session::get('level') == 1) { ?>                           
                                <?php $getData = Meeting::getUserIdbyMeetingdetailId($_GET['id']); ?>                                
                                <?php $totalHours = Meeting::getTotalTimeByDateAndUserId($getData['added_date'], $getData['created_by']); ?>
                                <span id="remaningTimeedit"><?php echo m2h("i:s", $totalHours); ?></span><span style="display:none;">1440</span>                                
                                <input type="hidden" id="remaningTimehiddenedit" value="<?php echo $totalHours; ?>"/>
                                <input type="hidden" id="remaningTimehiddeneditcalc" value="<?php echo $totalHours; ?>"/>
                            <?php } ?>
                        </td>
                        <td colspan="5"></td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="clearfix floatRight">
            <a href="" id="updateMeeting"  class="redBtn">Save</a>
            <input type="hidden" name="hiddenid" id="hiddenid" value="<?php echo Session::get('level'); ?>"/>
        </div>
    </div>
</section>                 

<script type="text/javascript">
    $('#updateMeeting').click(function () {
        var time = '';
        var h = '';
        var m = '';
        var hours = $("#user_hours").val();
        var minutes = $("#user_minutes").val();
        var activity_category = $("#activity_category").val();
        var activity_type = $("input[name='activity_type']:checked").val();
        var activity_rate = $("input[name='activity_type']:checked").parents("tr").find(".fullStar").attr("title");
        var activity_commentval = $("input[name='activity_type']:checked").parents('tr').find(".activity_commentvalue");
        var activity_comment = activity_commentval.val();
        var meeting_detail_id = $('#meeting_detail_id').val();
        if (hours == 0 && minutes == 0) {
            $("#user_hours").parents('td').css("background-color", "#e80703");
            fancyAlert("Please insert time.");
            return false;
        } else {
            $("#user_hours").parents('td').css("background-color", "");
        }
       
        if (!activity_rate)
        {
            $(".rateHide" + activity_type).parents('td').css("background-color", "#e80703");
            fancyAlert('Please select difficulty level.');
            return false;
        } else {
            $(".rateHide" + activity_type).parents('td').css("background-color", "");
        }
        if (activity_type == 21 || activity_type == 35 || activity_type == 36 || activity_type == 37)
        {
            if (!activity_comment) {
                $(".activity_comment" + activity_type).parents('td').css("background-color", "#e80703");
                fancyAlert('Comments Are Required When Submitting an OTHER Activity.');
                return false;
            } else {
                $(".activity_comment" + activity_type).parents('td').css("background-color", "");
            }
        }
        if (hours != '') {
            h = parseInt(hours);
            m = parseInt(minutes);
            time = (h * 60 + m);
        } else {
            time = m;
        }
        $.ajax({
            type: "POST",
            cache: false,
            url: "updateMeetingDetail.php",
            data: {
                hours: time,
                activity_type: activity_type,
                activity_rate: activity_rate,
                activity_comment: activity_comment,
                meeting_detail_id: meeting_detail_id
            }
        }).done(function (data) {
            var userlevel = $('#hiddenid').val();
            if (data == 'success') {
                if (userlevel == 1) {
                    fancyAlertConfirm('Activity Successfully Updated', '<?php echo $site_url; ?>editMeetingHours.php');
                } else {
                    fancyAlertConfirm('Your Activity Successfully Updated', '<?php echo $site_url; ?>viewUnSavedDays.php');
                }
            }
        });
        return false;
    });
    function getNum(val)
    {
        if (isNaN(val))
            return 0;
        else
            return val;
    }
    function checkhoursEdit() {
        var totalmin = 1440;
        var currentmin = $('#remaningTimeedit').text();
        var remaningTimehiddeneditcalc = $('#remaningTimehiddeneditcalc').val();
        var current_meeting_hours = $('#current_meeting_hours').val();
        var user_hour = $('#user_hours').val();
        var remaingmin = (totalmin - remaningTimehiddeneditcalc);
        var previousHours = $('#previousHours').val();
        var previousMinutes = $('#previousMinutes').val();
        var calculattime = '';
        var hour = $('#user_hours').val();
        var min = $('#user_minutes').val();
        if (hour > 0 || min > 0) {
            if (hour != '') {
                if (min > 0) {
                    calculattime += (parseInt(hour) * 60 + parseInt(min));
                } else {
                    calculattime += (parseInt(hour) * 60);
                }
            } else {
                calculattime += parseInt(min);
            }
        }
        var remaningTimehidden = $('#remaningTimehiddenedit').val();
        var remg = (parseInt(remaningTimehidden) - parseInt(current_meeting_hours));
        $('#remaningTimeedit').text(convertMintoHours(parseInt(calculattime) + parseInt(remg)));
        $('#remaningTimehiddeneditcalc').val(parseInt(calculattime) + parseInt(remg));
        if (remaningTimehiddeneditcalc != 1440) {
            if (calculattime > remaingmin) {
                fancyAlert('You can not add above ' + remaingmin + ' minutes');
                $('#user_hours').val(previousHours).trigger('chosen:updated');
                $('#user_minutes').val(previousMinutes).trigger('chosen:updated');
                $('#remaningTimeedit').text(convertMintoHours(remaningTimehidden));
                $('#remaningTimehiddeneditcalc').val(remaningTimehiddeneditcalc);
            }
        }
    }
    function enabledCheckbox(obj) {
        $('input[type="checkbox"]').each(function () {
            $(this).attr('disabled', true);
            $(this).attr('checked', false);
            var id = $(this).attr('id');
            $('.rateHide' + id).css('display', 'none');
            $('.messagerateHide' + id).show();
            $('#activity_comment' + id).attr("disabled", true);
            $('textarea.activity_commentall').val('');
        });
        $('.activityratingstaredit').hide();
        $(obj).parents('tr').find('input[type="checkbox"]').removeAttr('disabled');
        $(obj).parents('tr').find('input[type="checkbox"]').click();
        $('.ratingAll').removeAttr('checked');
        $('.activity_commentvalueedit').attr("disabled", true);
        $('.ratingedit').attr("disabled", true);
    }
    function showingRating(id) {
        $('.showratingstar' + id).each(function () {
            if ($(this).prop("checked") == true) {
                $('.rateHide' + id).css('display', 'block');
                $('.messagerateHide' + id).hide();
                $('.activity_comment' + id).removeAttr("disabled");
            }
            else if ($(this).prop("checked") == false) {
                $('textarea.activity_commentall').val('');
                $('.rateHide' + id).css('display', 'none');
                $('.messagerateHide' + id).show();
                $('.activity_comment' + id).attr("disabled", true);
                $('.ratingAll').removeAttr('checked');
                $(this).parents('td').find("a").removeClass("fullStar");
            }
        });
    }
    function removeOneditradio() {
        $('.ratingedit').removeAttr('disabled');
        $('.messagerateHideall').show();
        $('.ratehideall').hide();
        $('.ratingAll').removeAttr('checked');
        $('.activity_commentvalueedit').removeAttr('disabled');
        $('.activity_commentall').val('');
        $('.activity_commentall').attr("disabled", true);
        $('.ratingShow').attr('checked', false).attr("disabled", true);
        $('.activityratingstaredit').show();
        $('.activityratingFullstar').parents('td').find("a").removeClass("fullStar");
    }

    $(".chosen-select").chosen({width: "90%"});
</script>
<?php include('includes/footer.php'); 