<?php
error_reporting(0);
include('includes/top.php');
if (Session::get('login')) {
    Redirect::to('dashboard.php');
} else {
    Redirect::to('login.php');
}