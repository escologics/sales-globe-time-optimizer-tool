<?php
$page_title = 'Rotations';
include('includes/top.php');
if (!Session::get('login')) {
    Redirect::to('index.php');
}
if (Session::get('level') == 3 || Session::get('level') == 4) {
    Redirect::to('viewMyday.php');
}
include('includes/header.php');
$rotations = User::getAllRotations();
?>
<div class="tabs_container clearfix">
    <?php if (isset($_GET['add'])) : ?>
        <p style="color: green; font-weight: bold;">Your Rotation has been added successfully</p>
    <?php elseif (isset($_GET['edit'])) : ?>
        <p style="color: green; font-weight: bold;">Your Rotation has been Updated successfully</p>
    <?php endif; ?>
    <div class="tableSec admin clearfix">
        <div class="tableScroll clearfix">
            <table>
                <thead>
                    <tr>
                        <th align="left" width="5%">#</th>
                        <th align="left">Rotation Title</th>                           
                        <th align="left">Start Date</th>                           
                        <th align="left">End Date</th>                           
                        <th align="left"><a href="addRotation.php" id="addRotation" class="darkBtn">Add Rotation</a></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($rotations as $key => $rotation) {
                        $srno = $key + 1
                        ?>
                        <tr style="" id="row_2" class="light">
                            <td><?php echo $srno; ?></td>
                            <td><?php echo $rotation['rotation_title']; ?></td>                         
                            <td><?php echo date("m-d-Y", strtotime($rotation['start_date'])); ?></td>                         
                            <td><?php echo date("m-d-Y", strtotime($rotation['end_date'])); ?></td>                         
                            <td align="center" width="10%" class="tableBtn">                               
                                <a href="editRotation.php?rotation_id=<?php echo $rotation['id']; ?>" class="fancybox"><img src="assets/images/settingRed.jpg" alt=""></a>
                                <a href="#delete_rotation_<?php echo $rotation['id']; ?>" class="fancybox"><img src="assets/images/closeRed.jpg" alt=""></a>
                                <div id="delete_rotation_<?php echo $rotation['id']; ?>" class="popupBox alignCenter width_240">
                                    <p><img src="assets/images/success.png" alt=""></p>
                                    <h3>Are you sure you want to delete this rotation?</h3>
                                    <form action="deleteRotation.php" method="post">                                        
                                        <input type="hidden" name="rotation_id" value="<?php echo $rotation['id']; ?>">
                                        <input type="hidden" name="current_page" value="<?php echo curPageURL(); ?>" >
                                        <input type="submit" id="submit" name="delete_rotation" value="Yes" class="marginTop_27">
                                    </form>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
include('includes/footer.php');
