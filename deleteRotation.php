<?php

include('includes/top.php');

if (isset($_POST['delete_rotation'])) {

    $rotation_id = $_POST['rotation_id'];
    $delete = DB::getInstance()->delete('rotations', $rotation_id);
    $delete2 = DB::getInstance()->deleteBatch('rotations_users', 'rotation_id', $rotation_id);

    if ($delete2) {
        Redirect::to('rotations.php');
    }
}