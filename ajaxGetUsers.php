<?php

include('includes/top.php');
$user_level = $_POST['user_level'];
$html = '';
$results = DB::getInstance()->GetAllResults("Select * from users where level = $user_level And status = 1");
$allmanagers = DB::getInstance()->GetAllResults("Select id, first_name, last_name from users where level = 3 And status = 1");
$industries = User::getAllIndustry();
$get_rotations = User::getAllRotations();
$checked = '';

if ($user_level == 3) {
    if ($results) {
        $sr = 0;
        foreach ($results as $result) {
            $get_manager_rotations_array = array();
            $get_manager_rotations = User::getRotationsFromRotationsUsersByUserID($result['id']);
            foreach ($get_manager_rotations as $key => $get_manager_rotation) {
                $get_manager_rotations_array[$key] = $get_manager_rotation['rotation_id'];
            }
            $html .= '<tr style="" id="row_' . $result['id'] . '">
            <td>' . ($sr + 1) . '</td>
            <td>' . $result['first_name'] . ' ' . $result['last_name'] . '</td>';
            $manager_industries = User::getIndustriesByUserId($result['id']);
            $html .= '<td>';
            foreach ($manager_industries as $manager_industry) {
                $html .= '<p>' . User::getIndustryByUserId($manager_industry['sales_team_id']) . '</p>';
            }
            $html .= '</td>';
            $html .= ' <td><a href="mailto:' . $result['email'] . '">' . $result['email'] . '</a></td>
            <td>' . $result['username'] . '</td>    
            <td>';

            $rotationUsers = User::getRotationsFromRotationsUsersByUserID($result['id']);
            foreach ($rotationUsers as $rotationUser) {

                $html .= ' <p>' . User::getRotationsTitleByRotationID($rotationUser['rotation_id']) . '</p>';
            }
            $html .= '  </td>
            <td align="center" width="8%"  class="tableBtn">
            <div id="viewManagerDelete_' . $result['id'] . '" class="popupBox alignCenter width_240">
                    <p><img src="assets/images/success.png" alt=""></p>
                    <h3>Are you sure you want to delete this user?</h3>
                    <form action="deleteUser.php" method="post">
                        <input type="hidden" name="user_type" value="manager">
                        <input type="hidden" name="user_id" value="' . $result['id'] . '">
                        <input type="hidden" name="current_page" value="viewUsers.php" >
                        <input type="submit" id="submit" name="submit" value="Yes" class="marginTop_27">
                    </form>
            </div>
                
            <div id="delete_' . $result['id'] . '" class="popupBox alignCenter">
                <p><img src="assets/images/success.png" alt=""></p>
                <h3>Please assign all Sales Representatives under this Manager to another Manager.</h3>
                <a href="#deleteNext_' . $result['id'] . '" class="fancybox redBtn">OK</a>
                <input type="button" value="No" class="redBtn" onclick="jQuery.fancybox.close()" />
            </div>
            
            <div id="deleteNext_' . $result['id'] . '" class="popupBox width_240">
                <h3>Select Manager</h3>
                <form action="deleteUser.php" method="post" >
                    <select required="required"  name="assigned_to">';
            foreach ($allmanagers as $allmanager) {
                $html .= ' <option value="' . $allmanager['id'] . '">' . $allmanager['first_name'] . ' ' . $allmanager['last_name'] . '</option>';
            }
            $html .= ' </select>
                <input type="hidden" name="user_id" value="' . $result['id'] . '">
                <input type="hidden" name="user_type" value="manager">
                <input type="hidden" name="current_page" value="viewUsers.php">
                <input type="submit" id="submit" name="submit" value="Save" class="marginTop_27">
                </form>
            </div>
          

            <a href="edituser.php?id=' . $result['id'] . '&level=' . $result['level'] . '" class=""><img src="assets/images/settingRed.jpg" alt=""></a>';
            if (User::hasSalesRep($result['id']) > 0) {
                $html .= '  <a href="#delete_' . $result['id'] . '" class="fancybox"><img src="assets/images/closeRed.jpg" alt=""></a>';
            } else {
                $html .= '  <a href="#viewManagerDelete_' . $result['id'] . '" class="fancybox"><img src="assets/images/closeRed.jpg" alt=""></a>';
            }
            $html .= ' </td></tr>';
            $sr++;
        }
        echo $html;
    } else {
        echo 'No Record Found.';
    }
} elseif ($user_level == 4) {
    if ($results) {
        $sr = 0;
        foreach ($results as $result) {
            $html .= '<tr>
    <td>' . ($sr + 1) . ' <div id="viewSalesRepPopupDelete_' . $result['id'] . '" class="popupBox alignCenter width_240">
            <p><img src="assets/images/success.png" alt=""></p>
            <h3>Are you sure you want to delete this user?</h3>
            <form action="deleteUser.php" method="post">
                <input type="hidden" name="user_type" value="salesrep">
                <input type="hidden" name="user_id" value="' . $result['id'] . '">
                <input type="hidden" name="current_page" value="viewUsers.php" >
                <input type="submit" id="submit" name="submit" value="Yes" class="marginTop_27">
            </form>
        </div>

        </td>
        
    <td>' . $result['first_name'] . ' ' . $result['last_name'] . '</td>
    <td>' . User::getIndustryByUserId($result['industry_id']) . '</td>
    <td>' . User::getDesignationByUserId($result['designation']) . '</td>                                                                                                                                                                                                                                                                                                                
    <td>' . User::getFullNameByUserId($result['assigned_to']) . '</td>
    <td><a href="mailto:' . $result['email'] . '">' . $result['email'] . '</a></td>
    <td>' . $result['username'] . '</td>
    <td>';

            $rotationUsers = User::getRotationsFromRotationsUsersByUserID($result['id']);
            foreach ($rotationUsers as $rotationUser) {

                $html .='<p>' . User::getRotationsTitleByRotationID($rotationUser['rotation_id']) . '</p>';
            }
            $html .='</td>
    <td align="center" width="8%" class="tableBtn">
        <a href="edituser.php?id=' . $result['id'] . '&level=' . $result['level'] . '" class=""><img src="assets/images/settingRed.jpg" alt=""></a>
        <a href="#viewSalesRepPopupDelete_' . $result['id'] . '" class="fancybox"><img src="assets/images/closeRed.jpg" alt=""></a>
    </td>
</tr>';
            $sr++;
        }

        echo $html;
    } else {
        echo 'No Record Found.';
    }
}