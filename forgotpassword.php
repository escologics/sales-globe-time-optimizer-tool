<?php
include('includes/top.php');

//Check if Data posted
if (Input::exists()) {
    //check validation of fomr
    $validate = new Validate();
    $validation = $validate->check($_POST, array(
        'email' => array(
            'label' => 'Email',
            'required' => true,
            'validate' => 'email',
        ),
    ));

    //if validation is passed
    $msg = '';
    if ($validation->passed()) {
        $email = $_POST['email'];
        //if email is not already exists
        if (User::checkByEmail($email) > 0) {
            $chars = 'abcdefghijklmnopqrshdgahsgdjhADHGAJHDGasdtuvwx832479283749827344yz0123456789';
            $token = substr(str_shuffle(md5($chars)), 0, 100);
            $link = $GLOBALS['config']['configuration']['baseUrl'] . 'resetpassword.php?token=' . $token;
            $text = "Please click here to reset your password : " . $link;
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "From: noreply@salesglobe.com\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            // send email to give email address
            if (mail($email, "Password Reset", $text, $headers)) {
                $msg .= "Please check your mail to update your password"; // if password is not updated
                $inserttoken = DB::getInstance()->updateByEmail('users', $email, array(
                    'token' => $token
                ));
            } else {
                $msg .= "Something went wrong please try again."; // if email is not sent.
            }
        } else {
            $msg .= 'Email address is not found.'; //if email address is not found 
        }
    } else {
        //loop through error messages is $msg.
        foreach ($validation->errors() as $error) {
            $msg .= $error . "<br>";
        }
    }
}
?>
<!DOCTYPE html>
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="no-js ie9" lang="en"> <![endif]-->
<html class="no-js" lang="en"> 

    <head>

        <meta charset="utf-8">
        <title>Manager :: Forgot Password</title>
        <meta name="author" content="" />   
        <meta name="description" content="" /> 
        <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1" />

        <!-- ( CSS LINKS START ) -->
        <!--<link href="favicon.ico" type="image/x-icon" rel="icon">
        <link href="favicon.ico" type="image/x-icon" rel="shortcut icon" >-->
        <link href="assets/css/default.css" type="text/css" rel="stylesheet" /> 
        <link href="assets/css/jquery.fancybox.css" type="text/css" rel="stylesheet" /> 
        <link href="assets/css/responsive.css" type="text/css" rel="stylesheet" /> 
        <!-- ( CSS LINKS END ) -->

        <!-- ( SCRIPT LIBRARY START ) -->
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.1.1.js"></script>  
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--  ( SCRIPT LIBRARY END )  --> 

    </head>

    <body>

        <!-- ( MAIN CONTAINER BEGIN ) -->
        <div id="mainContainer" class=" clearfix">

            <!-- ( CONTAINER 12 BEGIN ) -->
            <div class="container_12">

                <!-- ( CONTENT BOX BEGIN ) -->
                <div class="contentBox grid_4 marginAuto loginBox">

                    <header class="header">

                        <div class="logo">
                            <a href="login.html"><img src="assets/images/logo.gif" alt=""></a>
                        </div>
                        <!-- ( .logo end ) -->

                    </header>
                    <!-- ( .header end ) -->

                    <section class="content clearfix">
                        <p style="color: #e80702; font-size: 14; font-weight: bold;"><?php echo isset($msg) ? $msg : ''; ?></p>
                        <h1>Forgot Password</h1>
                        <form action="" name="loginForm" id="loginForm" method="post">

                            <div class="clearfix">
                                <label for="email">Enter your email address</label>
                                <input type="text" name="email" id="email" value="<?php echo Input::get('email'); ?>">
                            </div>
                            <a style="display: none;" href="#forgot" id="callfancy" class="fancybox redBtn">Login</a>
                            <a href="login.php" class="redBtn" style="margin-left: 10px;">Cancel</a>
                            <input type="submit" name="submit" id="submit" class="submitGreen" value="Submit" style="background-color:#008000">

                            <?php if (isset($flag)) { ?>
                                <script>
                                    $(document).ready(function () {
                                        $("#callfancy").click();
                                    });
                                </script>
                            <?php } ?>
                            <!-- ( POPUP CONTENT BEGIN ) -->
                            <div id="forgot" class="popupBox alignCenter">
                                <p><img src="assets/images/success.png" alt=""></p>
                                <h3>The new password is sent to the provided email address.</h3>
                                <a href="login.php" class="fancybox redBtn">OK</a>
                            </div>
                            <!-- ( POPUP CONTENT END ) -->

                        </form>

                    </section>
                    <!-- ( .content end ) -->

                </div>
                <!-- ( CONTENT BOX END ) -->

            </div>
            <!-- ( CONTAINER 12 END ) -->

        </div>
        <!-- ( MAIN CONTAINER BEGIN ) -->

        <!-- ( PLUGIN START ) -->
        <script type="text/javascript" src="assets/javascripts/jquery.fancybox.pack.js"></script> 
        <script type="text/javascript" src="assets/javascripts/configuration.js"></script>  
        <!-- ( PLUGIN END ) -->

    </body>

</html>