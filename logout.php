<?php

//Required config file
require_once 'core/init.php';

DB::getInstance();

// Destroy all session
if (Session::get('login')) {
    Session::delete('login');
    Session::delete('user_id');
    Session::delete('level');

    //Redirect::to('https://desktop.lexisnexis.com/share/page/site/sales-desktop/team_site');
    Redirect::to('index.php');
} else {
    Redirect::to('index.php');
}
?>