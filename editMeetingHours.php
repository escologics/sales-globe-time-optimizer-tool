<?php
session_start();
$page_title = 'Edit Activity Hours';
include('includes/top.php');
if (!Session::get('login')) {
    Redirect::to('index.php');
}
if (Session::get('level') != 1 && Session::get('level') != 2) {
    Redirect::to('index.php');
}
if (isset($_POST['delete'])) {
    if ($_POST['deleteid']) {
        $delete = DB::getInstance()->delete('meeting_detail', $_POST['deleteid']);
        $check_meeting = Meeting::checkMeetingDetailsByMeetingId($_POST['meeting_id']);
        if ($check_meeting < 1) {
            $delete_main = DB::getInstance()->delete('meeting', $_POST['meeting_id']);
        }
        if ($delete) {
            $sucess_msg = "Entry deleted successfully.";
        }
    }
}
$err_s = '';
if (isset($_POST['manager'])) {
    $start_date_manager = $_POST['start_date_manager'];
    $start_date_manager = date("Y-m-d", strtotime($start_date_manager));
    $end_date_manager = $_POST['end_date_manager'];
    $end_date_manager = date("Y-m-d", strtotime($end_date_manager));
    $manager_id = $_POST['manager_id'];
    $_SESSION['start_date_manager']= date("Y-m-d", strtotime($start_date_manager));
    $_SESSION['end_date_manager']= date("Y-m-d", strtotime($end_date_manager));
    $_SESSION['manager_id']=$_POST['manager_id'];
    if ($_POST['m_strat_month'] == "" || $_POST['m_strat_day'] == "" || $_POST['m_strat_year'] == "" || $_POST['m_end_month'] == "" || $_POST['m_end_day'] == "" || $_POST['m_end_year'] == "")
        $err_s = "Please select date range.<br>";
    if (empty($manager_id))
        $err_s .= "Please select Manager.<br>";
    if ($err_s != '') {
        $success_s = "<p style='color: #e80702; font-weight: bold;'>" . $err_s . "</p>";
    }
}
if (isset($_POST['representative'])) {


    $start_date_sales = $_POST['start_date_sales'];
    $start_date_sales = date("Y-m-d", strtotime($start_date_sales));
    $end_date_sales = $_POST['end_date_sales'];
    $end_date_sales = date("Y-m-d", strtotime($end_date_sales));
    $representative_id = $_POST['representative_id'];
    
    $_SESSION['start_date_sales'] = $_POST['start_date_sales'];
    $_SESSION['start_date_sales'] = date("Y-m-d", strtotime($start_date_sales));
    $_SESSION['end_date_sales'] = $_POST['end_date_sales'];
    $_SESSION['end_date_sales'] = date("Y-m-d", strtotime($end_date_sales));
    $_SESSION['representative_id'] = $_POST['representative_id'];

    if ($_POST['m_strat_month_s'] == "" || $_POST['m_strat_day_s'] == "" || $_POST['m_strat_year_s'] == "" || $_POST['m_end_month_s'] == "" || $_POST['m_end_day_s'] == "" || $_POST['m_end_year_s'] == "")
        $err_s = "Please select date range.<br>";
    if (empty($representative_id))
        $err_s .= "Please select Sales Representative.<br>";
    if ($err_s != '') {
        $success_s = "<p style='color: #e80702; font-weight: bold;'>" . $err_s . "</p>";
    }
}

if (isset($id) && !empty($id)) {
    $meetingDetail = Meeting::getMeetingDetailsById($id);
    if ($meetingDetail) {
        $created_by = Meeting::getUserIdbyMeetingId($meetingDetail['meeting_id']);
        $meeting_category_id = Meeting::getMeetingCategoryById($id);
        $activity_types = Meeting::getActivityTypeByMeetingCategoryId($meeting_category_id);
//        $account_types = Meeting::getAccountType();
//        $customer_status = Meeting::getCustomerStatus();
//        $product_service = Meeting::getProductOrService();
//        $contact_mode = Meeting::getContactMode();
        $max = count($activity_types);
    }
}
//echo 'Max: '. $max . 'Activity type: '.count($activity_types). ' Account types : '. count($account_types). ' Custmoer status: '.count($customer_status). ' Product service: '.count($product_service). ' contact mode: '.count($contact_mode);
$i = 0;
$max = '';
for ($i = 0; $i < $max; $i++) {
    $html .= '<tr>';
    if ($i < 1) {
        $html .= '<td><strong>' . Meeting::getMeetingCategoryNameById($meetingDetail['meeting_category']) . '</strong></td>
                  <td align="center"><input  type="text" value="' . $meetingDetail['hours'] . '" name="hours" onKeyUp="checkhoursMeeting();" id="hours"></td>';
    } elseif ($i < 2) {
        $html .= '<td rowspan="12" class="light"></td>
                  <td rowspan="12" align="center" class="light"></td>';
    }
    if (!empty($activity_types[$i]['name'])) {
        if ($meetingDetail['activity_type'] == $activity_types[$i]['id']) {
            $html .= '<td><input type="radio"  name="activity_type" checked  value="' . $activity_types[$i]['id'] . '"> <label for="">' . $activity_types[$i]['name'] . '</label></td>';
        } else {
            $html .= '<td><input type="radio"  name="activity_type"  value="' . $activity_types[$i]['id'] . '"> <label for="">' . $activity_types[$i]['name'] . '</label></td>';
        }
    } else {
        $html .= '<td></td>';
    }
    if (!empty($account_types[$i]['name'])) {
        if ($account_types[$i]['id'] == $meetingDetail['account_type']) {
            $html .= '<td><input type="radio" checked name="account_type" value="' . $account_types[$i]['id'] . '"> <label for="">' . $account_types[$i]['name'] . '</label></td>';
        } else {
            $html .= '<td><input type="radio" name="account_type" value="' . $account_types[$i]['id'] . '"> <label for="">' . $account_types[$i]['name'] . '</label></td>';
        }
    } else {
        $html .= '<td></td>';
    }
    if (!empty($customer_status[$i]['name'])) {
        if ($customer_status[$i]['id'] == $meetingDetail['customer_status']) {
            $html .= '<td><input type="radio" checked name="customer_status" value="' . $customer_status[$i]['id'] . '"> <label for="">' . $customer_status[$i]['name'] . '</label></td>';
        } else {
            $html .= '<td><input type="radio" name="customer_status" value="' . $customer_status[$i]['id'] . '"> <label for="">' . $customer_status[$i]['name'] . '</label></td>';
        }
    } else {
        $html .= '<td></td>';
    }
    if (!empty($product_service[$i]['name'])) {
        if ($product_service[$i]['id'] == $meetingDetail['product_service']) {
            $html .= '<td><input type="radio" checked name="product_service" value="' . $product_service[$i]['id'] . '"> <label for="">' . $product_service[$i]['name'] . '</label></td>';
        } else {
            $html .= '<td><input type="radio" name="product_service" value="' . $product_service[$i]['id'] . '"> <label for="">' . $product_service[$i]['name'] . '</label></td>';
        }
    } else {
        $html .= '<td></td>';
    }
    if (!empty($contact_mode[$i]['name'])) {
        if ($contact_mode[$i]['id'] == $meetingDetail['contact_mode']) {
            $html .= '<td><input type="radio" checked name="contact_mode" value="' . $contact_mode[$i]['id'] . '"> <label for="">' . $contact_mode[$i]['name'] . '</label></td>';
        } else {
            $html .= '<td><input type="radio"  name="contact_mode" value="' . $contact_mode[$i]['id'] . '"> <label for="">' . $contact_mode[$i]['name'] . '</label></td>';
        }
    } else {
        $html .= '<td></td>';
    }
    $html .= '</tr>';
}
include('includes/header.php');
//echo $_POST['representative'];
?>
<script>
    $(document).ready(function () {

        $("#calender_image_viewhistory1").datepicker({
            showOn: "both",
            buttonImage: "assets/images/calendar.jpg",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            //dateFormat: "yy-mm-dd",
            dateFormat: "mm/dd/yy",
            yearRange: "-3:+0",
            onSelect: function (selectedDate, obj) {
                var date_array = selectedDate.split("/");
                var month = date_array[0];
                var days = date_array[1];
                var year = date_array[2];
                $('#m_strat_month').val(month);
                $('#m_strat_day').val(days);
                $('#m_strat_year').val(year);
                $('#start_date_manager').val(selectedDate);
            }
        });

        $("#calender_image_viewhistory2").datepicker({
            showOn: "both",
            buttonImage: "assets/images/calendar.jpg",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            //dateFormat: "yy-mm-dd",
            dateFormat: "mm/dd/yy",
            yearRange: "-3:+0",
            onSelect: function (selectedDate, obj) {
                var date_array = selectedDate.split("/");
                var month = date_array[0];
                var days = date_array[1];
                var year = date_array[2];
                $('#m_end_month').val(month);
                $('#m_end_day').val(days);
                $('#m_end_year').val(year);
                $('#end_date_manager').val(selectedDate);
            }
        });

        if ($('.hideField_m').is(':checked')) {
            $('#days_select_m').attr('disabled', false);
            $('#start_date_manager').attr('disabled', true);
            $('#end_date_manager').attr('disabled', true);
            $('#m_strat_month').attr('disabled', true);
            $('#m_strat_day').attr('disabled', true);
            $('#m_strat_year').attr('disabled', true);
            $('#m_end_month').attr('disabled', true);
            $('#m_end_day').attr('disabled', true);
            $('#m_end_year').attr('disabled', true);
        } else {

            $('#days_select_m').attr('disabled', true);
            $('#start_date_manager').attr('disabled', false);
            $('#end_date_manager').attr('disabled', false);
            $('#m_strat_month').attr('disabled', false);
            $('#m_strat_day').attr('disabled', false);
            $('#m_strat_year').attr('disabled', false);
            $('#m_end_month').attr('disabled', false);
            $('#m_end_day').attr('disabled', false);
            $('#m_end_year').attr('disabled', false);

        }

        $(".hideField_m").on('click', function () {
            if ($(this).is(':checked')) {
                $('#days_select_m').attr('disabled', false);
                $('#start_date_manager').attr('disabled', true);
                $('#end_date_manager').attr('disabled', true);
                $('#m_strat_month').attr('disabled', true);
                $('#m_strat_day').attr('disabled', true);
                $('#m_strat_year').attr('disabled', true);
                $('#m_end_month').attr('disabled', true);
                $('#m_end_day').attr('disabled', true);
                $('#m_end_year').attr('disabled', true);
            } else {

                $('#days_select_m').attr('disabled', true);
                $('#start_date_manager').attr('disabled', false);
                $('#end_date_manager').attr('disabled', false);
                $('#m_strat_month').attr('disabled', false);
                $('#m_strat_day').attr('disabled', false);
                $('#m_strat_year').attr('disabled', false);
                $('#m_end_month').attr('disabled', false);
                $('#m_end_day').attr('disabled', false);
                $('#m_end_year').attr('disabled', false);

            }
        });



        $(".m_date").change(function () {
            if ($(this).attr('id') == 'm_strat_month') {

                if ($(this).val() > 12) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }
            } else if ($(this).attr('id') == 'm_strat_day') {

                if ($(this).val() > daysInMonth($('#m_strat_month').val(), $('#m_strat_year').val())) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }

            } else if ($(this).attr('id') == 'm_strat_year') {

                if ($(this).val() > 2015) {
                    $(this).val('2015');
                } else if ($(this).val() < 2012) {
                    $(this).val('2015');
                }

            } else if ($(this).attr('id') == 'm_end_month') {

                if ($(this).val() > 12) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }

            } else if ($(this).attr('id') == 'm_end_day') {

                if ($(this).val() > daysInMonth($('#m_end_month').val(), $('#m_end_year').val())) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }

            } else if ($(this).attr('id') == 'm_end_year') {

                if ($(this).val() > 2015) {
                    $(this).val('2015');
                } else if ($(this).val() < 2012) {
                    $(this).val('2015');
                }

            }

            var m_strat_date = ($('#m_strat_month').val() + '/' + $('#m_strat_day').val() + '/' + $('#m_strat_year').val());
            $('#start_date_manager').val(m_strat_date);

            var m_end_date = ($('#m_end_month').val() + '/' + $('#m_end_day').val() + '/' + $('#m_end_year').val());
            $('#end_date_manager').val(m_end_date);

        });



        $("#calender_image_viewhistory1_s").datepicker({
            showOn: "both",
            buttonImage: "assets/images/calendar.jpg",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            //dateFormat: "yy-mm-dd",
            dateFormat: "mm/dd/yy",
            yearRange: "-3:+0",
            onSelect: function (selectedDate, obj) {
                var date_array = selectedDate.split("/");
                var month = date_array[0];
                var days = date_array[1];
                var year = date_array[2];
                $('#m_strat_month_s').val(month);
                $('#m_strat_day_s').val(days);
                $('#m_strat_year_s').val(year);
                $('#start_date_sales').val(selectedDate);
            }
        });

        $("#calender_image_viewhistory2_s").datepicker({
            showOn: "both",
            buttonImage: "assets/images/calendar.jpg",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            //dateFormat: "yy-mm-dd",
            dateFormat: "mm/dd/yy",
            yearRange: "-3:+0",
            onSelect: function (selectedDate, obj) {
                var date_array = selectedDate.split("/");
                var month = date_array[0];
                var days = date_array[1];
                var year = date_array[2];
                $('#m_end_month_s').val(month);
                $('#m_end_day_s').val(days);
                $('#m_end_year_s').val(year);
                $('#end_date_sales').val(selectedDate);
            }
        });

        if ($('.hideField_s').is(':checked')) {
            $('#days_select_s').attr('disabled', false);
            $('#start_date_sales').attr('disabled', true);
            $('#end_date_sales').attr('disabled', true);
            $('#m_strat_month_s').attr('disabled', true);
            $('#m_strat_day_s').attr('disabled', true);
            $('#m_strat_year_s').attr('disabled', true);
            $('#m_end_month_s').attr('disabled', true);
            $('#m_end_day_s').attr('disabled', true);
            $('#m_end_year_s').attr('disabled', true);
        } else {

            $('#days_select_s').attr('disabled', true);
            $('#start_date_sales').attr('disabled', false);
            $('#end_date_sales').attr('disabled', false);
            $('#m_strat_month_s').attr('disabled', false);
            $('#m_strat_day_s').attr('disabled', false);
            $('#m_strat_year_s').attr('disabled', false);
            $('#m_end_month_s').attr('disabled', false);
            $('#m_end_day_s').attr('disabled', false);
            $('#m_end_year_s').attr('disabled', false);

        }

        $(".hideField_s").on('click', function () {
            if ($(this).is(':checked')) {
                $('#days_select_s').attr('disabled', false);
                $('#start_date_sales').attr('disabled', true);
                $('#end_date_sales').attr('disabled', true);
                $('#m_strat_month_s').attr('disabled', true);
                $('#m_strat_day_s').attr('disabled', true);
                $('#m_strat_year_s').attr('disabled', true);
                $('#m_end_month_s').attr('disabled', true);
                $('#m_end_day_s').attr('disabled', true);
                $('#m_end_year_s').attr('disabled', true);
            } else {

                $('#days_select_s').attr('disabled', true);
                $('#start_date_sales').attr('disabled', false);
                $('#end_date_sales').attr('disabled', false);
                $('#m_strat_month_s').attr('disabled', false);
                $('#m_strat_day_s').attr('disabled', false);
                $('#m_strat_year_s').attr('disabled', false);
                $('#m_end_month_s').attr('disabled', false);
                $('#m_end_day_s').attr('disabled', false);
                $('#m_end_year_s').attr('disabled', false);

            }
        });



        $(".m_date_s").change(function () {
            if ($(this).attr('id') == 'm_strat_month_s') {

                if ($(this).val() > 12) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }
            } else if ($(this).attr('id') == 'm_strat_day_s') {

                if ($(this).val() > daysInMonth($('#m_strat_month_s').val(), $('#m_strat_year_s').val())) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }

            } else if ($(this).attr('id') == 'm_strat_year_s') {

                if ($(this).val() > 2015) {
                    $(this).val('2015');
                } else if ($(this).val() < 2012) {
                    $(this).val('2015');
                }

            } else if ($(this).attr('id') == 'm_end_month_s') {

                if ($(this).val() > 12) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }

            } else if ($(this).attr('id') == 'm_end_day_s') {

                if ($(this).val() > daysInMonth($('#m_end_month_s').val(), $('#m_end_year_s').val())) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }

            } else if ($(this).attr('id') == 'm_end_year_s') {

                if ($(this).val() > 2015) {
                    $(this).val('2015');
                } else if ($(this).val() < 2012) {
                    $(this).val('2015');
                }

            }

            var m_strat_date_s = ($('#m_strat_month_s').val() + '/' + $('#m_strat_day_s').val() + '/' + $('#m_strat_year_s').val());
            $('#start_date_sales').val(m_strat_date_s);

            var m_end_date_s = ($('#m_end_month_s').val() + '/' + $('#m_end_day_s').val() + '/' + $('#m_end_year_s').val());
            $('#end_date_sales').val(m_end_date_s);

        });
    });
    function daysInMonth(month, year) {
        return new Date(year, month, 0).getDate();
    }
</script>
<style>
    img.ui-datepicker-trigger {
        margin: -5px 0px 0px 0px;
    }
</style>
<section class="createManager clearfix tableSec">
    <div class="tabs createTab">
        <ul class="clearfix tabsNavigation">
            <li><a href="#tab1" class="
                <?php echo ($_POST['manager'] == 'Go') ? 'unique' : ''; ?><?php //echo (!$_POST) ? 'unique' : ''  ?><?php echo ($_GET['level'] == 'manager') ? ' unique' : ''; ?>
                   ">Manager Activity Hours</a></li>
            <li><a href="#tab2" <?php
                if ($_POST['manager']) {
                    
                } else {
                    ?> class="<?php echo ($_POST['representative'] == 'Go') ? 'unique' : ''; ?><?php echo ($_GET['level'] == 'representative') ? ' unique' : ''; ?>" <?php } ?> >Sales Representative Activity Hours</a></li>
        </ul><!-- ( TABS LINK END ) -->
        <div class="tabs_container clearfix">


            <div id="tab1" class="<?php echo ($_POST['manager']) ? 'unique' : ''; ?><?php //echo (!$_POST) ? 'unique' : ''                    ?><?php echo ($_GET['level'] == 'manager') ? 'unique' : ''; ?>">
                <div class="filterBox clearfix">
                    <h2>Enter Date(s) of Activity to Edit</h2>    
                    <form action="<?php echo $site_url; ?>editMeetingHours.php?level=manager" name="ViewUnsavedForm" id="ViewUnsavedForm" method="POST">
                        <?php if (!empty($sucess_msg)&& $_GET['level']=="manager") {
                            ?>
                            <p style="color:green; font-weight: bold;"><?php echo $sucess_msg; ?></p>
                        <?php } ?>
                        <div class="grid_7 dateArea clearfix marginNone">                     
<!--                            <p class="dateIcon customeDate">
                                <input type="text" name="start_date_manager" id="start_date_manager" class="datepicker" placeholder="Start Date">

                            </p>
                            <label class="customeDateLabel">to</label>
                            <p class="dateIcon customeDate">
                                <input type="text" name="end_date_manager" id="end_date_manager" class="datepicker" placeholder="End Date">
                            </p>-->
                            <p class="dateIcon customeDate" style="width: 160px;">                   
                                <input type="number" name="m_strat_month" id="m_strat_month" class="m_date" placeholder="mm" value="<?php echo $_POST['m_strat_month']; ?>" style="width: 38px;">
                                <input type="number" name="m_strat_day" id="m_strat_day" class="m_date" placeholder="dd" value="<?php echo $_POST['m_strat_day']; ?>" style="width: 30px;">
                                <input type="number" name="m_strat_year" id="m_strat_year" class="m_date" placeholder="yy" value="<?php echo $_POST['m_strat_year'] != "" ? $_POST['m_strat_year'] : '2015'; ?>" style="width: 47px;">
                                <input type="hidden" name="start_date_manager" id="start_date_manager" value="<?php echo $_POST['start_date_manager']; ?>">
                                <input type="hidden" id="calender_image_viewhistory1"  class="datepicker_view_my_history"/>
                                <?php //echo $totalHours;  ?>
                            </p>
                            <label class="customeDateLabel">to</label>
                            <p class="dateIcon customeDate" style="width: 160px;">
                                <input type="number" name="m_end_month" id="m_end_month" class="m_date" placeholder="mm" value="<?php echo $_POST['m_end_month']; ?>" style="width: 38px;">
                                <input type="number" name="m_end_day" id="m_end_day" class="m_date" placeholder="dd" value="<?php echo $_POST['m_end_day']; ?>" style="width: 30px;">
                                <input type="number" name="m_end_year" id="m_end_year" class="m_date" placeholder="yy" value="<?php echo $_POST['m_end_year'] != "" ? $_POST['m_end_year'] : '2015'; ?>" style="width: 47px;">                    
                                <input type="hidden" name="end_date_manager" id="end_date_manager" value="<?php echo $_POST['end_date_manager']; ?>">
                                <input type="hidden" id="calender_image_viewhistory2"  class="datepicker_view_myhistory"/>
                            </p>

<!--                            <input type="text" name="start_date_manager_2" id="start_date_manager_2" placeholder="Start Date">-->
                            <select name="manager_id" id="manager_id">
                                <option value="">Select Manager</option>
                                <?php
                                $results = User::getAllManagers();
                                foreach ($results as $result) {
                                    ?>
                                    <option  value="<?php echo $result['id']; ?>" <?php echo ($_POST['manager_id']) == $result['id'] ? 'selected="selected"' : ''; ?>><?php echo $result['first_name'] . ' ' . $result['last_name']; ?></option>
                                <?php } ?>
                            </select>
                            <input type="submit"  name="manager" value="Go" class="redBtn">   
                            <a href="adminDashboard.php" class="redBtn" style="padding: 13px 12px 11px; vertical-align:top;">Cancel</a>
                        </div>
                    </form>   
                </div>
                <!-- ( .filterBox end ) -->
                <?php if ($_POST['manager'] && $_POST['start_date_manager'] != '' && $_POST['end_date_manager'] != '' && $_POST['manager_id'] != ''  || (!empty($sucess_msg) && ($_GET['level']=="representative"))) { ?>
                    <?php

                     if(!empty($sucess_msg)){

                     $start_date_manager = $_SESSION['start_date_manager'];
                     $end_date_manager = $_SESSION['end_date_manager'];
                     $manager_id=$_SESSION['manager_id'];
                    }
                    $meetings = Meeting::getMeetingsByid($start_date_manager, $end_date_manager, $manager_id);
                    if ($meetings) {
                        foreach ($meetings as $meeting) {
                            ?>
                            <div class="archivePost">
                                <div class="archiveTilte">
                                    <?php $username = User::getFullNameByUserId($manager_id); ?>
                                    <h2><?php echo $username . ' ' . '-' . ' ' . date("F, d Y", strtotime($meeting['added_date'])); ?></h2>
                                </div><!-- ( .archiveTilte end ) -->
                                <div class="archiveInfo">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th align="left" width="5%">Sno.</th>
                                                <th align="left">Category</th>
                                                <th align="left">Sub Activity </th>
                                                <th align="left">Hours & Minutes </th>
                                                <th align="left">&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $meeting_details = Meeting::getMeetingDetailsByMeetingId($meeting['id']);
                                            if ($meeting_details) {
                                                $sr_no = 1;
                                                foreach ($meeting_details as $meeting_detail) {
                                                    ?>  
                                                    <tr>
                                                        <td><?php echo $sr_no; ?></td>
                                                        <td><?php echo Meeting::getMeetingCategoryNameById($meeting_detail['meeting_category']); ?></td>
                                                        <td><?php echo Meeting::getActivitynameByMeetingCategoryId($meeting_detail['activity_type']); ?></td>
                                                        <td><?php echo m2h("i:s", $meeting_detail['hours']); ?></td>
                                                        <td align="center" width="8%"  class="tableBtn">
                                                            <a href="EditUnsavedDay.php?id=<?php echo $meeting_detail['id']; ?>"><img src="assets/images/settingRed.jpg" alt=""></a>
                                                            <a href="#delete_<?php echo $meeting_detail['id']; ?>"  class="fancybox"><img src="assets/images/closeRed.jpg" alt=""></a>
                                                            <div id="delete_<?php echo $meeting_detail['id']; ?>" class="popupBox alignCenter">
                                                                <form action="" method="post">
                                                                    <p><img src="assets/images/success.png" alt=""></p>
                                                                    <h3>Are you sure you want to delete this activity?</h3>
                                                                    <input type="hidden" value="<?php echo $meeting_detail['id']; ?>" name="deleteid">
                                                                    <input type="hidden" value="<?php echo $meeting_detail['meeting_id']; ?>" name="meeting_id" />
                                                                    <input type="submit" name="delete" value="Yes">
                                                                    <input type="button" class="closeBtn" name="no" value="No">
                                                                </form>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $sr_no++;
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div><!-- ( .archiveInfo end  ) -->
                            </div><!-- ( .archivePost end ) -->
                            <?php
                        }
                    } else {
                        echo 'No Data Found...';
                    }
                    ?>      
                <?php } ?>
                <!-- ( .tableSec end ) -->
            </div><!-- ( TAB 1 END ) -->



            <div id="tab2" class="<?php echo ($_POST['representative']) ? 'unique' : ''; ?><?php echo ($_GET['level'] == 'representative') ? 'unique' : ''; ?>">
                <div class="filterBox clearfix">
                    <h2>Enter Date(s) of Activity to Edit</h2>    
                    <form action="<?php echo $site_url; ?>editMeetingHours.php?level=representative" name="ViewUnsavedForm" id="ViewUnsavedForm" method="POST">
                        <?php if (!empty($sucess_msg) && $_GET['level']=="representative") {
                            ?>
                            <p style="color:green; font-weight: bold;"><?php echo $sucess_msg; ?></p>
                        <?php } ?>
                        <div class="grid_7 dateArea clearfix marginNone">
<!--                            <p class="dateIcon customeDate">
                                <input type="text" name="start_date_sales" id="start_date_sales" class="datepicker" placeholder="Start Date">
                            </p>
                            <label class="customeDateLabel">to</label>
                            <p class="dateIcon customeDate">
                                <input type="text" name="end_date_sales" id="end_date_sales" class="datepicker" placeholder="End Date">
                            </p>-->
                            <p class="dateIcon" style="width: 160px;">                   
                                <input type="number" name="m_strat_month_s" id="m_strat_month_s" class="m_date_s" placeholder="mm" value="<?php echo $_POST['m_strat_month_s']; ?>" style="width: 38px;">
                                <input type="number" name="m_strat_day_s" id="m_strat_day_s" class="m_date_s" placeholder="dd" value="<?php echo $_POST['m_strat_day_s']; ?>" style="width: 30px;">
                                <input type="number" name="m_strat_year_s" id="m_strat_year_s" class="m_date_s" placeholder="yy" value="<?php echo $_POST['m_strat_year_s'] != "" ? $_POST['m_strat_year_s'] : '2015'; ?>" style="width: 47px;">
                                <input type="hidden" name="start_date_sales" id="start_date_sales" value="<?php echo $_POST['start_date_sales']; ?>">
                                <input type="hidden" id="calender_image_viewhistory1_s"  class="datepicker_view_my_history_s"/>
                                <?php //echo $totalHours;  ?>
                            </p>
                            <label>to</label>
                            <p class="dateIcon" style="width: 160px;">
                                <input type="number" name="m_end_month_s" id="m_end_month_s" class="m_date_s" placeholder="mm" value="<?php echo $_POST['m_end_month_s']; ?>" style="width: 38px;">
                                <input type="number" name="m_end_day_s" id="m_end_day_s" class="m_date_s" placeholder="dd" value="<?php echo $_POST['m_end_day_s']; ?>" style="width: 30px;">
                                <input type="number" name="m_end_year_s" id="m_end_year_s" class="m_date_s" placeholder="yy" value="<?php echo $_POST['m_end_year_s'] != "" ? $_POST['m_end_year_s'] : '2015'; ?>" style="width: 47px;">                    
                                <input type="hidden" name="end_date_sales" id="end_date_sales"  value="<?php echo $_POST['end_date_sales']; ?>">
                                <input type="hidden" id="calender_image_viewhistory2_s"  class="datepicker_view_myhistory_s"/>
                            </p>
                            <select name="representative_id" id="representative_id" style="margin-bottom:10px;">
                                <option value="">Select Sales Representative</option>
                                <?php
                                $results = User::getAllSalesRepresentative();
                                foreach ($results as $result) {
                                    ?>
                                    <option value="<?php echo $result['id']; ?>" <?php echo ($_POST['representative_id']) == $result['id'] ? 'selected="selected"' : ''; ?>><?php echo $result['first_name'] . ' ' . $result['last_name']; ?></option>
                                <?php } ?>
                            </select>
                            <input type="submit"  name="representative" value="Go" class="redBtn">
                            <a href="adminDashboard.php" class="redBtn" style="padding: 13px 12px 11px; vertical-align:top;">Cancel</a>
                        </div>
                    </form>   
                </div>
                <!-- ( .filterBox end ) -->
                <?php if ($_POST['representative'] && $_POST['start_date_sales'] != '' && $_POST['end_date_sales'] != '' && $_POST['representative_id'] != '' || (!empty($sucess_msg) && ($_GET['level']=="representative"))) { ?>
                    <?php
                    if(!empty($sucess_msg)){

                     $start_date_sales = $_SESSION['start_date_sales'];
                     $end_date_sales = $_SESSION['end_date_sales'];
                     $representative_id=$_SESSION['representative_id'];
                    }
                    $meetings = Meeting::getMeetingsByid($start_date_sales, $end_date_sales, $representative_id);
                    if ($meetings) {
                        foreach ($meetings as $meeting) {
                            ?>
                            <div class="archivePost">
                                <div class="archiveTilte">
                                    <?php $username = User::getFullNameByUserId($representative_id); ?>
                                    <h2><?php echo $username . ' ' . '-' . ' ' . date("F d, Y", strtotime($meeting['added_date'])); ?></h2>
                                </div><!-- ( .archiveTilte end ) -->
                                <div class="archiveInfo">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th align="left" width="5%">Sno.</th>
                                                <th align="left">Category</th>
                                                <th align="left">Sub - Category</th>
                                                <th align="left">Hours & Minutes</th>
                                                <th align="left">&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $meeting_details = Meeting::getMeetingDetailsByMeetingId($meeting['id']);
                                            if ($meeting_details) {
                                                $sr_no = 1;
                                                foreach ($meeting_details as $meeting_detail) {
                                                    ?>  
                                                    <tr>
                                                        <td><?php echo $sr_no; ?></td>
                                                        <td><?php echo Meeting::getMeetingCategoryNameById($meeting_detail['meeting_category']); ?></td>
                                                        <td><?php echo Meeting::getActivitynameByMeetingCategoryId($meeting_detail['activity_type']); ?></td>
                                                        <td><?php echo m2h("i:s", $meeting_detail['hours']); ?></td>
                                                        <td align="center" width="8%"  class="tableBtn">
                                                            <a href="EditUnsavedDay.php?id=<?php echo $meeting_detail['id']; ?>"><img src="assets/images/settingRed.jpg" alt=""></a>
                                                            <a href="#delete_<?php echo $meeting_detail['id']; ?>"  class="fancybox"><img src="assets/images/closeRed.jpg" alt=""></a>
                                                            <div id="delete_<?php echo $meeting_detail['id']; ?>" class="popupBox alignCenter">
                                                                <form action="" method="post">
                                                                    <p><img src="assets/images/success.png" alt=""></p>
                                                                    <h3>Are you sure you want to delete this activity? </h3>
                                                                    <input type="hidden" value="<?php echo $meeting_detail['id']; ?>" name="deleteid">
                                                                    <input type="hidden" value="<?php echo $meeting_detail['meeting_id']; ?>" name="meeting_id" />
                                                                    <input type="submit" name="delete" value="Yes">
                                                                    <input type="button" class="closeBtn" name="no" value="No">
                                                                </form>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $sr_no++;
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div><!-- ( .archiveInfo end  ) -->
                            </div><!-- ( .archivePost end ) -->
                            <?php
                        }
                    } else {
                        echo 'No Data Found';
                    }
                    ?>     
                    <!-- ( .tableSec end ) -->
                <?php } ?>
            </div><!-- ( TAB 1 END ) -->

        </div>
        <!-- ( TABS CONTAINER END ) -->
    </div>
    <!-- ( TABS END ) -->
</section>        
<?php include('includes/footer.php'); ?>                 
<script>
    $(document).ready(function () {

        var meeting_detail_id = <?php echo $id; ?>;
        $.ajax({
            type: "POST",
            cache: false,
            url: "gethourtotal.php",
            data: {
                meeting_detail_id: meeting_detail_id
            }
        }).done(function (data) {
            if (data != 'error') {
                $('#totalhours').text(data);
            }
        });
    });
    $('#updateMeeting').click(function () {
        var hours = $("#hours").val();
        var activity_type = $("input[name='activity_type']:checked").val();
        var account_type = $("input[name='account_type']:checked").val();
        var customer_status = $("input[name='customer_status']:checked").val();
        var product_service = $("input[name='product_service']:checked").val();
        var contact_mode = $("input[name='contact_mode']:checked").val();
        var meeting_detail_id = <?php echo $id; ?>;
        if (hours == "" || hours == 0) {
            fancyAlert("Please insert some hours.");
            return false;
        }
        $.ajax({
            type: "POST",
            cache: false,
            url: "updateMeetingDetail.php",
            data: {
                hours: hours,
                activity_type: activity_type,
                account_type: account_type,
                customer_status: customer_status,
                product_service: product_service,
                contact_mode: contact_mode,
                meeting_detail_id: meeting_detail_id
            }
        }).success(function (data) {
            window.location.href = '<?php echo $site_url ?>adminDashboard.php';
        });
        return false;
    });
    function checkhoursMeeting() {
        var totalhours = $('#totalhours').text();
        var hours = $('#hours').val();
        var total = parseFloat(totalhours) + parseFloat(hours);
        if (isNaN($('#hours').val())) {
            $('#hours').val('');
        }
        total = parseFloat(total);
        if (total > 24) {
            fancyAlert('You can not add above 24 hours');
            $('#hours').val('');
        }
    }
</script>

<script>
    $(document).ready(function () {
        $("#end_date_manager").change(function () {
            var start_date = $('#start_date_manager').val();
            var end_date = $('#end_date_manager').val();
            if (end_date < start_date) {
                fancyAlert('Please select valid date range');
                $('#end_date_manager').val('');
            }
        });
        $("#end_date_sales").change(function () {
            var start_date = $('#start_date_sales').val();
            var end_date = $('#end_date_sales').val();
            if (end_date < start_date) {
                fancyAlert('Please select valid date range');
                $('#end_date_sales').val('');
            }
        });
<?php if (isset($_POST['start_date_manager'])) {
    ?>
            $("#start_date_manager").datepicker("setDate", "<?php echo $_POST['start_date_manager']; ?>");
    <?php
} else {
    ?>
            $("#start_date_manager").datepicker("setDate", "");
<?php } ?>
<?php if (isset($_POST['end_date_manager'])) {
    ?>
            $("#end_date_manager").datepicker("setDate", "<?php echo $_POST['end_date_manager']; ?>");
    <?php
} else {
    ?>
            $("#end_date_manager").datepicker("setDate", "");
<?php } ?>
<?php if (isset($_POST['start_date_sales'])) {
    ?>
            $("#start_date_sales").datepicker("setDate", "<?php echo $_POST['start_date_sales']; ?>");
    <?php
} else {
    ?>
            $("#start_date_sales").datepicker("setDate", "");
<?php } ?>
<?php if (isset($_POST['end_date_sales'])) {
    ?>
            $("#end_date_sales").datepicker("setDate", "<?php echo $_POST['end_date_sales']; ?>");
    <?php
} else {
    ?>
            $("#end_date_sales").datepicker("setDate", "");
<?php } ?>
    });
</script> 
<script type="text/javascript"> // FancyBox close event on button 
    $('.closeBtn').click(function () {
        $.fancybox.close();
    });
</script>