<?php

include('includes/top.php');

$meeting_date = $_POST['meeting_date'];
$meeting_date = date("Y-m-d", strtotime($meeting_date));
//$meeting_category = $_POST['meeting_category'];
$user_id = $_POST['user_id'];
$checkAddanotheractivity = $_POST['checkAddanotheractivity'];

$data_array = $_POST['data_array'];

//echo '<pre>';
//print_r($data_array);


$count = Meeting::checkMeetingByDate($meeting_date, $user_id);

if ($count < 1) {

    $meeting = DB::getInstance()->insert('meeting', array(
        'added_date' => $meeting_date,
        'status' => '1',
        'created_by' => $user_id
    ));
    $id_meeting = $meeting;
    if ($meeting) {
        foreach ($data_array as $data) {
            $houra_array = explode(':', $data['hours']);
//            $hour = $houra_array[0];
//            $minutes = $houra_array[1];
//            $time = convert($hour, $minutes);
            $time = $data['hours'];

            $meetingDetail = DB::getInstance()->insert('meeting_detail', array(
                'meeting_category' => $data['activity_category'],
                'hours' => $time,
                'activity_type' => $data['activity_type'],
                'activity_rate' => $data['activity_rate'],
                'activity_comment' => $data['activity_comment'],
                'added_date' => $meeting_date,
                'meeting_id' => $id_meeting,
                'created_byuser' => $user_id
            ));
        }
    }
} elseif ($count > 0) {

    if ($checkAddanotheractivity == 0) {
        $meetingStatus = Meeting::checkMeetingStatusByDateAndUserId($meeting_date, $user_id);
        $meeting_id = Meeting::getMeetingIdByDateAndUserId($meeting_date, $user_id);

        $selectedDate = $_POST['meeting_date'];
        $selectedDateformat = date("Y-m-d", strtotime($selectedDate));
        $totalHours = Meeting::getTotalTimeByDateAndUserId($selectedDateformat, $user_id);

        if ($totalHours >= 1440) {
            echo 'error';
        } else {
           // DB::getInstance()->deleteBatch('meeting_detail', 'meeting_id', $meeting_id);
            foreach ($data_array as $data) {

                $houra_array = explode(':', $data['hours']);
//            $hour = $houra_array[0];
//            $minutes = $houra_array[1];
//            $time = convert($hour, $minutes);
                $time = $data['hours'];

                $meetingDetail = DB::getInstance()->insert('meeting_detail', array(
                    'meeting_category' => $data['activity_category'],
                    'hours' => $time,
                    'activity_type' => $data['activity_type'],
                    'activity_rate' => $data['activity_rate'],
                    'activity_comment' => $data['activity_comment'],
                    'added_date' => $meeting_date,
                    'meeting_id' => $meeting_id,
                    'created_byuser' => $user_id
                ));
            }
        }
    } else {
        $meetingStatus = Meeting::checkMeetingStatusByDateAndUserId($meeting_date, $user_id);
        $meeting_id = Meeting::getMeetingIdByDateAndUserId($meeting_date, $user_id);

        $selectedDate = $_POST['meeting_date'];
        $selectedDateformat = date("Y-m-d", strtotime($selectedDate));
        $totalHours = Meeting::getTotalTimeByDateAndUserId($selectedDateformat, $user_id);

        if ($totalHours >= 1440) {
            echo 'error';
        } else {
            DB::getInstance()->deleteBatch('meeting_detail', 'meeting_id', $meeting_id);
            foreach ($data_array as $data) {

                $houra_array = explode(':', $data['hours']);
//            $hour = $houra_array[0];
//            $minutes = $houra_array[1];
//            $time = convert($hour, $minutes);
                $time = $data['hours'];

                $meetingDetail = DB::getInstance()->insert('meeting_detail', array(
                    'meeting_category' => $data['activity_category'],
                    'hours' => $time,
                    'activity_type' => $data['activity_type'],
                    'activity_rate' => $data['activity_rate'],
                    'activity_comment' => $data['activity_comment'],
                    'added_date' => $meeting_date,
                    'meeting_id' => $meeting_id,
                    'created_byuser' => $user_id
                ));
            }
        }
    }
}
?>
