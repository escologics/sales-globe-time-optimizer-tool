<?php
$page_title = 'Edit User';
include('includes/top.php');
if (!Session::get('login')) {
    Redirect::to('index.php');
}
include('includes/header.php');
if (isset($_GET['id']) && !empty($_GET['id']) && isset($_GET['level']) && !empty($_GET['level'])) {
    $user = User::getUserByid($_GET['id'], $_GET['level']);
    unset($user['password']);
} else {
    Redirect::to('viewUsers.php');
}

if (Input::exists()) {
    if ($_POST['user_type'] == 'manager') {
        $err = '';
        $success = '';
        $validate = new Validate();
        $rule = array(
            'first_name' => array(
                'label' => 'First Name',
                'required' => true,
                'validate' => 'name',
                'min' => '3',
                'max' => '20',
            ),
            'last_name' => array(
                'label' => 'Last Name',
                'required' => true,
            ),
            'username' => array(
                'label' => 'Username',
                'required' => true,
                'validate' => 'username',
            ),
            'email' => array(
                'label' => 'Email',
                'required' => true,
                'validate' => 'email',
            ),
            'sales_team' => array(
                'label' => 'Sales Team',
                'is_selected' => true,
            )
        );
        if ($_POST['password'] != '' || $_POST['c_password'] != '') {
            $rule = array_merge($rule, array(
                'password' => array(
                    'label' => 'Password',
                    'required' => true,
                    'min' => '6',
                    'max' => '20',
                ),
                'c_password' => array(
                    'label' => "Confirm Password",
                    'required' => true,
                    'matches' => 'password',
                )
            ));
        }
        $validation = $validate->check($_POST, $rule);
        if (User::checkByUsername(Input::post('username'), $user['id']) != 0) {
            $err .= 'Username is already exists.<br />';
        }
        if (User::checkByEmail(Input::post('email'), $user['id']) != 0) {
            $err .= "Email address is already exists.<br />";
        }
        if ($validation->passed() && $err == '') {
            $rule_2 = array(
                'first_name' => Input::post('first_name'),
                'last_name' => Input::post('last_name'),
                'username' => Input::post('username'),
                'email' => Input::post('email'),
            );
            if ($_POST['password'] != '' || $_POST['c_password'] != '') {
                $rule_2 = array_merge($rule_2, array(
                    'password' => md5(Input::post('password'))));
            }
            DB::getInstance()->update('users', $user['id'], $rule_2);
            $data_rotation_ids = $_POST['add_rotation'];
            if (!empty($data_rotation_ids)) {
                DB::getInstance()->deleteBatch('rotations_users', 'roation_user_id', $user['id']);
                foreach ($data_rotation_ids as $data_rotation_id) {
                    DB::getInstance()->insert('rotations_users', array(
                        'rotation_id' => $data_rotation_id,
                        'user_level' => 3,
                        'roation_user_id' => $user['id']
                    ));
                }
            } else {
                DB::getInstance()->deleteBatch('rotations_users', 'roation_user_id', $user['id']);
            }
            $user_sales_teams_ids = $_POST['sales_team'];
            if (!empty($user_sales_teams_ids)) {
                DB::getInstance()->deleteBatch('user_sales_team', 'user_id', $user['id']);
                foreach ($user_sales_teams_ids as $user_sales_teams_id) {
                    DB::getInstance()->insert('user_sales_team', array(
                        'sales_team_id' => $user_sales_teams_id,
                        'user_id' => $user['id']
                    ));
                }
            } else {
                DB::getInstance()->deleteBatch('user_sales_team', 'user_id', $user['id']);
            }
            $success = "Manager updated successfully.";
            Redirect::to('viewUsers.php');
        } else {
            foreach ($validation->errors() as $error) {
                $err .= $error . "<br>";
            }
        }
    }

    if ($_POST['user_type'] == 'salesrep') {
        $err = '';
        $success = '';
        $validate = new Validate();
        $rule = array(
            'first_name' => array(
                'label' => 'First Name',
                'required' => true,
                'validate' => 'name',
                'min' => '3',
                'max' => '20',
            ),
            'last_name' => array(
                'label' => 'Last Name',
                'required' => true,
            ),
            'username' => array(
                'label' => 'Username',
                'required' => true,
                'validate' => 'username',
            ),
            'email' => array(
                'label' => 'Email',
                'required' => true,
                'validate' => 'email',
            ),
            'industry_sector' => array(
                'label' => 'Sales Team',
                'required' => true,
            ),
            'assigned_to' => array(
                'label' => 'Assign Manager',
                'required' => true
            ),
            'designation' => array(
                'label' => 'Assign Designation',
                'required' => true
            )
        );
        if ($_POST['password'] != '' || $_POST['c_password'] != '') {
            $rule = array_merge($rule, array(
                'password' => array(
                    'label' => 'Password',
                    'required' => true,
                    'min' => '6',
                    'max' => '20',
                ),
                'c_password' => array(
                    'label' => "Confirm Password",
                    'required' => true,
                    'matches' => 'password',
                )
            ));
        }
        $validation = $validate->check($_POST, $rule);
        if (User::checkByUsername(Input::post('username'), $user['id']) != 0) {
            $err .= 'Username is already exists.<br />';
        }
        if (User::checkByEmail(Input::post('email'), $user['id']) != 0) {
            $err .= "Email address is already exists.<br />";
        }
        if ($validation->passed() && $err == '') {
            $rule_2 = array(
                'first_name' => Input::post('first_name'),
                'last_name' => Input::post('last_name'),
                'username' => Input::post('username'),
                'email' => Input::post('email'),
                'industry_id' => Input::post('industry_sector'),
                'assigned_to' => Input::post('assigned_to'),
                'designation' => Input::post('designation'),
            );
            if ($_POST['password'] != '' || $_POST['c_password'] != '') {
                $rule_2 = array_merge($rule_2, array(
                    'password' => md5(Input::post('password'))));
            }
            DB::getInstance()->update('users', $user['id'], $rule_2);
            $data_rotation_ids = $_POST['add_rotation'];
            if (!empty($data_rotation_ids)) {
                DB::getInstance()->deleteBatch('rotations_users', 'roation_user_id', $user['id']);
                foreach ($data_rotation_ids as $data_rotation_id) {
                    DB::getInstance()->insert('rotations_users', array(
                        'rotation_id' => $data_rotation_id,
                        'user_level' => 4,
                        'roation_user_id' => $user['id']
                    ));
                }
            } else {
                DB::getInstance()->deleteBatch('rotations_users', 'roation_user_id', $user['id']);
            }
            $success = "Sales Representative updated successfully.";
            Redirect::to('viewUsers.php');
        } else {
            foreach ($validation->errors() as $error) {
                $err .= $error . "<br>";
            }
        }
    }
}
?>
<style>
    .container_rotation { border:2px solid #ccc; height: 186px; overflow-y: scroll; }
</style>
<script>
    $(document).ready(function () {
        $("#selectAll").click(function () {
            check = $("#selectAll").is(":checked");
            if (check) {
                $(".select_all_checkbox").prop("checked", true);
            } else {
                $(".select_all_checkbox").prop("checked", false);
            }
        });
        $("#selectAll2").click(function () {
            check = $("#selectAll2").is(":checked");
            if (check) {
                $(".select_all_checkbox2").prop("checked", true);
            } else {
                $(".select_all_checkbox2").prop("checked", false);
            }
        });
    });
</script>
<section class="createManager clearfix">
    <div class="tabs createTab">
        <ul class="clearfix tabsNavigation">
            <?php if ($_GET['level'] == 3) : ?>
                <h2>Edit Manager (<?php echo $user['first_name'] . ' ' . $user['last_name']; ?>)</h2>
            <?php elseif ($_GET['level'] == 4) : ?>
                <h2>Edit Sales Representatives (<?php echo $user['first_name'] . ' ' . $user['last_name']; ?>)</h2>
            <?php endif; ?>

        </ul>
        <div class="tabs_container clearfix">
            <div id="tab1" class="createTabContent unique">
                <form action="" class="clearfix" id="myform" method="POST">
                    <?php if ($success != "" && !empty($success)) { ?>
                        <p class="success_messages"><?php echo $success; ?></p>
                    <?php } ?>
                    <?php if ($err != "") { ?>
                        <p class="error_messages"><?php echo $err; ?></p>
                    <?php } ?>
                    <div class="width_240 floatLeft">
                        <p>
                            <label for="First Name" class="req">First Name</label>
                            <input type="text" tabindex="1" name="first_name" id="firstname" value="<?php echo $user['first_name']; ?>">
                        </p>
                        <p>
                            <label for="Username" class="req">Username</label>
                            <input type="text" tabindex="3" name="username" id="username" value="<?php echo $user['username'] ?>">
                        </p>
                        <p>
                            <label for="Password" class="req">Password</label>
                            <input type="password" tabindex="5" id="password" name="password" value="">
                        </p>
                        <div>
                            <?php
                            $get_user_rotations = User::getRotationsFromRotationsUsersByUserID($user['id']);
                            $get_user_rotations_array = array();
                            foreach ($get_user_rotations as $key => $get_user_rotation) {
                                $get_user_rotations_array[$key] = $get_user_rotation['rotation_id'];
                            }
                            ?>

                            <label>Select Rotations</label>
                            <?php $get_rotations = User::getAllRotations(); ?>                                      
                            <div class="container_rotation">
                                <input type="checkbox" id="selectAll" name="" /> Select All<br />
                                <?php foreach ($get_rotations as $get_rotation) : ?>
                                    <input type="checkbox" class="select_all_checkbox" name="add_rotation[]" value="<?php echo $get_rotation['id']; ?>" <?php echo (in_array($get_rotation['id'], $get_user_rotations_array)) ? 'checked="checked"' : ''; ?>/> <?php echo $get_rotation['rotation_title']; ?> <br />                           
                                <?php endforeach; ?>                            
                            </div>
                        </div>

                    </div>
                    <div class="width_240 floatRight">
                        <p>
                            <label for="Last Name" class="req">Last Name</label>
                            <input type="text" tabindex="2" id="lastname" name="last_name" value="<?php echo $user['last_name']; ?>">
                        </p>
                        <p>
                            <label for="Email Address" class="req">Email Address</label>
                            <input type="text" tabindex="4" id="emailaddress" name="email" value="<?php echo $user['email']; ?>">
                        </p>
                        <p>
                            <label for="Confirm Password" class="req">Confirm Password</label>
                            <input tabindex="6" type="password" id="confirmpassword" name="c_password" value="">                            
                        </p>

                        <?php if ($_GET['level'] == 3) : ?>  
                            <div style="width: 105%;">
                                <?php
                                $get_all_salesteams = User::getAllIndustry();
                                $manager_sales_teams = User::getIndustriesByUserId($user['id']);
                                $manager_sales_teams_array = array();
                                foreach ($manager_sales_teams as $key => $manager_sales_team) {
                                    $manager_sales_teams_array[$key] = $manager_sales_team['sales_team_id'];
                                }
                                ?>
                                <label>Select Sales Team</label>                                                                
                                <div class="container_rotation">
                                    <input type="checkbox" id="selectAll2" name="" tabindex="8" /> Select All<br />
                                    <?php foreach ($get_all_salesteams as $get_all_salesteam) : ?>
                                        <input type="checkbox" class="select_all_checkbox2" name="sales_team[]" value="<?php echo $get_all_salesteam['id']; ?>" <?php echo (in_array($get_all_salesteam['id'], $manager_sales_teams_array)) ? 'checked="checked"' : ''; ?> /> <?php echo $get_all_salesteam['name']; ?> <br />
                                    <?php endforeach; ?>                            
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ($_GET['level'] == 4) : ?>            
                            <label for="Sales Team" class="req">Sales Team</label>                          
                            <select tabindex="8" name="industry_sector" class="industry_sector_salesrep">
                                <option value="">Select Sales Team</option>
                                <?php
                                $results = User::getAllIndustry();
                                foreach ($results as $result) {
                                    ?>
                                    <option value="<?php echo $result['id']; ?>" <?php echo $result['id'] == $user['industry_id'] ? 'selected="selected"' : '' ?>><?php echo $result['name']; ?></option>  
                                <?php } ?>
                            </select>

                            <p>
                                <label for="Designations" class="req">Title</label>
                                <select tabindex="8" name="designation" class="designation_sales_rep">
                                    <?php
                                    $titles = User::getAllDesignation($user['industry_id']);
                                    foreach ($titles as $title) {
                                        ?>
                                        <option value="<?php echo $title['id']; ?>" <?php echo ($user['designation'] == $title['id']) ? 'selected="selected"' : ''; ?>><?php echo $title['designation']; ?></option>  
                                    <?php } ?>
                                </select>
                            </p>
                            <div>
                                <label>Assigned to</label>
                                <select required="required" name="assigned_to" id="assignto">                      
                                    <?php
                                    $allmanagers = User::getAllManagers();
                                    foreach ($allmanagers as $allmanager) {
                                        ?>
                                        <option value="<?php echo $allmanager['id']; ?>" <?php echo ($user['assigned_to'] == $allmanager['id']) ? 'selected="selected"' : ''; ?>><?php echo $allmanager['first_name'] . ' ' . $allmanager['last_name']; ?></option>  
                                    <?php } ?>
                                </select>
                            </div>
                        <?php endif; ?>
                        <p style="width: 240px;  padding-top: 0%;">
                            <input type="submit" id="submit" tabindex="8" name="submit" value="Submit" class="floatRight submitGreen">
                            <?php if ($_GET['level'] == 3) : ?>
                                <input type="hidden" name="user_type" value="manager" >
                            <?php elseif ($_GET['level'] == 4) : ?>
                                <input type="hidden" name="user_type" value="salesrep" >
                            <?php endif; ?>
                        </p>
                    </div>
                </form>
            </div>      
        </div>
    </div>
</section>   

<?php include('includes/footer.php'); ?>    