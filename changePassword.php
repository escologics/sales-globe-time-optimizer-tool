<?php
$page_title = 'Profile';
include('includes/top.php');
if (!Session::get('login')) {
    Redirect::to('index.php');
}
if (Input::exists()) {
    if ($_POST['user_type'] == 'profile') {
        $err = '';
        $validate = new Validate();
        if ($_POST['password'] != '' || $_POST['c_password'] != '') {
            $rule = array(
                'password' => array(
                    'label' => 'Password',
                    'required' => true,
                    'min' => '6',
                    'max' => '20',
                ),
                'c_password' => array(
                    'label' => "Confirm Password",
                    'required' => true,
                    'matches' => 'password',
                )
            );
        }
        $validation = $validate->check($_POST, $rule);
        if ($validation->passed()) {

            if ($_POST['password'] != '' || $_POST['c_password'] != '') {

                $rule_2 = array(
                    'password' => md5(Input::post('password'))
                );
            }
            DB::getInstance()->update('users', Input::post('user_id'), $rule_2);
            $success_m = "Password updated successfully."; // success message
        } else {
            //loop through error which is passed by validate class
            foreach ($validation->errors() as $error) {
                $err .= $error . "<br>";
            }
            $success_m = "<p style='color: #e80702; font-weight: bold;'>" . $err . "</p>"; // error message
        }

        $user_type_var = 'profile';
    }
}
include('includes/header.php');
?>
<?php $userInfo = User::getAllInformationByUserId(Session::get('user_id')); ?>
<script type="text/javascript">
    function validation_password()
    {
        var password = $("#password").val();
        var confirmpassword = $("#confirmpassword").val();


        if (password.length < 6)
        {
            $("#customessage").html("Password should be minimum of 6 characters");
            return false;
        }
        if (password != confirmpassword)
        {
            $("#customessage").html("Password and confirm passowrd does not match");
            return false;
        }


        return true;
    }
</script>
<section class="createManager clearfix">
    <div class="createTab">
        <ul class="clearfix tabsNavigation">
            <li style="background: #3B3B3B; border: 1px solid #3B3B3B; color: #FFFFFF; padding: 12px 10px;">Change Password</li>
        </ul>
        <!-- ( TABS LINK END ) -->

        <div class="tabs_container clearfix">
            <div id="tab1" class="createTabContent <?php echo (!$_POST['user_type']) ? 'unique' : ''; ?> <?php echo ($_POST['user_type'] == 'profile') ? 'unique' : ''; ?>">
                <form action="" class="clearfix" id="myform" method="POST">

                    <?php if (!empty($success_m)) { ?>
                        <p style="color:green; font-weight: bold;"><?php echo $success_m; ?></p>
                    <?php } ?>

                    <?php if ($msg != "") {
                        ?>
                        <p style="color: #e80702; font-weight: bold;"><?php echo $msg; ?></p>
                    <?php } ?>
                    <div class="width_240 floatLeft">
                        <p>
                            <label for="Password" class="req">Change Password</label>
                            <input type="password" tabindex="5" id="password" name="password" value="">
                        </p>

                    </div>
                    <div class="width_240 floatRight">
                        <p class="confirmForm">
                            <label for="Confirm Password" class="req">Confirm Password</label>
                            <input tabindex="6" type="password" id="confirmpassword" name="c_password" value="">
                            <input type="hidden" name="user_type" value="profile" >
                            <input type="hidden" name="user_id" value="<?php echo Session::get('user_id'); ?>" >
                            <span style="color: #e80702; font-weight: bold;" id="customessage"></span>
                        </p>
                    </div>
                    <br clear="all" />

                    <div class="clearfix" style="text-align: right;  ">
                        <p style="padding: 15px 0 0 0;display: inline-block;">
                            <input type="submit" id="submit" name="submit" value="Save" class="floatRight"  onclick="return validation_password();" > 
                        </p>
                        <p style="padding: 15px 0 0 0;display: inline-block;">
                            <input type="button" id="cancel" value="Cancel" class="floatRight" onclick="location.href = 'dashboard.php'">
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>   
</section>
<?php include('includes/footer.php'); ?>