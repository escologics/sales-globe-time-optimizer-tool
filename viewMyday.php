<?php
$page_title = 'Enter My Day';
include('includes/top.php');
if (!Session::get('login')) {
    Redirect::to('index.php');
}
include('includes/header.php');
?>
<?php if (isset($_GET['meeting_date'])) { ?>
    <input type="hidden" name="meeting_date_check" id="meeting_date_check" value="<?php echo $_GET['meeting_date']; ?>"/>
<?php } else { ?>
    <input type="hidden" name="meeting_date_check" id="meeting_date_check" value="0"/>
<?php } ?>
<script src="assets/javascripts/drop/chosen.jquery.js" type="text/javascript"></script>
<style>
    img.ui-datepicker-trigger {padding-top: 7px;}
    body .ui-tooltip {font-size: 12px;}
</style>

<script>
    var siteURL = '<?php echo $site_url; ?>';
</script>
<?php if (!isset($_GET['meeting_date'])) { ?>
    <script>
        $(document).ready(function () {
            var user_id = $('#current_loggedin_user').val();
            var selectedDate = '';
            var date = new Date(),
                    m = date.getMonth(),
                    y = date.getFullYear();
            d = date.getDate();

            var month = (m + 1);
            var todayDate = (month + '/' + d + '/' + y);
            $('#mm').val(month);
            $('#dd').val(d);
            $('#yy').val(y);

            selectedDate = (month + '/' + d + '/' + y);
            $('#meeting_date').val(selectedDate);
            $.ajax({
                type: "POST",
                cache: false,
                url: "gethourtotal.php",
                data: {
                    user_id: user_id,
                    selectedDate: selectedDate
                }
            }).done(function (totmin) {
                if (totmin > 0) {
                    $('#remaningTime').text(totmin);
                    $('#remaningTimehidden').val(totmin);
                    if (todayDate != selectedDate) {
                        fancyAlertConfirmviewmyday('You have already started logging activities for ' + selectedDate + '.  Would you like to choose a different day or edit the existing day?', siteURL + '/viewUnSavedDays.php');
                    }
                } else {
                    $('#remaningTime').text('0');
                    $('#remaningTimehidden').val('0');
                }
            });

            $('.user_date').on('change', function () {
                var date = new Date(),
                        m = date.getMonth(),
                        y = date.getFullYear();
                d = date.getDate();
                var months = (m + 1);

                var month = $('#mm').val();
                var day = $('#dd').val();
                var year = $('#yy').val();
                var todayDatemenual = (months + '/' + d + '/' + y);
                var selectedDatemenual = (month + '/' + day + '/' + year);
                $('#meeting_date').val(month + '/' + day + '/' + year);


                $.ajax({
                    type: "POST",
                    cache: false,
                    url: "checksubmitday.php",
                    data: {
                        user_id: user_id,
                        date_submit: month + '/' + day + '/' + year
                    }
                }).done(function (status) {
                    if (status == 0) {
                        fancyAlertConfirm('You have already submitted the hours for this day ' + month + '/' + day + '/' + year + '. ', siteURL + 'viewmyhistory.php');
                    } else {
                        $.ajax({
                            type: "POST",
                            cache: false,
                            url: "gethourtotal.php",
                            data: {
                                user_id: user_id,
                                selectedDate: month + '/' + day + '/' + year
                            }
                        }).done(function (totmin) {
                            if (totmin > 0) {
                                $('#remaningTime').text(totmin);
                                $('#remaningTimehidden').val(totmin);
                                if (status == 1) {
                                    fancyAlertConfirmviewmyday('You have already started logging activities for ' + selectedDatemenual + '.  Would you like to choose a different day or edit the existing day?', siteURL + 'viewUnSavedDays.php?selected_date=' + selectedDatemenual);
                                }
                            } else {
                                $('#remaningTime').text('0');
                                $('#remaningTimehidden').val('0');
                            }
                        });
                    }
                });

            });
        });

        var date = new Date(),
                m = date.getMonth(),
                y = date.getFullYear();
        d = date.getDate();

        var month = (m + 1);


        function checkDate() {

            var getdatefromdb = $('#getdatefromdb').val();
            var date_array = getdatefromdb.split("/");
            var db_month = date_array[0];
            var db_day = date_array[1];
            var db_year = +'20' + date_array[2];
            var finaldatefromdb = (db_month + '/' + db_day + '/' + db_year);
            var c_month = $('#mm').val();
            var c_day = $('#dd').val();
            var c_year = $('#yy').val();
            var current_date = (c_month + '/' + c_day + '/' + c_year);
            var startDate = new Date();
            var currentDate = new Date();
            currentDate.setDate(startDate.getDate() + 7);
            var finalafterdate = ((currentDate.getMonth() + 1) + "/" + currentDate.getDate() + "/" + currentDate.getFullYear());
            var date = new Date(),
                    m = date.getMonth(),
                    y = date.getFullYear();
            d = date.getDate();

            var month = (m + 1);
            if (c_month != '' && c_day != '' && c_year != '') {
                if (c_month > 12) {
                    $('#mm').val(month);
                } else if (c_month < 1) {
                    $('#mm').val(month);
                }

                if (c_day > daysInMonth(c_month, c_year)) {
                    $('#dd').val(d);
                } else if (c_day < 1) {
                    $('#dd').val(d);
                }
            }
            if (c_year > 2015) {
                $('#yy').val(y);
            } else if (c_year < 2012) {
                $('#yy').val(y);
            }

            if (c_month != '' && c_day != '' && c_year != '') {
                if (new Date(current_date) < new Date(finaldatefromdb)) {

                    $('#mm').val(month);
                    $('#dd').val(d);
                    $('#yy').val(y);


                } else if (new Date(current_date) > new Date(finalafterdate)) {

                    $('#mm').val(month);
                    $('#dd').val(d);
                    $('#yy').val(y);
                }
            }
        }

        function daysInMonth(month, year) {
            return new Date(year, month, 0).getDate();
        }
    </script>
<?php } ?>

<section class="mainContent viewMyDay clearfix">
    <form action="meetingForm" id="meetingForm" method="post">
        <input type="hidden" name="user_id" id="current_loggedin_user" value="<?php echo Session::get('user_id'); ?>">

        <div class="filterBox tableContent clearfix">
            <h3>Add Activity</h3>
            <div class="indent clearfix">
                <div class="grid_6">
                    <?php if (isset($_GET['meeting_date'])) : ?>
                        <div class="clearfix">
                            <label for="">Date:</label>
                            <p class="dateIcon">
                                <input type="text" id="meeting_date" value="<?php echo date("m/d/Y", strtotime($_GET['meeting_date'])); ?>" disabled="disabled" class="" placeholder="Select Date" style="width:320px;">
                            </p>
                        </div>
                    <?php else : ?>
                        <div class="clearfix datefields">
                            <label for="">Select a Date:</label>
                            <input type="hidden" id="meeting_date"/>
                            <p class="dateIcon" style="padding-top: 5px;">
                                <input type="number" id="mm" placeholder="mm" onChange="checkDate();" class="user_date" style="width:38px;">                                
                                <input type="number" id="dd" placeholder="dd" onChange="checkDate();" class="user_date" style="width:30px;">                                
                                <input type="number" id="yy" placeholder="yy" onChange="checkDate();"  class="user_date" min="2012" max="2015" style="width:47px;">                                
                            </p>
                            <input type="hidden" id="calender_image"  class="datepicker_view_my_day_2"/>
                            <input type="hidden" id="getdatefromdb"/>
                        </div>

                    <?php endif; ?>
                    <div class="clearfix datefields">                      
                        <?php $meeting_categories = Meeting::getMeetingCategories(); ?>
                        <?php foreach ($meeting_categories as $meeting_category) { ?>
                            <input type="hidden" name="meeting_category[]" id="meeting_category" value="<?php echo $meeting_category['id']; ?>"/>
                        <?php } ?>                      
                    </div>
                </div>
            </div>
        </div>
        <!-- ( .indentBox end ) -->

        <div class="tableSec clearfix">
            <div class="tableScroll clearfix activityData" id="ajaxbody" style="display:none;">

            </div>
            <div class="loader" style="text-align: center; margin-top: 5%; display: none;">
                <img src="assets/images/loader.gif"/>
            </div>
            <?php
            if (isset($_GET['meeting_date'])) :
                $selectedDateformat = $_GET['meeting_date'];
                $user_id = Session::get('user_id');
                $totalHours = Meeting::getTotalTimeByDateAndUserId($selectedDateformat, $user_id);
                ?>
                <section class="mainDashbord tableSec clearfix previousActivtyDetails" style="display:none">
                    <?php
                    $meetings = Meeting::getMeetingsByUserIdAndDate(Session::get('user_id'), $selectedDateformat);
                    if ($meetings) {
                        foreach ($meetings as $meeting) {
                            ?>
                            <div class="archivePost">
                                <div class="archiveTilte active">
                                    <h2 class="arrowClose"><?php echo date("F, d Y", strtotime($meeting['added_date'])); ?></h2>
                                </div><!-- ( .archiveTilte end ) -->
                                <div class="archiveInfo" style="display: block;">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th style="width:1%; text-align:left;">#</th>
                                                <th style="text-align: left; width: 28%;">Activity</th>
                                                <th style="text-align: left; width: 22%;">Sub - Activity</th>                                                
                                                <?php $meeting_date = $meeting['added_date']; ?>
                                                <?php $totalHours = Meeting::getTotalTimeByDateAndUserId($meeting['added_date'], Session::get('user_id')); ?>
                                                <th style="text-align: left; width: 11%;">Hours & Minutes</th>
                                                <th style="text-align: left; width: 11%;">&nbsp</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $meeting_details = Meeting::getMeetingDetailsByMeetingId($meeting['id']);
                                            if ($meeting_details) {
                                                $sr_no = 1;
                                                foreach ($meeting_details as $meeting_detail) {
                                                    ?>  
                                                    <tr>
                                                        <td><?php echo $sr_no; ?></td>
                                                        <td><?php echo Meeting::getMeetingCategoryNameById($meeting_detail['meeting_category']); ?></td>
                                                        <td><?php echo Meeting::getMeetingActivityNameById($meeting_detail['activity_type']); ?></td>                                                
                                                        <td><?php echo gmdate("i:s", $meeting_detail['hours']); ?></td>
                                                        <td align="right" width="8%"  class="tableBtn">
                                                            <a href="EditUnsavedDay.php?id=<?php echo $meeting_detail['id']; ?>"><img src="assets/images/settingRed.jpg" alt=""></a>
                                                            <a href="#delete_<?php echo $meeting_detail['id']; ?>"  class="fancybox"><img src="assets/images/closeRed.jpg" alt=""></a>
                                                            <div id="delete_<?php echo $meeting_detail['id']; ?>" class="popupBox alignCenter">
                                                                <form action="" method="post">
                                                                    <p><img src="assets/images/success.png" alt=""></p>
                                                                    <h3>Are you sure you want to delete this activity? </h3>
                                                                    <input type="hidden" value="<?php echo $meeting_detail['id']; ?>" name="deleteid">
                                                                    <input type="hidden" value="<?php echo $meeting_detail['meeting_id']; ?>" name="meeting_id" />
                                                                    <input type="submit" name="delete" value="Yes">
                                                                    <input type="button" value="No" class="redBtn" onclick="jQuery.fancybox.close()" />
                                                                </form>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $sr_no++;
                                                }
                                            }
                                            ?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>

                                                </td>
                                                <td><?php echo gmdate("i:s", $totalHours); ?></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>        
                </section>     
            <?php endif; ?><br/>
            <div class="clearfix floatRight">
                <a href="#" id="continueid" class="darkBtn  submitGrey">Save and Continue Later</a>
                <a href="#" id="submitdayid" class="redBtn submitGreen">Save and Submit Day</a>
                <a href="#submitMyDay" id="submitMyDaypopup" class="fancybox"></a>
            </div>

            <!-- ( POPUP CONTENT BEGIN ) -->
            <div id="dayContinue" class="popupBox activitiesPopup continuepopupbox">
                <div class="indent clearfix">
                    <div class="clearfix">
                        <h3>Do you want to add other activities now?</h3>
                        <div class="clearfix"> <input type="radio" name="otherActivities" id="otherActivitiesYes" value="no"> <label for="otherActivitiesYes">Yes - Add activities and submit day now</label> </div>
                        <div class="clearfix"> <input type="radio" name="otherActivities" id="otherActivitiesNo" value="yes"> <label for="otherActivitiesNo">No -  Add additional activities and submit day at another time</label></div>
                    </div>              
                    <div style="float: right">
                        <a href="" id="continueSumit" class="darkBtn continuepopup">OK</a>
                    </div>
                </div>
            </div>

            <div id="alertmessage" class="popupBox submitMyDay alignCenter">
                <h3>Please Note: Once hours are submitted, you can not edit/change the information submitted. <br> Do you want to continue?</h3>
            </div>

            <div id="submitMyDay" class="popupBox submitMyDay alignCenter">
                <p><img src="assets/images/success.png" alt=""></p>
                <h3>Your activities for this date will be submitted.<br> Do you want to continue?</h3>
                <a href="" id="submitmydayyes" class="fancybox redBtn">Yes</a>
                <a href="javascript: ;" class="fancybox darkBtn">No</a>
            </div>
        </div>
        <!-- ( .tableSec end ) -->
    </form>
</section> 
<br/>
<?php if (isset($_GET['meeting_date'])) { ?>
    <script>
        function checkhours_viewMyDay(obj) {
            var totalmin = 1440;
            var currentmin = $('#remaningTime').text();
            var currentmincalc = $('#remaningTimecalc').val();
            var remaningTimehidden = $('#remaningTimehidden').val();
            var remaingmin = (totalmin - currentmincalc);
            var hours = 0;
            var mins = 0;
            var calculattime = 0;

            $(obj).parents('.activityData').find('.timeFields').each(function () {
                if ($(this).parents('tr').find('.activity_typeclass').is(':checked')) {
                    hours = $(this).parents('tr').find('#hours').val();
                    mins = $(this).parents('tr').find('#minutes').val();
                    if (hours > 0 || mins > 0) {
                        if (hours != '') {
                            if (mins > 0) {
                                calculattime += (parseInt(hours) * 60 + parseInt(mins));
                            } else {
                                calculattime += (parseInt(hours) * 60);
                            }
                        } else {
                            calculattime += parseInt(mins);
                        }
                    }
                }
            });
            $('#remaningTime').text(convertMintoHours(parseInt(calculattime)));
            $('#remaningTimecalc').val(parseInt(calculattime));

            if (currentmincalc != 1440) {
                if (calculattime > 1440) {
                    fancyAlert('You can not add above ' + remaingmin + ' minutes');
                    if ($(obj).parents('tr').find('#hours').val() > 0) {
                        $(obj).parents('tr').find('#hours').val('0').trigger('chosen:updated');
                    }
                    if ($(obj).parents('tr').find('#minutes').val() > 0) {
                        $(obj).parents('tr').find('#minutes').val('0').trigger('chosen:updated');
                    }
                    $('#remaningTime').text(convertMintoHours(remaningTimehidden));
                    $('#remaningTimecalc').val(remaningTimehidden);
                    $(obj).parents('tr').find('.activity_typeclass').trigger("click");
                }
            }
        }
    </script>
<?php } else { ?>
    <script>
        function checkhours_viewMyDay(obj) {
            var totalmin = 1440;
            var remaningTimehidden = $('#remaningTimehidden').val();
            var hours = 0;
            var mins = 0;
            var calculattime = 0;

            $(obj).parents('.activityData').find('.timeFields').each(function () {
                if ($(this).parents('tr').find('.activity_typeclass').is(':checked')) {
                    hours = $(this).parents('tr').find('#hours').val();
                    mins = $(this).parents('tr').find('#minutes').val();
                    if (hours > 0 || mins > 0) {
                        if (hours != '') {
                            if (mins > 0) {
                                calculattime += (parseInt(hours) * 60 + parseInt(mins));
                            } else {
                                calculattime += (parseInt(hours) * 60);
                            }
                        } else {
                            calculattime += parseInt(mins);
                        }

                    }
                }
            });
            $('#remaningTime').text(convertMintoHours(parseInt(calculattime)));
            $('#remaningTimehidden').val(parseInt(calculattime));
            $('#remaningTimecalc').val(parseInt(calculattime));
            var currentmin = $('#remaningTime').text();
            var currentmincalc = $('#remaningTimecalc').val();
            var remaingmin = (totalmin - remaningTimehidden);

            if (calculattime > 1440) {
                fancyAlert('You can not add above ' + remaingmin + ' minutes');
                if ($(obj).parents('tr').find('#hours').val() > 0) {
                    $(obj).parents('tr').find('#hours').val('0').trigger('chosen:updated');
                }
                if ($(obj).parents('tr').find('#minutes').val() > 0) {
                    $(obj).parents('tr').find('#minutes').val('0').trigger('chosen:updated');
                }
                $('#remaningTime').text(convertMintoHours(remaningTimehidden));
                $('#remaningTimecalc').val(remaningTimehidden);
                $(obj).parents('tr').find('.activity_typeclass').trigger("click");
            }
        }
    </script>
    <?php
}
include('includes/footer.php');
