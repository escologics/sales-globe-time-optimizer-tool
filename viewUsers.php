<?php
$page_title = 'View User';
include('includes/top.php');
if (!Session::get('login')) {
    Redirect::to('index.php');
}

include('includes/header.php');
?>
<script type="text/javascript">
    $(document).ready(function () {
        getManagers();
        $('#tab_sales_rep').click(function () {
            getsalesrepresentatives();
        });
        $('#tab_assigned').click(function () {
            getassignedUsers();
        });
    });

    function getManagers() {
        $('#ajax_managers').html('');
        $('#manager_table').hide();
        $.ajax({
            type: "POST",
            cache: false,
            url: "ajaxGetUsers.php",
            data: {
                user_level: 3
            },
            beforeSend: function () {
                $('.loader_manager').show();
            }
        }).done(function (html) {
            $('#ajax_managers').html(html);
            $('.loader_manager').hide();
            $('#manager_table').show();
        });
    }

    function getsalesrepresentatives() {
        $('#ajax_sales_representatives').html('');
        $('#sales_rep_table').hide();
        $.ajax({
            type: "POST",
            cache: false,
            url: "ajaxGetUsers.php",
            data: {
                user_level: 4
            },
            beforeSend: function () {
                $('.loader_sales_representatives').show();
            }
        }).done(function (html) {
            $('#ajax_sales_representatives').html(html);
            $('.loader_sales_representatives').hide();
            $('#sales_rep_table').show();
        });
    }

    function getassignedUsers() {
        $('#ajax_assigned_users').html('');
        $('#assign_table').hide();
        $.ajax({
            type: "POST",
            cache: false,
            url: "ajaxGetassignedUsers.php",
            beforeSend: function () {
                $('.loader_assigned_users').show();
            }
        }).done(function (html) {
            $('#ajax_assigned_users').html(html);
            $('.loader_assigned_users').hide();
            $('#assign_table').show();
        });
    }

</script>

<section class="createManager clearfix">
    <div class="tabs createTab">
        <ul class="clearfix tabsNavigation">
            <li><a href="#tab1" id="tab_managers" class="<?php echo ($_POST['editUser'] == 'editManager') ? 'unique' : '' ?> <?php echo ($_POST['editUser'] == 'editsalesr') ? 'unique' : '' ?> <?php echo (!$_POST['editUser']) ? 'unique' : '' ?> <?php echo ($_GET['deleted'] == 'manager') ? 'unique' : ''; ?>" >View Managers</a></li>
            <li><a href="#tab2" id="tab_sales_rep" class="<?php echo ($_POST['editUser'] == 'editsalesr') ? 'unique' : '' ?> <?php echo ($_GET['deleted'] == 'salesrep') ? 'unique' : ''; ?>" >View Sales Representatives</a></li>
            <li><a href="#tab3" id="tab_assigned" class="<?php echo ($_POST['editUser'] == 'editManagerView' || $_POST['editUser'] == 'editsalesrView' ) ? 'unique' : '' ?>">View Assigned Sales Representatives Under Manager</a></li>
        </ul>
        <div class="tabs_container clearfix">

            <div id="tab1" class=" <?php echo ($_POST['editUser'] == 'editManager') ? 'unique' : '' ?> <?php echo ($_POST['editUser'] == 'editsalesr') ? 'unique' : '' ?> <?php echo (!$_POST['editUser']) ? 'unique' : '' ?> <?php echo ($_GET['deleted'] == 'manager') ? 'unique' : ''; ?>" >
                <div class="tableSec admin clearfix">

                  
                    <div class="tableScroll clearfix">
                        <table id="manager_table" style="display:none;">
                            <thead>
                                <tr>
                                    <th align="left" width="5%">#</th>
                                    <th align="left">Name</th>
                                    <th align="left">Sales Team</th>
                                    <th align="left">Email Address</th>
                                    <th align="left">Username</th>
                                    <th align="left">Rotations</th>
                                    <th align="left">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody id="ajax_managers"></tbody>
                            <div class="loader_manager" style="text-align: center; margin-top: 5%; display: none;">
                                <img src="assets/images/loader.gif"/>
                            </div>
                        </table>
                    </div>
                </div>
            </div>

            <div id="tab2" class="<?php echo ($_POST['editUser'] == 'editsalesr') ? 'unique' : '' ?> <?php echo ($_GET['deleted'] == 'salesrep') ? 'unique' : ''; ?>">
                <div class="tableSec admin clearfix">
                
                    <div class="tableScroll clearfix">
                        <table id="sales_rep_table" style="display: none;">
                            <thead>
                                <tr>
                                    <th align="left" width="5%">#</th>
                                    <th align="left">Name</th>                                    
                                    <th align="left">Sales Team</th>
                                    <th align="left">Title</th>
                                    <th align="left">Assigned Manager</th>
                                    <th align="left">Email Address</th>
                                    <th align="left">Username</th>
                                    <th align="left">Rotations</th>
                                    <th align="left">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody id="ajax_sales_representatives"></tbody>
                            <div class="loader_sales_representatives" style="text-align: center; margin-top: 5%; display: none;">
                                <img src="assets/images/loader.gif"/>
                            </div>
                        </table>
                    </div>
                </div>
            </div>

            <div id="tab3" class="<?php echo ($_POST['editUser'] == 'editManagerView' || $_POST['editUser'] == 'editsalesrView' ) ? 'unique' : '' ?>">
                <div class="tableSec admin clearfix">
                    <?php if ($_POST['editUser'] == 'editManagerView' && isset($success_m) && !empty($success_m)) {
                        ?>
                        <p style="color:green; font-weight: bold;"><?php echo $success_m; ?></p>
                    <?php } ?>
                    <?php if ($_POST['editUser'] == 'editsalesrView' && isset($success_s) && !empty($success_s)) {
                        ?>
                        <p style="color:green; font-weight: bold;"><?php echo $success_s; ?></p>
                    <?php } ?>
                    <p>Click on manager to see the assigned reps</p>
                    <div class="grid_12a tableScroll customeAssigneTable  clearfix">
                        <table class="assignedTable" id="assign_table"  style="display:none;">
                            <thead>
                                <tr>
                                    <th width="4%" align="left">#</th>
                                    <th width="16%" align="left">First Name</th>
                                    <th width="16%" align="left">Last Name</th>
                                    <th width="16%" align="left">Sales Team</th>
                                    <th width="16%" align="left">Email Address</th>
                                    <th width="14%" align="left">Username</th>
                                    <th width="14%" align="left">Access Role</th>
                                    <th width="14%" align="left">Action</th>
                                </tr>
                            </thead>
                            <tbody id="ajax_assigned_users"></tbody>
                        </table>
                        <div class="loader_assigned_users" style="text-align: center; margin-top: 5%; display: none;">
                            <img src="assets/images/loader.gif"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>             
<?php include('includes/footer.php'); ?>                 
<?php if ($demotion == 1) { ?>
    <?php
} 