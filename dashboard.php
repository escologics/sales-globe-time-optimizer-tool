<?php

$page_title = 'Dashboard';
include('includes/top.php');
if (!Session::get('login')) {
    //if Session is not true the it will redirect to index.php
    //Redirect::to class is in classes/Redirect.php
    Redirect::to('index.php');
}


if (Session::get('level') == 3 || Session::get('level') == 4) {
    Redirect::to('viewMyday.php');
}
include('includes/header.php');
?>

<section class="mainDashbord clearfix">

    <ul class="clearfix">
        <li><a href="createUser.php" title="Create Users" class="createUserIcon">Create Users</a></li>
        <li><a href="viewUsers.php" title="View Users" class="viewUserIcon">View Users</a></li>
        <li><a href="adminDashboard.php" title="View Logged Activity Hours" class="activityHoursIcon">View Logged Activity Hours</a></li>
        <li><a href="archive.php" title="Archives" class="archivesIcon">Archives</a></li>
    </ul>

</section>
<!-- ( .mainDashbord end ) -->

<?php include('includes/footer.php'); ?>