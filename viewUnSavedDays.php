<?php
$page_title = 'View Unsaved Day';
include('includes/top.php');
// if post deleted
if (isset($_POST['delete'])) {
    // Delete id is hidden type in form
    if ($_POST['deleteid']) {
        //Delete meeting detail by id
        $delete = DB::getInstance()->delete('meeting_detail', $_POST['deleteid']);
        //check meeting detail by meeting id
        $check_meeting = Meeting::checkMeetingDetailsByMeetingId($_POST['meeting_id']);
        // if all meeting detail then remove meeting from meeting table 
        if ($check_meeting < 1) {
            $delete_main = DB::getInstance()->delete('meeting', $_POST['meeting_id']);
        }
        if ($delete) {
            $sucess_msg = "Entry deleted successfully.";
        }
    }
}
include('includes/header.php');

?>
<section class="mainDashbord tableSec clearfix">
    <?php if ($sucess_msg != "" && !empty($sucess_msg)) {
        ?>
        <p style="color:green; font-weight: bold;"><?php echo $sucess_msg; ?></p>
    <?php } ?>
    <?php
    //Get meeting by current logged in user
    $meetings = Meeting::getMeetingsByUserId(Session::get('user_id'));
    if ($meetings) {
        foreach ($meetings as $meeting) {
            ?>
            <div class="archivePost">
                <?php
                if (isset($_GET['selected_date'])) {
                    $selected_date = date("Y-m-d", strtotime($_GET['selected_date']));
                    if ($selected_date == $meeting['added_date']) {
                        ?>
                        <div class="archiveTilte active">
                            <h2 class="arrowClose"><?php echo date("F d, Y", strtotime($meeting['added_date'])); ?></h2>
                        </div><!-- ( .archiveTilte end ) -->
                        <div class="archiveInfo" style="display:block;">
                    <?php } else { ?>
                        <div class="archiveTilte">
                            <h2><?php echo date("F d, Y", strtotime($meeting['added_date'])); ?></h2>
                        </div><!-- ( .archiveTilte end ) -->
                        <div class="archiveInfo">
                    <?php } ?>
                <?php } else { ?>
                    <div class="archiveTilte">
                        <h2><?php echo date("F d, Y", strtotime($meeting['added_date'])); ?></h2>
                    </div><!-- ( .archiveTilte end ) -->
                    <div class="archiveInfo">
                <?php } ?>
                
                    <table>
                        <thead>
                            <tr>
                                <th style="width:1%; text-align:left;">#</th>
                                <th style="text-align: left; width: 28%;">Activity</th>
                                <th style="text-align: left; width: 22%;">Sub - Activity</th>
                                <?php // $meeting_date = date("d-m-Y", strtotime($meeting['added_date'])); // change date formate just only for SQL query ?>
                                <?php $meeting_date = $meeting['added_date']; // change date formate just only for SQL query ?>
                                <?php $totalHours = Meeting::getTotalTimeByDateAndUserId($meeting['added_date'], Session::get('user_id')); // total hours    ?>
                                <th style="text-align: left; width: 11%;">Hours & Minutes</th>
                                <th style="text-align: left; width: 11%;">
                                    <?php if ($totalHours < 1440) { ?>
                                    <a style="margin-left: 20px;" href="viewMyday.php?meeting_date=<?php echo $meeting_date; ?>" class="redBtn">Add Another Activity</a>
                                    <?php } else { ?>
                                        <a style="margin-left: 10px; background: grey;" href="#" class="redBtn" title="You can not add above 1440 minutes">Add Another Activity</a>
                                    <?php } ?>
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $meeting_details = Meeting::getMeetingDetailsByMeetingId($meeting['id']);
                            if ($meeting_details) {
                                $sr_no = 1;
                                foreach ($meeting_details as $meeting_detail) {
                                    ?>  
                                    <tr>
                                        <td><?php echo $sr_no; ?></td>
                                        <td><?php echo Meeting::getMeetingCategoryNameById($meeting_detail['meeting_category']); ?></td>
                                        <td><?php echo Meeting::getMeetingActivityNameById($meeting_detail['activity_type']); ?></td>
                                <!--                                        <td><?php // echo number_format((float) $meeting_detail['hours'], 2, '.', '');         ?></td>-->
                                        <td><?php echo gmdate("i:s", $meeting_detail['hours']); ?></td>
                                        <td align="right" width="8%"  class="tableBtn">
                                            <a href="EditUnsavedDay.php?id=<?php echo $meeting_detail['id']; ?>"><img src="assets/images/settingRed.jpg" alt=""></a>
                                            <a href="#delete_<?php echo $meeting_detail['id']; ?>"  class="fancybox"><img src="assets/images/closeRed.jpg" alt=""></a>
                                            <div id="delete_<?php echo $meeting_detail['id']; ?>" class="popupBox alignCenter">
                                                <form action="" method="post">
                                                    <p><img src="assets/images/success.png" alt=""></p>
                                                    <h3>Are you sure you want to delete this activity? </h3>
                                                    <input type="hidden" value="<?php echo $meeting_detail['id']; ?>" name="deleteid">
                                                    <input type="hidden" value="<?php echo $meeting_detail['meeting_id']; ?>" name="meeting_id" />
                                                    <input type="submit" name="delete" value="Yes">
                                                    <input type="button" value="No" class="redBtn" onclick="jQuery.fancybox.close()" />
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $sr_no++;
                                }
                            }
                            ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                    <a style="cursor: pointer" onclick="submitMyDay1(<?php echo $meeting['id'] ?>);">
                                        <span class="redBtn submitGreen">Submit Day</span> 
                                    </a>
                                </td>
                                <td><?php echo gmdate("i:s", $totalHours); ?></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- ( .archiveInfo end  ) -->
            </div><!-- ( .archivePost end ) -->
            <?php
        }
    }
    ?>        
</section>         
<?php include('includes/footer.php'); ?>                 
