<?php
error_reporting(0);
if ($_POST['updateUser']) {
    $updateUserAdmin = DB::getInstance()->update('users', Input::post('user_id'), array(
        'first_name' => Input::post('first_name'),
        'last_name' => Input::post('last_name'),
        'email' => Input::post('email'),
        'company' => Input::post('company'),
    ));
    if ($updateUserAdmin) {
        $adminSuccess = "Information is updated successfully.";
    }
}
?>
<!DOCTYPE html>
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="no-js ie9" lang="en"> <![endif]-->
<html class="no-js" lang="en"> 
    <head>
        <meta charset="utf-8">
        <title><?php echo User::getLevelNameByLevelId(Session::get('level')); ?> :: <?php echo $page_title; ?></title>
        <meta name="author" content="" />   
        <meta name="description" content="" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- ( CSS LINKS START ) -->
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link href="assets/css/default.css" type="text/css" rel="stylesheet" /> 
        <link href="assets/css/jquery.fancybox.css" type="text/css" rel="stylesheet" />
        <?php if ($currentPage == 'viewMyday' || $currentPage == 'viewmyhistory' || $currentPage == 'managerDashboard' || $currentPage == 'adminDashboard' || $currentPage == 'editMeetingHours' || $currentPage == 'reporting' || $currentPage == 'EditUnsavedDay' || $currentPage == 'addRotation' || $currentPage == 'editRotation') { ?>           
            <link href="assets/css/jquery.ui.all.css" type="text/css" rel="stylesheet">   
        <?php } ?>
        <link href="assets/css/responsive.css" type="text/css" rel="stylesheet" /> 
        <link href="assets/css/jquery.timeentry.css" type="text/css" rel="stylesheet" /> 
        <!-- ( CSS LINKS END ) -->
<!--        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>-->
        <!-- ( SCRIPT LIBRARY START ) -->
        <script type="text/javascript" src="assets/javascripts/jquery-2.1.1.js"></script>
<!--        <script type="text/javascript" src="assets/javascripts/jquery-migrate-1.1.1.js"></script>  -->
        <link rel="stylesheet" href="assets/css/rating.css" type="text/css" media="screen" title="Rating CSS">
        <script type="text/javascript" src="assets/javascripts/rating.js"></script>
        <script src="assets/javascripts/bootstrap.min.js"></script>
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--  ( SCRIPT LIBRARY END )  --> 
    </head>
    <body>
        <!-- ( MAIN CONTAINER BEGIN ) -->
        <div id="mainContainer" class=" clearfix">
            <!-- ( CONTAINER 12 BEGIN ) -->
            <div class="container_12">
                <!-- ( CONTENT BOX BEGIN ) -->
                <div class="contentBox grid_12 marginAuto clearfix">
                    <header class="header">
                        <div class="headTop">
                            <div class="logo">
                                <?php
                                if (Session::get('level') == 1 || Session::get('level') == 2) {
                                    $link = 'dashboard.php';
                                } elseif (Session::get('level') == 3) {
                                    $link = 'managerDashboard.php';
                                } elseif (Session::get('level') == 4) {
                                    $link = 'viewMyday.php';
                                } else {
                                    $link = 'http://www.salesglobe.com';
                                }
                                ?>
                                <a href="<?php echo $link; ?>"><img src="assets/images/logo.gif" alt="" style="height: 28px; padding-top: 10px;" /></a>
                            </div>
                            <!-- ( .logo end ) -->
                            <div class="topTitle">
                                <h2>Sales Time Optimizer</h2>
                            </div>
                            <!-- ( .topTitle end ) -->
                            <div class="topMenu">

                                <?php /* ?><li>
                                  <?php echo User::getCompanyByUserId(Session::get('user_id')); ?>
                                  </li><?php */ ?>



                                <!-- <li class="topBtn"><a href="logout.php" style="margin-top:7px;">Log Off</a></li>-->

                                <div class="top-login-btn topAdminsection">
                                    <ul>
                                        <li class="top-designation">

                                            <p>     <a href="profile.php?u_id=<?php echo Session::get('user_id'); ?>"><?php echo User::getFullNameByUserId(Session::get('user_id')); ?></a></p>

                                            <div class="designation admin">
                                                <?php
                                                $resultuser = User::getAllInformationByUserId(Session::get('user_id'));
                                                if (Session::get('level') == 3) {
                                                    //$usertype = User::getIndustryByUserId($resultuser['industry_id'][0]);
                                                }
                                                if (Session::get('level') == 4) {
                                                    $usertype = User::getDesignationByUserId($resultuser['designation']);
                                                }
                                                ?>
                                                <?php if (Session::get('level') == 4) { ?>
                                                    <?php echo User::getIndustryByUserId($resultuser['industry_id']); ?><?php if (Session::get('level') == 3 || Session::get('level') == 4) { ?> - <?php echo $usertype; ?> <?php } ?>
                                                    <?php
                                                } elseif (Session::get('level') == 3) {
                                                    $manager_industries = User::getIndustriesByUserId($resultuser['id']);
                                                    ?>
                                                    <?php
                                                    foreach ($manager_industries as $manager_industry) {
                                                        echo '<p>' . User::getIndustryByUserId($manager_industry['sales_team_id']) . '</p>';
                                                    }
                                                    ?>
                                                <?php } ?>
                                            </div>
                                        </li> 
                                        <?php if (Session::get('level') == 1 || Session::get('level') == 2) { ?>

                                            <li class="topBtn">
                                                <a href="#profilePopup" class="fancybox"><img src="assets/images/blank.gif" alt="" class="setting">
                                                </a>
                                            </li>

                                        <?php } ?>
                                        <?php if (Session::get('level') == 4 || Session::get('level') == 3) { ?>

                                            <li class="topBtn">
                                                <a href="changePassword.php" class="fancybox"><img src="assets/images/blank.gif" alt="" class="setting">
                                                </a>
                                            </li>

                                        <?php } ?>
                                        <li class="topBtn"><a href="logout.php" class="fancybox" style="background-color: #e80703;" title="Log Out"><img src="assets/images/blank.gif" alt="" class="logout"></a></li>
                                    </ul>
                                </div></div>

                            <!-- ( .topMenu end ) -->
                            <!-- ( POPUP CONTENT BEGIN ) -->
                            <div id="profilePopup" class="popupBox width_240">
                                <?php $information = User::getAllInformationByUserId(Session::get('user_id')); ?>
                                <form action="" name="profileForm" id="profileForm" method="post">
                                    <h3>Edit Your Information </h3>
                                    <p>First Name:<br /><input type="text" name="first_name" id="profileName" required value="<?php echo $information['first_name']; ?>" placeholder="Name" /></p>
                                    <p>Last Name:<br /><input type="text" name="last_name" id="profileName" required  value="<?php echo $information['last_name']; ?>" placeholder="Name" /></p>
                                    <?php if (Session::get('level') != 1) { ?>
                                        <p>Company:<br /><input type="text" name="company" id="profileCompany" required  value="<?php echo $information['company']; ?>" placeholder="Company" /></p>
                                    <?php } ?>
                                    <p>Email:<br /><input type="email" required  style="border: 1px solid #9e9e9e; padding: 7px 5px;width: 94%;margin-bottom: 15px;font-size: 12px;" name="email" id="profileEmail" value="<?php echo $information['email']; ?>" placeholder="Email" /></p>
                                    <input type="hidden" name="user_id" value="<?php echo Session::get('user_id'); ?>" >
                                    <input type="hidden" name="current_page" value="<?php echo curPageURL(); ?>" >
                                    <input type="submit" id="submit" name="updateUser" value="Submit" class="marginTop_27 submitGreen">
                                </form>
                            </div>
                            <!-- ( POPUP CONTENT END ) -->
                        </div>
                        <!-- ( .headTop end ) -->
                        <?php if (Session::get('level') == 1 || Session::get('level') == 2) { ?>
                            <nav class="mainNav">
                                <ul>
                                    <li><a href="dashboard.php" title="Dashboard" <?php echo ($currentPage == 'dashboard') ? 'class="active"' : ''; ?>>Dashboard</a></li>
                                    <li><a href="createUser.php" title="Create Users" <?php echo ($currentPage == 'createUser') ? 'class="active"' : ''; ?>>Create Users</a></li>
                                    <li><a href="viewUsers.php" title="View Users" <?php echo ($currentPage == 'viewUsers') ? 'class="active"' : ''; ?>>View Users</a></li>
                                    <li><a href="adminDashboard.php" <?php echo ($currentPage == 'adminDashboard' || $currentPage == 'editMeetingHours') ? 'class="active"' : ''; ?> title="View Logged Activity Hours & Minutes">View Logged Activity Hours & Minutes</a></li>
                                    <li><a href="archive.php" <?php echo ($currentPage == 'archive') ? 'class="active"' : ''; ?> title="Archives">Archives</a></li>
                                    <li><a href="rotations.php" <?php echo ($currentPage == 'rotations') ? 'class="active"' : ''; ?> title="Rotations">Rotations</a></li>
                                    <li><a href="usersImport.php" <?php echo ($currentPage == 'usersImport') ? 'class="active"' : ''; ?> title="Users Import">Users Import</a></li>
    <!--                                    <li><a href="reporting.php" <?php //echo ($currentPage == 'reporting') ? 'class="active"' : '';      ?> title="Reporting">Reporting</a></li>-->
                                </ul>
                            </nav>
                        <?php } ?>
                        <!-- ( .mainNav end ) -->
                        <?php if (Session::get('level') == 3) { ?>
                            <nav class="mainNav">
                                <ul>
                                    <?php if (isset($_GET['meeting_date'])) { ?>
                                        <li><a href="viewMyday.php" title="Enter My Day" <?php echo ($currentPage == 'viewMyday') ? '' : ''; ?>>Enter My Day</a></li>
                                        <li><a href="viewUnSavedDays.php" title="View Unsubmitted Days" class="active">View Unsubmitted Days</a></li>
                                    <?php } else { ?>
                                        <li><a href="viewMyday.php" title="Enter My Day" <?php echo ($currentPage == 'viewMyday') ? 'class="active"' : ''; ?>>Enter My Day</a></li>
                                        <li><a href="viewUnSavedDays.php" title="View Unsubmitted Days" <?php echo ($currentPage == 'viewUnSavedDays') ? 'class="active"' : ''; ?>>View Unsubmitted Days</a></li>
                                    <?php } ?>

                                    <li><a href="viewmyhistory.php" <?php echo ($currentPage == 'viewmyhistory') ? 'class="active"' : ''; ?> title="View My History">View My History</a></li>
    <!--                                    <li><a href="managerDashboard.php" <?php echo ($currentPage == 'managerDashboard') ? 'class="active"' : ''; ?> title="Manager Dashboard">Manager Dashboard</a></li>-->
                                </ul>
                            </nav>
                        <?php } ?>	
                        <?php if (Session::get('level') == 4) { ?>
                            <nav class="mainNav">
                                <ul>
                                    <?php if (isset($_GET['meeting_date'])) { ?>
                                        <li><a href="viewMyday.php" title="Enter My Day">Enter My Day</a></li>
                                        <li><a href="viewUnSavedDays.php" title="View Unsubmitted Days" class="active">View Unsubmitted Days</a></li>
                                    <?php } else { ?>
                                        <li><a href="viewMyday.php" title="Enter My Day" <?php echo ($currentPage == 'viewMyday') ? 'class="active"' : ''; ?>>Enter My Day</a></li>
                                        <li><a href="viewUnSavedDays.php" title="View Unsubmitted Days" <?php echo ($currentPage == 'viewUnSavedDays') ? 'class="active"' : ''; ?>>View Unsubmitted Days</a></li>
                                    <?php } ?>                                    
                                    <li><a href="viewmyhistory.php" <?php echo ($currentPage == 'viewmyhistory') ? 'class="active"' : ''; ?> title="View My History">View My History</a></li>
                                </ul>
                            </nav>
                        <?php } ?>
                    </header>
                    <?php if (isset($adminSuccess) && !empty($adminSuccess)) { ?>
                        <p style="color:green; font-weight: bold;"><?php echo $adminSuccess; ?></p>
                    <?php } ?>
