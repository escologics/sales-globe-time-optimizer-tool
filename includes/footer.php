<!-- ( FOOTER BEGIN ) -->
<footer class="footer">
<!--    <p>&copy; SalesGlobe. <?php // echo date('Y');  ?> All Rights Reserved | 10945 State Bridge Road, STE 401-170 | Alpharetta, GA 30022 | Tel. 770 337 9897<?php // echo Session::get('meeting_date');  ?></p>-->
    <p>&copy; SalesGlobe. <?php echo date('Y'); ?> All Rights Reserved | salesglobe.com</p>
</footer>
<!-- ( FOOTER END ) --> 
</div>
<!-- ( CONTENT BOX END ) -->
</div>
<!-- ( CONTAINER 12 END ) -->
</div>
<!-- ( MAIN CONTAINER BEGIN ) -->
<!-- ( PLUGIN START ) -->
<script type="text/javascript" src="assets/javascripts/jquery.fancybox.pack.js"></script>
<?php if ($currentPage == 'viewMyday' || $currentPage == 'viewmyhistory' || $currentPage == 'managerDashboard' || $currentPage == 'adminDashboard' || $currentPage == 'editMeetingHours' || $currentPage == 'reporting' || $currentPage == 'EditUnsavedDay' || $currentPage == 'addRotation' || $currentPage == 'editRotation') { ?>
    <script src="assets/javascripts/jquery-ui.js"></script>
<?php } ?>
<script type="text/javascript" src="assets/javascripts/jquery.plugin.js"></script>
<script type="text/javascript" src="assets/javascripts/jquery.timeentry.js"></script>
<script type="text/javascript" src="assets/javascripts/configuration.js"></script>
<!-- ( PLUGIN END ) -->
</body>
</html>