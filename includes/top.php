<?php

require_once 'core/init.php';

DB::getInstance();
$currentPage = getPage();
if (!ini_get('date.timezone')) {
    date_default_timezone_set('GMT');
}
?>