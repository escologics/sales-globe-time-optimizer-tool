<?php

error_reporting(0);

require_once 'core/init.php';
$filename = '';
if (isset($_POST['exportAllManager'])) {

    $level_id = 3;
    $start_date = $_POST['start_date'];
    $start_date = date("y-m-d", strtotime($start_date));

    $end_date = $_POST['end_date'];
    $end_date = date("y-m-d", strtotime($end_date));

    $totalHours = Reporting::getTotalHoursByDateRangeAndLevelId($start_date, $end_date, $level_id);
    $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndLevel($start_date, $end_date, $level_id);
    $filename = 'Export-(All-Managers)-(' . $_POST['start_date'] . '- To - ' . $_POST['end_date'] . ')';
} elseif (isset($_POST['exportAllSalesRep'])) {

    $level_id = 4;
    $start_date = $_POST['start_date'];
    $start_date = date("y-m-d", strtotime($start_date));

    $end_date = $_POST['end_date'];
    $end_date = date("y-m-d", strtotime($end_date));

    $totalHours = Reporting::getTotalHoursByDateRangeAndLevelId($start_date, $end_date, $level_id);
    $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndLevel($start_date, $end_date, $level_id);
    $filename = 'Export-(All-Salesrep)-(' . $_POST['start_date'] . '- To - ' . $_POST['end_date'] . ')';
} elseif (isset($_POST['exportIndManager']) || isset($_POST['exportIndSalesRep'])) {

    $start_date = $_POST['start_date'];
    $start_date = date("y-m-d", strtotime($start_date));

    $end_date = $_POST['end_date'];
    $end_date = date("y-m-d", strtotime($end_date));

    $user_id = $_POST['manager_id'];

    if (is_array($user_id))
        $user_id = implode("", $user_id);

    $totalHours = Reporting::getTotalHoursByDateRangeAndUserId($start_date, $end_date, $user_id);
    $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndUserId($start_date, $end_date, $user_id);
    $filename = 'Export-(Indivisual-User)-(' . $_POST['start_date'] . '- To - ' . $_POST['end_date'] . ')';
}

$table = '';

if (!empty($meetingHistoryData)) {
    $table .= '<table width="500" border="1">';
    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Activity and Type Summary</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Hours</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Percent to Total</td>  
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">Difficulty Level</td>                                               
        </tr>';
    foreach ($meetingHistoryData as $meeting) {
        $totalpercnt += round($meeting['hours'] / $totalHours * 100, 2);

        if (isset($_POST['byLevel']) == 'yes') {

            $ActivityTypedata_rating = Reporting::getActivityTypeHoursByLevelId($start_date, $end_date, $level_id, $meeting['meeting_category']);
            $rate_count_rating = 0;
            foreach ($ActivityTypedata_rating as $row_count_rating) {
                $rate_count_rating += ($row_count_rating['Rate']);
            }
        } else if (isset($_POST['individual']) == 'yes') {

            $ActivityTypedata_rating = Reporting::getActivityTypeHours($start_date, $end_date, $user_id, $meeting['meeting_category']);
            $rate_count_rating = 0;
            foreach ($ActivityTypedata_rating as $row_count_rating) {
                $rate_count_rating += ($row_count_rating['Rate']);
            }
        } else if (isset($_POST['individual']) == 'yes1') {

            $ActivityTypedata_rating = Reporting::getActivityTypeHours($start_date, $end_date, $user_id, $meeting['meeting_category']);
            $rate_count_rating = 0;
            foreach ($ActivityTypedata_rating as $row_count_rating) {
                $rate_count_rating += ($row_count_rating['Rate']);
            }
        }

        $table .= '<tr>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . $meeting['name'] . '</td>
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . time_format($meeting['hours']) . '</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . round($meeting['hours'] / $totalHours * 100, 2) . '%</td>        
        <td style="padding: 15px 10px;" bgcolor="#C4EA94">' . number_format($rate_count_rating / sizeof($ActivityTypedata_rating), 2) . '</td>              
        </tr>';

        if (isset($_POST['byLevel']) == 'yes') {

            $ActivityTypedata = Reporting::getActivityTypeHoursByLevelId($start_date, $end_date, $level_id, $meeting['meeting_category']);
            $max = count($ActivityTypedata);
        } else if (isset($_POST['individual']) == 'yes') {

            $ActivityTypedata = Reporting::getActivityTypeHours($start_date, $end_date, $user_id, $meeting['meeting_category']);
            $max = count($ActivityTypedata);
        } else if (isset($_POST['individual']) == 'yes1') {

            $ActivityTypedata = Reporting::getActivityTypeHours($start_date, $end_date, $user_id, $meeting['meeting_category']);
            $max = count($ActivityTypedata);
        }


        for ($i = 0; $i < $max; $i++) {
            $table .= '<tr>';
            $table .= ' <td style="padding: 15px 10px;">' . $ActivityTypedata[$i]['name'] . '</td>';
            if (isset($ActivityTypedata[$i]['hours'])) {
                $table .= '<td style="padding: 15px 10px;">' . time_format($ActivityTypedata[$i]['hours']) . '</td>';
                $table .= '<td style="padding: 15px 10px;">' . round($ActivityTypedata[$i]['hours'] / $totalHours * 100, 2) . '%</td>';
                $table .= '<td style="padding: 15px 10px;">' . number_format($ActivityTypedata[$i]['Rate'], 2) . '</td>';
            } else {
                $table .= '<td style="padding: 15px 10px;"></td>';
            }
            $table .= ' </tr>';
        }
    }
    $table .= '<tr>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">TOTAL HOURS FOR DATE PERIOD</td>
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . time_format($totalHours) . '</td>     
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">' . $totalpercnt . '%</td> 
        <td style="font-weight:bold; border-bottom:2px solid #000000; padding: 15px 10px;" bgcolor="#E5E5E5">&nbsp;</td> 
        </tr>';
    $table .= '</table>';
} else {
    $table .= 'No Record Found';
}
header('Content-type: application/vnd.xls');
header('Content-Disposition: attachment; filename="' . $filename . '.xls"');

print $table;
