<?php
include('includes/top.php');

   $result = Archive::getSalesRepresentativesIdsUnderManagerByManagerId(Input::post('user_id'));
    $person_name = User::getFullNameByUserId(Input::post('user_id'));
    $company_name = User::getCompanyByUserId(Input::post('user_id'));
    $email = User::getEmailByUserId(Input::post('user_id'));
  $under_people = json_encode($result);
     DB::getInstance()->insertJson('archive', array(
        'action' => 'demoted',
        'entry_id' => Input::post('user_id'),
        'date_added' => date('Y-m-d', time()),
        'under_people' => $under_people,
        'previous_position' => "Manager",
        'current_position' => "Sales representative",
        'person_name' => $person_name,
        'company' => $company_name,
        'email' => $email
    ));
 
 $update = DB::getInstance()->updateByAssigned('users', Input::post('user_id'), array(
        'assigned_to' => Input::post('assigned_to')
            ));
 $updateManager =   DB::getInstance()->update('users', Input::post('user_id'), array(
            'first_name' => Input::post('first_name'),
            'last_name' => Input::post('last_name'),
            'company' => Input::post('company'),
            'level' => 4
        ));
		
		
		


if($updateManager){
	
        Redirect::to(Input::post('current_page') . '?updated=manager');
    
}
 ?>