<?php
// inclued top file for loading all classes and database connectivity
$page_title = 'Profile';
include('includes/top.php');
//check login session  if $_Session['login'] is true or false
//Session::get class is in classes/Session.php
if (!Session::get('login')) {
    //if Session is not true the it will redirect to index.php
    //Redirect::to class is in classes/Redirect.php
    Redirect::to('index.php');
}
//Check if form is posted 
//Input::exists class is in classes/Input.php
if (Input::exists()) {
    //user_type is hidden field in form to check which form is submitted
    if ($_POST['user_type'] == 'profile') {
        $err = '';
        $validate = new Validate();
        $rule = array(
            //Create rule for username
            'username' => array(
                'label' => 'Username',
                'required' => true,
                'validate' => 'username',
//                'min' => '3',
//                'max' => '20',
            ),
            'first_name' => array(
                'label' => 'First Name',
                'required' => true,
                'validate' => 'name',
                'min' => '3',
                'max' => '20',
            ),
            'last_name' => array(
                'label' => 'Last Name',
                'required' => true,
                'validate' => 'name',
                'min' => '3',
                'max' => '20',
            ),
            'company' => array(
                'label' => 'Company',
                'required' => true,
                'min' => '3',
                'max' => '80',
            ),
            'email' => array(
                'label' => 'Email',
                'required' => true,
                'validate' => 'email',
            )
        );

        if ($_POST['password'] != '' || $_POST['c_password'] != '') {
            $rule = array_merge($rule, array(
                'password' => array(
                    'label' => 'Password',
                    'required' => true,
                    'min' => '6',
                    'max' => '20',
                ),
                'c_password' => array(
                    'label' => "Confirm Password",
                    'required' => true,
                    'matches' => 'password',
                )
            ));
        }

        // Validate sumbited form  by check function
        // will return true or false 
        $validation = $validate->check($_POST, $rule);

        //check if username is not already exists
        //checkByUsername function is in classes/User.php class
        if (User::checkByUsername(Input::post('username'), Input::post('user_id')) != 0) {
            $err .= 'Username is already exists.<br />';
        }
        //check if email is not already exists
        //checkByEmail function is in classes/User.php class
        if (User::checkByEmail(Input::post('email'), Input::post('user_id')) != 0) {
            $err .= "Email address is already exists.<br />";
        }

        //if validation is passed
        if ($validation->passed() && $err == '') {



            $rule_2 = array(
                'first_name' => Input::post('first_name'),
                'last_name' => Input::post('last_name'),
                'username' => Input::post('username'),
                'email' => Input::post('email'),
                'company' => Input::post('company'),
                'industry_id' => Input::post('industry_sector'),
                'designation' => Input::post('designation'),
                    //'level' => Input::post('level'),
            );

            if ($_POST['password'] != '' || $_POST['c_password'] != '') {

                $rule_2 = array_merge($rule_2, array(
                    'password' => md5(Input::post('password'))));
            }
            DB::getInstance()->update('users', Input::post('user_id'), $rule_2);
            $success_m = "Profile updated successfully."; // success message
        } else {
            //loop through error which is passed by validate class
            foreach ($validation->errors() as $error) {
                $err .= $error . "<br>";
            }
            $success_m = "<p style='color: #e80702; font-weight: bold;'>" . $err . "</p>"; // error message
        }

        $user_type_var = 'profile';
    }
}
include('includes/header.php');
?>
<?php $userInfo = User::getAllInformationByUserId(Session::get('user_id')); ?>
<script type="text/javascript">
    function validation_password()
    {
        var password = $("#password").val();
        var confirmpassword = $("#confirmpassword").val();


        if (password.length < 6)
        {
            $("#customessage").html("Password should be minimum of 6 characters");
            return false;
        }
        if (password != confirmpassword)
        {
            $("#customessage").html("Password and confirm passowrd does not match");
            return false;
        }


        return true;
    }
</script>
<section class="createManager clearfix">
    <div class="createTab">
        <ul class="clearfix tabsNavigation">
            <li style="background: #3B3B3B; border: 1px solid #3B3B3B; color: #FFFFFF; padding: 12px 10px;">User Profile</li>
        </ul>
        <!-- ( TABS LINK END ) -->

        <div class="tabs_container clearfix">
            <div id="tab1" class="createTabContent <?php echo (!$_POST['user_type']) ? 'unique' : ''; ?> <?php echo ($_POST['user_type'] == 'profile') ? 'unique' : ''; ?>">
                <form action="" class="clearfix" id="myform" method="POST">

                    <?php if (!empty($success_m)) { ?>
                        <p style="color:green; font-weight: bold;"><?php echo $success_m; ?></p>
                    <?php } ?>

                    <?php if ($msg != "") {
                        ?>
                        <p style="color: #e80702; font-weight: bold;"><?php echo $msg; ?></p>
                    <?php } ?>



                    <div class="width_240 floatLeft">
                        <p>
                            <label for="First Name" class="req">First Name</label>
                            <input type="text" tabindex="1" name="first_name" id="firstname" value="<?php echo $userInfo['first_name']; ?>">
                        </p>
                        <p>
                            <label for="Username" class="req">Username</label>
                            <input type="text" tabindex="3" name="username" id="username" value="<?php echo $userInfo['username']; ?>">
                        </p>
                        <p>
                            <label for="Password" class="req">Change Password</label>
                            <input type="password" tabindex="5" id="password" name="password" value="">
                        </p>
                        <p>
                            <label for="Password" class="req">Company</label>
                            <input type="text"  tabindex="7" id="company" name="company" value="<?php echo $userInfo['company']; ?>">
                        </p>

                        <p>
                            <?php if ($userInfo['level'] == 3) : ?>
                                <label for="Sales Team" class="req">Sales Team</label>
                                <select tabindex="8" name="industry_sector">                               
                                    <?php
                                    $results = User::getAllIndustry();
                                    foreach ($results as $result) {
                                        ?>
                                        <option value="<?php echo $result['id']; ?>" <?php echo ($userInfo['industry_id'] == $result['id']) ? 'selected="selected"' : ''; ?>><?php echo $result['name']; ?></option>                                      
                                    <?php } ?>
                                </select>
                            <?php endif; ?>
                            <?php if ($userInfo['level'] == 4) : ?>
                                <label>Designation</label>
                                <select  name="designation" id="designation">                      
                                    <?php
                                    $results = User::getAllDesignation($userInfo['industry_id']);
                                    foreach ($results as $result) {
                                        ?>
                                        <option value="<?php echo $result['id']; ?>" <?php echo ($userInfo['designation'] == $result['id']) ? 'selected="selected"' : ''; ?>><?php echo $result['designation']; ?></option>  
                                    <?php } ?>
                                </select>                            
                            <?php endif; ?>
                        </p>
                    </div>
                    <div class="width_240 floatRight">
                        <p>
                            <label for="Last Name" class="req">Last Name</label>
                            <input type="text" tabindex="2" id="lastname" name="last_name" value="<?php echo $userInfo['last_name']; ?>">
                        </p>
                        <p>
                            <label for="Email Address" class="req">Email Address</label>
                            <input type="text" tabindex="4" id="emailaddress" name="email" value="<?php echo $userInfo['email']; ?>">
                        </p>
                        <p class="confirmForm">
                            <label for="Confirm Password" class="req">Confirm Password</label>
                            <input tabindex="6" type="password" id="confirmpassword" name="c_password" value="">
                            <input type="hidden" name="user_type" value="profile" >
                            <input type="hidden" name="user_id" value="<?php echo Session::get('user_id'); ?>" >
                            <span style="color: #e80702; font-weight: bold;" id="customessage"></span>
                        </p>
                        <?php /* ?><div style="display:inline">
                          <p style="padding:18px;">
                          <input type="submit"   id="submit" name="submit" value="Save" class="floatRight"> </p>
                          <p style="padding:18px;">
                          <input type="button"   id="cancel" value="Cancel" class="floatRight" onclick="location.href='viewMyday.php'"></p></div><?php */ ?>


                    </div>
                    <br clear="all" />

                    <div class="clearfix" style="text-align: right;  ">
                        <p style="padding: 15px 0 0 0;display: inline-block;">
                            <input type="submit" id="submit" name="submit" value="Save" class="floatRight"  onclick="return validation_password();" > </p>
                        <p style="padding: 15px 0 0 0;display: inline-block;">
                            <input type="button" id="cancel" value="Cancel" class="floatRight" onclick="location.href = 'dashboard.php'"></p></div>
                </form>
            </div>
            <!-- ( TAB 1 END ) --> 

        </div>
        <!-- ( TABS CONTAINER END ) --> 

    </div>
    <!-- ( TABS END ) --> 

</section>
<?php include('includes/footer.php'); ?>