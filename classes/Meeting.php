<?php

class Meeting {

    public static function getTotalHoursofMeeting($id) {
        $result = DB::getInstance()->getResult("SELECT SUM(hours) AS total_hours FROM meeting_detail md LEFT JOIN meeting m ON(md.`meeting_id` = m.`id`)  WHERE m.id = $id");

        if ($result) {
            return $result['total_hours'];
        }

        return false;
    }

    public static function getMeetingCategories() {
        $result = DB::getInstance()->getAllRows("Select  id, name from meeting_category");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getActivityTypeByMeetingCategoryId($id) {
        $result1 = DB::getInstance()->getAllRows("Select * from activity_type where meeting_category_id = " . $id . " and `name` != 'Other'");
        $result2 = DB::getInstance()->getAllRows("Select * from activity_type where meeting_category_id = " . $id . " and `name` = 'Other'");

        $result = array_merge($result1, $result2);

        if ($result) {
            return $result;
        }
        return false;
    }

    public function getActivityTypeByMeetingCategory() {
        $results = DB::getInstance()->getAllRows("Select * from activity_type at INNER JOIN meeting_category mc where mc.id = at.meeting_category_id");
        if ($results) {
            $data_array = array();
            foreach ($results as $key => $result) :
                $data_array[$result['id']][$key]['meeting_category_id'] = $result['meeting_category_id'];
                $data_array[$result['id']][$key]['activity_name'] = $result['1'];
                $data_array[$result['id']][$key]['activity_id'] = $result['0'];
                $data_array[$result['id']][$key]['category_name'] = $result['name'];
            endforeach;
            return $data_array;
        }
        return false;
    }

    public function getActivitynameByMeetingCategoryId($id) {
        if (isset($id) && !empty($id)) {
            $result = DB::getInstance()->getResult("Select name from activity_type where id = $id");
            return $result['name'];
        }
        return false;
    }

    public function getAccountType() {
        $result1 = DB::getInstance()->getAllRows("Select * from account_type where `name` != 'Other' order by name");
        $result2 = DB::getInstance()->getAllRows("Select * from account_type where `name` = 'Other'");
        $result = array_merge($result1, $result2);

        if ($result) {
            return $result;
        }
        return false;
    }

    public function getCustomerStatus() {
        $result1 = DB::getInstance()->getAllRows("Select * from customer_status where `name` != 'Other' order by name");
        $result2 = DB::getInstance()->getAllRows("Select * from customer_status where `name` = 'Other'");
        $result = array_merge($result1, $result2);


        if ($result) {
            return $result;
        }
        return false;
    }

    public function getProductOrService() {
        $result1 = DB::getInstance()->getAllRows("Select * from product_service where `name` != 'Other' order by name");
        $result2 = DB::getInstance()->getAllRows("Select * from product_service where `name` = 'Other'");

        $result = array_merge($result1, $result2);

        if ($result) {
            return $result;
        }
        return false;
    }

    public function getContactMode() {
        $result1 = DB::getInstance()->getAllRows("Select * from contact_mode where `name` != 'Other' order by name");
        $result2 = DB::getInstance()->getAllRows("Select * from contact_mode where `name` = 'Other'");

        $result = array_merge($result1, $result2);

        if ($result) {
            return $result;
        }
        return false;
    }

    public function getMeetingsByUserId($user_id) {

        //echo "Select *  from meeting where created_by = $user_id and status = 1"; exit;
        if (isset($user_id) && !empty($user_id)) {
           // $result = DB::getInstance()->getAllRows("Select *  from meeting where created_by = $user_id and status = 1 AND added_date > DATE(NOW()) - INTERVAL 9 DAY AND added_date <= DATE(NOW()) ORDER BY `id` DESC");
            $result = DB::getInstance()->getAllRows("Select *  from meeting where created_by = $user_id and status = 1 ORDER BY `added_date` ASC");
            return $result;
        }
        return false;
    }

    public function getMeetingsByUserIdAndDate($user_id, $date) {

        //echo "Select *  from meeting where created_by = $user_id and status = 1"; exit;
        if (isset($user_id) && !empty($user_id)) {
            $result = DB::getInstance()->getAllRows("Select * from meeting where created_by = $user_id and added_date = '$date' and status = 1 ORDER BY `id` ASC");
            return $result;
        }
        return false;
    }

    public function getMeetingsByid($start_date, $end_date, $user_id) {
        if (isset($user_id) && !empty($user_id)) {
            if ($user_id > 0) {
                $result = DB::getInstance()->getAllRows("Select * from meeting where created_by = $user_id  and status = 0 and added_date BETWEEN '$start_date' and '$end_date'");
            } else {
                $result = DB::getInstance()->getAllRows("Select * from meeting where id = $id and status = 0");
            }

            return $result;
        }
        return false;
    }

    public function getMeetingDetailsByMeetingId($meeting_id) {
        if (isset($meeting_id) && !empty($meeting_id)) {
            $result = DB::getInstance()->getAllRows("Select *  from meeting_detail where meeting_id = $meeting_id ORDER BY meeting_category ASC");
            return $result;
        }
        return false;
    }

    public function checkMeetingDetailsByMeetingId($meeting_id) {
        if (isset($meeting_id) && !empty($meeting_id)) {
            $result = DB::getInstance()->getResult("SELECT COUNT(*) AS total_rows FROM meeting_detail WHERE meeting_id = $meeting_id");
            return $result['total_rows'];
        }
        return false;
    }

    public function getMeetingDetailsById($id) {
        if (isset($id) && !empty($id)) {
            $result = DB::getInstance()->getResult("Select *  from meeting_detail where id = $id");
            return $result;
        }
        return false;
    }

    public function checkMeetingByDate($date, $user_id) {
        if (isset($date) && !empty($date) && !empty($user_id)) {
            $result = DB::getInstance()->rowCount("Select * from meeting where added_date = '$date' and created_by = $user_id");
            return $result;
        }
        return false;
    }

    public function checkMeetingStatusByDateAndUserId($date, $user_id) {
        if (isset($date) && !empty($date) && !empty($user_id)) {
            $result = DB::getInstance()->getResult("Select status from meeting where added_date = '$date' and created_by = $user_id");
            return $result['status'];
        }
        return false;
    }

    public function checkMeetingStatusByselectedDateAndUserId($date, $user_id) {
        if (isset($date) && !empty($date) && !empty($user_id)) {
            $result = DB::getInstance()->getResult("Select * from meeting where added_date = '$date' and created_by = $user_id");
            return $result;
        }
        return false;
    }

    public function getMeetingCategoryNameById($meeting_category_id) {
        if (isset($meeting_category_id) && !empty($meeting_category_id)) {
            $result = DB::getInstance()->getResult("Select name from meeting_category where id = $meeting_category_id");
            return $result['name'];
        }
        return false;
    }

    public function getMeetingActivityNameById($meeting_activity_type_id) {
        if (isset($meeting_activity_type_id) && !empty($meeting_activity_type_id)) {
            $result = DB::getInstance()->getResult("Select name from activity_type where id = $meeting_activity_type_id");
            return $result['name'];
        }
        return false;
    }

    public function getMeetingIdByDateAndUserId($date, $user_id) {
        if (isset($date) && !empty($date) && !empty($user_id)) {
            $result = DB::getInstance()->getResult("Select id from meeting where added_date = '$date' and created_by = $user_id");
            return $result['id'];
        }
        return false;
    }

    public function getTotalTimeByDateAndUserId($date, $user_id) {
        if (isset($date) && !empty($date) && !empty($user_id)) {
            $result = DB::getInstance()->getResult("SELECT SUM( hours ) AS totalhours
                                                        FROM meeting m
                                                        JOIN meeting_detail md ON ( m.id = md.meeting_id ) 
                                                        WHERE m.added_date =  '$date'
                                                        AND m.created_by =$user_id");
            return $result['totalhours'];
        }
        return false;
    }

    public function getTotalHoursByMeetingId($meeting_id) {
        if (isset($meeting_id) && !empty($meeting_id)) {
            $result = DB::getInstance()->getResult("SELECT SUM( hours ) AS totalhours
                                                        FROM meeting m
                                                        JOIN meeting_detail md ON ( m.id = md.meeting_id ) 
                                                        WHERE md.meeting_id =  '$meeting_id'
                                                       ");
            //return number_format($result['totalhours'], 2);
            //return $result['totalhours'];
            return number_format($result['totalhours'], 2, '.', '');
        }
        return false;
    }

    public function getMeetingCategoryById($id) {
        if (isset($id) && !empty($id)) {
            $result = DB::getInstance()->getResult("Select meeting_category from meeting_detail where id = $id");
            return $result['meeting_category'];
        }
        return false;
    }

    public function getMeetingactivitycommentById($id) {
        if (isset($id) && !empty($id)) {
            $result = DB::getInstance()->getResult("Select * from meeting_detail where id = $id");
            return $result['activity_comment'];
        }
        return false;
    }

    public function getUserIdbyMeetingId($id) {
        if (isset($id) && !empty($id)) {
            $result = DB::getInstance()->getResult("Select created_by from meeting where id = $id");
            return $result['created_by'];
        }
        return false;
    }

    public function getUserIdbyMeetingdetailId($id) {
        if (isset($id) && !empty($id)) {
            $result = DB::getInstance()->getResult("Select meeting_id from meeting_detail where id = $id");
            $meeting_id = $result['meeting_id'];
            $resultt = DB::getInstance()->getResult("Select * from meeting where id = $meeting_id");
            return $resultt;
        }
        return false;
    }

    public function GetMeeting($activity_type, $date, $meeting_id, $user) {
        if (isset($activity_type) && !empty($activity_type)) {
            $result = DB::getInstance()->GetSingleResult("SELECT * FROM `meeting_detail` WHERE `activity_type` = $activity_type AND `added_date` = '$date' AND `meeting_id` = $meeting_id AND `created_byuser` = $user");
            if ($result) {
                return $result;
            }
            return false;
        }
        return false;
    }
}

?>