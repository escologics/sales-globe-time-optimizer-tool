<?php

class Validate {

    private $_passed = false,
            $_errors = array(),
            $_db = null;

    public function __construct() {
        $this->_db = DB::getInstance();
    }

    public function check($source, $items = array()) {       
        foreach ($items as $item => $rules) {
            foreach ($rules as $rule => $rule_value) {
                $value = trim($source[$item]);

                if ($rule === 'required' && empty($value)) {
                    $this->addError($rules['label'] . " is required.");
                } elseif (!empty($value)) {
                    switch ($rule) {
                        case 'min':
                            if (strlen($value) < $rule_value) {
                                $this->addError("{$rules['label']} must be minimum  {$rule_value} of character.");
                            }
                            break;
                        case 'max':
                            if (strlen($value) > $rule_value) {
                                $this->addError("{$rules['label']} must be maximum of {$rule_value}  character.");
                            }
                            break;
                        case 'matches':
                            if ($value != $source[$rule_value]) {
                                $this->addError("{$rules['label']} must be equal to password .");
                            }
                            break;

                        case 'unique':

                            break;
                        case 'validate':
                            if ($rule_value == 'email') {
                                if (!filter_var($value, FILTER_VALIDATE_EMAIL))
                                    $this->addError("Please insert valid email address.");
                            } elseif ($rule_value == 'name') {
                                if (preg_match("/[^a-z A-Z']+/", stripslashes($value)))
                                    $this->addError("Please insert valid {$items[$item]['label']}.");
                            } elseif ($rule_value == 'username') {
//                                if (!preg_match("/^(?=[a-z]{2})(?=.{3,20})(?=[^.]*\.?[^.]*$)(?=[^_]*_?[^_]*$)[\w.]+$/iD", $value))
//                                    $this->addError("Please insert valid username.");
                            } 
                            break;
                    }
                }
                if ($rule === 'is_selected' && !isset($source[$item])) {
                    $this->addError($rules['label'] . " is required.");
                }
            }
        }

        if (empty($this->_errors)) {
            $this->_passed = true;
        }

        return $this;
    }

    private function addError($error) {

        $this->_errors[] = $error;
    }

    public function errors() {
        return $this->_errors;
    }

    public function passed() {
        return $this->_passed;
    }

}

?>