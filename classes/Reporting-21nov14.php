<?php

class Reporting {

    public static function getMeetingHistoryByDateRangeAndUserId($start_date, $end_date, $user_id) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($user_id) && !empty($user_id)) {
            $result = DB::getInstance()->getAllRows("SELECT SUM( md.hours ) AS hours, md.meeting_category, mc.name, mc.id, md.activity_rate as Rate,md.activity_comment as Comments
                                                        FROM  `meeting` m
                                                        JOIN meeting_detail md ON ( m.id = md.meeting_id ) 
                                                        JOIN meeting_category mc ON ( mc.id = md.meeting_category )                                                         
                                                        WHERE m.STATUS = 0
                                                        AND created_by = $user_id
                                                        AND m.added_date
                                                        BETWEEN  '$start_date'
                                                        AND  '$end_date' Group by meeting_category");
            return $result;
        }
        return false;
    }

    public static function getActivityTypeHours($start_date, $end_date, $user_id, $meeting_category) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($user_id) && !empty($user_id) && isset($meeting_category) && !empty($meeting_category)) {
            $result = DB::getInstance()->getAllRows("SELECT sum(md.hours) as hours, at.name, at.id, md.activity_rate as Rate FROM
                                                        `meeting_detail` md 
                                                         join activity_type at on(at.id = md.activity_type) 
                                                         join meeting m on(m.id = md.meeting_id) 
                                                         where meeting_category = $meeting_category 
                                                         And m.created_by = $user_id 
                                                        And m.status = 0
                                                         And m.added_date
                                                        BETWEEN  '$start_date'
                                                        AND  '$end_date'
                                                        group by activity_type");
            return $result;
        }
        return false;
    }

    public static function getAccountTypeHours($start_date, $end_date, $user_id, $meeting_category) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($user_id) && !empty($user_id) && isset($meeting_category) && !empty($meeting_category)) {
            $result = DB::getInstance()->getAllRows("SELECT sum(md.hours) as hours, at.name, at.id FROM
                                                        `meeting_detail` md 
                                                         join account_type at on(at.id = md.account_type) 
                                                         join meeting m on(m.id = md.meeting_id) 
                                                         where meeting_category = $meeting_category 
                                                         And m.created_by = $user_id
                                                         And m.status = 0
                                                         And m.added_date
                                                         
                                                        BETWEEN  '$start_date'
                                                        AND  '$end_date'
                                                        group by account_type");
            return $result;
        }
        return false;
    }

    public static function getCustomerStatusHours($start_date, $end_date, $user_id, $meeting_category) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($user_id) && !empty($user_id) && isset($meeting_category) && !empty($meeting_category)) {
            $result = DB::getInstance()->getAllRows("SELECT sum(md.hours) as hours, at.name, at.id FROM
                                                        `meeting_detail` md 
                                                         join customer_status at on(at.id = md.customer_status) 
                                                         join meeting m on(m.id = md.meeting_id) 
                                                         where meeting_category = $meeting_category 
                                                         And m.created_by = $user_id 
                    And m.status = 0
                                                         And m.added_date
                                                         
                                                        BETWEEN  '$start_date'
                                                        AND  '$end_date'
                                                        group by customer_status");
            return $result;
        }
        return false;
    }

    public static function getProductServiceHours($start_date, $end_date, $user_id, $meeting_category) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($user_id) && !empty($user_id) && isset($meeting_category) && !empty($meeting_category)) {
            $result = DB::getInstance()->getAllRows("SELECT sum(md.hours) as hours, at.name, at.id FROM
                                                        `meeting_detail` md 
                                                         join product_service at on(at.id = md.product_service) 
                                                         join meeting m on(m.id = md.meeting_id) 
                                                         where meeting_category = $meeting_category 
                                                         And m.created_by = $user_id 
                      And m.status = 0
                                                         And m.added_date
                                                       
                                                        BETWEEN  '$start_date'
                                                        AND  '$end_date'
                                                        group by product_service ORDER BY at.`name`");
            return $result;
        }
        return false;
    }

    public static function getContactModeHours($start_date, $end_date, $user_id, $meeting_category) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($user_id) && !empty($user_id) && isset($meeting_category) && !empty($meeting_category)) {
            $result = DB::getInstance()->getAllRows("SELECT sum(md.hours) as hours, at.name, at.id FROM
                                                        `meeting_detail` md 
                                                         join contact_mode at on(at.id = md.contact_mode) 
                                                         join meeting m on(m.id = md.meeting_id) 
                                                         where meeting_category = $meeting_category 
                                                         And m.created_by = $user_id 
                      And m.status = 0
                                                         And m.added_date
                                                       
                                                        BETWEEN  '$start_date'
                                                        AND  '$end_date'
                                                        group by contact_mode");
            return $result;
        }
        return false;
    }

    public static function getTotalHoursByDateRangeAndUserId($start_date, $end_date, $user_id) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($user_id) && !empty($user_id)) {
            $result = DB::getInstance()->getResult("SELECT SUM( md.hours ) AS hours
                                                        FROM  `meeting` m
                                                        JOIN meeting_detail md ON ( m.id = md.meeting_id ) 
                                                        JOIN meeting_category mc ON ( mc.id = md.meeting_category ) 
                                                        WHERE m.STATUS =0
                                                        AND created_by = $user_id
                                                        AND m.added_date
                                                        BETWEEN  '$start_date'
                                                        AND  '$end_date'");
            return $result['hours'];
        }
        return false;
    }

    /*
     * Sales Representative Section Assinged by logged in manager
     *  */

    public static function getMeetingHistoryByDateRangeAndLevelAndManager($start_date, $end_date, $level_id, $manager_id) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($level_id) && !empty($level_id) && isset($manager_id) && !empty($manager_id)) {
            $result = DB::getInstance()->getAllRows("SELECT SUM( md.hours ) AS hours, md.meeting_category, m.`created_by`, mc.name, mc.id
                                                            FROM `meeting` m 
                                                            JOIN meeting_detail md 
                                                            ON ( m.id = md.meeting_id ) 
                                                            JOIN meeting_category mc 
                                                            ON ( mc.id = md.meeting_category ) 
                                                            JOIN users u 
                                                            ON (u.id = m.created_by)
                                                            WHERE m.STATUS = 0 
                                                            AND u.level = $level_id
															AND u.assigned_to = $manager_id
															And u.status = 1
                                                            AND m.added_date BETWEEN '$start_date' AND '$end_date' 
                                                            GROUP BY meeting_category");
            return $result;
        }
        return false;
    }

    public static function getTotalHoursByDateRangeAndLevelIdAndManager($start_date, $end_date, $level_id, $manager_id) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($level_id) && !empty($level_id) && isset($manager_id) && !empty($manager_id)) {
            $result = DB::getInstance()->getResult("SELECT SUM( md.hours ) AS hours 
                                                            FROM `meeting` m 
                                                            JOIN meeting_detail md 
                                                            ON ( m.id = md.meeting_id ) 
                                                            JOIN meeting_category mc 
                                                            ON ( mc.id = md.meeting_category )
                                                            JOIN users u 
                                                            ON (u.id = m.created_by) 
                                                            WHERE m.STATUS = 0 
                                                            AND u.level = $level_id
                                                            AND u.assigned_to = $manager_id
															And u.status = 1
                                                            AND m.added_date BETWEEN '$start_date' 
                                                            AND '$end_date'");
            return $result['hours'];
        }
        return false;
    }

    public static function getActivityTypeHoursByLevelIdAndManager($start_date, $end_date, $level_id, $meeting_category, $manager_id) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($level_id) && !empty($level_id) && isset($meeting_category) && !empty($meeting_category) && isset($manager_id) && !empty($manager_id)) {
            $result = DB::getInstance()->getAllRows("SELECT SUM(md.hours) AS hours, at.name, at.id 
                                                            FROM `meeting_detail` md 
                                                            JOIN activity_type at 
                                                            ON(at.id = md.activity_type) 
                                                            JOIN meeting m ON(m.id = md.meeting_id) 
                                                            JOIN users u 
                                                            ON (u.id = m.created_by) 
                                                            WHERE meeting_category = $meeting_category 
                                                            AND m.status = 0 
                                                            AND u.level = $level_id
                                                            AND u.assigned_to = $manager_id
                                                            AND m.added_date BETWEEN '$start_date' AND '$end_date' 
                                                            GROUP BY activity_type");
            return $result;
        }
        return false;
    }

    public static function getAccountTypeHoursByLevelIdAndManager($start_date, $end_date, $level_id, $meeting_category, $manager_id) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($level_id) && !empty($level_id) && isset($meeting_category) && !empty($meeting_category) && isset($manager_id) && !empty($manager_id)) {
            $result = DB::getInstance()->getAllRows("SELECT SUM(md.hours) AS hours, at.name, at.id 
                                                            FROM `meeting_detail` md 
                                                            JOIN account_type at 
                                                            ON(at.id = md.account_type) 
                                                            JOIN meeting m ON(m.id = md.meeting_id) 
                                                            JOIN users u 
                                                            ON (u.id = m.created_by) 
                                                            WHERE meeting_category = $meeting_category 
                                                            AND m.status = 0 
                                                            AND u.level = $level_id
                                                            AND u.assigned_to = $manager_id
                                                            AND m.added_date BETWEEN '$start_date' AND '$end_date' 
                                                            GROUP BY account_type");
            return $result;
        }
        return false;
    }

    public static function getCustomerStatusHoursByLevelIdAndManager($start_date, $end_date, $level_id, $meeting_category, $manager_id) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($level_id) && !empty($level_id) && isset($meeting_category) && !empty($meeting_category) && isset($manager_id) && !empty($manager_id)) {
            $result = DB::getInstance()->getAllRows("SELECT SUM(md.hours) AS hours, at.name, at.id 
                                                            FROM `meeting_detail` md 
                                                            JOIN customer_status at 
                                                            ON(at.id = md.customer_status) 
                                                            JOIN meeting m ON(m.id = md.meeting_id) 
                                                            JOIN users u 
                                                            ON (u.id = m.created_by) 
                                                            WHERE meeting_category = $meeting_category 
                                                            AND m.status = 0 
                                                            AND u.level = $level_id
                                                            AND u.assigned_to = $manager_id
                                                            AND m.added_date BETWEEN '$start_date' AND '$end_date' 
                                                            GROUP BY customer_status");
            return $result;
        }
        return false;
    }

    public static function getProductServiceHoursByLevelIdAndManager($start_date, $end_date, $level_id, $meeting_category, $manager_id) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($level_id) && !empty($level_id) && isset($meeting_category) && !empty($meeting_category) && isset($manager_id) && !empty($manager_id)) {
            $result = DB::getInstance()->getAllRows("SELECT SUM(md.hours) AS hours, at.name, at.id 
                                                            FROM `meeting_detail` md 
                                                            JOIN product_service at 
                                                            ON(at.id = md.product_service) 
                                                            JOIN meeting m ON(m.id = md.meeting_id) 
                                                            JOIN users u 
                                                            ON (u.id = m.created_by) 
                                                            WHERE meeting_category = $meeting_category 
                                                            AND m.status = 0 
                                                            AND u.level = $level_id
                                                            AND u.assigned_to = $manager_id
                                                            AND m.added_date BETWEEN '$start_date' AND '$end_date' 
                                                            GROUP BY product_service");
            return $result;
        }
        return false;
    }

    public static function getContactModeHoursByLevelIdAndManager($start_date, $end_date, $level_id, $meeting_category, $manager_id) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($level_id) && !empty($level_id) && isset($meeting_category) && !empty($meeting_category) && isset($manager_id) && !empty($manager_id)) {
            $result = DB::getInstance()->getAllRows("SELECT SUM(md.hours) AS hours, at.name, at.id 
                                                            FROM `meeting_detail` md 
                                                            JOIN contact_mode at 
                                                            ON(at.id = md.contact_mode) 
                                                            JOIN meeting m ON(m.id = md.meeting_id) 
                                                            JOIN users u 
                                                            ON (u.id = m.created_by) 
                                                            WHERE meeting_category = $meeting_category 
                                                            AND m.status = 0 
                                                            AND u.level = $level_id
                                                            AND u.assigned_to = $manager_id
                                                            AND m.added_date BETWEEN '$start_date' AND '$end_date' 
                                                            GROUP BY contact_mode");
            return $result;
        }
        return false;
    }

    /*
     * All function to get data by level id
     *  */

    public static function getMeetingHistoryByDateRangeAndLevel($start_date, $end_date, $level_id) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($level_id) && !empty($level_id)) {
            $result = DB::getInstance()->getAllRows("SELECT SUM( md.hours ) AS hours, md.meeting_category, m.`created_by`, mc.name, mc.id, md.activity_rate as Rate,md.activity_comment as Comments
                                                            FROM `meeting` m 
                                                            JOIN meeting_detail md 
                                                            ON ( m.id = md.meeting_id ) 
                                                            JOIN meeting_category mc 
                                                            ON ( mc.id = md.meeting_category ) 
                                                            JOIN users u 
                                                            ON (u.id = m.created_by)
                                                            WHERE m.STATUS = 0 
															And u.status = 1
                                                            AND u.level = $level_id
                                                            AND m.added_date BETWEEN '$start_date' AND '$end_date' 
                                                            GROUP BY meeting_category");
            return $result;
        }
        return false;
    }

    public static function getTotalHoursByDateRangeAndLevelId($start_date, $end_date, $level_id) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($level_id) && !empty($level_id)) {
            $result = DB::getInstance()->getResult("SELECT SUM( md.hours ) AS hours 
                                                            FROM `meeting` m 
                                                            JOIN meeting_detail md 
                                                            ON ( m.id = md.meeting_id ) 
                                                            JOIN meeting_category mc 
                                                            ON ( mc.id = md.meeting_category )
                                                            JOIN users u 
                                                            ON (u.id = m.created_by) 
                                                            WHERE m.STATUS = 0 
                                                            AND u.level = $level_id
															And u.status = 1
                                                            AND m.added_date BETWEEN '$start_date' 
                                                            AND '$end_date'");
            return $result['hours'];
        }
        return false;
    }

    public static function getActivityTypeHoursByLevelId($start_date, $end_date, $level_id, $meeting_category) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($level_id) && !empty($level_id) && isset($meeting_category) && !empty($meeting_category)) {
            $result = DB::getInstance()->getAllRows("SELECT SUM(md.hours) AS hours, at.name, at.id 
                                                            FROM `meeting_detail` md 
                                                            JOIN activity_type at 
                                                            ON(at.id = md.activity_type) 
                                                            JOIN meeting m ON(m.id = md.meeting_id) 
                                                            JOIN users u 
                                                            ON (u.id = m.created_by) 
                                                            WHERE meeting_category = $meeting_category 
                                                            AND m.status = 0 
                                                            AND u.level = $level_id
                                                            AND m.added_date BETWEEN '$start_date' AND '$end_date' 
                                                            GROUP BY activity_type");
            return $result;
        }
        return false;
    }

    public static function getAccountTypeHoursByLevelId($start_date, $end_date, $level_id, $meeting_category) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($level_id) && !empty($level_id) && isset($meeting_category) && !empty($meeting_category)) {
            $result = DB::getInstance()->getAllRows("SELECT SUM(md.hours) AS hours, at.name, at.id 
                                                            FROM `meeting_detail` md 
                                                            JOIN account_type at 
                                                            ON(at.id = md.account_type) 
                                                            JOIN meeting m ON(m.id = md.meeting_id) 
                                                            JOIN users u 
                                                            ON (u.id = m.created_by) 
                                                            WHERE meeting_category = $meeting_category 
                                                            AND m.status = 0 
                                                            AND u.level = $level_id
                                                            AND m.added_date BETWEEN '$start_date' AND '$end_date' 
                                                            GROUP BY account_type");
            return $result;
        }
        return false;
    }

    public static function getCustomerStatusHoursByLevelId($start_date, $end_date, $level_id, $meeting_category) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($level_id) && !empty($level_id) && isset($meeting_category) && !empty($meeting_category)) {
            $result = DB::getInstance()->getAllRows("SELECT SUM(md.hours) AS hours, at.name, at.id 
                                                            FROM `meeting_detail` md 
                                                            JOIN customer_status at 
                                                            ON(at.id = md.customer_status) 
                                                            JOIN meeting m ON(m.id = md.meeting_id) 
                                                            JOIN users u 
                                                            ON (u.id = m.created_by) 
                                                            WHERE meeting_category = $meeting_category 
                                                            AND m.status = 0 
                                                            AND u.level = $level_id
                                                            AND m.added_date BETWEEN '$start_date' AND '$end_date' 
                                                            GROUP BY customer_status");
            return $result;
        }
        return false;
    }

    public static function getProductServiceHoursByLevelId($start_date, $end_date, $level_id, $meeting_category) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($level_id) && !empty($level_id) && isset($meeting_category) && !empty($meeting_category)) {
            $result = DB::getInstance()->getAllRows("SELECT SUM(md.hours) AS hours, at.name, at.id 
                                                            FROM `meeting_detail` md 
                                                            JOIN product_service at 
                                                            ON(at.id = md.product_service) 
                                                            JOIN meeting m ON(m.id = md.meeting_id) 
                                                            JOIN users u 
                                                            ON (u.id = m.created_by) 
                                                            WHERE meeting_category = $meeting_category 
                                                            AND m.status = 0 
                                                            AND u.level = $level_id
                                                           
                                                            AND m.added_date BETWEEN '$start_date' AND '$end_date' 
                                                            GROUP BY product_service");
            return $result;
        }
        return false;
    }

    public static function getContactModeHoursByLevelId($start_date, $end_date, $level_id, $meeting_category) {
        if (isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date) && isset($level_id) && !empty($level_id) && isset($meeting_category) && !empty($meeting_category)) {
            $result = DB::getInstance()->getAllRows("SELECT SUM(md.hours) AS hours, at.name, at.id 
                                                            FROM `meeting_detail` md 
                                                            JOIN contact_mode at 
                                                            ON(at.id = md.contact_mode) 
                                                            JOIN meeting m ON(m.id = md.meeting_id) 
                                                            JOIN users u 
                                                            ON (u.id = m.created_by) 
                                                            WHERE meeting_category = $meeting_category 
                                                            AND m.status = 0 
                                                            AND u.level = $level_id
                                                         	 AND m.added_date BETWEEN '$start_date' AND '$end_date' 
                                                            GROUP BY contact_mode");
            return $result;
        }
        return false;
    }

    public function getActivityTypeNotInIDs($ids, $meeting_id) {
        $result = DB::getInstance()->getAllRows("Select * from activity_type where meeting_category_id = $meeting_id and id NOT IN  ($ids)");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getAccountTypeNotInIDs($ids) {
        $result = DB::getInstance()->getAllRows("Select * from account_type where id NOT IN ($ids) order by id");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getCustomerStatusNotInIDs($ids) {
        $result = DB::getInstance()->getAllRows("Select * from customer_status where id NOT IN ($ids) order by id");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getProductServiceNotInIDs($ids) {
        $result = DB::getInstance()->getAllRows("Select * from product_service where id NOT IN ($ids) order by id");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getContactModeNotInIDs($ids) {
//echo "Select * from contact_mode where id NOT IN ($ids) order by id"; exit;
        $result = DB::getInstance()->getAllRows("Select * from contact_mode where id NOT IN ($ids) order by id");
        if ($result) {
            return $result;
        }
        return false;
    }

}

?>