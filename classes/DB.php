<?php

class DB {

    private static $_instance = null;
    private $_pdo,
            $_query,
            $_error = false,
            $_results,
            $_count = 0;

    // Create construct for connecting data when class will be instantiate
    private function __construct() {

        //connect with server
        $connection = mysql_connect(Config::get('mysql/host'), Config::get('mysql/username'), Config::get('mysql/password'));

        //if database is connected
        if ($connection) {
            //if server is connected select database
            $database = mysql_select_db(Config::get('mysql/db'));
            if (!$database) {
                //die and return mysql error
                die(mysql_error());
            }
        } else {
            //die and return mysql error
            die(mysql_error());
        }
    }

    /*
     * Create query function to run any query 
     */

    public function query($sql) {
        // check parameter if its not empty
        if (isset($sql) && !empty($sql)) {
            // run query and return true or false
            $query = mysql_query($sql);
            //if mysql_query return true
            if ($query)
                return $query;
            else
                die(mysql_error()); // else die and return mysql error
        }
        return false;
    }

    // end here


    /*
     *  Create insert function for insert data
     * parameter: tablename, and fields
     */
    public function insert($table, $fields = array()) {
        // if fields exists
        if (count($fields)) {
            //get array keys from field
            $keys = array_keys($fields);
            $values = '';
            $x = 1;

            foreach ($fields as $field) {
                $values .= '"' . $field . '"';
                if ($x < count($fields)) {
                    $values .= ', ';
                }
                $x++;
            }
            $sql = "Insert INTO $table (`" . implode('`, `', $keys) . "`) VALUES ({$values})";

            if ($this->query($sql))
                return mysql_insert_id();
            else
                die(mysql_error());
        }

        return false;
    }

    //ends here


    /*
     * Create update function for run update query
     * Parameter: tablename, Affeceted row id, and updated data
     */

    public function update($table, $id, $fields) {
        $set = '';
        $x = 1;
        foreach ($fields as $name => $value) {
            $set .= "{$name} = '{$value}'";
            if ($x < count($fields)) {
                $set .= ', ';
            }
            $x++;
        }

        $sql = "Update {$table} SET {$set} where id = $id";
        if ($this->query($sql))
            return true;
        else
            return die(mysql_error());


//        if(!$this->query($sql, $fields)->error()){
//            return true;
//            
//        }
        //return false;
    }

    //end here

    /*
     * Create delete function for delete row will take two paramenter
     * Paramenter: tablename, id
     */
    public function delete($table, $id) {
        $sql = "Delete from {$table} where id = $id";
        if ($this->query($sql)) {
            return true;
        }

        return false;
    }
    
    public function deleteBatch($table, $column, $id) {
        $sql = "Delete from {$table} where $column = $id";
        if ($this->query($sql)) {
            return true;
        }

        return false;
    }

    //end here

    /*
     * Create function to update user table by assigned id.
     *  */
    public function updateByAssigned($table, $id, $fields) {
        $set = '';
        $x = 1;
        foreach ($fields as $name => $value) {
            $set .= "{$name} = '{$value}'";
            if ($x < count($fields)) {
                $set .= ', ';
            }
            $x++;
        }

        $sql = "Update {$table} SET {$set} where assigned_to = $id";
        if ($this->query($sql))
            return true;
        else
            return die(mysql_error());
    }

    // end here

    /*
     * Create function to update user by by email address
     * Paramenter: table name, email, and fields
     */
    public function updateByEmail($table, $email, $fields) {
        $set = '';
        $x = 1;
        foreach ($fields as $name => $value) {
            $set .= "{$name} = '{$value}'";
            if ($x < count($fields)) {
                $set .= ', ';
            }
            $x++;
        }

        $sql = "Update {$table} SET {$set} where email = '$email'";

        if ($this->query($sql))
            return true;
        else
            return die(mysql_error());

        echo $sql;
//        if(!$this->query($sql, $fields)->error()){
//            return true;
//            
//        }
        //return false;
    }

    //ends here


    /*
     * Create function to count rows of any query
     */
    public function rowCount($sql) {
        if (isset($sql) && !empty($sql)) {
            $result = $this->query($sql);
            return mysql_num_rows($result);
        }
        return false;
    }

    /// ends here

    /*
     * Create function to get all rows of any query
     */
    public function getAllRows($sql) {
        if (isset($sql) && !empty($sql)) {
            $rows = array();
            $results = $this->query($sql);
            while ($result = mysql_fetch_array($results)) {
                $rows[] = $result;
            }

            return $rows;
        }
        return false;
    }
    
    public function GetAllResults($sql) {
        if (isset($sql) && !empty($sql)) {
            $rows = array();
            $results = $this->query($sql);
            while ($result = mysql_fetch_array($results, MYSQL_ASSOC)) {
                $rows[] = $result;
            }

            return $rows;
        }
        return false;
    }

    // Create function to get first row of any table
    public function getResult($sql) {
        if (isset($sql) && !empty($sql)) {
            $result = $this->query($sql);
            return mysql_fetch_array($result);
        }
        return false;
    }
    
    public function GetSingleResult($sql) {
        if (isset($sql) && !empty($sql)) {
            $result = $this->query($sql);
            return mysql_fetch_array($result, MYSQL_ASSOC);
        }
        return false;
    }

    public static function getInstance() {
        if (!isset(self::$_instance)) {
            self::$_instance = new DB();
        }
        return self::$_instance;
    }

    public function insertJson($table, $fields = array()) {
        // if fields exists
        if (count($fields)) {
            //get array keys from field
            $keys = array_keys($fields);
            $values = '';
            $x = 1;

            foreach ($fields as $field) {
                $values .= "'$field'";
                if ($x < count($fields)) {
                    $values .= ', ';
                }
                $x++;
            }

            $sql = "Insert INTO $table (`" . implode('`, `', $keys) . "`) VALUES ({$values})";



            if ($this->query($sql))
                return true;
            else
                die(mysql_error());
        }

        return false;
    }

}

?>