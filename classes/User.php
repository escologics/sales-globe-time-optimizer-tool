<?php

class User {

    public static function checkByUsername($username, $user_id = 0) {
        $where = '';
        if (isset($username) && !empty($username)) {
            if ($user_id > 0) {
                $where = ' AND id != ' . $user_id;
            }
            $result = DB::getInstance()->rowCount("Select * from users where username = '$username' and status = 1 $where ");
            return $result;
        }
        return false;
    }

    public static function checkByEmail($email, $user_id = 0) {
        $where = '';
        if (isset($email) && !empty($email)) {
            if ($user_id > 0) {
                $where = ' AND id != ' . $user_id;
            }
            $result = DB::getInstance()->rowCount("Select * from users where email = '$email' and status = 1 $where ");
            return $result;
        }
        return false;
    }

    public static function getPasswordByUserName($username) {
        if (isset($username) && !empty($username)) {
            $result = DB::getInstance()->getResult("Select password from users where username = '$username' and status = 1 ");
            return $result['password'];
        }
        return false;
    }

    public static function getLevelByUserName($username) {
        if (isset($username) && !empty($username)) {
            $result = DB::getInstance()->getResult("Select level from users where username = '$username' ");
            return $result['level'];
        }
        return false;
    }

    public static function getIdByUserName($username) {
        if (isset($username) && !empty($username)) {
            $result = DB::getInstance()->getResult("Select id from users where username = '$username' ");
            return $result['id'];
        }
        return false;
    }

    public function getFullNameByUserId($user_id) {
        if (isset($user_id) && !empty($user_id)) {
            $result = DB::getInstance()->getResult("Select * from users where id = $user_id ");
            return $result['first_name'] . ' ' . $result['last_name'];
        }
        return false;
    }

    public function getDesignationByUserId($designation_id) {
        if (isset($designation_id) && !empty($designation_id)) {
            $result = DB::getInstance()->getResult("Select * from designations where id = $designation_id ");
            return $result['designation'];
        }
        return false;
    }

    public function getIndustryByUserId($industry_id) {
        if (isset($industry_id) && !empty($industry_id)) {
            $result = DB::getInstance()->getResult("Select * from industry_sectors where id = $industry_id ");
            return $result['name'];
        }
        return false;
    }

    public function getIndustriesByUserId($user_id) {
        if (isset($user_id) && !empty($user_id)) {
            $result = DB::getInstance()->GetAllResults("Select * from user_sales_team where user_id = $user_id");
            return $result;
        }
        return false;
    }

    public function getCompanyByUserId($user_id) {
        if (isset($user_id) && !empty($user_id)) {
            $result = DB::getInstance()->getResult("Select * from users where id = $user_id ");
            return $result['company'];
        }
        return false;
    }

    public function getEmailByUserId($user_id) {
        if (isset($user_id) && !empty($user_id)) {
            $result = DB::getInstance()->getResult("Select * from users where id = $user_id ");
            return $result['email'];
        }
        return false;
    }

    public function getLevelNameByLevelId($level) {
        if (isset($level) && !empty($level)) {
            switch ($level) {
                case 1:
                    return "Super Admin";
                    break;
                case 2:
                    return "Admin";
                    break;
                case 3:
                    return "Manager";
                    break;
                case 4:
                    return "Sales Representative";
                    break;
            }
        }
        return false;
    }

    public function getLevelByUserId($user_id) {
        if (isset($user_id) && !empty($user_id)) {
            $result = DB::getInstance()->getResult("Select * from users where id = $user_id ");
            return $result['level'];
        }
        return false;
    }

    public function getAllManagers() {
        $result = DB::getInstance()->getAllRows("Select * from users where level = 3 And status = 1");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getAllManagersByDateRange($start_date, $end_date) {
        $result = DB::getInstance()->getAllRows("Select * from users where level = 3 AND created_user BETWEEN '$start_date' AND '$end_date' AND status = 1 order by created_user");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getAllManagersByDay($value) {
        $text = '';
        if ($value == '6m') {
            $text = 6 . ' ' . 'MONTH';
        } elseif ($value == '12m') {
            $text = 12 . ' ' . 'MONTH';
        } else {
            $text = $value . ' ' . 'DAY';
        }

        $result = DB::getInstance()->getAllRows("Select * from users where level = 3 AND status = 1 AND created_user > DATE(NOW()) - INTERVAL $text AND created_user <= DATE(NOW()) order by created_user");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getAllusersByRotation($id, $level) {

        $result = DB::getInstance()->getAllRows("select * from rotations_users ru left join rotations r on ru.rotation_id = r.id left join users u on ru.rotation_id = r.id and u.id = ru.roation_user_id where r.id = $id and ru.user_level = $level");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getAllDesignation($industry_id) {
        $result = DB::getInstance()->getAllRows("Select * from designations where ind_id = '" . $industry_id . "'");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getAllIndustry() {
        $result = DB::getInstance()->getAllRows("Select * from industry_sectors");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getIndustryById($id) {
        $result = DB::getInstance()->getResult("Select name from industry_sectors where id = $id");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getAllSalesRepresentative($id = '') {
        if ($id == '') {
            $result = DB::getInstance()->getAllRows("Select * from users where level = '4' And status = 1");
        } else {
            $result = DB::getInstance()->getAllRows("Select * from users where assigned_to = '" . $id . "' And status = 1");
        }
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getAllSalesRepresentativeByDateRange($start_date, $end_date) {
        $result = DB::getInstance()->getAllRows("Select * from users where level = '4' AND created_user BETWEEN '$start_date' AND '$end_date' AND status = 1 order by created_user");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getAllSalesRepresentativeByDay($value) {
        $text = '';
        if ($value == '6m') {
            $text = 6 . ' ' . 'MONTH';
        } elseif ($value == '12m') {
            $text = 12 . ' ' . 'MONTH';
        } else {
            $text = $value . ' ' . 'DAY';
        }
        $result = DB::getInstance()->getAllRows("Select * from users where level = '4' AND status = 1 AND created_user > DATE(NOW()) - INTERVAL $text AND created_user <= DATE(NOW()) order by created_user");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getUsersByAssignedId($id) {
        if (isset($id) && !empty($id)) {
            $result = DB::getInstance()->getAllRows("Select * from users where assigned_to = $id And status = 1");
            if ($result) {
                return $result;
            }
        }
        return false;
    }

    public function hasSalesRep($id) {
        if (isset($id) && !empty($id)) {
            $result = DB::getInstance()->rowCount("Select * from users where assigned_to = '$id' and status = 1 ");
            return $result;
        }
        return false;
    }

    public function getAllInformationByUserId($id) {
        if (isset($id) && !empty($id)) {
            $result = DB::getInstance()->getResult("Select * from users where id = '$id' ");
            return $result;
        }
        return false;
    }

    public function getAllInformationBySalesId($id, $level) {
        if (isset($id) && !empty($id)) {
            $result = DB::getInstance()->getAllRows("Select * from users where industry_id = '$id' and status = 1 and level = $level");
            if ($result) {
                return $result;
            }
        }
        return false;
    }

    public function getAllInformationBySalesIdForManager($id, $level) {
        if (isset($id) && !empty($id)) {
            $result = DB::getInstance()->getAllRows("Select * from `user_sales_team` st JOIN users u ON ( st.user_id = u.id ) where u.status = 1 and u.level = $level and sales_team_id = $id");
            if ($result) {
                return $result;
            }
        }
        return false;
    }

    public function getAllInformationByRoleId($level) {
        if (isset($level) && !empty($level)) {
            $result = DB::getInstance()->getAllRows("Select * from users where level = '$level' ");
            return $result;
        }
        return false;
    }

    public function getAllInformationByRoleIdAndDateRange($level, $start_date, $end_date) {
        if (isset($level) && !empty($level)) {
            $result = DB::getInstance()->getAllRows("Select * from users where level = '$level' AND created_user BETWEEN '$start_date' AND '$end_date' AND status = 1 order by created_user");
            return $result;
        }
        return false;
    }

    public function getAllRotations() {
        $result = DB::getInstance()->getAllRows("Select * from rotations");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getRotationById($id) {
        $result = DB::getInstance()->GetSingleResult("Select * from rotations where id = $id");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getRotationUserByLevelAndRotationId($rotation_id, $level_id) {
        $result = DB::getInstance()->getAllRows("Select roation_user_id from rotations_users where rotation_id = $rotation_id AND user_level = $level_id");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getexistingusersById($id_rotation) {
        $result = DB::getInstance()->GetAllResults("Select id_user_rotation from check_rotation_users where id_rotation = $id_rotation");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getRotationsByUserID($id_user) {
        $result = DB::getInstance()->GetAllResults("Select id_rotation from check_rotation_users where id_user_rotation = $id_user");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getRotationsFromRotationsUsersByUserID($id_user) {
        $result = DB::getInstance()->GetAllResults("Select rotation_id from rotations_users where roation_user_id = $id_user");
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getRotationsTitleByRotationID($id_rotation) {
        $result = DB::getInstance()->GetSingleResult("Select rotation_title from rotations where id = $id_rotation");
        if ($result) {
            return $result['rotation_title'];
        }
        return false;
    }

    public function getUserByid($id, $level) {
        $result = DB::getInstance()->GetSingleResult("SELECT * FROM `users` WHERE `id` = $id AND `level` = $level");
        if ($result) {
            return $result;
        }
        return false;
    }

}
