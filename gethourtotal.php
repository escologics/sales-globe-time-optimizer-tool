<?php

include('includes/top.php');
//Get posted meeting detail id through ajax
if (isset($_POST['meeting_detail_id'])) {

//Get all meeting detail by id 
    $result = Meeting::getMeetingDetailsById($_POST['meeting_detail_id']);


//if detail is exist
    if ($result) {

        $meeting_id = $result['meeting_id'];


        //get totals hours of meeting detail by id
        $totalHours = Meeting::getTotalHoursByMeetingId($meeting_id);


        //if hours is avaible return hours else error
        if ($totalHours) {
            $totalHours = number_format((float) $totalHours, 2, '.', '');
            echo $totalHours;
        } else {

            echo "error";
        }
    }
} else {
    $user_id = $_POST['user_id'];
    $selectedDate = $_POST['selectedDate'];
    $selectedDateformat = date("Y-m-d", strtotime($selectedDate));
    $totalHours = Meeting::getTotalTimeByDateAndUserId($selectedDateformat, $user_id);
    echo $totalHours;
}
