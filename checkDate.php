<?php
include('includes/top.php');

$meeting_date = $_POST['meeting_date'];
$meeting_date = date("Y-m-d", strtotime($meeting_date));
$user_id = $_POST['user_id'];


$count = Meeting::checkMeetingByDate($meeting_date, $user_id);

$status = Meeting::checkMeetingStatusByDateAndUserId($meeting_date, $user_id);

if($count > 0 && $status == 1){
    echo json_encode('status1');
} elseif($count > 0 && $status == 0){
    echo json_encode('status0');
} elseif($count == 0){
    echo json_encode('count0');
}

?>
