<?php
$page_title = 'Archive';
include('includes/top.php');
include('includes/header.php');
?>
<section class="mainDashbord tableSec clearfix">

    <?php
    $resultData = Archive::getArchiveDataAll();

    if ($resultData) {

        foreach ($resultData as $result) {
            ?>
            <div class="archivePost">

                <div class="archiveTilte">
                    <?php if ($result['action'] == 'promoted') { ?>
                        <h2><?php echo $result['person_name'] ?> was <?php echo $result['action'] ?> from <?php echo $result['previous_position']; ?> to <?php echo $result['current_position']; ?> on <?php echo date("F, d Y", strtotime($result['date_added'])); ?></h2>
                    <?php } else if ($result['action'] == 'demoted') { ?> 

                        <h2><?php echo $result['person_name'] ?> was <?php echo $result['action'] ?> from <?php echo $result['previous_position']; ?> to <?php echo $result['current_position']; ?> on <?php echo date("F, d Y", strtotime($result['date_added'])); ?></h2>
                    <?php } else { ?>
                        <h2><?php echo $result['person_name'] ?> was <?php echo $result['action'] ?> from system by Admin on <?php echo date("F, d Y", strtotime($result['date_added'])); ?></h2>
                    <?php } ?>
                </div><!-- ( .archiveTilte end ) -->

                <div class="archiveInfo">

                    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="pinkRow">
                        <thead>
                            <tr>
                                <th align="left" width="5%">#</th>
                                <th align="left">Name</th>

                                <th align="left">Email Address</th>
                                <?php if ($result['action'] == 'promoted') { ?>
                                    <th align="left">Promotion Date</th>

                                <?php } elseif (isset($result['under_people']) && $result['under_people'] != 'false' && !empty($result['under_people'])) { ?>

                                <?php } else { ?>
                                    <th align="left">Deletion Date</th>
                                <?php } ?>
                                <th align="left">Access Role</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (isset($result['under_people']) && $result['under_people'] != 'false' && !empty($result['under_people'])) {
                                $userIdArray = json_decode($result['under_people']);
                                if (!empty($userIdArray)) {
                                    ?>   



                                    <?php
                                    $i = 1;
                                    foreach ($userIdArray as $user) {
                                        ?>
                                        <tr class="headingTrBold">
                                            <td><?php echo $i; ?></td>
                                            <td> <?php echo User::getFullNameByUserId($user); ?></td>

                                            <td><a href="mailto:<?php echo User::getEmailByUserId($user); ?>"><?php echo User::getEmailByUserId($user); ?></a></td>
                                            <?php $level_id = User::getLevelByUserId($user); ?>
                                            <td><?php echo User::getLevelNameByLevelId($level_id); ?></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>


                                <?php
                                }
                            } else {
                                ?>

                                <tr class="headingTrBold">
                                    <td>1</td>
                                    <td> <?php echo $result['person_name'] ?></td>
                                    
                                    <td><a href="<?php echo $result['email']; ?>"><?php echo $result['email']; ?></a></td>
                                    <td><?php echo date("F, d Y", strtotime($result['date_added'])); ?></td>
                                    <td><?php echo $result['previous_position']; ?></td>
                                </tr>     
        <?php } ?>
                        </tbody>


                    </table>

                </div>
            </div><!-- ( .archivePost end ) -->

        <?php } ?>

<?php } ?>   


</section>               


<?php include('includes/footer.php'); ?>   