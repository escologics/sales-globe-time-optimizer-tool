<?php
$page_title = 'View Logged Activity Hours';
include('includes/top.php');

if (!Session::get('login')) {
    Redirect::to('index.php');
}
if (Session::get('level') != 1 && Session::get('level') != 2) {
    Redirect::to('index.php');
}
include('includes/header.php');
require_once 'export_reporting.php';
$view_t = '';
$view_all = '';
if (isset($_POST['manager']) == 'View') {
    $manager_id = $_POST['manager_id'];
    $start_date = $_POST['start_date_m'];
    $start_date = date("Y-m-d", strtotime($start_date));
    $end_date = $_POST['end_date_m'];
    $end_date = date("Y-m-d", strtotime($end_date));
    if (isset($_POST['days_select'])) {
        
    } else {
        if ($_POST['m_strat_month'] == "" || $_POST['m_strat_day'] == "" || $_POST['m_strat_year'] == "" || $_POST['m_end_month'] == "" || $_POST['m_end_month'] == "" || $_POST['m_end_month'] == "")
            $err = "Please select date range.<br>";
    }
    if (empty($manager_id))
        $err .= "Please select Manager.<br>";
    if ($err != '') {
        $success_m = "<p style='color: #e80702; font-weight: bold;'>" . $err . "</p>";
    }
}

if (isset($_POST['representative']) == 'View') {
    $representative_id = $_POST['representative_id'];
    $start_date_s = $_POST['start_date_s'];
    $start_date_s = date("Y-m-d", strtotime($start_date_s));
    $end_date_s = $_POST['end_date_s'];
    $end_date = date("Y-m-d", strtotime($end_date));
    if (isset($_POST['days_select_s'])) {
        
    } else {
        if ($_POST['m_strat_month_s'] == "" || $_POST['m_strat_day_s'] == "" || $_POST['m_strat_year_s'] == "" || $_POST['m_end_month_s'] == "" || $_POST['m_end_day_s'] == "" || $_POST['m_end_year_s'] == "")
            $err_s = "Please select date range.<br>";
    }
    if (empty($representative_id))
        $err_s .= "Please select Sales Representative.<br>";
    if ($err_s != '') {
        $success_s = "<p style='color: #e80702; font-weight: bold;'>" . $err_s . "</p>";
    }
}

if (isset($_POST['sales_team_export_view']) == 'View') {
    $sales_team_id = $_POST['sales_team'];
    if (empty($sales_team_id)) {
        $success_t = "<p style='color: #e80702; font-weight: bold;'>Please Select Sales Team</p>";
    } else {
        $view_t = 1;
    }
}

if (isset($_POST['all_users_export_report']) == 'View') {
    $start_date_all = $_POST['all_users_start_date'];
    $start_date_all = date("Y-m-d", strtotime($start_date_all));
    $end_date_all = $_POST['all_users_end_date'];
    $end_date_all = date("Y-m-d", strtotime($end_date_all));
    if (isset($_POST['days_select_all_user'])) {
        $view_all = 1;
    } else {
        if ($_POST['m_strat_month_all'] == "" || $_POST['m_strat_day_all'] == "" || $_POST['m_strat_year_all'] == "" || $_POST['m_end_month_all'] == "" || $_POST['m_end_day_all'] == "" || $_POST['m_end_year_all'] == "") {
            $success_all = "Please select date range.<br>";
        } else {
            $view_all = 1;
        }
    }
}

if (isset($_POST['user_rotation_export_view']) == 'View') {
    $rotation = $_POST['rotation'];
    if (empty($rotation)) {
        $success_c = "<p style='color: #e80702; font-weight: bold;'>Please Select Rotation</p>";
    } else {
        $view_c = 1;
    }
}
?>
<script src="assets/javascripts/custom.js"></script>
<script>
    $(document).ready(function () {
        $('#button_export').click(function () {
            if ($('.hideField_m').is(':checked')) {
                if ($('#manager_id').val() == null) {
                    fancyAlert('Please select manager');
                    return false;
                }
            } else {
                var m_strat_month = $('#m_strat_month').val();
                var m_strat_day = $('#m_strat_day').val();
                var m_strat_year = $('#m_strat_year').val();
                var m_end_month = $('#m_end_month').val();
                var m_end_day = $('#m_end_day').val();
                var m_end_year = $('#m_end_year').val();
                if (!m_strat_month || !m_strat_day || !m_strat_year || !m_end_month || !m_end_day || !m_end_year) {
                    fancyAlert('Please select valid date range');
                    return false;
                }
                if ($('#manager_id').val() == null) {
                    fancyAlert('Please select manager');
                    return false;
                }
            }
        });

        $('#button_export_minify').click(function () {
            if ($('.hideField_m').is(':checked')) {
                if ($('#manager_id').val() == null) {
                    fancyAlert('Please select manager');
                    return false;
                }
            } else {
                var m_strat_month = $('#m_strat_month').val();
                var m_strat_day = $('#m_strat_day').val();
                var m_strat_year = $('#m_strat_year').val();
                var m_end_month = $('#m_end_month').val();
                var m_end_day = $('#m_end_day').val();
                var m_end_year = $('#m_end_year').val();
                if (!m_strat_month || !m_strat_day || !m_strat_year || !m_end_month || !m_end_day || !m_end_year) {
                    fancyAlert('Please select valid date range');
                    return false;
                }
                if ($('#manager_id').val() == null) {
                    fancyAlert('Please select manager');
                    return false;
                }
            }
        });

        $('#button_export_representative').click(function () {
            if ($('.hideField_s').is(':checked')) {
                if ($('#representative_id').val() == null) {
                    fancyAlert('Please select Sales Representative');
                    return false;
                }
            } else {
                var m_strat_month_s = $('#m_strat_month_s').val();
                var m_strat_day_s = $('#m_strat_day_s').val();
                var m_strat_year_s = $('#m_strat_year_s').val();
                var m_end_month_s = $('#m_end_month_s').val();
                var m_end_day_s = $('#m_end_day_s').val();
                var m_end_year_s = $('#m_end_year_s').val();
                if (!m_strat_month_s || !m_strat_day_s || !m_strat_year_s || !m_end_month_s || !m_end_day_s || !m_end_year_s) {
                    fancyAlert('Please select valid date range');
                    return false;
                }
                if ($('#representative_id').val() == null) {
                    fancyAlert('Please select Sales Representative');
                    return false;
                }
            }
        });

        $('#button_export_representative_minify').click(function () {
            if ($('.hideField_s').is(':checked')) {
                if ($('#representative_id').val() == null) {
                    fancyAlert('Please select Sales Representative');
                    return false;
                }
            } else {
                var m_strat_month_s = $('#m_strat_month_s').val();
                var m_strat_day_s = $('#m_strat_day_s').val();
                var m_strat_year_s = $('#m_strat_year_s').val();
                var m_end_month_s = $('#m_end_month_s').val();
                var m_end_day_s = $('#m_end_day_s').val();
                var m_end_year_s = $('#m_end_year_s').val();
                if (!m_strat_month_s || !m_strat_day_s || !m_strat_year_s || !m_end_month_s || !m_end_day_s || !m_end_year_s) {
                    fancyAlert('Please select valid date range');
                    return false;
                }
                if ($('#representative_id').val() == null) {
                    fancyAlert('Please select Sales Representative');
                    return false;
                }
            }
        });

        $('#sales_team_export').click(function () {
            if ($('#sales_team').val() == "") {
                fancyAlert('Please select Sales Team');
                return false;
            } else {
                $('#sales_team_reporting').attr('action', 'export_reporting.php');
            }
        });
        $('#sales_team_export_minify').click(function () {
            if ($('#sales_team').val() == "") {
                fancyAlert('Please select Sales Team');
                return false;
            } else {
                $('#sales_team_reporting').attr('action', 'export_reporting_minify.php');
            }
        });


        $('#all_users_export').click(function () {
            if ($('.hideField_all_user').is(':checked')) {
                $('#all_users_reporting').attr('action', 'export_reporting.php');
            } else {
                var m_strat_month_all = $('#m_strat_month_all').val();
                var m_strat_day_all = $('#m_strat_day_all').val();
                var m_strat_year_all = $('#m_strat_year_all').val();
                var m_end_month_all = $('#m_end_month_all').val();
                var m_end_day_all = $('#m_end_day_all').val();
                var m_end_year_all = $('#m_end_year_all').val();
                if (!m_strat_month_all || !m_strat_day_all || !m_strat_year_all || !m_end_month_all || !m_end_day_all || !m_end_year_all) {
                    fancyAlert('Please select valid date range');
                    return false;
                }
                $('#all_users_reporting').attr('action', 'export_reporting.php');
            }
        });

        $('#all_users_export_minify').click(function () {
            if ($('.hideField_all_user').is(':checked')) {
                $('#all_users_reporting').attr('action', 'export_reporting_minify.php');
            } else {
                var m_strat_month_all = $('#m_strat_month_all').val();
                var m_strat_day_all = $('#m_strat_day_all').val();
                var m_strat_year_all = $('#m_strat_year_all').val();
                var m_end_month_all = $('#m_end_month_all').val();
                var m_end_day_all = $('#m_end_day_all').val();
                var m_end_year_all = $('#m_end_year_all').val();
                if (!m_strat_month_all || !m_strat_day_all || !m_strat_year_all || !m_end_month_all || !m_end_day_all || !m_end_year_all) {
                    fancyAlert('Please select valid date range');
                    return false;
                }
                $('#all_users_reporting').attr('action', 'export_reporting_minify.php');
            }
        });


        $('#button_form').click(function () {
            $('.manager_form').attr('action', '');
        });
        $('#button_export').click(function () {
            $('.manager_form').attr('action', 'export_reporting.php');
        });
        $('#button_export_minify').click(function () {
            $('.manager_form').attr('action', 'export_reporting_minify.php');
        });

        $('#button_form_representative').click(function () {
            $('.representative_form').attr('action', '');
        });

        $('#button_export_representative').click(function () {
            $('.representative_form').attr('action', 'export_reporting.php');
        });
        $('#button_export_representative_minify').click(function () {
            $('.representative_form').attr('action', 'export_reporting_minify.php');
        });

        $('#user_rotation_export_view').click(function () {
            $('#user_export_by_rotation').attr('action', '');
        });
        $('#button_user_rotation_export').click(function () {
            if ($('#rotation_select').val() == "") {
                fancyAlert('Please select Rotation');
                return false;
            } else {
                $('#user_export_by_rotation').attr('action', 'export_reporting.php');
            }
        });
        $('#button_user_rotation_export_minify').click(function () {
            if ($('#rotation_select').val() == "") {
                fancyAlert('Please select Rotation');
                return false;
            } else {
                $('#user_export_by_rotation').attr('action', 'export_reporting_minify.php');
            }
        });

        $("#calender_image_viewhistory1").datepicker({
            showOn: "both",
            buttonImage: "assets/images/calendar.jpg",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            yearRange: "-3:+0",
            onSelect: function (selectedDate, obj) {
                var date_array = selectedDate.split("/");
                var month = date_array[0];
                var days = date_array[1];
                var year = date_array[2];
                $('#m_strat_month').val(month);
                $('#m_strat_day').val(days);
                $('#m_strat_year').val(year);
                $('#start_date_m').val(selectedDate);
            }
        });

        $("#calender_image_viewhistory2").datepicker({
            showOn: "both",
            buttonImage: "assets/images/calendar.jpg",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            yearRange: "-3:+0",
            onSelect: function (selectedDate, obj) {
                var date_array = selectedDate.split("/");
                var month = date_array[0];
                var days = date_array[1];
                var year = date_array[2];
                $('#m_end_month').val(month);
                $('#m_end_day').val(days);
                $('#m_end_year').val(year);
                $('#end_date_m').val(selectedDate);
            }
        });

        if ($('.hideField_m').is(':checked')) {
            $('#days_select_m').attr('disabled', false);
            $('#start_date_m').attr('disabled', true);
            $('#end_date_m').attr('disabled', true);
            $('#m_strat_month').attr('disabled', true);
            $('#m_strat_day').attr('disabled', true);
            $('#m_strat_year').attr('disabled', true);
            $('#m_end_month').attr('disabled', true);
            $('#m_end_day').attr('disabled', true);
            $('#m_end_year').attr('disabled', true);
        } else {

            $('#days_select_m').attr('disabled', true);
            $('#start_date_m').attr('disabled', false);
            $('#end_date_m').attr('disabled', false);
            $('#m_strat_month').attr('disabled', false);
            $('#m_strat_day').attr('disabled', false);
            $('#m_strat_year').attr('disabled', false);
            $('#m_end_month').attr('disabled', false);
            $('#m_end_day').attr('disabled', false);
            $('#m_end_year').attr('disabled', false);

        }

        $(".hideField_m").on('click', function () {
            if ($(this).is(':checked')) {
                $('#days_select_m').attr('disabled', false);
                $('#start_date_m').attr('disabled', true);
                $('#end_date_m').attr('disabled', true);
                $('#m_strat_month').attr('disabled', true);
                $('#m_strat_day').attr('disabled', true);
                $('#m_strat_year').attr('disabled', true);
                $('#m_end_month').attr('disabled', true);
                $('#m_end_day').attr('disabled', true);
                $('#m_end_year').attr('disabled', true);
            } else {

                $('#days_select_m').attr('disabled', true);
                $('#start_date_m').attr('disabled', false);
                $('#end_date_m').attr('disabled', false);
                $('#m_strat_month').attr('disabled', false);
                $('#m_strat_day').attr('disabled', false);
                $('#m_strat_year').attr('disabled', false);
                $('#m_end_month').attr('disabled', false);
                $('#m_end_day').attr('disabled', false);
                $('#m_end_year').attr('disabled', false);

            }
        });



        $(".m_date").change(function () {
            if ($(this).attr('id') == 'm_strat_month') {
                if ($(this).val() > 12) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }
            } else if ($(this).attr('id') == 'm_strat_day') {
                if ($(this).val() > daysInMonth($('#m_strat_month').val(), $('#m_strat_year').val())) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }
            } else if ($(this).attr('id') == 'm_strat_year') {
                if ($(this).val() > 2015) {
                    $(this).val('2015');
                } else if ($(this).val() < 2012) {
                    $(this).val('2015');
                }
            } else if ($(this).attr('id') == 'm_end_month') {
                if ($(this).val() > 12) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }
            } else if ($(this).attr('id') == 'm_end_day') {
                if ($(this).val() > daysInMonth($('#m_end_month').val(), $('#m_end_year').val())) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }
            } else if ($(this).attr('id') == 'm_end_year') {
                if ($(this).val() > 2015) {
                    $(this).val('2015');
                } else if ($(this).val() < 2012) {
                    $(this).val('2015');
                }
            }
            var m_strat_date = ($('#m_strat_month').val() + '/' + $('#m_strat_day').val() + '/' + $('#m_strat_year').val());
            $('#start_date_m').val(m_strat_date);
            var m_end_date = ($('#m_end_month').val() + '/' + $('#m_end_day').val() + '/' + $('#m_end_year').val());
            $('#end_date_m').val(m_end_date);
        });

        $("#calender_image_viewhistory1_s").datepicker({
            showOn: "both",
            buttonImage: "assets/images/calendar.jpg",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            yearRange: "-3:+0",
            onSelect: function (selectedDate, obj) {
                var date_array = selectedDate.split("/");
                var month = date_array[0];
                var days = date_array[1];
                var year = date_array[2];
                $('#m_strat_month_s').val(month);
                $('#m_strat_day_s').val(days);
                $('#m_strat_year_s').val(year);
                $('#start_date_s').val(selectedDate);
            }
        });

        $("#calender_image_viewhistory2_s").datepicker({
            showOn: "both",
            buttonImage: "assets/images/calendar.jpg",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            yearRange: "-3:+0",
            onSelect: function (selectedDate, obj) {
                var date_array = selectedDate.split("/");
                var month = date_array[0];
                var days = date_array[1];
                var year = date_array[2];
                $('#m_end_month_s').val(month);
                $('#m_end_day_s').val(days);
                $('#m_end_year_s').val(year);
                $('#end_date_s').val(selectedDate);
            }
        });

        if ($('.hideField_s').is(':checked')) {
            $('#days_select_s').attr('disabled', false);
            $('#start_date_s').attr('disabled', true);
            $('#end_date_s').attr('disabled', true);
            $('#m_strat_month_s').attr('disabled', true);
            $('#m_strat_day_s').attr('disabled', true);
            $('#m_strat_year_s').attr('disabled', true);
            $('#m_end_month_s').attr('disabled', true);
            $('#m_end_day_s').attr('disabled', true);
            $('#m_end_year_s').attr('disabled', true);
        } else {

            $('#days_select_s').attr('disabled', true);
            $('#start_date_s').attr('disabled', false);
            $('#end_date_s').attr('disabled', false);
            $('#m_strat_month_s').attr('disabled', false);
            $('#m_strat_day_s').attr('disabled', false);
            $('#m_strat_year_s').attr('disabled', false);
            $('#m_end_month_s').attr('disabled', false);
            $('#m_end_day_s').attr('disabled', false);
            $('#m_end_year_s').attr('disabled', false);

        }

        $(".hideField_s").on('click', function () {
            if ($(this).is(':checked')) {
                $('#days_select_s').attr('disabled', false);
                $('#start_date_s').attr('disabled', true);
                $('#end_date_s').attr('disabled', true);
                $('#m_strat_month_s').attr('disabled', true);
                $('#m_strat_day_s').attr('disabled', true);
                $('#m_strat_year_s').attr('disabled', true);
                $('#m_end_month_s').attr('disabled', true);
                $('#m_end_day_s').attr('disabled', true);
                $('#m_end_year_s').attr('disabled', true);
            } else {
                $('#days_select_s').attr('disabled', true);
                $('#start_date_s').attr('disabled', false);
                $('#end_date_s').attr('disabled', false);
                $('#m_strat_month_s').attr('disabled', false);
                $('#m_strat_day_s').attr('disabled', false);
                $('#m_strat_year_s').attr('disabled', false);
                $('#m_end_month_s').attr('disabled', false);
                $('#m_end_day_s').attr('disabled', false);
                $('#m_end_year_s').attr('disabled', false);

            }
        });

        $(".m_date_s").change(function () {
            if ($(this).attr('id') == 'm_strat_month_s') {
                if ($(this).val() > 12) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }
            } else if ($(this).attr('id') == 'm_strat_day_s') {
                if ($(this).val() > daysInMonth($('#m_strat_month_s').val(), $('#m_strat_year_s').val())) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }
            } else if ($(this).attr('id') == 'm_strat_year_s') {
                if ($(this).val() > 2015) {
                    $(this).val('2015');
                } else if ($(this).val() < 2012) {
                    $(this).val('2015');
                }
            } else if ($(this).attr('id') == 'm_end_month_s') {
                if ($(this).val() > 12) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }
            } else if ($(this).attr('id') == 'm_end_day_s') {
                if ($(this).val() > daysInMonth($('#m_end_month_s').val(), $('#m_end_year_s').val())) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }
            } else if ($(this).attr('id') == 'm_end_year_s') {
                if ($(this).val() > 2015) {
                    $(this).val('2015');
                } else if ($(this).val() < 2012) {
                    $(this).val('2015');
                }
            }

            var m_strat_date_s = ($('#m_strat_month_s').val() + '/' + $('#m_strat_day_s').val() + '/' + $('#m_strat_year_s').val());
            $('#start_date_s').val(m_strat_date_s);
            var m_end_date_s = ($('#m_end_month_s').val() + '/' + $('#m_end_day_s').val() + '/' + $('#m_end_year_s').val());
            $('#end_date_s').val(m_end_date_s);
        });

        $(".hideField_all_user").on('click', function () {
            if ($(this).is(':checked')) {
                $('#days_select_all_user').attr('disabled', false);
                $('#all_users_start_date').attr('disabled', true);
                $('#all_users_end_date').attr('disabled', true);
                $('#m_strat_month_all').attr('disabled', true);
                $('#m_strat_day_all').attr('disabled', true);
                $('#m_strat_year_all').attr('disabled', true);
                $('#m_end_month_all').attr('disabled', true);
                $('#m_end_day_all').attr('disabled', true);
                $('#m_end_year_all').attr('disabled', true);
            } else {
                $('#days_select_all_user').attr('disabled', true);
                $('#all_users_start_date').attr('disabled', false);
                $('#all_users_end_date').attr('disabled', false);
                $('#m_strat_month_all').attr('disabled', false);
                $('#m_strat_day_all').attr('disabled', false);
                $('#m_strat_year_all').attr('disabled', false);
                $('#m_end_month_all').attr('disabled', false);
                $('#m_end_day_all').attr('disabled', false);
                $('#m_end_year_all').attr('disabled', false);

            }
        });
        if ($('#hideField_all_user').is(':checked')) {
            $('#days_select_all_user').attr('disabled', false);
            $('#all_users_start_date').attr('disabled', true);
            $('#all_users_end_date').attr('disabled', true);
            $('#m_strat_month_all').attr('disabled', true);
            $('#m_strat_day_all').attr('disabled', true);
            $('#m_strat_year_all').attr('disabled', true);
            $('#m_end_month_all').attr('disabled', true);
            $('#m_end_day_all').attr('disabled', true);
            $('#m_end_year_all').attr('disabled', true);
        }
    });
    function daysInMonth(month, year) {
        return new Date(year, month, 0).getDate();
    }
</script>
<style>
    img.ui-datepicker-trigger {
        margin: -5px 0px 0px 0px;
    }
</style>
<section class="createManager clearfix">
    <div class="tabs createTab">
        <ul class="clearfix tabsNavigation">
            <li><a href="#tab1" class="<?php echo (isset($_POST['manager']) || isset($_POST['manager_export'])) ? 'unique' : ''; ?><?php echo (!$_POST) ? 'unique' : ''; ?>">Manager</a></li>
            <li><a href="#tab2" class="<?php echo ($_POST['representative']) ? 'unique' : ''; ?>">Sales Representative</a></li>
            <li><a href="#tab3" class="<?php echo ($_POST['sales_team_export_view']) ? 'unique' : ''; ?>">Team View</a></li>
            <li><a href="#tab4" class="<?php echo ($_POST['all_users_export_report']) ? 'unique' : ''; ?>">All Users</a></li>
            <li><a href="#tab5" class="<?php echo ($_POST['user_rotation_export_view']) ? 'unique' : ''; ?>">Rotation</a></li>
        </ul>

        <div class="tabs_container clearfix">
            <div id="tab1" class="<?php echo ($_POST['manager']) ? 'unique' : ''; ?><?php echo (!$_POST) ? 'unique' : ''; ?>">
                <?php
                if (empty($success_m)) {
                    if ($_POST['manager']) {
                        $start_date = $_POST['start_date_m'];
                        $start_date = date("Y-m-d", strtotime($start_date));
                        $end_date = $_POST['end_date_m'];
                        $end_date = date("Y-m-d", strtotime($end_date));
                        $users = $_POST['manager_id'];
                        $is_manager = 1;
                    } else {
                        $users = array();
                        $start_date = '0000-00-00';
                        $start_date = date("Y-m-d", strtotime($start_date));
                        $end_date = '0000-00-00';
                        $end_date = date("Y-m-d", strtotime($end_date));
                        $is_manager = 1;
                    }
                }
                foreach ($users as $user_id) {
                    if ($is_manager) {
                        if (isset($_POST['days_select'])) {
                            $totalHours_manager = Reporting::getTotalHoursByDaysAndUserId($_POST['days_select'], $user_id);
                        } else {
                            $totalHours_manager = Reporting::getTotalHoursByDateRangeAndUserId($start_date, $end_date, $user_id);
                        }
                    } else {
                        $totalHours += Reporting::getTotalHoursByDateRangeAndLevelId($start_date, $end_date, $level_id);
                    }
                }
                ?>
                <div class="filterBox clearfix">
                    <h2>Select a Date Range</h2>
                    <form action="" name="ViewUnsavedForm" id="ViewUnsavedForm" class="manager_form" method="POST">
                        <?php if (!empty($success_m)) {
                            ?>
                            <p style="color:green; font-weight: bold;"><?php echo $success_m; ?></p>
                        <?php } ?>
                        <div class="grid_6 dateArea clearfix marginNone" style="margin-bottom:0px!important;">
                            <p class="dateIcon" style="width: 160px;">                   
                                <input type="number" name="m_strat_month" id="m_strat_month" class="m_date" placeholder="mm" value="<?php echo $_POST['m_strat_month']; ?>" style="width: 38px;">
                                <input type="number" name="m_strat_day" id="m_strat_day" class="m_date" placeholder="dd" value="<?php echo $_POST['m_strat_day']; ?>" style="width: 30px;">
                                <input type="number" name="m_strat_year" id="m_strat_year" class="m_date" placeholder="yy" value="<?php echo $_POST['m_strat_year'] != "" ? $_POST['m_strat_year'] : '2015'; ?>" style="width: 47px;">
                                <input type="hidden" name="start_date_m" id="start_date_m" value="<?php echo $_POST['start_date_m'] ?>">
                                <input type="hidden" id="calender_image_viewhistory1"  class="datepicker_view_my_history"/>                              
                            </p>
                            <label>to</label>
                            <p class="dateIcon" style="width: 160px;">
                                <input type="number" name="m_end_month" id="m_end_month" class="m_date" placeholder="mm" value="<?php echo $_POST['m_end_month']; ?>" style="width: 38px;">
                                <input type="number" name="m_end_day" id="m_end_day" class="m_date" placeholder="dd" value="<?php echo $_POST['m_end_day']; ?>" style="width: 30px;">
                                <input type="number" name="m_end_year" id="m_end_year" class="m_date" placeholder="yy" value="<?php echo $_POST['m_end_year'] != "" ? $_POST['m_end_year'] : '2015'; ?>" style="width: 47px;">                    
                                <input type="hidden" name="end_date_m" id="end_date_m" value="<?php echo $_POST['end_date_m'] ?>">
                                <input type="hidden" id="calender_image_viewhistory2"  class="datepicker_view_myhistory"/>
                            </p>
                            <div class="bracklin"></div>
                            <p class="dateIcon">
                                <?php if (isset($_POST['days_select'])) { ?>
                                    <input type="checkbox" name="hideField" value="true" id="" class="hideField_m" checked="checked">
                                <?php } else { ?>
                                    <input type="checkbox" name="hideField" value="true" id="" class="hideField_m">
                                <?php } ?>
                            </p>
                            <div class="dateArea clearfix marginNone">
                                <select name="days_select" disabled="disabled" id="days_select_m">
                                    <option value="7" <?php echo ($_POST['days_select']) == 7 ? 'selected="selected"' : ''; ?>>Last 7 Days</option>
                                    <option value="30" <?php echo ($_POST['days_select']) == '30' ? 'selected="selected"' : ''; ?>>Last 30 Days</option>
                                    <option value="60" <?php echo ($_POST['days_select']) == '60' ? 'selected="selected"' : ''; ?>>Last 60 Days</option>
                                    <option value="90" <?php echo ($_POST['days_select']) == '90' ? 'selected="selected"' : ''; ?>>Last 90 Days</option>
                                    <option value="6m" <?php echo ($_POST['days_select']) == '6m' ? 'selected="selected"' : ''; ?>>Last 6 Months</option>
                                    <option value="12m" <?php echo ($_POST['days_select']) == '12m' ? 'selected="selected"' : ''; ?>>Last 12 Months</option>
                                </select>                                
                            </div>
                            <select name="manager_id[]" id="manager_id" multiple="multiple" style="margin-bottom:10px;">
                                <option value="" selected="selected" disabled>Select Manager</option>
                                <?php
                                $results = User::getAllManagers();
                                foreach ($results as $result) {
                                    ?>
                                    <option <?php echo (isset($_POST['manager_id']) && in_array($result['id'], $_POST['manager_id'])) ? 'selected="selected"' : ''; ?> value="<?php echo $result['id']; ?>"><?php echo $result['first_name'] . ' ' . $result['last_name']; ?></option>
                                <?php } ?>
                            </select>
                            <div class="centerbtn">
                                <input type="submit" name="manager" value="View" id="button_form" class="redBtn">
                                <input type="submit" name="manager_export" value="Export Activity Level" id="button_export" class="redBtn">
                                <input type="submit" name="manager_export_minify" value="Export Summary Level" id="button_export_minify" class="redBtn">
                            </div>
                            <input type="hidden" name="tab" value="Manager Activity Hours" />

                        </div>
                    </form>
                    <div class="filterBtn">
                        <div class="clearfix"><a href="editMeetingHours.php?level=manager" class="darkBtn clearfix">Edit Manager Activity Hours & Minutes</a></div>                      
                        <div id="meetingHourManagerPopup1" class="popupBox width_240">
                            <form action="export.php" method="POST">
                                <h3>Select a Date Range  </h3>
                                <p class="dateIcon">
                                    <input type="text" name="start_date" class="datepicker" placeholder="Start Date">
                                </p>
                                <p class="dateIcon">
                                    <input type="text" name="end_date" class="datepicker" placeholder="End Date">
                                    <input type="hidden" name="byLevel" value="yes">
                                </p>
                                <input type="submit" name="exportAllManager" value="Export" >
                            </form>
                        </div>                      
                        <div id="meetingHourManagerPopup2" class="popupBox width_240">
                            <form action="export.php" method="POST">
                                <h3>Select a Date Range</h3>
                                <p class="dateIcon">
                                    <input type="text" name="start_date" class="datepicker" placeholder="Start Date">
                                </p>
                                <p class="dateIcon">
                                    <input type="text" name="end_date" class="datepicker" placeholder="End Date">
                                    <input type="hidden" name="individual" value="yes">
                                </p>
                                <div>
                                    <select name="manager_id[]" id="manager_id">
                                        <option value="">Select Manager</option>
                                        <?php
                                        $results = User::getAllManagers();
                                        foreach ($results as $result) {
                                            ?>
                                            <option <?php echo ($_POST['manager_id'] == $result['id']) ? 'selected="selected"' : ''; ?> value="<?php echo $result['id']; ?>"><?php echo $result['first_name'] . ' ' . $result['last_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <input type="submit" name="exportIndManager" value="Export" >
                            </form>
                        </div>                       
                    </div>
                </div>
                <?php if (isset($totalHours_manager) && $totalHours_manager != 0) {
                    ?>
                    <p class="viewhistoryTopcontent"> Total Hours & Minutes For Selected Date Range: <?php echo m2h("i:s", $totalHours_manager); ?> </p>
                <?php } ?>              
                <?php

                if (empty($success_m)) {
                    foreach ($users as $user_id) {
                        if ($is_manager) {
                            if (isset($_POST['days_select'])) {
                                $totalHours = Reporting::getTotalHoursByDaysAndUserId($_POST['days_select'], $user_id);
                                $meetingHistoryData = Reporting::getMeetingHistoryByDaysAndUserId($_POST['days_select'], $user_id);
                                $user_full_name = User::getFullNameByUserId($user_id);
                            } else {
                                $totalHours = Reporting::getTotalHoursByDateRangeAndUserId($start_date, $end_date, $user_id);
                                $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndUserId($start_date, $end_date, $user_id);
                                $user_full_name = User::getFullNameByUserId($user_id);
                            }
                        } else {
                            $level_id = $user_id;
                            $totalHours = Reporting::getTotalHoursByDateRangeAndLevelId($start_date, $end_date, $level_id);
                            $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndLevel($start_date, $end_date, $level_id);
                            $user_full_name = User::getFullNameByUserId($user_id);
                        }
                        ?>
                        <div class="tableSec summryTable clearfix">
                            <div class="summryrow clearfix">
                                <div class="summryindent clearfix">
                                    <?php if ($meetingHistoryData) {
                                        ?>
                                        <div class="summryTableHead clearfix">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <thead>
                                                    <tr class="mainTitle">
                                                        <th align="left" class="tdCol-1"><span>Activity Summary (<?php echo $user_full_name; ?>)</span></th>
                                                        <th align="center" class="tdCol-2"><span>Hours & Minutes</span></th>
                                                        <th class="tdCol-3"><span>% of Total Hours & Minutes for Selected Date Range</span></th>
                                                        <th align="center" class="tdCol-2"><span>Difficulty Level</span></th>            
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    <?php } ?>                                   
                                    <?php
                                    if ($meetingHistoryData) {
                                        $csvdata[] = $meetingHistoryData;
                                        $totalhoursAzeem =0;
                                        foreach ($meetingHistoryData as $meeting) {
                                            if (isset($_POST['days_select'])) {
                                                $day_selected = $_POST['days_select'];
                                                $ActivityTypedatarating = Reporting::getActivityTypeHoursbyDays($day_selected, $user_id, $meeting['meeting_category']);
                                            } else {
                                                $ActivityTypedatarating = Reporting::getActivityTypeHours($start_date, $end_date, $user_id, $meeting['meeting_category']);
                                            }
                                            $rate_count_rating = 0;
                                            foreach ($ActivityTypedatarating as $row_count_rating) {
                                                $rate_count_rating += ($row_count_rating['Rate']);
                                            }
                                            $totalhoursAzeem +=$meeting['hours'];
                                            ?>
                                            <div class="percentageTable clearfix">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <thead>
                                                        <tr class="subTitle">
                                                            <th align="left" class="tdCol-1"><span><?php echo $meeting['name'] ?></span></th>                                                            
                                                            <th align="center" class="tdCol-2"><span><?php echo m2h("i:s", $meeting['hours']); ?></span></th>
                                                            <th align="center" class="tdCol-3a"><span><?php echo round($meeting['hours'] / $totalHours * 100, 2) . '%'; ?></span></th>
                                                            <th align="center" class="tdCol-2"><span><?php echo number_format($rate_count_rating / sizeof($ActivityTypedatarating), 2); ?></span></th>                
                                                        </tr>
                                                    </thead>
                                                </table>                                               
                                                <div class="grid_12a leftTableInfo">
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <?php
                                                            if (isset($_POST['manager'])) {
                                                                if (isset($_POST['days_select'])) {
                                                                    $ActivityTypedata = Reporting::getActivityTypeHoursbyDays($_POST['days_select'], $user_id, $meeting['meeting_category']);
                                                                    $ActivityTypedatacomments = Reporting::getActivityTypeCommentsbyDays($_POST['days_select'], $user_id, $meeting['meeting_category']);
                                                                } else {
                                                                    $ActivityTypedata = Reporting::getActivityTypeHours($start_date, $end_date, $user_id, $meeting['meeting_category']);
                                                                    $ActivityTypedatacomments = Reporting::getActivityTypeComments($start_date, $end_date, $user_id, $meeting['meeting_category']);
                                                                }
                                                            } else {
                                                                $ActivityTypedata = Reporting::getActivityTypeHoursByLevelId($start_date, $end_date, $level_id, $meeting['meeting_category']);
                                                            }
                                                            $csvdata[] = $ActivityTypedata;
                                                            $ActivityTypeNameData = Meeting::getActivityTypeByMeetingCategoryId($meeting['meeting_category']);
                                                            $other_fill = '';
                                                            $other_value_fill = '';
                                                            foreach ($ActivityTypedata as $activity_type) {
                                                                $activity_type_id[] = $activity_type['id'];
                                                                $activeper = $activity_type['hours'] / $totalHours * 100;
                                                                ?>
                                                                <tr>
                                                                    <td class="tdCol-1 noBorderTop"><?php echo $activity_type['name']; ?></td>                                                                      
                                                                    <td class="tdCol-2 noBorderTop" align="center"><?php echo m2h("i:s", $activity_type['hours']); ?></td>     
                                                                    <td class="tdCol-3 noBorderTop" align="center"><?php echo round($activeper, 2) . '%'; ?></td>                                                   
                                                                    <td class="tdCol-2 noBorderTop"><?php echo number_format($activity_type['Rate'], 2); ?></td>                                                                
                                                                </tr>
                                                                <?php
                                                                $i++;
                                                            }
                                                            ?>
                                                              </tbody>
                                                    </table>
                                                </div>                                            
                                            </div>
                                            <?php
                                        }
                                    } else if (empty($meetingHistoryData) && $_POST['manager']) {
                                        ?>
                                        <table cellpadding="0" border="0" cellspacing="0">
                                            <thead>
                                                <tr class="subTitle">
                                                    <th align="center" class="tdCol-3 tdNone"><span><?php echo User::getFullNameByUserId($user_id); ?> doesn't have any records</span></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    <?php } ?>
                                    <table><tr><td>Total Hours</td> <td style="width:830px;"> <?php echo m2h("i:s", $totalhoursAzeem); ?></td></tr>
                                                      </table>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>

            <div id="tab2" class="<?php echo ($_POST['representative']) ? 'unique' : ''; ?>">
                <?php
                if (empty($success_s)) {
                    if ($_POST['representative']) {
                        $start_date = $_POST['start_date_s'];
                        $start_date = date("Y-m-d", strtotime($start_date));
                        $end_date = $_POST['end_date_s'];
                        $end_date = date("Y-m-d", strtotime($end_date));
                        $date_array = explode('-', $end_date);
                        $month = $date_array[0];
                        $date = $date_array[1];
                        $year = $date_array[2];
                        $users = $_POST['representative_id'];
                        $is_representative = 1;
                    } else {
                        $users = array(4);
                        $start_date = '0000-00-00';
                        $start_date = date("Y-m-d", strtotime($start_date));
                        $end_date = '0000-00-00';
                        $end_date = date("Y-m-d", strtotime($end_date));
                        $is_representative = 1;
                    }
                }
                foreach ($users as $user_id) {
                    if ($is_representative) {
                        if (isset($_POST['days_select_s'])) {
                            $totalHours = Reporting::getTotalHoursByDaysAndUserId($_POST['days_select_s'], $user_id);
                        } else {
                            $totalHours = Reporting::getTotalHoursByDateRangeAndUserId($start_date, $end_date, $user_id);
                        }
                    } else {
                        $level_id = $user_id;
                        $totalHours = Reporting::getTotalHoursByDateRangeAndLevelId($start_date, $end_date, $level_id);
                    }
                }
                ?>
                <div class="filterBox clearfix">
                    <h2>Select a Date Range</h2>
                    <form action="" name="ViewUnsavedForm" id="ViewUnsavedForm" class="representative_form" method="POST">
                        <?php if (!empty($success_s)) {
                            ?>
                            <p style="color:green; font-weight: bold;"><?php echo $success_s; ?></p>
                        <?php } ?>
                        <div class="grid_6 dateArea clearfix marginNone" style="margin-bottom:0px!important;">

                            <p class="dateIcon" style="width: 160px;">                   
                                <input type="number" name="m_strat_month_s" id="m_strat_month_s" class="m_date_s" placeholder="mm" value="<?php echo $_POST['m_strat_month_s']; ?>" style="width: 38px;">
                                <input type="number" name="m_strat_day_s" id="m_strat_day_s" class="m_date_s" placeholder="dd" value="<?php echo $_POST['m_strat_day_s']; ?>" style="width: 30px;">
                                <input type="number" name="m_strat_year_s" id="m_strat_year_s" class="m_date_s" placeholder="yy" value="<?php echo $_POST['m_strat_year_s'] != "" ? $_POST['m_strat_year_s'] : '2015'; ?>" style="width: 47px;">
                                <input type="hidden" name="start_date_s" id="start_date_s" value="<?php echo $_POST['start_date_s'] ?>">
                                <input type="hidden" id="calender_image_viewhistory1_s"  class="datepicker_view_my_history_s"/>

                            </p>
                            <label>to</label>
                            <p class="dateIcon" style="width: 160px;">
                                <input type="number" name="m_end_month_s" id="m_end_month_s" class="m_date_s" placeholder="mm" value="<?php echo $_POST['m_end_month_s']; ?>" style="width: 38px;">
                                <input type="number" name="m_end_day_s" id="m_end_day_s" class="m_date_s" placeholder="dd" value="<?php echo $_POST['m_end_day_s']; ?>" style="width: 30px;">
                                <input type="number" name="m_end_year_s" id="m_end_year_s" class="m_date_s" placeholder="yy" value="<?php echo $_POST['m_end_year_s'] != "" ? $_POST['m_end_year_s'] : '2015'; ?>" style="width: 47px;">                    
                                <input type="hidden" name="end_date_s" id="end_date_s" value="<?php echo $_POST['end_date_s'] ?>">
                                <input type="hidden" id="calender_image_viewhistory2_s"  class="datepicker_view_myhistory_s"/>
                            </p>
                            <div class="bracklin"></div>
                            <p class="dateIcon">
                                <?php if (isset($_POST['days_select_s'])) { ?>
                                    <input type="checkbox" name="hideField_s" value="true" id="" class="hideField_s" checked="checked">
                                <?php } else { ?>
                                    <input type="checkbox" name="hideField_s" value="true" id="" class="hideField_s">
                                <?php } ?>
                            </p>
                            <div class="dateArea clearfix marginNone">
                                <select name="days_select_s" disabled="disabled" id="days_select_s">
                                    <option value="7" <?php echo ($_POST['days_select_s']) == 7 ? 'selected="selected"' : ''; ?>>Last 7 Days</option>
                                    <option value="30" <?php echo ($_POST['days_select_s']) == '30' ? 'selected="selected"' : ''; ?>>Last 30 Days</option>
                                    <option value="60" <?php echo ($_POST['days_select_s']) == '60' ? 'selected="selected"' : ''; ?>>Last 60 Days</option>
                                    <option value="90" <?php echo ($_POST['days_select_s']) == '90' ? 'selected="selected"' : ''; ?>>Last 90 Days</option>
                                    <option value="6m" <?php echo ($_POST['days_select_s']) == '6m' ? 'selected="selected"' : ''; ?>>Last 6 Months</option>
                                    <option value="12m" <?php echo ($_POST['days_select_s']) == '12m' ? 'selected="selected"' : ''; ?>>Last 12 Months</option>
                                </select>                                
                            </div>
                            <select name="representative_id[]" id="representative_id" multiple="multiple" style="margin-bottom:10px;">
                                <option value="" selected="selected" disabled>Select Sales Representative</option>
                                <?php
                                $results = User::getAllSalesRepresentative();
                                foreach ($results as $result) {
                                    ?>
                                    <option <?php echo (isset($_POST['representative_id']) && in_array($result['id'], $_POST['representative_id'])) ? 'selected="selected"' : ''; ?> value="<?php echo $result['id']; ?>"><?php echo $result['first_name'] . ' ' . $result['last_name']; ?></option>
                                <?php } ?>
                            </select>
                            <div class="centerbtn"> <input type="submit" name="representative" value="View" id="button_form_representative" class="redBtn">
                                <input type="submit" name="representative_export" value="Export Activity Level" id="button_export_representative" class="redBtn">
                                <input type="submit" name="representative_export_minify" value="Export Summary Level" id="button_export_representative_minify" class="redBtn">
                            </div>
                        </div>

                    </form>

                    <div class="filterBtn">
                        <div class="clearfix"><a href="editMeetingHours.php?level=representative" class="darkBtn clearfix">Edit Sales Representative Activity Log Hours & Minutes</a></div>

                        <div id="meetingHourSalesRepPopup1" class="popupBox width_240">
                            <form action="export.php" method="POST">
                                <h3>Select a Date Range </h3>
                                <p class="dateIcon">
                                    <input type="text" name="start_date" class="datepicker" placeholder="Start Date">
                                </p>
                                <p class="dateIcon">
                                    <input type="text" name="end_date" class="datepicker" placeholder="End Date">
                                    <input type="hidden" name="byLevel" value="yes">
                                </p>
                                <input type="submit" name="exportAllSalesRep" value="Export" >
                            </form>
                        </div>

                        <div id="meetingHourSalesRepPopup2" class="popupBox width_240">
                            <form action="export.php" method="POST">
                                <h3>Select a Date Range</h3>
                                <p class="dateIcon">
                                    <input type="text" name="start_date" class="datepicker" placeholder="Start Date">
                                </p>
                                <p class="dateIcon">
                                    <input type="text" name="end_date" class="datepicker" placeholder="End Date">
                                    <input type="hidden" name="individual" value="yes">
                                </p>
                                <div>
                                    <select name="manager_id">
                                        <option value="">Select Sales Representative</option>
                                        <?php
                                        $results = User::getAllSalesRepresentative();
                                        foreach ($results as $result) {
                                            ?>
                                            <option <?php echo ($_POST['representative_id'] == $result['id']) ? 'selected="selected"' : ''; ?> value="<?php echo $result['id']; ?>"><?php echo $result['first_name'] . ' ' . $result['last_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <input type="submit" name="exportIndSalesRep" value="Export" >
                            </form>
                        </div>

                    </div>
                </div>
                <?php if (isset($totalHours) && $totalHours != 0) {
                    ?>
                    <p class="viewhistoryTopcontent">Total Hours & Minutes For Selected Date Range: <?php echo m2h("i:s", $totalHours); ?></p>
                <?php } ?>

                <?php
                if (empty($success_s)) {
                    foreach ($users as $user_id) {
                        if ($is_representative) {
                            if (isset($_POST['days_select_s'])) {
                                $totalHours = Reporting::getTotalHoursByDaysAndUserId($_POST['days_select_s'], $user_id);
                                $meetingHistoryData = Reporting::getMeetingHistoryByDaysAndUserId($_POST['days_select_s'], $user_id);
                                $user_full_name = User::getFullNameByUserId($user_id);
                            } else {
                                $totalHours = Reporting::getTotalHoursByDateRangeAndUserId($start_date, $end_date, $user_id);
                                $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndUserId($start_date, $end_date, $user_id);
                                $user_full_name = User::getFullNameByUserId($user_id);
                            }
                        } else {
                            $level_id = $user_id;
                            $totalHours = Reporting::getTotalHoursByDateRangeAndLevelId($start_date, $end_date, $level_id);
                            $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndLevel($start_date, $end_date, $level_id);
                            $user_full_name = User::getFullNameByUserId($user_id);
                        }
                        ?>
                        <div class="tableSec summryTable clearfix">
                            <?php if ($meetingHistoryData) {
                                ?>
                                <div class="summryTableHead clearfix">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <thead>
                                            <tr class="mainTitle">
                                                <th width="20%" align="left" class="tdCol-1"><span>Activity Summary (<?php echo $user_full_name; ?>)</span></th>
                                                <th width="8%"  align="center" class="tdCol-2"><span>Hours & Minutes</span></th>
                                                <th width="8%" class="tdCol-3 noBorderTop " align="center"><span>% of Total Hours & Minutes for Selected Date Range</span></th>
                                                <th width="8%"  align="center" class="tdCol-2"><span>Difficulty Level</span></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            <?php } ?>

                            <?php
                            if ($meetingHistoryData) {
                                foreach ($meetingHistoryData as $meeting) {

                                    if (isset($_POST['days_select_s'])) {
                                        $day_selected = $_POST['days_select_s'];
                                        $ActivityTypedatarating = Reporting::getActivityTypeHoursbyDays($day_selected, $user_id, $meeting['meeting_category']);
                                    } else {
                                        $ActivityTypedatarating = Reporting::getActivityTypeHours($start_date, $end_date, $user_id, $meeting['meeting_category']);
                                    }
                                    $rate_count_rating = 0;
                                    foreach ($ActivityTypedatarating as $row_count_rating) {
                                        $rate_count_rating += ($row_count_rating['Rate']);
                                    }
                                    ?>
                                    <div class="percentageTable clearfix">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <thead>
                                                <tr class="subTitle">
                                                    <th align="left" class="tdCol-1"><span><?php echo $meeting['name'] ?></span></th>
                                                    <th align="center" class="tdCol-2"><span><?php echo m2h("i:s", $meeting['hours']); ?></span></th>
                                                    <th align="center" class="tdCol-3 tdNone"><span><?php echo round($meeting['hours'] / $totalHours * 100, 2) . '%'; ?></span></th>
                                                    <th align="center" class="tdCol-2"><span><?php echo number_format($rate_count_rating / sizeof($ActivityTypedatarating), 2); ?></span></th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <!-- ( .tableHead end ) -->
                                        <div class="grid_12a leftTableInfo">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <?php
                                                    if ($_POST['representative']) {
                                                        if (isset($_POST['days_select_s'])) {
                                                            $ActivityTypedata = Reporting::getActivityTypeHoursbyDays($_POST['days_select_s'], $user_id, $meeting['meeting_category']);
                                                            $ActivityTypedatacomments = Reporting::getActivityTypeCommentsbyDays($_POST['days_select_s'], $user_id, $meeting['meeting_category']);
                                                        } else {
                                                            $ActivityTypedata = Reporting::getActivityTypeHours($start_date, $end_date, $user_id, $meeting['meeting_category']);
                                                            $ActivityTypedatacomments = Reporting::getActivityTypeComments($start_date, $end_date, $user_id, $meeting['meeting_category']);
                                                        }
                                                    } else {
                                                        $ActivityTypedata = Reporting::getActivityTypeHoursByLevelId($start_date, $end_date, $level_id, $meeting['meeting_category']);
                                                    }
                                                    $other_fill = '';
                                                    $other_value_fill = '';
                                                    foreach ($ActivityTypedata as $activity_type) {
                                                        $activity_type_id[] = $activity_type['id'];
                                                        $activeper = $activity_type['hours'] / $totalHours * 100;
                                                        ?>                                                    
                                                        <tr>
                                                            <td class="tdCol-1 noBorderTop"><?php echo $activity_type['name']; ?></td>
                                                            <td class="tdCol-2 noBorderTop" align="center"><?php echo m2h("i:s", $activity_type['hours']); ?></td>
                                                            <td class="tdCol-3 noBorderTop" align="center"><?php echo round($activeper, 2) . '%'; ?></td>
                                                            <td class="tdCol-2 noBorderTop"><?php echo number_format($activity_type['Rate'], 2); ?></td>
                                                            <td class="tdCol-3 noBorderTop tdNone"></td>
                                                        </tr>
                                                        <?php
                                                        $i++;
                                                    }
                                                    ?>                                            
                                                </tbody>
                                            </table>
                                        </div>                                      
                                    </div>
                                    <?php
                                }
                            } else if (empty($meetingHistoryData) && $_POST['representative']) {
                                ?>
                                <table cellpadding="0" border="0" cellspacing="0">
                                    <thead>
                                        <tr class="subTitle">
                                            <th align="center" class="tdCol-3 tdNone"><span><?php echo User::getFullNameByUserId($user_id); ?> doesn't have any records</span></th>
                                        </tr>
                                    </thead>
                                </table>
                            <?php } ?>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>

            <div id="tab3" class="" style="display: none;">
                <div class="filterBox clearfix">                    
                    <h2>Select Sales Team</h2>
                    <?php if (!empty($success_t)) {
                        ?>
                        <p style="color:red; font-weight: bold;"><?php echo $success_t; ?></p>
                    <?php } ?>
                    <form action="" name="sales_team_reporting" id="sales_team_reporting" method="POST">
                        <div class="grid_6 dateArea marginNone" style="margin-bottom:0px!important;">                           
                            <div class="dateArea marginNone">
                                <select tabindex="8" name="sales_team" id="sales_team">
                                    <option value="">Select Sales Team</option>
                                    <?php
                                    $results = User::getAllIndustry();
                                    foreach ($results as $result) {
                                        ?>
                                        <option value="<?php echo $result['id']; ?>" <?php echo ($result['id'] == $_POST['sales_team']) ? 'selected="selected"' : ''; ?>><?php echo $result['name']; ?></option>  
                                    <?php } ?>
                                </select>                               
                            </div>                            
                            <div class="centerbtn">
                                <input type="submit" name="sales_team_export_view" id="sales_team_export_view" value="View" class="redBtn" style="margin-top: -28px;">                            
                                <input type="submit" name="sales_team_export" id="sales_team_export" value="Export Activity Level" class="redBtn" style="margin-top: -28px;">                            
                                <input type="submit" name="sales_team_export_minify" id="sales_team_export_minify" value="Export Summary Level" class="redBtn" style="margin-top: -28px;">               
                            </div>
                        </div>
                    </form>                 
                </div> 
                <?php
                if (!empty($view_t)) {
                    $sales_team_id = $_POST['sales_team'];
                    $sales_team_name = User::getIndustryById($sales_team_id);
                    $get_info_managers = User::getAllInformationBySalesIdForManager($sales_team_id, 3);                   
                    $get_info_sales_reps = User::getAllInformationBySalesId($sales_team_id, 4);
                    $total_hours_export_manager_salesteam = 0;
                    $total_hours_export_sales_salesteam = 0;
                    $number_m = 0;
                    $number_s = 0;
                    ?>
                    <div class="tableSec summryTable clearfix">
                        <table cellpadding="0" border="0" cellspacing="0">
                            <thead>
                                <tr class="subTitle">
                                    <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="font-size: 20px; line-height: initial;">MANAGERS</span></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <?php
                    if (!empty($get_info_managers)) {
                        foreach ($get_info_managers as $get_info_manager) {

                            $sales_team_name = User::getIndustryByUserId($get_info_manager['industry_id']);
                            $totalHours = Reporting::getTotalHoursByUserIdForexportData($get_info_manager['id']);
                            $total_hours_export_manager_salesteam += $totalHours;
                            $meetingHistoryData = Reporting::getMeetingHistoryUserIdforExportData($get_info_manager['id']);
                            ?>
                            <div class="tableSec summryTable clearfix">

                                <?php
                                if ($meetingHistoryData) {
                                    $number_m = 1;
                                    ?>
                                    <div class="summryTableHead clearfix">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <thead>
                                                <tr class="mainTitle">
                                                    <th width="20%" align="left" class="tdCol-1"><span>Activity Summary (<?php echo $get_info_manager['first_name'] . ' ' . $get_info_manager['last_name']; ?>) (<?php echo m2h("i:s", $totalHours); ?>)</span></th>
                                                    <th width="8%"  align="center" class="tdCol-2"><span>Hours & Minutes</span></th>
                                                    <th width="8%" class="tdCol-3 noBorderTop " align="center"><span>% of Total Hours & Minutes for Selected Date Range</span></th>
                                                    <th width="8%"  align="center" class="tdCol-2"><span>Difficulty Level</span></th>              
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <?php
                                    $totalpercntt = 0;
                                    foreach ($meetingHistoryData as $meeting) {
                                        $totalpercntt += round($meeting['hours'] / $totalHours * 100, 2);
                                        $ActivityTypedata = Reporting::getActivityTypeHoursForExportData($get_info_manager['id'], $meeting['meeting_category']);
                                        $ActivityTypedatacomments = Reporting::getActivityTypeCommentsForExportData($get_info_manager['id'], $meeting['meeting_category']);
                                        $max = count($ActivityTypedata);
                                        $rate_count_rating = 0;
                                        foreach ($ActivityTypedata as $row_count_rating) {
                                            $rate_count_rating += ($row_count_rating['Rate']);
                                        }
                                        ?>
                                        <div class="percentageTable clearfix">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <thead>
                                                    <tr class="subTitle">
                                                        <th align="left" class="tdCol-1"><span><?php echo $meeting['name'] ?></span></th>                
                                                        <th align="center" class="tdCol-2"><span><?php echo m2h("i:s", $meeting['hours']); ?></span></th>
                                                        <th align="center" class="tdCol-3 tdNone"><span><?php echo round($meeting['hours'] / $totalHours * 100, 2) . '%'; ?></span></th>
                                                        <th align="center" class="tdCol-2"><span><?php echo number_format($rate_count_rating / sizeof($ActivityTypedata), 2); ?></span></th>                    
                                                    </tr>
                                                </thead>
                                            </table>

                                            <div class="grid_12a leftTableInfo">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <?php
                                                        $other_fill = '';
                                                        $other_value_fill = '';
                                                        foreach ($ActivityTypedata as $activity_type) {
                                                            $activity_type_id[] = $activity_type['id'];
                                                            $activeper = $activity_type['hours'] / $totalHours * 100;
                                                            ?>                                                    
                                                            <tr>
                                                                <td class="tdCol-1 noBorderTop"><?php echo $activity_type['name']; ?></td>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <!--                                                                <td class="tdCol-2 noBorderTop" align="center"><?php // echo $activity_type['hours'];                                                                                                                    ?></td>-->
                                                                <td class="tdCol-2 noBorderTop" align="center"><?php echo m2h("i:s", $activity_type['hours']); ?></td>
                                                                <td class="tdCol-3 noBorderTop" align="center"><?php echo round($activeper, 2) . '%'; ?></td>
                                                                <td class="tdCol-2 noBorderTop"><?php echo number_format($activity_type['Rate'], 2); ?></td>
                                                                <td class="tdCol-3 noBorderTop tdNone"></td>
                                                            </tr>
                                                            <?php
                                                            $i++;
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>                                        
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <table cellpadding="0" border="0" cellspacing="0" style="margin-top: 2px;">
                                        <thead>
                                            <tr class="subTitle">
                                                <th align="center" class="tdCol-3 tdNone"><span> <?php echo $get_info_manager['first_name'] . ' ' . $get_info_manager['last_name']; ?> doesn't have any records</span></th>
                                            </tr>
                                        </thead>
                                    </table>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <?php if ($number_m == 1) { ?>
                            <div class="tableSec summryTable clearfix">
                                <table cellpadding="0" border="0" cellspacing="0" style="margin-top: 10px;">
                                    <thead>
                                        <tr class="subTitle" style="background-color: black;">
                                            <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="font-size: 15px; line-height: 27px; border-right: 0px solid;">Total # of Hours For Managers (<?php echo m2h("i:s", $total_hours_export_manager_salesteam); ?>)</span></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="tableSec summryTable clearfix">
                            <table cellpadding="0" border="0" cellspacing="0">
                                <thead>
                                    <tr class="subTitle">
                                        <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="line-height: initial;">No User Found</span></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    <?php } ?>
                    <div class="tableSec summryTable clearfix">
                        <table cellpadding="0" border="0" cellspacing="0" style="margin-top: 10px;">
                            <thead>
                                <tr class="subTitle">
                                    <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="font-size: 20px; line-height: initial;">SALES REPRESENTATIVES</span></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <?php
                    if (!empty($get_info_sales_reps)) {
                        foreach ($get_info_sales_reps as $get_info_sales_rep) {
                            $sales_team_name = User::getIndustryByUserId($get_info_sales_rep['industry_id']);
                            $totalHours = Reporting::getTotalHoursByUserIdForexportData($get_info_sales_rep['id']);
                            $total_hours_export_sales_salesteam += $totalHours;
                            $meetingHistoryData = Reporting::getMeetingHistoryUserIdforExportData($get_info_sales_rep['id']);
                            ?>
                            <div class="tableSec summryTable clearfix">

                                <?php
                                if ($meetingHistoryData) {
                                    $number_s = 1;
                                    ?>
                                    <div class="summryTableHead clearfix">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <thead>
                                                <tr class="mainTitle">
                                                    <th width="20%" align="left" class="tdCol-1"><span>Activity Summary (<?php echo $get_info_sales_rep['first_name'] . ' ' . $get_info_sales_rep['last_name']; ?>) (<?php echo m2h("i:s", $totalHours); ?>)</span></th>
                                                    <th width="8%"  align="center" class="tdCol-2"><span>Hours & Minutes</span></th>
                                                    <th width="8%" class="tdCol-3 noBorderTop " align="center"><span>% of Total Hours & Minutes for Selected Date Range</span></th>
                                                    <th width="8%"  align="center" class="tdCol-2"><span>Difficulty Level</span></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <?php
                                    $totalpercntt = 0;
                                    foreach ($meetingHistoryData as $meeting) {
                                        $totalpercntt += round($meeting['hours'] / $totalHours * 100, 2);
                                        $ActivityTypedata = Reporting::getActivityTypeHoursForExportData($get_info_sales_rep['id'], $meeting['meeting_category']);
                                        $ActivityTypedatacomments = Reporting::getActivityTypeCommentsForExportData($get_info_sales_rep['id'], $meeting['meeting_category']);
                                        $max = count($ActivityTypedata);
                                        $rate_count_rating = 0;
                                        foreach ($ActivityTypedata as $row_count_rating) {
                                            $rate_count_rating += ($row_count_rating['Rate']);
                                        }
                                        ?>
                                        <div class="percentageTable clearfix">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <thead>
                                                    <tr class="subTitle">
                                                        <th align="left" class="tdCol-1"><span><?php echo $meeting['name'] ?></span></th>                
                                                        <th align="center" class="tdCol-2"><span><?php echo m2h("i:s", $meeting['hours']); ?></span></th>
                                                        <th align="center" class="tdCol-3 tdNone"><span><?php echo round($meeting['hours'] / $totalHours * 100, 2) . '%'; ?></span></th>
                                                        <th align="center" class="tdCol-2"><span><?php echo number_format($rate_count_rating / sizeof($ActivityTypedata), 2); ?></span></th>                    
                                                    </tr>
                                                </thead>
                                            </table>

                                            <div class="grid_12a leftTableInfo">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <?php
                                                        $other_fill = '';
                                                        $other_value_fill = '';
                                                        foreach ($ActivityTypedata as $activity_type) {
                                                            $activity_type_id[] = $activity_type['id'];
                                                            $activeper = $activity_type['hours'] / $totalHours * 100;
                                                            ?>                                                    
                                                            <tr>
                                                                <td class="tdCol-1 noBorderTop"><?php echo $activity_type['name']; ?></td>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <!--                                                                <td class="tdCol-2 noBorderTop" align="center"><?php // echo $activity_type['hours'];                                                                                                                    ?></td>-->
                                                                <td class="tdCol-2 noBorderTop" align="center"><?php echo m2h("i:s", $activity_type['hours']); ?></td>
                                                                <td class="tdCol-3 noBorderTop" align="center"><?php echo round($activeper, 2) . '%'; ?></td>
                                                                <td class="tdCol-2 noBorderTop"><?php echo number_format($activity_type['Rate'], 2); ?></td>                            
                                                            </tr>
                                                            <?php
                                                            $i++;
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>                                        

                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <table cellpadding="0" border="0" cellspacing="0" style="margin-top: 2px;">
                                        <thead>
                                            <tr class="subTitle">
                                                <th align="center" class="tdCol-3 tdNone"><span> <?php echo $get_info_sales_rep['first_name'] . ' ' . $get_info_sales_rep['last_name']; ?> doesn't have any records</span></th>
                                            </tr>
                                        </thead>
                                    </table>
                                <?php } ?>
                            </div>
                        <?php }
                        ?>
                        <?php if ($number_s == 1) { ?>
                            <div class="tableSec summryTable clearfix">
                                <table cellpadding="0" border="0" cellspacing="0" style="margin-top: 10px;">
                                    <thead>
                                        <tr class="subTitle" style="background-color: black;">
                                            <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="font-size: 15px; line-height: 27px; border-right: 0px solid">Total # of Hours For Sales Representatives (<?php echo m2h("i:s", $total_hours_export_sales_salesteam); ?>)</span></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        <?php } ?>
                    <?php } else {
                        ?>
                        <div class="tableSec summryTable clearfix">
                            <table cellpadding="0" border="0" cellspacing="0">
                                <thead>
                                    <tr class="subTitle">
                                        <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="line-height: initial;">No User Found</span></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>

            <div id="tab4" class="unique" style="display: block;">
                <div class="filterBox clearfix">
                    <h2>Export All Users</h2>
                    <?php if (!empty($success_all)) {
                        ?>
                        <p style="color:red; font-weight: bold;"><?php echo $success_all; ?></p>
                    <?php } ?>
                    <form action="" name="all_users_reporting" id="all_users_reporting" method="POST">
                        <div class="grid_6 dateArea marginNone" style="margin-bottom:0px!important;">                            
                            <div class="dateArea marginNone">
                                <p class="dateIcon" style="width: 160px;">                   
                                    <input type="number" name="m_strat_month_all" id="m_strat_month_all" class="m_date" placeholder="mm" value="<?php echo $_POST['m_strat_month_all']; ?>" style="width: 38px;">
                                    <input type="number" name="m_strat_day_all" id="m_strat_day_all" class="m_date" placeholder="dd" value="<?php echo $_POST['m_strat_day_all']; ?>" style="width: 30px;">
                                    <input type="number" name="m_strat_year_all" id="m_strat_year_all" class="m_date" placeholder="yy" value="<?php echo $_POST['m_strat_year_all'] != "" ? $_POST['m_strat_year_all'] : '2015'; ?>" style="width: 47px;">
                                    <input type="hidden" name="all_users_start_date" id="all_users_start_date" value="<?php echo $_POST['all_users_start_date']; ?>">
                                    <input type="hidden" id="calender_image_all_user_start" class=""/>                                    
                                </p>
                                <label>to</label>
                                <p class="dateIcon" style="width: 160px;">
                                    <input type="number" name="m_end_month_all" id="m_end_month_all" class="m_date" placeholder="mm" value="<?php echo $_POST['m_end_month_all']; ?>" style="width: 38px;">
                                    <input type="number" name="m_end_day_all" id="m_end_day_all" class="m_date" placeholder="dd" value="<?php echo $_POST['m_end_day_all']; ?>" style="width: 30px;">
                                    <input type="number" name="m_end_year_all" id="m_end_year_all" class="m_date" placeholder="yy" value="<?php echo $_POST['m_end_year_all'] != "" ? $_POST['m_end_year_all'] : '2015'; ?>" style="width: 47px;">                    
                                    <input type="hidden" name="all_users_end_date" id="all_users_end_date" value="<?php echo $_POST['all_users_start_date']; ?>">
                                    <input type="hidden" id="calender_image_all_user_end" class=""/>
                                </p>
                                <div class="bracklin"></div>
                                <p class="dateIcon">
                                    <input type="checkbox" name="hideField_all_user" value="true" <?php echo (isset($_POST['hideField_all_user'])) ? 'checked="checked"' : ''; ?> id="hideField_all_user" class="hideField_all_user">
                                </p>
                                <div class="dateArea clearfix marginNone">
                                    <select name="days_select_all_user" disabled="disabled" id="days_select_all_user">
                                        <option value="7" <?php echo ($_POST['days_select_all_user']) == 7 ? 'selected="selected"' : ''; ?>>Last 7 Days</option>
                                        <option value="30" <?php echo ($_POST['days_select_all_user']) == '30' ? 'selected="selected"' : ''; ?>>Last 30 Days</option>
                                        <option value="60" <?php echo ($_POST['days_select_all_user']) == '60' ? 'selected="selected"' : ''; ?>>Last 60 Days</option>
                                        <option value="90" <?php echo ($_POST['days_select_all_user']) == '90' ? 'selected="selected"' : ''; ?>>Last 90 Days</option>
                                        <option value="6m" <?php echo ($_POST['days_select_all_user']) == '6m' ? 'selected="selected"' : ''; ?>>Last 6 Months</option>
                                        <option value="12m" <?php echo ($_POST['days_select_all_user']) == '12m' ? 'selected="selected"' : ''; ?>>Last 12 Months</option>
                                    </select>                                
                                </div>
                                <div class="centerbtn"> <input type="submit" name="all_users_export_report" id="all_users_export_report" value="View" class="redBtn" style="margin-top: -16%;  margin-left: 97%;">
                                    <input type="submit" name="all_users_export" id="all_users_export" value="Export Activity Level" class="redBtn" style="margin-top: -21%;  margin-left: 107%;">
                                    <input type="submit" name="all_users_export_minify" id="all_users_export_minify" value="Export Summary Level" class="redBtn" style="margin-top: -26%;  margin-left: 134%;"></div>
                            </div>  
                        </div>
                    </form>                 
                </div>    
                <?php
                if (!empty($view_all)) {

                    if (isset($_POST['days_select_all_user'])) {
                        $get_info_managers = User::getAllManagersByDay($_POST['days_select_all_user']);
                        $get_info_sales_reps = User::getAllSalesRepresentativeByDay($_POST['days_select_all_user']);
                    } else {
                        $get_info_managers = User::getAllManagersByDateRange($start_date_all, $end_date_all);
                        $get_info_sales_reps = User::getAllSalesRepresentativeByDateRange($start_date_all, $end_date_all);
                    }

                    $total_hours_export_manager_salesteam = 0;
                    $total_hours_export_sales_salesteam = 0;
                    $number_m = 0;
                    $number_s = 0;
                    ?>
                    <div class="tableSec summryTable clearfix">
                        <table cellpadding="0" border="0" cellspacing="0">
                            <thead>
                                <tr class="subTitle">
                                    <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="font-size: 20px; line-height: initial;">All MANAGERS</span></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <?php
                    if (!empty($get_info_managers)) {
                        foreach ($get_info_managers as $get_info_manager) {

                            $sales_team_name = User::getIndustryByUserId($get_info_manager['industry_id']);
                            $totalHours = Reporting::getTotalHoursByUserIdForexportData($get_info_manager['id']);
                            $total_hours_export_manager_salesteam += $totalHours;
                            $meetingHistoryData = Reporting::getMeetingHistoryUserIdforExportData($get_info_manager['id']);
                            ?>
                            <div class="tableSec summryTable clearfix">

                                <?php
                                if ($meetingHistoryData) {
                                    $number_m = 1;
                                    ?>
                                    <div class="summryTableHead clearfix">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <thead>
                                                <tr class="mainTitle">
                                                    <th width="20%" align="left" class="tdCol-1"><span>Activity Summary (<?php echo $get_info_manager['first_name'] . ' ' . $get_info_manager['last_name']; ?>) (<?php echo m2h("i:s", $totalHours); ?>)</span></th>
                                                    <th width="8%"  align="center" class="tdCol-2"><span>Hours & Minutes</span></th>
                                                    <th width="8%" class="tdCol-3 noBorderTop " align="center"><span>% of Total Hours & Minutes for Selected Date Range</span></th>
                                                    <th width="8%"  align="center" class="tdCol-2"><span>Difficulty Level</span></th>
                <!--                                                    <th width="8%"  align="center" class="tdCol-2"><span>Comments</span></th>
                                                    <th align="left" class="tdCol-3 "></th>-->
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <?php
                                    $totalpercntt = 0;
                                    foreach ($meetingHistoryData as $meeting) {
                                        $totalpercntt += round($meeting['hours'] / $totalHours * 100, 2);
                                        $ActivityTypedata = Reporting::getActivityTypeHoursForExportData($get_info_manager['id'], $meeting['meeting_category']);
                                        $ActivityTypedatacomments = Reporting::getActivityTypeCommentsForExportData($get_info_manager['id'], $meeting['meeting_category']);
                                        $max = count($ActivityTypedata);
                                        $rate_count_rating = 0;
                                        foreach ($ActivityTypedata as $row_count_rating) {
                                            $rate_count_rating += ($row_count_rating['Rate']);
                                        }
                                        ?>
                                        <div class="percentageTable clearfix">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <thead>
                                                    <tr class="subTitle">
                                                        <th align="left" class="tdCol-1"><span><?php echo $meeting['name'] ?></span></th>                
                                                        <th align="center" class="tdCol-2"><span><?php echo m2h("i:s", $meeting['hours']); ?></span></th>
                                                        <th align="center" class="tdCol-3 tdNone"><span><?php echo round($meeting['hours'] / $totalHours * 100, 2) . '%'; ?></span></th>
                                                        <th align="center" class="tdCol-2"><span><?php echo number_format($rate_count_rating / sizeof($ActivityTypedata), 2); ?></span></th>
                    <!--                                                        <th align="center" class="tdCol-2"><span>&nbsp;</span></th>-->
                                                    </tr>
                                                </thead>
                                            </table>

                                            <div class="grid_12a leftTableInfo">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <?php
                                                        $other_fill = '';
                                                        $other_value_fill = '';
                                                        foreach ($ActivityTypedata as $activity_type) {
                                                            $activity_type_id[] = $activity_type['id'];
                                                            $activeper = $activity_type['hours'] / $totalHours * 100;
                                                            ?>                                                    
                                                            <tr>
                                                                <td class="tdCol-1 noBorderTop"><?php echo $activity_type['name']; ?></td>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <!--                                                                <td class="tdCol-2 noBorderTop" align="center"><?php // echo $activity_type['hours'];                                                                                                                    ?></td>-->
                                                                <td class="tdCol-2 noBorderTop" align="center"><?php echo m2h("i:s", $activity_type['hours']); ?></td>
                                                                <td class="tdCol-3 noBorderTop" align="center"><?php echo round($activeper, 2) . '%'; ?></td>
                                                                <td class="tdCol-2 noBorderTop"><?php echo number_format($activity_type['Rate'], 2); ?></td>
                                                                <td class="tdCol-3 noBorderTop tdNone"></td>
                                                            </tr>
                                                            <?php
                                                            $i++;
                                                        }
                                                        ?>

                                                    </tbody>
                                                </table>
                                            </div>                                        

                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <table cellpadding="0" border="0" cellspacing="0" style="margin-top: 2px;">
                                        <thead>
                                            <tr class="subTitle">
                                                <th align="center" class="tdCol-3 tdNone"><span> <?php echo $get_info_manager['first_name'] . ' ' . $get_info_manager['last_name']; ?> doesn't have any records</span></th>
                                            </tr>
                                        </thead>
                                    </table>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <?php if ($number_m == 1) { ?>
                            <div class="tableSec summryTable clearfix">
                                <table cellpadding="0" border="0" cellspacing="0" style="margin-top: 10px;">
                                    <thead>
                                        <tr class="subTitle" style="background-color: black;">
                                            <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="font-size: 15px; line-height: 27px; border-right: 0px solid;">Total # of Hours For Managers (<?php echo m2h("i:s", $total_hours_export_manager_salesteam); ?>)</span></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="tableSec summryTable clearfix">
                            <table cellpadding="0" border="0" cellspacing="0">
                                <thead>
                                    <tr class="subTitle">
                                        <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="line-height: initial;">No User Found</span></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    <?php } ?>
                    <div class="tableSec summryTable clearfix">
                        <table cellpadding="0" border="0" cellspacing="0" style="margin-top: 10px;">
                            <thead>
                                <tr class="subTitle">
                                    <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="font-size: 20px; line-height: initial;">All SALES REPRESENTATIVES</span></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <?php
                    if (!empty($get_info_sales_reps)) {
                        foreach ($get_info_sales_reps as $get_info_sales_rep) {
                            $sales_team_name = User::getIndustryByUserId($get_info_sales_rep['industry_id']);
                            $totalHours = Reporting::getTotalHoursByUserIdForexportData($get_info_sales_rep['id']);
                            $total_hours_export_sales_salesteam += $totalHours;
                            $meetingHistoryData = Reporting::getMeetingHistoryUserIdforExportData($get_info_sales_rep['id']);
                            ?>
                            <div class="tableSec summryTable clearfix">

                                <?php
                                if ($meetingHistoryData) {
                                    $number_s = 1;
                                    ?>
                                    <div class="summryTableHead clearfix">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <thead>
                                                <tr class="mainTitle">
                                                    <th width="20%" align="left" class="tdCol-1"><span>Activity Summary (<?php echo $get_info_sales_rep['first_name'] . ' ' . $get_info_sales_rep['last_name']; ?>) (<?php echo m2h("i:s", $totalHours); ?>)</span></th>
                                                    <th width="8%"  align="center" class="tdCol-2"><span>Hours & Minutes</span></th>
                                                    <th width="8%" class="tdCol-3 noBorderTop " align="center"><span>% of Total Hours & Minutes for Selected Date Range</span></th>
                                                    <th width="8%"  align="center" class="tdCol-2"><span>Difficulty Level</span></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <?php
                                    $totalpercntt = 0;
                                    foreach ($meetingHistoryData as $meeting) {
                                        $totalpercntt += round($meeting['hours'] / $totalHours * 100, 2);
                                        $ActivityTypedata = Reporting::getActivityTypeHoursForExportData($get_info_sales_rep['id'], $meeting['meeting_category']);
                                        $ActivityTypedatacomments = Reporting::getActivityTypeCommentsForExportData($get_info_sales_rep['id'], $meeting['meeting_category']);
                                        $max = count($ActivityTypedata);
                                        $rate_count_rating = 0;
                                        foreach ($ActivityTypedata as $row_count_rating) {
                                            $rate_count_rating += ($row_count_rating['Rate']);
                                        }
                                        ?>
                                        <div class="percentageTable clearfix">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <thead>
                                                    <tr class="subTitle">
                                                        <th align="left" class="tdCol-1"><span><?php echo $meeting['name'] ?></span></th>                
                                                        <th align="center" class="tdCol-2"><span><?php echo m2h("i:s", $meeting['hours']); ?></span></th>
                                                        <th align="center" class="tdCol-3 tdNone"><span><?php echo round($meeting['hours'] / $totalHours * 100, 2) . '%'; ?></span></th>
                                                        <th align="center" class="tdCol-2"><span><?php echo number_format($rate_count_rating / sizeof($ActivityTypedata), 2); ?></span></th>                    
                                                    </tr>
                                                </thead>
                                            </table>

                                            <div class="grid_12a leftTableInfo">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <?php
                                                        $other_fill = '';
                                                        $other_value_fill = '';
                                                        foreach ($ActivityTypedata as $activity_type) {
                                                            $activity_type_id[] = $activity_type['id'];
                                                            $activeper = $activity_type['hours'] / $totalHours * 100;
                                                            ?>                                                    
                                                            <tr>
                                                                <td class="tdCol-1 noBorderTop"><?php echo $activity_type['name']; ?></td>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <!--                                                                <td class="tdCol-2 noBorderTop" align="center"><?php // echo $activity_type['hours'];                                                                                                                   ?></td>-->
                                                                <td class="tdCol-2 noBorderTop" align="center"><?php echo m2h("i:s", $activity_type['hours']); ?></td>
                                                                <td class="tdCol-3 noBorderTop" align="center"><?php echo round($activeper, 2) . '%'; ?></td>
                                                                <td class="tdCol-2 noBorderTop"><?php echo number_format($activity_type['Rate'], 2); ?></td>                            
                                                                <td class="tdCol-3 noBorderTop tdNone"></td>
                                                            </tr>
                                                            <?php
                                                            $i++;
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>                                        
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <table cellpadding="0" border="0" cellspacing="0" style="margin-top: 2px;">
                                        <thead>
                                            <tr class="subTitle">
                                                <th align="center" class="tdCol-3 tdNone"><span> <?php echo $get_info_sales_rep['first_name'] . ' ' . $get_info_sales_rep['last_name']; ?> doesn't have any records</span></th>
                                            </tr>
                                        </thead>
                                    </table>
                                <?php } ?>
                            </div>
                        <?php }
                        ?>
                        <?php if ($number_s == 1) { ?>
                            <div class="tableSec summryTable clearfix">
                                <table cellpadding="0" border="0" cellspacing="0" style="margin-top: 10px;">
                                    <thead>
                                        <tr class="subTitle" style="background-color: black;">
                                            <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="font-size: 15px; line-height: 27px; border-right: 0px solid">Total # of Hours For Sales Representatives (<?php echo m2h("i:s", $total_hours_export_sales_salesteam); ?>)</span></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        <?php } ?>
                    <?php } else {
                        ?>
                        <div class="tableSec summryTable clearfix">
                            <table cellpadding="0" border="0" cellspacing="0">
                                <thead>
                                    <tr class="subTitle">
                                        <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="line-height: initial;">No User Found</span></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>

            <div id="tab5" class="" style="display: none;">
                <div class="filterBox clearfix">                    
                    <h2>Select Rotation</h2>
                    <?php if (!empty($success_c)) {
                        ?>
                        <p style="color:red; font-weight: bold;"><?php echo $success_c; ?></p>
                    <?php } ?>
                    <form action="" name="user_export_by_rotation" id="user_export_by_rotation" method="POST">
                        <div class="grid_6 dateArea marginNone" style="margin-bottom:0px!important;">                           
                            <div class="dateArea marginNone">
                                <select tabindex="8" name="rotation" id="rotation_select">
                                    <option value="">Select Rotation</option>
                                    <?php
                                    $results = User::getAllRotations();
                                    foreach ($results as $result) {
                                        ?>
                                        <option value="<?php echo $result['id']; ?>" <?php echo ($result['id'] == $_POST['rotation']) ? 'selected="selected"' : ''; ?>><?php echo $result['rotation_title']; ?></option>  
                                    <?php } ?>
                                </select>                               
                            </div>                            

                            <div class="centerbtn">  <input type="submit" name="user_rotation_export_view" id="user_rotation_export_view" value="View" class="redBtn" style="margin-top: -28px;">                            
                                <input type="submit" name="user_rotation_export" id="button_user_rotation_export" value="Export Activity Level" class="redBtn" style="margin-top: -28px;">                            
                                <input type="submit" name="user_rotation_export_minify" id="button_user_rotation_export_minify" value="Export Summary Level" class="redBtn" style="margin-top: -28px;">                          </div>  
                        </div>
                    </form>                 
                </div> 
                <?php
                if (!empty($view_c)) {

                    if (isset($_POST['user_rotation_export_view'])) {
                        $rotation_id = $_POST['rotation'];
                        $rotation_fetch_data = User::getRotationById($rotation_id);
                        $get_info_managers = User::getAllusersByRotation($rotation_id, 3);
                        $get_info_sales_reps = User::getAllusersByRotation($rotation_id, 4);
                    }
                    $total_hours_export_manager_salesteam = 0;
                    $total_hours_export_sales_salesteam = 0;
                    $number_m = 0;
                    $number_s = 0;
                    ?>
                    <div class="tableSec summryTable clearfix">
                        <table cellpadding="0" border="0" cellspacing="0">
                            <thead>
                                <tr class="subTitle">
                                    <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="font-size: 20px; line-height: initial;">All MANAGERS</span></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <?php
                    if (!empty($get_info_managers)) {
                        foreach ($get_info_managers as $get_info_manager) {
                            $totalHours = Reporting::getTotalHoursByDateRangeAndUserId($rotation_fetch_data['start_date'], $rotation_fetch_data['end_date'], $get_info_manager['roation_user_id']);
                            $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndUserId($rotation_fetch_data['start_date'], $rotation_fetch_data['end_date'], $get_info_manager['roation_user_id']);
                            $sales_team_name = User::getIndustryByUserId($get_info_manager['industry_id']);
                            $total_hours_export_manager_salesteam += $totalHours;
                            ?>
                            <div class="tableSec summryTable clearfix">

                                <?php
                                if ($meetingHistoryData) {
                                    $number_m = 1;
                                    ?>
                                    <div class="summryTableHead clearfix">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <thead>
                                                <tr class="mainTitle">
                                                    <th width="20%" align="left" class="tdCol-1"><span>Activity Summary (<?php echo $get_info_manager['first_name'] . ' ' . $get_info_manager['last_name']; ?>) (<?php echo m2h("i:s", $totalHours); ?>)</span></th>
                                                    <th width="8%"  align="center" class="tdCol-2"><span>Hours & Minutes</span></th>
                                                    <th width="8%" class="tdCol-3 noBorderTop " align="center"><span>% of Total Hours & Minutes for Selected Date Range</span></th>
                                                    <th width="8%"  align="center" class="tdCol-2"><span>Difficulty Level</span></th>                
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <?php
                                    $totalpercntt = 0;
                                    foreach ($meetingHistoryData as $meeting) {
                                        $totalpercntt += round($meeting['hours'] / $totalHours * 100, 2);
                                        $ActivityTypedata = Reporting::getActivityTypeHoursForExportData($get_info_manager['roation_user_id'], $meeting['meeting_category']);
                                        $ActivityTypedatacomments = Reporting::getActivityTypeCommentsForExportData($get_info_manager['roation_user_id'], $meeting['meeting_category']);
                                        $max = count($ActivityTypedata);
                                        $rate_count_rating = 0;
                                        foreach ($ActivityTypedata as $row_count_rating) {
                                            $rate_count_rating += ($row_count_rating['Rate']);
                                        }
                                        ?>
                                        <div class="percentageTable clearfix">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <thead>
                                                    <tr class="subTitle">
                                                        <th align="left" class="tdCol-1"><span><?php echo $meeting['name'] ?></span></th>                
                                                        <th align="center" class="tdCol-2"><span><?php echo m2h("i:s", $meeting['hours']); ?></span></th>
                                                        <th align="center" class="tdCol-3 tdNone"><span><?php echo round($meeting['hours'] / $totalHours * 100, 2) . '%'; ?></span></th>
                                                        <th align="center" class="tdCol-2"><span><?php echo number_format($rate_count_rating / sizeof($ActivityTypedata), 2); ?></span></th>
                                                    </tr>
                                                </thead>
                                            </table>

                                            <div class="grid_12a leftTableInfo">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <?php
                                                        $other_fill = '';
                                                        $other_value_fill = '';
                                                        foreach ($ActivityTypedata as $activity_type) {
                                                            $activity_type_id[] = $activity_type['id'];
                                                            $activeper = $activity_type['hours'] / $totalHours * 100;
                                                            if ($activity_type['name'] == 'Other') {
                                                                $other_fill = $activity_type['name'];
                                                                $other_value_fill = round($activeper, 2) . '%';
                                                            } else {
                                                                ?>                                                    
                                                                <tr>
                                                                    <td class="tdCol-1 noBorderTop"><?php echo $activity_type['name']; ?></td>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <!--                                                                <td class="tdCol-2 noBorderTop" align="center"><?php // echo $activity_type['hours'];                                                                                                                    ?></td>-->
                                                                    <td class="tdCol-2 noBorderTop" align="center"><?php echo m2h("i:s", $activity_type['hours']); ?></td>
                                                                    <td class="tdCol-3 noBorderTop" align="center"><?php echo round($activeper, 2) . '%'; ?></td>
                                                                    <td class="tdCol-2 noBorderTop"><?php echo number_format($activity_type['Rate'], 2); ?></td>                          
                                                                    <td class="tdCol-3 noBorderTop tdNone"></td>
                                                                </tr>
                                                                <?php
                                                                $i++;
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>                                        

                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <table cellpadding="0" border="0" cellspacing="0" style="margin-top: 2px;">
                                        <thead>
                                            <tr class="subTitle">
                                                <th align="center" class="tdCol-3 tdNone"><span> <?php echo $get_info_manager['first_name'] . ' ' . $get_info_manager['last_name']; ?> doesn't have any records</span></th>
                                            </tr>
                                        </thead>
                                    </table>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <?php if ($number_m == 1) { ?>
                            <div class="tableSec summryTable clearfix">
                                <table cellpadding="0" border="0" cellspacing="0" style="margin-top: 10px;">
                                    <thead>
                                        <tr class="subTitle" style="background-color: black;">
                                            <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="font-size: 15px; line-height: 27px; border-right: 0px solid;">Total # of Hours For Managers (<?php echo m2h("i:s", $total_hours_export_manager_salesteam); ?>)</span></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="tableSec summryTable clearfix">
                            <table cellpadding="0" border="0" cellspacing="0">
                                <thead>
                                    <tr class="subTitle">
                                        <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="line-height: initial;">No User Found</span></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    <?php } ?>
                    <div class="tableSec summryTable clearfix">
                        <table cellpadding="0" border="0" cellspacing="0" style="margin-top: 10px;">
                            <thead>
                                <tr class="subTitle">
                                    <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="font-size: 20px; line-height: initial;">All SALES REPRESENTATIVES</span></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <?php
                    if (!empty($get_info_sales_reps)) {
                        foreach ($get_info_sales_reps as $get_info_sales_rep) {
                            $totalHours = Reporting::getTotalHoursByDateRangeAndUserId($rotation_fetch_data['start_date'], $rotation_fetch_data['end_date'], $get_info_sales_rep['roation_user_id']);
                            $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndUserId($rotation_fetch_data['start_date'], $rotation_fetch_data['end_date'], $get_info_sales_rep['roation_user_id']);
                            $sales_team_name = User::getIndustryByUserId($get_info_sales_rep['industry_id']);
                            $total_hours_export_sales_salesteam += $totalHours;
                            ?>
                            <div class="tableSec summryTable clearfix">
                                <?php
                                if ($meetingHistoryData) {
                                    $number_s = 1;
                                    ?>
                                    <div class="summryTableHead clearfix">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <thead>
                                                <tr class="mainTitle">
                                                    <th width="20%" align="left" class="tdCol-1"><span>Activity Summary (<?php echo $get_info_sales_rep['first_name'] . ' ' . $get_info_sales_rep['last_name']; ?>) (<?php echo m2h("i:s", $totalHours); ?>)</span></th>
                                                    <th width="8%"  align="center" class="tdCol-2"><span>Hours & Minutes</span></th>
                                                    <th width="8%" class="tdCol-3 noBorderTop " align="center"><span>% of Total Hours & Minutes for Selected Date Range</span></th>
                                                    <th width="8%"  align="center" class="tdCol-2"><span>Difficulty Level</span></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <?php
                                    $totalpercntt = 0;
                                    foreach ($meetingHistoryData as $meeting) {
                                        $totalpercntt += round($meeting['hours'] / $totalHours * 100, 2);
                                        $ActivityTypedata = Reporting::getActivityTypeHoursForExportData($get_info_sales_rep['roation_user_id'], $meeting['meeting_category']);
                                        $ActivityTypedatacomments = Reporting::getActivityTypeCommentsForExportData($get_info_sales_rep['roation_user_id'], $meeting['meeting_category']);
                                        $max = count($ActivityTypedata);
                                        $rate_count_rating = 0;
                                        foreach ($ActivityTypedata as $row_count_rating) {
                                            $rate_count_rating += ($row_count_rating['Rate']);
                                        }
                                        ?>
                                        <div class="percentageTable clearfix">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <thead>
                                                    <tr class="subTitle">
                                                        <th align="left" class="tdCol-1"><span><?php echo $meeting['name'] ?></span></th>                
                                                        <th align="center" class="tdCol-2"><span><?php echo m2h("i:s", $meeting['hours']); ?></span></th>
                                                        <th align="center" class="tdCol-3 tdNone"><span><?php echo round($meeting['hours'] / $totalHours * 100, 2) . '%'; ?></span></th>
                                                        <th align="center" class="tdCol-2"><span><?php echo number_format($rate_count_rating / sizeof($ActivityTypedata), 2); ?></span></th>                    
                                                    </tr>
                                                </thead>
                                            </table>

                                            <div class="grid_12a leftTableInfo">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <?php
                                                        $other_fill = '';
                                                        $other_value_fill = '';
                                                        foreach ($ActivityTypedata as $activity_type) {
                                                            $activity_type_id[] = $activity_type['id'];
                                                            $activeper = $activity_type['hours'] / $totalHours * 100;
                                                            ?>                                                    
                                                            <tr>
                                                                <td class="tdCol-1 noBorderTop"><?php echo $activity_type['name']; ?></td>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <!--                                                                <td class="tdCol-2 noBorderTop" align="center"><?php // echo $activity_type['hours'];                                                                                                                   ?></td>-->
                                                                <td class="tdCol-2 noBorderTop" align="center"><?php echo m2h("i:s", $activity_type['hours']); ?></td>
                                                                <td class="tdCol-3 noBorderTop" align="center"><?php echo round($activeper, 2) . '%'; ?></td>
                                                                <td class="tdCol-2 noBorderTop"><?php echo number_format($activity_type['Rate'], 2); ?></td>                            
                                                                <td class="tdCol-3 noBorderTop tdNone"></td>
                                                            </tr>
                                                            <?php
                                                            $i++;
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>                                        

                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <table cellpadding="0" border="0" cellspacing="0" style="margin-top: 2px;">
                                        <thead>
                                            <tr class="subTitle">
                                                <th align="center" class="tdCol-3 tdNone"><span> <?php echo $get_info_sales_rep['first_name'] . ' ' . $get_info_sales_rep['last_name']; ?> doesn't have any records</span></th>
                                            </tr>
                                        </thead>
                                    </table>
                                <?php } ?>
                            </div>
                        <?php }
                        ?>
                        <?php if ($number_s == 1) { ?>
                            <div class="tableSec summryTable clearfix">
                                <table cellpadding="0" border="0" cellspacing="0" style="margin-top: 10px;">
                                    <thead>
                                        <tr class="subTitle" style="background-color: black;">
                                            <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="font-size: 15px; line-height: 27px; border-right: 0px solid">Total # of Hours For Sales Representatives (<?php echo m2h("i:s", $total_hours_export_sales_salesteam); ?>)</span></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        <?php } ?>
                    <?php } else {
                        ?>
                        <div class="tableSec summryTable clearfix">
                            <table cellpadding="0" border="0" cellspacing="0">
                                <thead>
                                    <tr class="subTitle">
                                        <th align="center" class="tdCol-3 tdNone" style="padding: 8px 9px 8px 10px;"><span style="line-height: initial;">No User Found</span></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>       
    </div>
    <!-- ( TABS END ) --> 
</section>
<?php include('includes/footer.php'); ?>
<script>
    $(document).ready(function () {
        $("#end_date_m").change(function () {
            var start_date = $('#start_date_m').val();
            var end_date = $('#end_date_m').val();
            if (end_date < start_date) {
                fancyAlert('Please select valid date range');
                $('#end_date_m').val('');
            }
        });
        $("#end_date_s").change(function () {
            var start_date = $('#start_date_s').val();
            var end_date = $('#end_date_s').val();
            if (end_date < start_date) {
                fancyAlert('Please select valid date range');
                $('#end_date_s').val('');
            }
        });
        $("#end_date").change(function () {
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            if (end_date < start_date) {
                fancyAlert('Please select valid date range');
                $('#end_date').val('');
            }
        });
<?php if (isset($_POST['start_date_m'])) {
    ?>
            $("#start_date_m").datepicker("setDate", "<?php echo $_POST['start_date_m']; ?>");
    <?php
} else {
    ?>
            $("#start_date_m").datepicker("setDate", "");
<?php } ?>
<?php if (isset($_POST['end_date_m'])) {
    ?>
            $("#end_date_m").datepicker("setDate", "<?php echo $_POST['end_date_m']; ?>");
    <?php
} else {
    ?>
            $("#end_date_m").datepicker("setDate", "");
<?php } ?>
<?php if (isset($_POST['start_date_s'])) {
    ?>
            $("#start_date_s").datepicker("setDate", "<?php echo $_POST['start_date_s']; ?>");
    <?php
} else {
    ?>
            $("#start_date_s").datepicker("setDate", "");
<?php } ?>
<?php if (isset($_POST['end_date_s'])) {
    ?>
            $("#end_date_s").datepicker("setDate", "<?php echo $_POST['end_date_s']; ?>");
    <?php
} else {
    ?>
            $("#end_date_s").datepicker("setDate", "");
<?php } ?>
    });
    $('#manager_id').change(function () {
        var optionVal = $('#manager_id').val();
        if (optionVal == 'selectAll') {
            $('#manager_id option').prop('selected', true);
        }
    });
</script> 
