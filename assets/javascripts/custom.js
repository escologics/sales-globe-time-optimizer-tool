$(document).ready(function () {

    $("#calender_image_all_user_start").datepicker({
        showOn: "both",
        buttonImage: "assets/images/calendar.jpg",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: "mm/dd/yy",
        yearRange: "-3:+0",
        onSelect: function (selectedDate, obj) {
            var date_array = selectedDate.split("/");
            var month = date_array[0];
            var days = date_array[1];
            var year = date_array[2];
            $('#m_strat_month_all').val(month);
            $('#m_strat_day_all').val(days);
            $('#m_strat_year_all').val(year);
            $('#all_users_start_date').val(selectedDate);
        }
    });

    $("#calender_image_all_user_end").datepicker({
        showOn: "both",
        buttonImage: "assets/images/calendar.jpg",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: "mm/dd/yy",
        yearRange: "-3:+0",
        onSelect: function (selectedDate, obj) {
            var date_array = selectedDate.split("/");
            var month = date_array[0];
            var days = date_array[1];
            var year = date_array[2];
            $('#m_end_month_all').val(month);
            $('#m_end_day_all').val(days);
            $('#m_end_year_all').val(year);
            $('#all_users_end_date').val(selectedDate);
        }
    });

    $(".m_date").change(function () {
        if ($(this).attr('id') == 'm_strat_month_all') {

            if ($(this).val() > 12) {
                $(this).val('');
            } else if ($(this).val() < 1) {
                $(this).val('');
            }
        } else if ($(this).attr('id') == 'm_strat_day_all') {

            if ($(this).val() > daysInMonth_repoting($('#m_strat_month_all').val(), $('#m_strat_year_all').val())) {
                $(this).val('');
            } else if ($(this).val() < 1) {
                $(this).val('');
            }

        } else if ($(this).attr('id') == 'm_strat_year_all') {

            if ($(this).val() > 2015) {
                $(this).val('2015');
            } else if ($(this).val() < 2012) {
                $(this).val('2015');
            }

        } else if ($(this).attr('id') == 'm_end_month_all') {

            if ($(this).val() > 12) {
                $(this).val('');
            } else if ($(this).val() < 1) {
                $(this).val('');
            }

        } else if ($(this).attr('id') == 'm_end_day_all') {

            if ($(this).val() > daysInMonth_repoting($('#m_end_month_all').val(), $('#m_end_year_all').val())) {
                $(this).val('');
            } else if ($(this).val() < 1) {
                $(this).val('');
            }

        } else if ($(this).attr('id') == 'm_end_year_all') {

            if ($(this).val() > 2015) {
                $(this).val('2015');
            } else if ($(this).val() < 2012) {
                $(this).val('2015');
            }

        }

        var m_strat_date_all = ($('#m_strat_month_all').val() + '/' + $('#m_strat_day_all').val() + '/' + $('#m_strat_year_all').val());
        $('#all_users_start_date').val(m_strat_date_all);

        var m_end_date_all = ($('#m_end_month_all').val() + '/' + $('#m_end_day_all').val() + '/' + $('#m_end_year_all').val());
        $('#all_users_end_date').val(m_end_date_all);

    });
    
    $("#calender_image_role_start").datepicker({
        showOn: "both",
        buttonImage: "assets/images/calendar.jpg",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: "mm/dd/yy",
        yearRange: "-3:+0",
        onSelect: function (selectedDate, obj) {
            var date_array = selectedDate.split("/");
            var month = date_array[0];
            var days = date_array[1];
            var year = date_array[2];
            $('#m_strat_month_role').val(month);
            $('#m_strat_day_role').val(days);
            $('#m_strat_year_role').val(year);
            $('#role_start_date').val(selectedDate);
        }
    });

    $("#calender_image_role_end").datepicker({
        showOn: "both",
        buttonImage: "assets/images/calendar.jpg",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: "mm/dd/yy",
        yearRange: "-3:+0",
        onSelect: function (selectedDate, obj) {
            var date_array = selectedDate.split("/");
            var month = date_array[0];
            var days = date_array[1];
            var year = date_array[2];
            $('#m_end_month_role').val(month);
            $('#m_end_day_role').val(days);
            $('#m_end_year_role').val(year);
            $('#role_end_date').val(selectedDate);
        }
    });

    $(".m_date").change(function () {
        if ($(this).attr('id') == 'm_strat_month_role') {

            if ($(this).val() > 12) {
                $(this).val('');
            } else if ($(this).val() < 1) {
                $(this).val('');
            }
        } else if ($(this).attr('id') == 'm_strat_day_role') {

            if ($(this).val() > daysInMonth_repoting($('#m_strat_month_role').val(), $('#m_strat_year_role').val())) {
                $(this).val('');
            } else if ($(this).val() < 1) {
                $(this).val('');
            }

        } else if ($(this).attr('id') == 'm_strat_year_role') {

            if ($(this).val() > 2015) {
                $(this).val('2015');
            } else if ($(this).val() < 2012) {
                $(this).val('2015');
            }

        } else if ($(this).attr('id') == 'm_end_month_role') {

            if ($(this).val() > 12) {
                $(this).val('');
            } else if ($(this).val() < 1) {
                $(this).val('');
            }

        } else if ($(this).attr('id') == 'm_end_day_role') {

            if ($(this).val() > daysInMonth_repoting($('#m_end_month_role').val(), $('#m_end_year_role').val())) {
                $(this).val('');
            } else if ($(this).val() < 1) {
                $(this).val('');
            }

        } else if ($(this).attr('id') == 'm_end_year_role') {

            if ($(this).val() > 2015) {
                $(this).val('2015');
            } else if ($(this).val() < 2012) {
                $(this).val('2015');
            }

        }

        var m_strat_date_role = ($('#m_strat_month_role').val() + '/' + $('#m_strat_day_role').val() + '/' + $('#m_strat_year_role').val());
        $('#role_start_date').val(m_strat_date_role);

        var m_end_date_role = ($('#m_end_month_role').val() + '/' + $('#m_end_day_role').val() + '/' + $('#m_end_year_role').val());
        $('#role_end_date').val(m_end_date_role);

    });

});
function daysInMonth_repoting(month, year) {
    return new Date(year, month, 0).getDate();
}