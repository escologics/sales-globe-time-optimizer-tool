var baseURL = 'http://localhost/salesglobetot';
(function ($) {
    $(document).ready(function () {

        /* Input Place holder Script */
//        if ($.browser.version <= 9) {
//
//            $('input[placeholder]').each(function () {
//                var input = $(this);
//                $(input).val(input.attr('placeholder'));
//
//                $(input).focus(function () {
//                    if (input.val() == input.attr('placeholder')) {
//                        input.val('');
//                    }
//                });
//
//                $(input).blur(function () {
//                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
//                        input.val(input.attr('placeholder'));
//                    }
//                });
//            });
//        }
        /* Tabs Script */
        $(function () {
            var tabsContainers = $('div.tabs > div > div');
            tabsContainers.hide().filter($(".unique")).show();

            $('div.tabs ul.tabsNavigation a').click(function () {
                tabsContainers.hide();
                tabsContainers.filter(this.hash).show();
                $('div.tabs ul.tabsNavigation a').removeClass('selected');
                $(this).addClass('selected');
                return false;
            }).filter($(".unique")).click();
        });

        // Nth Child
        $(".tableSec table tbody tr:nth-child(2n+1)").addClass("light");
        $(".tableSec table.pinkRow tbody tr:nth-child(even)").addClass("pinkLight").removeClass("light");
        $(".tableSec table.assignedTable tbody tr:nth-child(2n+1)").removeClass("light");
        $("td.assignedTd table.pinkTable tbody tr:nth-child(odd)").addClass("pinkLight");


        /* =============== FAQ Accordian ============== */
        if ($('.faq').length)
            $('.acc_container').hide();
        //$('.faqQues:first').addClass('active').next().show(); 

        //On Click
        $('.archiveTilte').click(function () {

            $('.archiveTilte').removeClass('active').next().slideUp('slow');
            $('.archiveTilte').children('h2').removeClass('arrowClose');

            if ($(this).next().is(':hidden')) {
                $(this).toggleClass('active').next().slideDown('slow');
                $(this).children('h2').toggleClass('arrowClose');
                return false;
            }

        });
        $('.tilteTab').click(function () {
            $('.tilteTab').removeClass('active').parent().next('.archiveInfo').slideUp('slow');
            if ($(this).parent().next('.archiveInfo').is(':hidden')) {
                $(this).toggleClass('active').parent().next('.archiveInfo').slideDown('slow');
                return false;
            }
        });

        $(".industry_sector_salesrep").change(function () {
            var indID = $(this).val();
            $.ajax({
                type: "POST",
                cache: false,
                url: "ajaxDesignation.php",
                data: {
                    ind_id: indID
                }
            }).done(function (html) {
                $('.designation_sales_rep').html(html);
            });
        });

        // $("#meeting_category").change(function () {
        $('#meeting_category').on('change', '', function (e) {
            var val = $(this).val();
            //alert(val);
            if (val == 2) {
                $('#hours').css('width', '43px');
            }
        });

        if ($('.hideField').is(':checked')) {
            $('#start_date_s').attr('disabled', true);
            $('#end_date_s').attr('disabled', true);
            $('#days_select').attr('disabled', false);
        }

        $(".hideField").on('click', function () {

            if ($(this).is(':checked')) {
                $('#start_date_s').attr('disabled', true);
                $('#end_date_s').attr('disabled', true);
                $('#days_select').attr('disabled', false);
            } else {
                $('#start_date_s').attr('disabled', false);
                $('#end_date_s').attr('disabled', false);
                $('#days_select').attr('disabled', true);
            }
        });

        if ($('.hideField_m').is(':checked')) {
            $('#start_date_m').attr('disabled', true);
            $('#end_date_m').attr('disabled', true);
            $('#days_select_m').attr('disabled', false);
        }

        $(".hideField_m").on('click', function () {

            if ($(this).is(':checked')) {
                $('#start_date_m').attr('disabled', true);
                $('#end_date_m').attr('disabled', true);
                $('#days_select_m').attr('disabled', false);
            } else {
                $('#start_date_m').attr('disabled', false);
                $('#end_date_m').attr('disabled', false);
                $('#days_select_m').attr('disabled', true);
            }
        });


        var meeting_date_check = $('#meeting_date_check').val();

        var data_array = new Array();
        $("input#meeting_category").each(function (index)
        {
            var category = $(this).val();
            data_array[index] = {category: category};
        });
        $.ajax({
            type: "POST",
            cache: false,
            url: "ajaxActivityType.php",
            data: {
                category: data_array,
                meeting_date_check: meeting_date_check
            },
            beforeSend: function () {
                $('.loader').show();
            }
        }).done(function (data) {
            $("#ajaxbody").html(data);
            $('.activityrating').rating();
            $('.loader').hide();
            $('#ajaxbody').show();
            $('.previousActivtyDetails').show();
            $('[data-toggle="tooltip"]').tooltip();
            $('#ajaxbody').find('.activityrating').each(function (index) {
                var checkedValue = $(this).find('.checked').val();
                $(this).find('.stars').find('.star').each(function (indexx) {
                    if ($(this).attr('title') == checkedValue) {
                        $(this).addClass('fullStar');
                    }
                });
            });
        });

        var user_id = $('#current_loggedin_user').val();
        $.ajax({
            type: "POST",
            cache: false,
            url: "ajaxGetselecteddays.php",
            data: {
                user_id: user_id
            }
        }).done(function (datee) {

            $.ajax({
                type: "POST",
                cache: false,
                url: "ajaxGetpreviousdays.php",
                dataType: "json",
                data: {
                    id_user: user_id
                }
            }).done(function (output) {
                $('#getdatefromdb').val(output.previousDays);
                var disabledDays = eval(datee);
                var date = new Date();
                $(".datepicker_view_my_day_2").datepicker({
                    showOn: "both",
                    buttonImage: "assets/images/calendar.jpg",
                    buttonImageOnly: true,
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "mm/dd/yy",
                    minDate: output.previousDays,
                    maxDate: output.nextDays,
                    beforeShowDay: function (date) {
                        var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
                        for (i = 0; i < disabledDays.length; i++) {
                            if ($.inArray(y + '-' + (m + 1) + '-' + d, disabledDays) != -1) {
                                //return [false];
                                return [true, 'ui-state-active', ''];
                            }
                        }
                        // var day = date.getDay();
                        //return [(day != 0 && day != 6)];
                        return [true];

                    },
                    onSelect: function (selectedDate, obj) {

                        var date_array = selectedDate.split("/");
                        var month = date_array[0];
                        var days = date_array[1];
                        var year = date_array[2];
                        $('#mm').val(month);
                        $('#dd').val(days);
                        $('#yy').val(year);
                        $('#meeting_date').val(selectedDate);
                        $.ajax({
                            type: "POST",
                            cache: false,
                            url: "checksubmitday.php",
                            data: {
                                user_id: user_id,
                                date_submit: selectedDate
                            }
                        }).done(function (status) {

                            if (status == 0) {
                                fancyAlertConfirm('You have already submitted the hours for this day ' + selectedDate + '. ', baseURL + '/viewmyhistory.php');
                            } else {
                                $.ajax({
                                    type: "POST",
                                    cache: false,
                                    url: "gethourtotal.php",
                                    data: {
                                        user_id: user_id,
                                        selectedDate: selectedDate
                                    }
                                }).done(function (totmin) {
                                    if (totmin > 0) {
                                        $('#remaningTime').text(totmin);
                                        $('#remaningTimehidden').val(totmin);
                                        if (status == 1) {
                                            fancyAlertConfirmviewmyday('You have already started logging activities for ' + selectedDate + '.  Would you like to choose a different day or edit the existing day?', baseURL + '/viewUnSavedDays.php?selected_date=' + selectedDate);
                                        }
                                    } else {
                                        $('#remaningTime').text('0');
                                        $('#remaningTimehidden').val('0');
                                    }
                                });
                            }
                        });

                    }
                });

            });

        });

//For Rotations Only


    });


    $('#deleteLog').click(function () {
        $('#delete').trigger('click');
    });

    /*
     *Submitted when continue button clicked
     **/

    $('#submitdayid').click(function () {
        var error = 0;
        var meeting_date = '';

        if ($('#menualDatecheck').is(':checked')) {
            var mm = $('#mm').val();
            var dd = $('#dd').val();
            var yy = $('#yy').val();
            meeting_date = (mm + '/' + dd + '/' + yy);

        } else {
            meeting_date = $("#meeting_date").val();

        }

        if (meeting_date == "") {
            error = 1
            fancyAlert('Please select date..');
            return false;

        }

        $("input.activity_typeclass:checkbox:checked").each(function (index)
        {
            var rowid = $(this).attr('data-rowid');
            var hours = $(".hours" + rowid).val();
            var minutes = $(".minutes" + rowid).val();
            var activity_category = $(".activity_category" + rowid).val();
            var activity_type = $(this).val();
            var activity_rate = $(".rating" + rowid + " .fullStar").length;
            var activity_comment = $(".comment" + rowid).val();

            if ((hours == '' && minutes == ''))
            {
                error = 1;
                $(this).parents('td').css("background-color", "#e80703");
               // fancyAlert('Please Select Time.');
               // return false;
            } else {
                $(this).parents('td').css("background-color", "");
            }
            if (!activity_rate)
            {
                error = 1;
                $(".rating" + rowid).parents('td').css("background-color", "#e80703");
               // fancyAlert('Please select difficulty level.');
               // return false;
            } else {
                $(".rating" + rowid).parents('td').css("background-color", "");
            }

//            if (activity_category == 3)
//            {
//                if (!activity_comment) {
//                    error = 1;
//                    fancyAlert('Comments Are Required When Submitting an OTHER Activity.');
//                    return false;
//                }
//            }

            if (activity_type == 21 || activity_type == 35 || activity_type == 36 || activity_type == 37)
            {
                if (!activity_comment) {
                    error = 1;
                    $(".comment" + rowid).parents('td').css("background-color", "#e80703");
                   // fancyAlert('Comments Are Required When Submitting an OTHER Activity.');
                   // return false;
                } else {
                    $(".comment" + rowid).parents('td').css("background-color", "");
                }
            }

        });

        if ($("input.activity_typeclass:checkbox:checked").length == 0) {
            fancyAlert('Please select at least one Sub-Activity.');
            //return false;
        }

        if (error == 0) {
            $('#submitMyDaypopup').trigger('click');
        } else {
            fancyAlert('Please Select Mandatory Fields.');
            return false;
        }

    });

    $('#submitmydayyes').click(function () {
        var meeting_date = '';
        if ($('#menualDatecheck').is(':checked')) {
            var mm = $('#mm').val();
            var dd = $('#dd').val();
            var yy = $('#yy').val();
            meeting_date = (mm + '/' + dd + '/' + yy);
        } else {
            meeting_date = $("#meeting_date").val();
        }
        var user_id = $('#current_loggedin_user').val();
        var data_array = new Array();
        var time = '';
        var h = '';
        var m = '';
        $("input.activity_typeclass:checkbox:checked").each(function (index)
        {
            var rowid = $(this).attr('data-rowid');
            var activity_category = $(".activity_category" + rowid).val();
            var hours = $(".hours" + rowid).val();
            var minutes = $(".minutes" + rowid).val();
            var activity_type = $(this).val();
            var activity_rate = $(".rating" + rowid + " .fullStar").attr('title');
            var activity_comment = $(".comment" + rowid).val();

            h = parseInt(hours);
            m = parseInt(minutes);

            if (hours != '') {
                if (minutes != '') {
                    time = (h * 60 + m);
                } else {
                    time = (h * 60);
                }
            } else {
                time = m;
            }

            data_array[index] = {hours: time, activity_type: activity_type, activity_rate: activity_rate, activity_comment: activity_comment, activity_category: activity_category};
            time = 0;
        });

        $.ajax({
            type: "POST",
            cache: false,
            url: "checksubmitday.php",
            data: {
                user_id: user_id,
                date_submit: meeting_date
            }
        }).done(function (status) {

            if (status == 0) {
                fancyAlertConfirm('You have already submitted the hours for this day ' + meeting_date + '. ', baseURL + '/viewmyhistory.php');
            } else {

                $.ajax({
                    type: "POST",
                    cache: false,
                    url: "submitMeeting.php",
                    data: {
                        meeting_date: meeting_date,
                        user_id: user_id,
                        data_array: data_array
                    }
                }).done(function (data) {
                    if (data == 'submited') {
                        fancyAlert('You can not add above 1440 minutes');
                        return false;
                    } else {
                        window.location = baseURL + "/viewmyhistory.php";
                    }
                });
            }
        });

        return false;
    });

    $('#continueid').click(function () {
        var error = 0;
        var meeting_date = '';

        if ($('#menualDatecheck').is(':checked')) {
            var mm = $('#mm').val();
            var dd = $('#dd').val();
            var yy = $('#yy').val();
            meeting_date = (mm + '/' + dd + '/' + yy);

        } else {
            meeting_date = $("#meeting_date").val();

        }

        if (meeting_date == "") {
            error = 1
            fancyAlert('Please select date..');
            return false;

        }

        $("input.activity_typeclass:checkbox:checked").each(function (index)
        {
            var rowid = $(this).attr('data-rowid');
            var hours = $(".hours" + rowid).val();
            var minutes = $(".minutes" + rowid).val();
            var activity_category = $(".activity_category" + rowid).val();
            var activity_type = $(this).val();
            var activity_rate = $(".rating" + rowid + " .fullStar").length;
            var activity_comment = $(".comment" + rowid).val();


            if ((hours == '' && minutes == ''))
            {
                error = 1;
                $(this).parents('td').css("background-color", "#e80703");
                //fancyAlert('Please Select Time.');
               // return false;
            } else {
                $(this).parents('td').css("background-color", "");
            }
            if (!activity_rate)
            {
                error = 1;
                $(".rating" + rowid).parents('td').css("background-color", "#e80703");
                //fancyAlert('Please select difficulty level.');
               // return false;
            } else {
                $(".rating" + rowid).parents('td').css("background-color", "");
            }

            if (activity_type == 21 || activity_type == 35 || activity_type == 36 || activity_type == 37)
            {
                if (!activity_comment) {
                    error = 1;
                    $(".comment" + rowid).parents('td').css("background-color", "#e80703");
                   // fancyAlert('Comments Are Required When Submitting an OTHER Activity.');
                   // return false;
                } else {
                    $(".comment" + rowid).parents('td').css("background-color", "");
                }
            }

        });

        if ($("input.activity_typeclass:checkbox:checked").length == 0) {
            fancyAlert('Please select at least one Sub-Activity.');
            error = 1;
            //return false;
        }

        if (error == 0) {
            $('#continueSumit').trigger('click');
        } else {
            fancyAlert('Please Select Mandatory Fields.');
            return false;
        }

    });

    $('#continueSumit').click(function () {
        var meeting_date = '';
        if ($('#menualDatecheck').is(':checked')) {
            var mm = $('#mm').val();
            var dd = $('#dd').val();
            var yy = $('#yy').val();
            meeting_date = (mm + '/' + dd + '/' + yy);
        } else {
            meeting_date = $("#meeting_date").val();
        }
        var user_id = $('#current_loggedin_user').val();
        var checkAddanotheractivity = $('#checkAddanotheractivity').val();
        var data_array = new Array();
        var time = '';
        var h = '';
        var m = '';
        $("input.activity_typeclass:checkbox:checked").each(function (index)
        {

            var rowid = $(this).attr('data-rowid');
            var activity_category = $(".activity_category" + rowid).val();
            var hours = $(".hours" + rowid).val();
            var minutes = $(".minutes" + rowid).val();
            var activity_type = $(this).val();
            var activity_rate = $(".rating" + rowid + " .fullStar").attr('title');
            var activity_comment = $(".comment" + rowid).val();

            h = parseInt(hours);
            m = parseInt(minutes);

            if (hours != '') {
                if (minutes != '') {
                    time = (h * 60 + m);
                } else {
                    time = (h * 60);
                }
            } else {
                time = m;
            }
            data_array[index] = {hours: time, activity_type: activity_type, activity_rate: activity_rate, activity_comment: activity_comment, activity_category: activity_category};
            time = 0;
        });
        console.log(data_array);
        $.ajax({
            type: "POST",
            cache: false,
            url: "checksubmitday.php",
            data: {
                user_id: user_id,
                date_submit: meeting_date
            }
        }).done(function (status) {
            if (status == 0) {
                fancyAlertConfirm('You have already submitted the hours for this day ' + meeting_date + '. ', baseURL + '/viewmyhistory.php');
            } else {
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: "continuemeeting.php",
                    data: {
                        meeting_date: meeting_date,
                        user_id: user_id,
                        data_array: data_array,
                        checkAddanotheractivity: checkAddanotheractivity
                    }

                }).done(function (getdata) {
                    if (getdata == 'error') {
                        fancyAlert('You can not add above 1440 minutes');
                        return false;

                    } else {
                        fancyAlertConfirm('Your activity has been saved successfully.', baseURL + '/viewUnSavedDays.php');
                    }
                });
            }
        });

        return false;

    });


})(jQuery);
/* Fancy Box Script */
jQuery('.fancybox').fancybox();
//Date Picker
//$(".datepicker").datepicker({
//    showOn: "both",
//    buttonImage: "assets/images/calendar.jpg",
//    buttonImageOnly: true,
//    changeMonth: true,
//    changeYear: true,
//    dateFormat: "mm/dd/yy",
//    yearRange: "-3:+0"
//});

$(".datepicker").change(function () {
    var meeting_date = $("#meeting_date").val();
    var user_id = $('#current_loggedin_user').val();
    $.ajax({
        type: "POST",
        cache: false,
        url: "checkDate.php",
        data: {
            meeting_date: meeting_date,
            user_id: user_id

        }
    }).done(function (data) {
        var status = JSON.parse(data);

        if (status == 'count0') {
            $('#totalhours').text('0');
        } else if (status == 'status0') {
            fancyAlert('You already have submitted activity of selected date.');
            $("#meeting_date").val('');
            $('#totalhours').text('0');
        } else if (status == 'status1') {

            $.ajax({
                type: "POST",
                cache: false,
                url: "gethourtotal.php",
                data: {
                    meeting_date: meeting_date,
                    user_id: user_id

                }
            }).done(function (hours) {
                $('#totalhours').text(hours);
                if (hours > 23) {
                    fancyAlert('You have added 24 hours on this date.');
                    $("#meeting_date").val('');
                }
            });

        }
    });

});

function checkhours() {
    var totalhours = $('#totalhours').text();
    var hours = $('#hours').val();
    var total = parseFloat(totalhours) + parseFloat(hours);
    if (isNaN(hours) && hours.toString().indexOf('.') == -1) {
        $('#hours').val('');
    }
    total = parseFloat(total);
    if (total > 24) {
        fancyAlert('You can not add above 24 hours');
        $('#hours').val('');
    }
}


function checkOneDayActivity() {
    var meeting_date = $("#meeting_date").val();
    var user_id = $('#current_loggedin_user').val();
    $.ajax({
        type: "POST",
        cache: false,
        url: "checkDate.php",
        data: {
            meeting_date: meeting_date,
            user_id: user_id

        }
    }).done(function (data) {
        var status = JSON.parse(data);

        if (status == 'count0') {
            $('#totalhours').text('0');
        } else if (status == 'status0') {
            fancyAlert('You already have submitted activity of selected date.');
            $("#meeting_date").val('');
            $('#totalhours').text('0');
        } else if (status == 'status1') {
            $.ajax({
                type: "POST",
                cache: false,
                url: "gethourtotal.php",
                data: {
                    meeting_date: meeting_date,
                    user_id: user_id
                }
            }).done(function (hours) {
                $('#totalhours').text(hours);
                if (hours > 23) {
                    fancyAlert('You have added 24 hours on this date.');
                    $("#meeting_date").val('');
                }
            });

        }
    });

}
function emptyFields() {

    var date = new Date(),
            m = date.getMonth()

    var month = (m + 1);

    $('#mm').val(month);
    $('#dd').val('');

    $('#remaningTime').text('0');
    $('#remaningTimehidden').val('');
    $('#meeting_date').val('');
    jQuery.fancybox.close();
}
function submitMyDay(meeting_id) {

    $.ajax({
        type: "POST",
        cache: false,
        url: "submitMyday.php",
        data: {
            meeting_id: meeting_id


        }
    }).done(function (data) {
        window.location = baseURL + "/viewmyhistory.php";
    });
}

function convertMintoHours(min) {
    var hours = Math.floor(min / 60);
    var minutes = min % 60;
    if (hours < 10) {
        h = '0' + hours;
    } else {
        h = hours;
    }

    if (minutes < 10) {
        m = '0' + minutes;
    } else {
        m = minutes;
    }
    return h + ':' + m;
}

function changeButtonUpdate(id) {
    var html = $('#deleteNextManager_' + id).html();
    var first_name = $('#first_name_' + id).val();
    var last_name = $('#last_name_' + id).val();
    var company = $('#company_' + id).val();


    changeButtonUpdatefancy(html, first_name, last_name, company);
}
function changeButtonUpdatefancy(content, first_name, last_name, company) {
    jQuery.fancybox({
        'modal': true,
        'content': "  <form action=\"editManager.php\" method=\"post\" > <h3>Please assign all Sales Representatives under <br> this Manager to another Manager. <br><br> Select Manager</h3>" + content + " <input type='hidden' name='first_name' value=" + first_name + "><input type='hidden' name='last_name' value=" + last_name + "><input type='hidden' name='company' value=" + company + "><input class=\"redBtn\" type=\"button\" style=\"margin-top:26px;\" onclick=\"jQuery.fancybox.close();\" value=\"Close\"></form>"
    });
}
function fancyAlert(msg) {
    jQuery.fancybox({
        'modal': true,
        'content': "<div style=\"margin:1px;width:auto;text-align:center;\"><p><img src=\"assets/images/success.png\" alt=\"\"></p><h3>" + msg + "</h3> <input class=\"redBtn\" type=\"button\" onclick=\"jQuery.fancybox.close();\"  value=\"OK\"></div>"
    });


}
function fancyAlertConfirm(msg, url) {
    jQuery.fancybox({
        'modal': true,
        'content': "<div style=\"margin:1px;width:auto;text-align:center;\"><p><img src=\"assets/images/success.png\" alt=\"\"></p><h3>" + msg + "</h3> <input class=\"redBtn\" type=\"button\" onclick=\"window.location.href='" + url + "';\" value=\"OK\"></div>"
    });


}
function fancyAlertConfirmviewmyday(msg, url) {
    jQuery.fancybox({
        'modal': true,
        'content': "<div style=\"margin:1px;width:auto;text-align:center;\"><p><img src=\"assets/images/success.png\" alt=\"\"></p><h3>" + msg + "</h3> <input class=\"redBtn\" type=\"button\" onclick=\"emptyFields();\" value=\"New Day\"> <input class=\"redBtn\" type=\"button\" onclick=\"window.location.href='" + url + "';\" value=\"Edit\"></div>"
    });


}
function submitMyDay1(id) {
    jQuery.fancybox({
        'modal': true,
        'content': "<div class=\"popupBox\" style=\"margin:1px;text-align:center;display:block;\"><p><img src=\"assets/images/success.png\" alt=\"\"></p> <h3>Please Note: Once hours are submitted, you can not edit/change the information submitted.<br> Do you want to continue?</h3> <input class=\"redBtn\" type=\"button\" onclick=\"submitMyDay('" + id + "');\" value=\"Yes\"><input class=\"redBtn darkBtn\" type=\"button\" onclick=\"jQuery.fancybox.close();\"  value=\"No\"></div>"
    });

}