<?php
ob_start();
include "simple_html_dom.php";
header('Content-Type: application/excel');
header('Set-Cookie: fileLoading=true');
header('Content-Disposition: attachment; filename="sample.csv"');
$fp = fopen('php://output', 'w');
$table = $_POST['tabledata'];
$html = str_get_html($table);
if(empty($html)){
    fputcsv($fp, array('Sorry, No data found...'));
} else {
    foreach($html->find('tr') as $element)
    {
       /* $td = array();
        foreach($element->find('th') as $row)
        {
            $td [] = $row->plaintext;
        }
        fputcsv($fp, $td);*/

        $td = array();
        foreach($element->find('td') as $row)
        {
            $td [] = $row->plaintext;
        }
        fputcsv($fp, $td);
    }
}

fclose($fp);
 
 ?>
