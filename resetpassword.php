<?php
include('includes/top.php');

//Check if Data posted
if (isset($_GET['token'])) {
    $token = $_GET['token'];
    $userdara = DB::getInstance()->getResult("SELECT * FROM `users` WHERE `token` = '$token'");
    if ($userdara > 0) {
        if (Input::exists()) {
            if ($_POST['resetFormsubmit']) {
                //check validation of fomr
                $validate = new Validate();
                $validation = $validate->check($_POST, array(
                    //Create rule for username
                    'password' => array(
                        'label' => 'Password',
                        'required' => true,
                        'min' => '6',
                        'max' => '20',
                    ),
                    'c_password' => array(
                        'label' => "Confirm Password",
                        'required' => true,
                        'matches' => 'password',
                    )
                ));
                //if validation is passed
                $msg = '';
                if ($validation->passed()) {
                    $resetPassword = DB::getInstance()->updateByEmail('users', $userdara['email'], array(
                        'password' => md5(Input::post('password')),
                        'token' => NULL,
                    ));
                    $text = 'Your Password has been successfully changed';
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "From: noreply@salesglobe.com\r\n";
                    $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                    mail($userdara['email'], "Password Changed", $text, $headers);
                    $superadmin = DB::getInstance()->getResult("SELECT * FROM `users` WHERE `level` = '2'");
                    $textsuperaddmin = 'User[' . $userdara['username'] . ']' . ' ' . 'was password chanaged here is the password' . ' ' . '[' . $_POST['password'] . ']';
                    mail($superadmin['email'], "Password Changed[" . $userdara['username'] . "]", $textsuperaddmin, $headers);
                    header('Location: login.php');
                } else {
                    //loop through error messages is $msg.
                    foreach ($validation->errors() as $error) {
                        $msg .= $error . "<br>";
                    }
                }
            }
        }
    } else {
        header('Location: login.php');
    }
} else {
    header('Location: login.php');
}
?>
<!DOCTYPE html>
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="no-js ie9" lang="en"> <![endif]-->
<html class="no-js" lang="en"> 

    <head>

        <meta charset="utf-8">
        <title>User :: Reset Password</title>
        <meta name="author" content="" />   
        <meta name="description" content="" /> 
        <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1" />

        <!-- ( CSS LINKS START ) -->
        <!--<link href="favicon.ico" type="image/x-icon" rel="icon">
        <link href="favicon.ico" type="image/x-icon" rel="shortcut icon" >-->
        <link href="assets/css/default.css" type="text/css" rel="stylesheet" /> 
        <link href="assets/css/jquery.fancybox.css" type="text/css" rel="stylesheet" /> 
        <link href="assets/css/responsive.css" type="text/css" rel="stylesheet" /> 
        <!-- ( CSS LINKS END ) -->

        <!-- ( SCRIPT LIBRARY START ) -->
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.1.1.js"></script>  
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--  ( SCRIPT LIBRARY END )  --> 

    </head>

    <body>

        <!-- ( MAIN CONTAINER BEGIN ) -->
        <div id="mainContainer" class=" clearfix">

            <!-- ( CONTAINER 12 BEGIN ) -->
            <div class="container_12">

                <!-- ( CONTENT BOX BEGIN ) -->
                <div class="contentBox grid_4 marginAuto loginBox">

                    <header class="header">

                        <div class="logo">
                            <a href="login.html"><img src="assets/images/logo.gif" alt=""></a>
                        </div>
                        <!-- ( .logo end ) -->

                    </header>
                    <!-- ( .header end ) -->

                    <section class="content clearfix">
                        <p style="color: #e80702; font-size: 14; font-weight: bold;"><?php echo isset($msg) ? $msg : ''; ?></p>
                        <h1>Reset Password</h1>
                        <form action="" name="resetForm" id="resetForm" method="post">

                            <div class="clearfix">
                                <label for="password">Please enter new password</label>
                                <input type="password" tabindex="5" id="password" name="password" value="<?php echo Input::get('password'); ?>">
                            </div>
                            <div class="clearfix">
                                <label for="password">Retype your password</label>
                                <input tabindex="6" type="password" id="confirmpassword" name="c_password" value="<?php echo Input::get('c_password'); ?>">
                            </div>
                            <a style="display: none;" href="#forgot" id="callfancy" class="fancybox redBtn">Reset</a>
                            <a href="login.php" class="redBtn" style="margin-left: 10px;">Cancel</a>
                            <input type="submit" name="resetFormsubmit" class="submitGreen" id="submit" value="Submit">

                            <?php if (isset($flag)) { ?>
                                <script>
                                    $(document).ready(function () {
                                        $("#callfancy").click();
                                    });
                                </script>
                            <?php } ?>
                            <!-- ( POPUP CONTENT BEGIN ) -->
                            <div id="forgot" class="popupBox alignCenter">
                                <p><img src="assets/images/success.png" alt=""></p>
                                <h3>The new password is sent to the provided email address.</h3>
                                <a href="login.php" class="fancybox redBtn">OK</a>
                            </div>
                            <!-- ( POPUP CONTENT END ) -->

                        </form>

                    </section>
                    <!-- ( .content end ) -->

                </div>
                <!-- ( CONTENT BOX END ) -->

            </div>
            <!-- ( CONTAINER 12 END ) -->

        </div>
        <!-- ( MAIN CONTAINER BEGIN ) -->

        <!-- ( PLUGIN START ) -->
        <script type="text/javascript" src="assets/javascripts/jquery.fancybox.pack.js"></script> 
        <script type="text/javascript" src="assets/javascripts/configuration.js"></script>  
        <!-- ( PLUGIN END ) -->

    </body>

</html>