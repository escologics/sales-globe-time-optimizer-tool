<?php
$page_title = 'Add Rotation';
include('includes/top.php');
if (!Session::get('login')) {
    Redirect::to('index.php');
}
if (Session::get('level') == 3 || Session::get('level') == 4) {
    Redirect::to('viewMyday.php');
}
include('includes/header.php');
$managers = User::getAllManagers();
$slaes_representatives = User::getAllSalesRepresentative();
$error_message = '';
$success_message = '';
if (isset($_POST['submit_rotation'])) {
    $start_date = $_POST['start_date'];
    $start_date = date("Y-m-d", strtotime($start_date));

    $end_date = $_POST['end_date'];
    $end_date = date("Y-m-d", strtotime($end_date));

    $managers_rotations = $_POST['managers_rotation'];
    $salesrep_rotations = $_POST['salesrep_rotation'];

    if ($_POST['rotation_title'] == '') {
        $error_message .= 'Please Type Rotation Title.<br/>';
    }
    if ($_POST['start_date'] == '') {
        $error_message .= 'Please Select Start Date.<br/>';
    }
    if ($_POST['end_date'] == '') {
        $error_message .= 'Please Select End Date.<br/>';
    }
    if (empty($managers_rotations) && empty($salesrep_rotations)) {
        $error_message .= 'Please Select Atleast One User.<br/>';
    }

    if ($error_message == '') {
        $rotation_data = array(
            'rotation_title' => $_POST['rotation_title'],
            'start_date' => $start_date,
            'end_date' => $end_date,
        );
        $rotation_id = DB::getInstance()->insert('rotations', $rotation_data);

        if (!empty($managers_rotations)) {
            foreach ($managers_rotations as $managers_rotation) {
                DB::getInstance()->insert('rotations_users', array(
                    'rotation_id' => $rotation_id,
                    'user_level' => 3,
                    'roation_user_id' => $managers_rotation
                ));
                DB::getInstance()->insert('check_rotation_users', array(
                    'id_rotation' => $rotation_id,
                    'id_user_rotation' => $managers_rotation
                ));
            }
        }

        if (!empty($salesrep_rotations)) {
            foreach ($salesrep_rotations as $salesrep_rotation) {
                DB::getInstance()->insert('rotations_users', array(
                    'rotation_id' => $rotation_id,
                    'user_level' => 4,
                    'roation_user_id' => $salesrep_rotation
                ));
                DB::getInstance()->insert('check_rotation_users', array(
                    'id_rotation' => $rotation_id,
                    'id_user_rotation' => $salesrep_rotation
                ));
            }
        }
        Redirect::to('rotations.php?add=yes');
    }
}
?>
<style>
    .container_rotation { border:2px solid #ccc; height: 200px; overflow-y: scroll; }
    .datepicker_rotation {
        width: 80% !important;
        margin-top: -9px;
    };
</style>
<script>
    $(document).ready(function () {
        var start_date = '';
        var end_date = '';
        $(".datepicker_rotation").datepicker({
            showOn: "both",
            buttonImage: "assets/images/calendar.jpg",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            minDate: '0',
            dateFormat: "mm/dd/yy",
            yearRange: "-0:+1",
            onSelect: function (selectedDate) {
                start_date = new Date($('#start_date_rotation').val());
                end_date = new Date($('#end_date_rotation').val());

                if ((start_date && end_date)) {
                    if (end_date < start_date) {
                        fancyAlert('Please Enter Valid End Date');
                        $('#end_date_rotation').val('');
                    }
                }

            }
        });
        $(".datepicker_rotation").on('change', function () {
            start_date = new Date($('#start_date_rotation').val());
            end_date = new Date($('#end_date_rotation').val());
            if ((start_date && end_date)) {
                if (end_date < start_date) {
                    fancyAlert('Please Enter Valid End Date');
                    $('#end_date_rotation').val('');
                }
            }
        });
        $('#selectAll_manager').click(function () {
            check = $("#selectAll_manager").is(":checked");
            if (check) {
                $(".select_all_checkbox_manager").prop("checked", true);
            } else {
                $(".select_all_checkbox_manager").prop("checked", false);
            }
        });
        $('#selectAll_sales').click(function () {
            check = $("#selectAll_sales").is(":checked");
            if (check) {
                $(".select_all_checkbox_sales").prop("checked", true);
            } else {
                $(".select_all_checkbox_sales").prop("checked", false);
            }
        });
    });
</script>
<section class="createManager clearfix">
    <div class="createTab">    
        <ul class="clearfix">
            <h3>Add Rotation</h3>
        </ul>
        <div class="tabs_container clearfix">
            <div id="" class="createTabContent">
                <?php if (!empty($error_message)) : ?>
                    <p style="color: #e80702; font-weight: bold;"><?php echo $error_message; ?></p>
                <?php elseif (!empty($success_message)) : ?>
                    <p style="color: green; font-weight: bold;"><?php echo $success_message; ?></p>
                <?php endif; ?>
                <form action="" class="clearfix" id="myform" method="POST">
                    <div class="width_240 floatLeft">
                        <p>
                            <label for="Rotation Title" class="req">Rotation Title</label>
                            <input type="text" tabindex="1" name="rotation_title" id="rotation_title" value="<?php echo isset($_POST['rotation_title']) ? $_POST['rotation_title'] : '' ?>">
                        </p>
                        <p>
                            <label for="Start Date" class="req">Start Date</label>
                            <input type="text" tabindex="2" name="start_date" id="start_date_rotation" class="datepicker_rotation" value="<?php echo isset($_POST['start_date']) ? $_POST['start_date'] : '' ?>">
                        </p>
                        <p>
                            <label for="Select Managers" class="">Select Managers</label>

                        <div class="container_rotation">
                            <input type="checkbox" id="selectAll_manager" name="" /> Select All<br />
                            <?php foreach ($managers as $key => $manager) :
                                ?>
                                <input type="checkbox" class="select_all_checkbox_manager" name="managers_rotation[]" value="<?php echo $manager['id']; ?>" tabindex="4" <?php echo (isset($_POST['managers_rotation']) && in_array($manager['id'], $_POST['managers_rotation'])) ? 'checked="checked"' : ''; ?> /> <?php echo ucfirst($manager['first_name']) . ' ' . ucfirst($manager['last_name']); ?> <br />                           
                            <?php endforeach; ?>
                        </div>
                        </p>

                    </div>
                    <div class="width_240 floatRight">
                        <p>
                            <br/><br/><br/>
                        </p>
                        <p style="margin-top: 15%;">
                            <label for="End Date" class="req">End Date</label>
                            <input type="text" tabindex="3" name="end_date" id="end_date_rotation" class="datepicker_rotation" value="<?php echo isset($_POST['end_date']) ? $_POST['end_date'] : '' ?>">
                        </p>
                        <p>
                            <label for="Select Sales Representatives" class="">Select Sales Representatives</label>
                        <div class="container_rotation">
                            <input type="checkbox" id="selectAll_sales" name="" /> Select All<br />
                            <?php foreach ($slaes_representatives as $slaes_representative) : ?>
                                <input type="checkbox" class="select_all_checkbox_sales" name="salesrep_rotation[]" value="<?php echo $slaes_representative['id']; ?>" tabindex="5" <?php echo (isset($_POST['salesrep_rotation']) && in_array($slaes_representative['id'], $_POST['salesrep_rotation'])) ? 'checked="checked"' : ''; ?>/> <?php echo ucfirst($slaes_representative['first_name']) . ' ' . ucfirst($slaes_representative['last_name']); ?> <br />                           
                            <?php endforeach; ?>                            
                        </div>
                        </p>
                        <p>
                            <input type="submit" id="submit_rotation" name="submit_rotation" tabindex="6" value="Submit" class="floatRight submitGreen">
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php
include('includes/footer.php');
