<?php

include('includes/top.php');
$html = '';
$managers = DB::getInstance()->GetAllResults("Select * from users where level = 3 And status = 1");
$get_rotations = User::getAllRotations();
$industries = User::getAllIndustry();
$srr_no = 1;
if ($managers) {
    foreach ($managers as $manager) {
        $get_manager_rotations_array = array();
        $get_manager_rotations = User::getRotationsFromRotationsUsersByUserID($manager['id']);
        foreach ($get_manager_rotations as $key => $get_manager_rotation) {
            $get_manager_rotations_array[$key] = $get_manager_rotation['rotation_id'];
        }

        $html .= '<tr>
            <td colspan="8" class="assignedTd customeAssigneTd">
                <table>
                    <tbody>
                        <tr class="gradientGrey">
                            <td width="4%">' . $srr_no . '</td>
                            <td width="16%" class="tilteTab" >' . $manager['first_name'] . '</td>
                            <td width="16%" class="tilteTab">' . $manager['last_name'] . '</td>';
        $manager_industries = User::getIndustriesByUserId($manager['id']);
        $html .= '<td width="16%" class="tilteTab">';
        foreach ($manager_industries as $manager_industry) {
            $html .= '<p>' . User::getIndustryByUserId($manager_industry['sales_team_id']) . '</p>';
        }
        $html .= '</td>';
        $html .= '<td width="16%" class="tilteTab"><a href="mailto:' . $manager['email'] . '">' . $manager['email'] . '</a></td>
                            <td width="14%" class="tilteTab">' . $manager['username'] . '</td>
                            <td width="14%" class="tilteTab"><a style="cursor: pointer;">' . User::getLevelNameByLevelId($manager['level']) . '</a></td>
                            <td width="14%" align="center" class="tableBtn">
                                <a href="edituser.php?id=' . $manager['id'] . '&level=' . $manager['level'] . '" class="">
                                    <img src="assets/images/settingRed.jpg" alt="">
                                </a>';
        if (User::hasSalesRep($manager['id']) > 0) {

            $html .= '<a href="#delete_' . $manager['id'] . '" class="fancybox"><img src="assets/images/closeRed.jpg" alt=""></a>';
        } else {

            $html .= '<a href="#viewManagerDelete_' . $manager['id'] . '" class="fancybox"><img src="assets/images/closeRed.jpg" alt=""></a>';
        }
        $html .= '</td><td>';

        $html.= '</tr>
                        <tr class="archiveInfo">
                            <td width="100%" colspan="8" class="assignedTd">
                                <table class="pinkTable">
                                    <thead>
                                        <tr>
                                            <th width="4%" align="left">#</th>
                                            <th width="16%" align="left">First Name</th>
                                            <th width="16%" align="left">Last Name</th>     
                                            <th width="16%" align="left">Sales Team</th>  
                                            <th width="16%" align="left">Title</th>                                                                                                                                                                                                                                  
                                            <th width="16%" align="left">Email Address</th>
                                            <th width="14%" align="left">Username</th>
                                            <th width="14%" align="left">Access Role</th>
                                            <th width="14%" align="left">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>';

        $salesrepresentatives = User::getUsersByAssignedId($manager['id']);
        $sr_noo = 1;
        if ($salesrepresentatives) {
            foreach ($salesrepresentatives as $salesrepresentative) {

                $html .= ' <tr>
                            <td width="4%">' . $sr_noo . '</td>
                            <td width="16%">' . $salesrepresentative['first_name'] . '</td>
                            <td width="16%">' . $salesrepresentative['last_name'] . '</td>
                            <td width="16%">' . User::getIndustryByUserId($salesrepresentative['industry_id']) . '</td>
                            <td width="16%">' . User::getDesignationByUserId($salesrepresentative['designation']) . '</td>
                            <td width="16%"><a href="mailto:' . $salesrepresentative['email'] . '">' . $salesrepresentative['email'] . '</a></td>
                            <td width="14%">' . $salesrepresentative['username'] . '</td>
                            <td width="14%">' . User::getLevelNameByLevelId($salesrepresentative['level']) . '</td>
                            <td width="14%" align="center" class="tableBtn">
                                <a href="edituser.php?id=' . $salesrepresentative['id'] . '&level=' . $salesrepresentative['level'] . '" class=""><img src="assets/images/smallSettingRed.jpg" alt=""></a>
                                <a href="#viewSalesRepPopupDelete_' . $salesrepresentative['id'] . '" class="fancybox"><img src="assets/images/smallCloseRed.jpg" alt=""></a>
                            </td>';

                $html .= '
    <td>' . ($sr + 1) . ' <div id="viewSalesRepPopupDelete_' . $salesrepresentative['id'] . '" class="popupBox alignCenter width_240">
            <p><img src="assets/images/success.png" alt=""></p>
            <h3>Are you sure you want to delete this user?</h3>
            <form action="deleteUser.php" method="post">
                <input type="hidden" name="user_type" value="salesrep">
                <input type="hidden" name="user_id" value="' . $salesrepresentative['id'] . '">
                <input type="hidden" name="current_page" value="viewUsers.php" >
                <input type="submit" id="submit" name="submit" value="Yes" class="marginTop_27">
            </form>
        </div>';

                $html .=' </tr>';

                $sr_noo++;
            }
        }

        $html .= '</tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>';

        $srr_no++;
    }
    $html .= '<script>
        $(".tilteTab").click(function () {
            $(".tilteTab").removeClass("active").parent().next(".archiveInfo").slideUp("slow");
            if ($(this).parent().next(".archiveInfo").is(":hidden")) {
                $(this).toggleClass("active").parent().next(".archiveInfo").slideDown("slow");
                return false;
            }
        });
        </script>';
    echo $html;
} else {
    echo 'No Record Found';
}
                      