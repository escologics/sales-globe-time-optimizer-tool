<?php
$page_title = 'Create User';
include('includes/top.php');
if (!Session::get('login')) {
    Redirect::to('index.php');
}

if (Input::exists()) {

    if ($_POST['user_type'] == 'manager') {

        $validate = new Validate();

        $validation = $validate->check($_POST, array(
            'username' => array(
                'label' => 'Username',
                'required' => true,
                'validate' => 'username',
            ),
            'password' => array(
                'label' => 'Password',
                'required' => true,
                'min' => '6',
                'max' => '20',
            ),
            'c_password' => array(
                'label' => "Confirm Password",
                'required' => true,
                'matches' => 'password',
            ),
            'first_name' => array(
                'label' => 'First Name',
                'required' => true,
                'validate' => 'name',
                'min' => '3',
                'max' => '20',
            ),
            'last_name' => array(
                'label' => 'Last Name',
                'required' => true,
            ),
            'email' => array(
                'label' => 'Email',
                'required' => true,
                'validate' => 'email',
            ),
            'sales_team' => array(
                'label' => 'Sales Team',
                'is_selected' => true,
            ),
        ));

        $msg = '';
        if ($validation->passed()) {
            if (User::checkByUsername(Input::post('username')) == 0) {
                if (User::checkByEmail(Input::post('email')) == 0) {
                    $result = DB::getInstance()->insert('users', array(
                        'first_name' => Input::post('first_name'),
                        'last_name' => Input::post('last_name'),
                        'username' => Input::post('username'),
                        'email' => Input::post('email'),
                        'password' => md5(Input::post('password')),
                        'created_user' => date('Y-m-d'),
                        'level' => 3
                    ));

                    $user_sales_teams_ids = $_POST['sales_team'];                  
                    if (!empty($user_sales_teams_ids)) {
                        foreach ($user_sales_teams_ids as $user_sales_teams_id) {
                            DB::getInstance()->insert('user_sales_team', array(
                                'sales_team_id' => $user_sales_teams_id,
                                'user_id' => $result
                            ));
                        }
                    }

                    if ($result) {
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                        $to = Input::post('email');
                        $text = 'Your account has been created on salesglobe. <br>
                                Following are your login credentials<br>
                                Username: ' . Input::post('username') . '<br>
                                    Password: ' . Input::post('password') . '<br> 
                                     ' . Config::get('configuration/baseUrl') . 'login.php';

                        if (mail($to, "Account Created", $text, $headers, "-f noreply@salesglobe.com")) {
                            $success_msg = "Manager created successfully.";
                            unset($_POST);
                            $user_type_var = 'manager';
                        }
                    }
                } else {
                    $msg .= "Email address is already exists.";
                }
            } else {
                $msg .= 'Username is already exists.';
            }
        } else {

            foreach ($validation->errors() as $error) {
                $msg .= $error . "<br>";
            }
            $user_type_var = 'manager';
        }
    } elseif ($_POST['user_type'] == 'salesrepresentative') {

        $validate = new Validate();

        $validation = $validate->check($_POST, array(
            's_username' => array(
                'label' => 'Username',
                'required' => true,
                'validate' => 'username',
            ),
            's_password' => array(
                'label' => 'Password',
                'required' => true,
                'min' => '6',
                'max' => '20',
            ),
            's_c_password' => array(
                'label' => "Confirm Password",
                'required' => true,
                'matches' => 's_password',
            ),
            's_first_name' => array(
                'label' => 'First Name',
                'required' => true,
                'validate' => 'name',
                'min' => '3',
                'max' => '20',
            ),
            's_last_name' => array(
                'label' => 'Last Name',
                'required' => true,
            ),
            's_email' => array(
                'label' => 'Email',
                'required' => true,
                'validate' => 'email',
            ),
            'assigned_to' => array(
                'label' => 'Assign Manager',
                'required' => true
            ),
            'sales_team_rep' => array(
                'label' => 'Sales Team',
                'required' => true,
            ),
            'designation' => array(
                'label' => 'Title',
                'required' => true
            ),
        ));

        $s_msg = '';
        if ($validation->passed()) {
            if (User::checkByUsername(Input::post('s_username')) == 0) {
                if (User::checkByEmail(Input::post('s_email')) == 0) {
                    $result = DB::getInstance()->insert('users', array(
                        'first_name' => Input::post('s_first_name'),
                        'last_name' => Input::post('s_last_name'),
                        'username' => Input::post('s_username'),
                        'email' => Input::post('s_email'),
                        'password' => md5(Input::post('s_password')),
                        'level' => 4,
                        'assigned_to' => Input::post('assigned_to'),
                        'designation' => Input::post('designation'),
                        'industry_id' => Input::post('sales_team_rep'),
                        'created_user' => date('Y-m-d'),
                    ));

                    if ($result) {
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";

                        $to = Input::post('s_email');

                        $text = 'Your account has been created on salesglobe. You can login your account <br>
                                Following are your login credentials<br>
                                Username: ' . Input::post('s_username') . '<br>
                                    Password: ' . Input::post('s_password') . '<br> 
                                     ' . Config::get('configuration/baseUrl') . 'login.php';

                        $mail = mail($to, "Account Created", $text, $headers, "-f noreply@salesglobe.com");
                        if ($mail) {
                            $s_success_msg = "Sales representative created successfully.";
                            unset($_POST);
                            $user_type_var = 'representative';
                        } else {
                            $s_success_msg = "Email is not sent to user..";
                        }
                    }
                } else {
                    $s_msg .= "Email address is already exists.";
                }
            } else {
                $s_msg .= 'Username is already exists.';
            }
        } else {

            foreach ($validation->errors() as $error) {
                $s_msg .= $error . "<br>";
            }
            $user_type_var = 'representative';
        }
    }
}
include('includes/header.php');
?>
<style>
    .container_rotation { border:2px solid #ccc; height: 186px; overflow-y: scroll; }
</style>
<script>
    $(document).ready(function () {
        $("#selectAll").click(function () {
            check = $("#selectAll").is(":checked");
            if (check) {
                $(".select_all_checkbox").prop("checked", true);
            } else {
                $(".select_all_checkbox").prop("checked", false);
            }
        });
    });
</script>
<section class="createManager clearfix">
    <div class="tabs createTab">
        <ul class="clearfix tabsNavigation">
            <li><a href="#tab1" class="<?php echo (!$_POST) ? 'unique' : ''; ?> <?php echo ($user_type_var == 'manager') ? 'unique' : ''; ?>" >Create Manager</a></li>
            <li><a href="#tab2" class="<?php echo ($user_type_var == 'representative') ? 'unique' : ''; ?>">Create Sales Representative</a></li>
        </ul><!-- ( TABS LINK END ) -->
        <div class="tabs_container clearfix">
            <div id="tab1" class="createTabContent <?php echo (!$_POST['user_type']) ? 'unique' : ''; ?> <?php echo ($_POST['user_type'] == 'manager') ? 'unique' : ''; ?>">
                <form action="" class="clearfix" id="myform" method="POST">
                    <?php if ($success_msg != "" && !empty($success_msg)) {
                        ?>
                        <p style="color:green; font-weight: bold;"><?php echo $success_msg; ?></p>
                    <?php } ?>
                    <?php if ($msg != "") {
                        ?>
                        <p style="color: #e80702; font-weight: bold;"><?php echo $msg; ?></p>
                    <?php } ?>
                    <div class="width_240 floatLeft">
                        <p>
                            <label for="First Name" class="req">First Name</label>
                            <input type="text" tabindex="1" name="first_name" id="firstname" value="<?php echo Input::get('first_name'); ?>">
                        </p>
                        <p>
                            <label for="Username" class="req">Username</label>
                            <input type="text" tabindex="3" name="username" id="username" value="<?php echo Input::get('username'); ?>">
                        </p>
                        <p>
                            <label for="Password" class="req">Password</label>
                            <input type="password" tabindex="5" id="password" name="password" value="<?php echo Input::get('password'); ?>">
                        </p>
                        <div style="width: 105%;">
                            <?php
                            $get_all_salesteams = User::getAllIndustry();
//                            echo '<pre>';
//                            print_r($get_all_salesteams);
                            ?>

                            <label>Select Sales Team</label>                                                                
                            <div class="container_rotation">
                                <input type="checkbox" id="selectAll" name="" tabindex="8" /> Select All<br />
                                <?php foreach ($get_all_salesteams as $get_all_salesteam) : ?>
                                    <input type="checkbox" class="select_all_checkbox" name="sales_team[]" value="<?php echo $get_all_salesteam['id']; ?>" <?php echo (in_array($get_all_salesteam['id'], $_POST['sales_team'])) ? 'checked="checked"' : ''; ?> /> <?php echo $get_all_salesteam['name']; ?> <br />
                                <?php endforeach; ?>                            
                            </div>
                        </div>                        
                    </div>
                    <div class="width_240 floatRight">
                        <p>
                            <label for="Last Name" class="req">Last Name</label>
                            <input type="text" tabindex="2" id="lastname" name="last_name" value="<?php echo Input::get('last_name'); ?>">
                        </p>
                        <p>
                            <label for="Email Address" class="req">Email Address</label>
                            <input type="text" tabindex="4" id="emailaddress" name="email" value="<?php echo Input::get('email'); ?>">
                        </p>
                        <p>
                            <label for="Confirm Password" class="req">Confirm Password</label>
                            <input tabindex="6" type="password" id="confirmpassword" name="c_password" value="<?php echo Input::get('c_password'); ?>">
                            <input type="hidden" name="user_type" value="manager" >
                        </p>
                        <p style="width: 75px;  padding-top: 15px;">
                            <input type="submit" id="submit" name="submit" value="Submit" class="floatRight submitGreen">
                        </p>
                    </div>
                </form>
            </div>

            <div id="tab2" class="createTabContent <?php echo ($_POST['user_type'] == 'salesrepresentative') ? 'unique' : ''; ?>">
                <form action="" class="clearfix" method="POST">
                    <?php
                    if ($s_success_msg != "" && !empty($s_success_msg)) {
                        ?>
                        <p style="color:green; font-weight: bold;"><?php echo $s_success_msg; ?></p>
                        <?php
                    }
                    if ($s_msg != "") {
                        ?>
                        <p style="color: #e80702; font-weight: bold;"><?php echo $s_msg; ?></p>
                    <?php } ?>
                    <div class="width_240 floatLeft">
                        <p>
                            <label for="First Name" class="req">First Name</label>
                            <input type="text" tabindex="1" id="firstname" name="s_first_name" value="<?php echo Input::get('s_first_name'); ?>">
                        </p>
                        <p>
                            <label for="Username" class="req">Username</label>
                            <input type="text" tabindex="3" name="s_username" id="username" value="<?php echo Input::get('s_username'); ?>">
                        </p>
                        <p>
                            <label for="Password" class="req">Password</label>
                            <input type="password" tabindex="5" id="password" name="s_password" value="<?php echo Input::get('s_password'); ?>">
                        </p>
                        <p>
                            <label for="Sales Team" class="req">Sales Team</label>
                            <select tabindex="8" name="sales_team_rep" class="industry_sector_salesrep">
                                <option value="">Select Sales Team</option>
                                <?php
                                $results = User::getAllIndustry();
                                foreach ($results as $result) {
                                    ?>
                                    <option value="<?php echo $result['id']; ?>"><?php echo $result['name']; ?></option>  
                                <?php } ?>
                            </select>
                        </p>
                        <p>
                            <label for="Assign to Manager" class="req">Assign to Manager</label>
                            <select tabindex="8" name="assigned_to">
                                <option value="">Select Manager</option>
                                <?php
                                $results = User::getAllManagers();
                                foreach ($results as $result) {
                                    ?>
                                    <option value="<?php echo $result['id']; ?>" <?php echo ($_POST['assigned_to'] == $result['id']) ? 'selected="selected"' : ''; ?>><?php echo $result['first_name'] . ' ' . $result['last_name']; ?></option>  
                                <?php } ?>
                            </select>

                        </p>
                    </div>
                    <div class="width_240 floatRight">
                        <p>
                            <label for="Last Name" class="req">Last Name</label>
                            <input type="text" tabindex="2" id="lastname" name="s_last_name" value="<?php echo Input::get('s_last_name'); ?>">
                        </p>
                        <p>
                            <label for="Email Address" class="req">Email Address</label>
                            <input type="text" tabindex="4" id="emailaddress" name="s_email" value="<?php echo Input::get('s_email'); ?>">
                        </p>
                        <p>
                            <label for="Confirm Password" class="req">Confirm Password</label>
                            <input type="password" tabindex="6" id="confirmpassword" name="s_c_password" value="<?php echo Input::get('s_c_password'); ?>">
                            <input type="hidden" name="user_type" value="salesrepresentative" >
                        </p>
                        <p>
                            <label for="Designations" class="req">Title</label>
                            <select tabindex="8" name="designation" class="designation_sales_rep">
                                <option value="">Select Sales Team First</option>
                            </select>
                        </p>

                        <input type="submit" id="submit" name="submit" value="Submit" class="marginTop_27  submitGreen">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>           
<?php include('includes/footer.php'); ?>                 
