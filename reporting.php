<?php
$page_title = 'Reporting';
include('includes/top.php');
include('includes/header.php');
if (!Session::get('login')) {
    Redirect::to('index.php');
}
?>
<script src="assets/javascripts/custom.js"></script>
<style>
    img.ui-datepicker-trigger {
        margin: -5px 0px 0px 0px;
    }
</style>
<section class="createManager clearfix">
    <div class="tabs createTab">
        <ul class="clearfix tabsNavigation">
            <li><a href="#tab1" class="unique selected">Team View</a></li>
<!--            <li><a href="#tab2" class="">Individual</a></li>-->
            <li><a href="#tab3" class="">By Role</a></li>
            <li><a href="#tab4" class="">Everybody</a></li>
        </ul>
        <!-- ( TABS LINK END ) -->
        <div class="tabs_container clearfix">

            <div id="tab1" class="unique" style="display: block;">
                <div class="filterBox clearfix">
                    <h2>Select Sales Team</h2>
                    <form action="export_reporting.php" name="sales_team_reporting" id="sales_team_reporting" method="POST">
                        <div class="grid_6 dateArea marginNone" style="margin-bottom:0px!important;">                           
                            <div class="dateArea marginNone">
                                <select tabindex="8" name="sales_team">
                                    <option value="">Select Sales Team</option>
                                    <?php
                                    $results = User::getAllIndustry();
                                    foreach ($results as $result) {
                                        ?>
                                        <option value="<?php echo $result['id']; ?>"><?php echo $result['name']; ?></option>  
                                    <?php } ?>
                                </select>                               
                            </div>                            
                            <input type="submit" name="sales_team_export" value="Export" class="redBtn" style="margin-top: -28px;">                            
                        </div>
                    </form>                 
                </div>          
            </div>



            <div id="tab2" class="" style="display: none;">
                <div class="filterBox clearfix">

                    <form action="export_reporting.php" name="individual_form" id="individual_form" method="POST">

                        <div class="grid_6 dateArea clearfix marginNone" style="margin-bottom:0px!important;">   
                            <h2>Manger</h2>
                            <select name="manager_id[]" id="manager_id" multiple="multiple" style="margin-bottom:10px;">
                                <option value="" disabled="">Select Manager</option>
                                <?php
                                $results = User::getAllManagers();
                                foreach ($results as $result) {
                                    ?>
                                    <option <?php echo (isset($_POST['manager_id']) && in_array($result['id'], $_POST['manager_id'])) ? 'selected="selected"' : ''; ?> value="<?php echo $result['id']; ?>"><?php echo $result['first_name'] . ' ' . $result['last_name']; ?></option>
                                <?php } ?>
                            </select>
                            <input type="submit" name="manager_export_ind" value="Export" class="redBtn">
                        </div>

                        <div class="grid_6 dateArea clearfix marginNone" style="margin-bottom:0px!important;">      
                            <h2>Sales Representative</h2>
                            <select name="sales_id[]" id="sales_id" multiple="multiple" style="margin-bottom:10px;">
                                <option value="" disabled="">Select Sales Representative</option>
                                <?php
                                $results = User::getAllSalesRepresentative();
                                foreach ($results as $result) {
                                    ?>
                                    <option <?php echo (isset($_POST['sales_id']) && in_array($result['id'], $_POST['sales_id'])) ? 'selected="selected"' : ''; ?> value="<?php echo $result['id']; ?>"><?php echo $result['first_name'] . ' ' . $result['last_name']; ?></option>
                                <?php } ?>                              
                            </select>
                            <input type="submit" name="sales_export_ind" value="Export" class="redBtn">
                        </div>

                    </form>                   
                </div>              
            </div>
            <!-- ( tab2 end ) --> 


            <div id="tab3" class="unique" style="display: block;">
                <div class="filterBox clearfix">
                    <h2>Export Users By Role And Date Range</h2>
                    <form action="export_reporting.php" name="role_reporting" id="role_reporting" method="POST">
                        <div class="grid_6 dateArea marginNone" style="margin-bottom:0px!important; width: 53% !important;">                           
                            <div class="dateArea marginNone">
                                <p class="dateIcon" style="width: 160px;">                   
                                    <input type="number" name="m_strat_month_role" id="m_strat_month_role" class="m_date" placeholder="mm" value="<?php echo $_POST['m_strat_month_role']; ?>" style="width: 38px;">
                                    <input type="number" name="m_strat_day_role" id="m_strat_day_role" class="m_date" placeholder="dd" value="<?php echo $_POST['m_strat_day_role']; ?>" style="width: 30px;">
                                    <input type="number" name="m_strat_year_role" id="m_strat_year_role" class="m_date" placeholder="yy" value="<?php echo $_POST['m_strat_year_role'] != "" ? $_POST['m_strat_year_role'] : '2015'; ?>" style="width: 47px;">
                                    <input type="hidden" name="role_start_date" id="role_start_date" value="<?php echo $_POST['role_start_date']; ?>">
                                    <input type="hidden" id="calender_image_role_start" class=""/>                                    
                                </p>
                                <label>to</label>
                                <p class="dateIcon" style="width: 160px;">
                                    <input type="number" name="m_end_month_role" id="m_end_month_role" class="m_date" placeholder="mm" value="<?php echo $_POST['m_end_month_role']; ?>" style="width: 38px;">
                                    <input type="number" name="m_end_day_role" id="m_end_day_role" class="m_date" placeholder="dd" value="<?php echo $_POST['m_end_day_role']; ?>" style="width: 30px;">
                                    <input type="number" name="m_end_year_role" id="m_end_year_role" class="m_date" placeholder="yy" value="<?php echo $_POST['m_end_year_role'] != "" ? $_POST['m_end_year_role'] : '2015'; ?>" style="width: 47px;">                    
                                    <input type="hidden" name="role_end_date" id="role_end_date" value="<?php echo $_POST['role_start_date']; ?>">
                                    <input type="hidden" id="calender_image_role_end" class=""/>
                                </p>
                                <select tabindex="8" name="role_type">
                                    <option value="">Select Role type</option>                                 
                                    <option value="3">Manager</option>                                 
                                    <option value="4">Sales Representative</option>                                 
                                </select>  

                            </div>                            
                            <input type="submit" name="role_type_export" value="Export" class="redBtn" style="margin-top: -28px;">                            
                        </div>
                    </form>                 
                </div>          
            </div>

            <div id="tab4" class="unique" style="display: block;">
                <div class="filterBox clearfix">
                    <h2>Export All Users</h2>
                    <form action="export_reporting.php" name="all_users_reporting" id="all_users_reporting" method="POST">
                        <div class="grid_6 dateArea marginNone" style="margin-bottom:0px!important;">                            
                            <div class="dateArea marginNone">
                                <p class="dateIcon" style="width: 160px;">                   
                                    <input type="number" name="m_strat_month_all" id="m_strat_month_all" class="m_date" placeholder="mm" value="<?php echo $_POST['m_strat_month_all']; ?>" style="width: 38px;">
                                    <input type="number" name="m_strat_day_all" id="m_strat_day_all" class="m_date" placeholder="dd" value="<?php echo $_POST['m_strat_day_all']; ?>" style="width: 30px;">
                                    <input type="number" name="m_strat_year_all" id="m_strat_year_all" class="m_date" placeholder="yy" value="<?php echo $_POST['m_strat_year_all'] != "" ? $_POST['m_strat_year_all'] : '2015'; ?>" style="width: 47px;">
                                    <input type="hidden" name="all_users_start_date" id="all_users_start_date" value="<?php echo $_POST['all_users_start_date']; ?>">
                                    <input type="hidden" id="calender_image_all_user_start" class=""/>                                    
                                </p>
                                <label>to</label>
                                <p class="dateIcon" style="width: 160px;">
                                    <input type="number" name="m_end_month_all" id="m_end_month_all" class="m_date" placeholder="mm" value="<?php echo $_POST['m_end_month_all']; ?>" style="width: 38px;">
                                    <input type="number" name="m_end_day_all" id="m_end_day_all" class="m_date" placeholder="dd" value="<?php echo $_POST['m_end_day_all']; ?>" style="width: 30px;">
                                    <input type="number" name="m_end_year_all" id="m_end_year_all" class="m_date" placeholder="yy" value="<?php echo $_POST['m_end_year_all'] != "" ? $_POST['m_end_year_all'] : '2015'; ?>" style="width: 47px;">                    
                                    <input type="hidden" name="all_users_end_date" id="all_users_end_date" value="<?php echo $_POST['all_users_start_date']; ?>">
                                    <input type="hidden" id="calender_image_all_user_end" class=""/>
                                </p>
                                <input type="submit" name="all_users_export" value="Export" class="redBtn" style="margin-top: -12px;">                                                          
                            </div>                                          
                        </div>
                    </form>                 
                </div>          
            </div>

        </div>
        <!-- ( TABS CONTAINER END ) --> 
    </div>
    <!-- ( TABS END ) --> 
</section>

<?php include('includes/footer.php'); ?>   