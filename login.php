<?php
include('includes/top.php');
if (Session::get('login')) {
    Redirect::to('index.php');
}
//Check posted data if available
if (Input::exists()) {
    //Instantiate valdiate class
    $validate = new Validate();
    //Create rules for validatain
    $validation = $validate->check($_POST, array(
        'username' => array(
            'label' => 'Username',
            'required' => true,
        ),
        'password' => array(
            'label' => 'Password',
            'required' => true
        )
    ));
    //if validation is passed
    $msg = '';
    if ($validation->passed()) {
        //get username
        $username = $_POST['username'];
        //get password
        $password = $_POST['password'];
        //check if username is exists or not
        if (User::checkByUsername($username) > 0 && User::getPasswordByUserName($username) == getMd5($password)) {
            Session::put('level', User::getLevelByUserName($username));
            Session::put('user_id', User::getIdByUserName($username));
            Session::put('login', true);
            if (Session::get('level') == 3 || Session::get('level') == 4) {
                Redirect::to('viewMyday.php');
            } else {
                Redirect::to('dashboard.php');
            }
        } else {
            $msg .= 'Username Or Password is not valid.';
        }
    } else {
        foreach ($validation->errors() as $error) {
            $msg .= $error . "<br>";
        }
    }
}
?>
<!DOCTYPE html>
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="no-js ie9" lang="en"> <![endif]-->
<html class="no-js" lang="en"> 
    <head>
        <meta charset="utf-8">
        <title>Sales Time Optimizer :: Login</title>
        <meta name="author" content="" />   
        <meta name="description" content="" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- ( CSS LINKS START ) -->
        <link href="favicon.ico" type="image/x-icon" rel="icon">
        <link href="favicon.ico" type="image/x-icon" rel="shortcut icon">
        <link href="assets/css/default.css" type="text/css" rel="stylesheet" /> 
        <link href="assets/css/responsive.css" type="text/css" rel="stylesheet" /> 
        <!-- ( CSS LINKS END ) -->
        <!-- ( SCRIPT LIBRARY START ) -->        
        <script type="text/javascript" src="assets/javascripts/jquery-2.1.1.js"></script>
<!--        <script type="text/javascript" src="assets/javascripts/jquery-migrate-1.1.1.js"></script>  -->
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--  ( SCRIPT LIBRARY END )  --> 
    </head>
    <body>
        <!-- ( MAIN CONTAINER BEGIN ) -->
        <div id="mainContainer" class=" clearfix">
            <!-- ( CONTAINER 12 BEGIN ) -->
            <div class="container_12">
                <!-- ( CONTENT BOX BEGIN ) -->
                <div class="contentBox grid_4 marginAuto loginBox">
                    <header class="header">
                        <div class="topTitle" style="left:0; top:0;">
                            <a href="http://www.salesglobe.com"><img src="assets/images/logo.gif" alt="" style="height: 20px;" /></a>
                            <h2 style="margin-top:8px;">Sales Time Optimizer</h2>
                        </div>
                        <!-- ( .logo end ) -->
                    </header>
                    <!-- ( .header end ) -->
                    <section class="content clearfix">
                        <?php if (!empty($msg) && $msg != "") { ?>
                            <p style="color:#F00; font-weight:bold;">  <?php echo $msg; ?></p>
                        <?php } ?>
                        <h1>Login</h1>
                        <form action="" name="loginForm" id="loginForm" method="post">
                            <div class="clearfix">
                                <label for="Username" class="req">Username</label>
                                <input type="text" name="username" id="Username" value="<?php echo Input::get('username'); ?>">
                            </div>
                            <div class="clearfix">
                                <label for="Password" class="req">Password</label>
                                <input type="password" name="password" id="Password" value="<?php echo Input::get('password'); ?>">
                            </div>
                            <p class="floatLeft"><a href="forgotpassword.php">Forgot your password?</a></p>
                            <a href="http://www.salesglobe.com" class="redBtn" style="margin-left:10px;">Exit</a>
                            <input type="submit" name="submit" id="submit" value="Login">
                        </form>
                    </section>
                    <!-- ( .content end ) -->
                </div>
                <!-- ( CONTENT BOX END ) -->
            </div>
            <!-- ( CONTAINER 12 END ) -->
        </div>
        <!-- ( MAIN CONTAINER BEGIN ) -->
        <!-- ( PLUGIN START ) -->
<!--        <script type="text/javascript" src="assets/javascripts/configuration.js"></script>-->
        <!-- ( PLUGIN END ) -->
    </body>
</html>
