<?php
$page_title = 'Manager Dashboard';
include('includes/top.php');
//check login session  if $_Session['login'] is true or false
//Session::get class is in classes/Session.php
if (!Session::get('login')) {
    //if Session is not true the it will redirect to index.php
    //Redirect::to class is in classes/Redirect.php
    Redirect::to('index.php');
}
// if not manager will redirect to index page
if (Session::get('level') != 3) {
    Redirect::to('index.php');
}
// Get id of current user logged in
$manager_id = Session::get('user_id');
if (isset($_POST['go']) == 'View') {
    $level_id = 4;
    if (!isset($_POST['days_select'])) {
        $start_date = $_POST['start_date'];
        $start_date = date("Y-m-d", strtotime($start_date));
        //$end_date = $_POST['end_date'];
        //$end_date = date("Y-m-d", strtotime($end_date));
        $end_date = $_POST['end_date'];
        $end_date = date("Y-m-d", strtotime($end_date));
        $date_array = explode('-', $end_date);
        $month = $date_array[0];
        $date = $date_array[1];
        $year = $date_array[2];
    }

    //print_r($date_array);
    //Check date format
    //$end_date = date("Y-m-d", mktime(0, 0, 0, $month, $date, $year));
    $err = '';
    if (isset($_POST['manager_id'])) {
        $users = $_POST['manager_id'];
    } else {
        $users = 0;
    }


    if (isset($_POST['days_select'])) {
        
    } else {
        if ($_POST['m_strat_month'] == "" || $_POST['m_strat_day'] == "" || $_POST['m_strat_year'] == "" || $_POST['m_end_month'] == "" || $_POST['m_end_day'] == "" || $_POST['m_end_year'] == "") {
            $err = "Please select date range.<br><br>";
        }
    }

    if ($users == 0) {
        $err = "Please select sales representative.<br><br>";
    }
    if ($err == '') {
        // $level_id = 4;
        // $start_date = '00000-00-00';
        //$end_date = '00000-00-00';
        /* $totalHours = Reporting::getTotalHoursByDateRangeAndLevelIdAndManager($start_date, $end_date, $level_id, $manager_id);
          $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndLevelAndManager($start_date, $end_date, $level_id, $manager_id); */
        //$totalHours = Reporting::getTotalHoursByDateRangeAndUserId($start_date, $end_date, $user_id);
        // $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndUserId($start_date, $end_date, $user_id); 
        $submitted = 1;
    } else {
        $success_m = "<p style='color: #e80702; font-weight: bold;'>" . $err . "</p>";
    }
}
include('includes/header.php');
?>
<script>
    $(document).ready(function () {


        $('#button_export_report').click(function () {
            if ($('.hideField_s').is(':checked')) {
                if ($('#representative_id_box').val() == null) {
                    fancyAlert('Please select Sales Representative');
                    return false;
                }
            } else {
                var m_strat_month = $('#m_strat_month').val();
                var m_strat_day = $('#m_strat_day').val();
                var m_strat_year = $('#m_strat_year').val();
                var m_end_month = $('#m_end_month').val();
                var m_end_day = $('#m_end_day').val();
                var m_end_year = $('#m_end_year').val();


                if (!m_strat_month || !m_strat_day || !m_strat_year || !m_end_month || !m_end_day || !m_end_year) {
                    fancyAlert('Please select valid date range');
                    return false;
                }
                if ($('#representative_id_box').val() == null) {
                    fancyAlert('Please select Sales Representative');
                    return false;
                }
            }
        });

        $('#button_export_report').click(function () {
            $('.sales_rep_form').attr('action', 'export_reporting.php');
        });

        $("#calender_image_viewhistory1").datepicker({
            showOn: "both",
            buttonImage: "assets/images/calendar.jpg",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            //dateFormat: "yy-mm-dd",
            dateFormat: "mm/dd/yy",
            yearRange: "-3:+0",
            onSelect: function (selectedDate, obj) {
                var date_array = selectedDate.split("/");
                var month = date_array[0];
                var days = date_array[1];
                var year = date_array[2];
                $('#m_strat_month').val(month);
                $('#m_strat_day').val(days);
                $('#m_strat_year').val(year);
                $('#start_date').val(selectedDate);
            }
        });

        $("#calender_image_viewhistory2").datepicker({
            showOn: "both",
            buttonImage: "assets/images/calendar.jpg",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            //dateFormat: "yy-mm-dd",
            dateFormat: "mm/dd/yy",
            yearRange: "-3:+0",
            onSelect: function (selectedDate, obj) {
                var date_array = selectedDate.split("/");
                var month = date_array[0];
                var days = date_array[1];
                var year = date_array[2];
                $('#m_end_month').val(month);
                $('#m_end_day').val(days);
                $('#m_end_year').val(year);
                $('#end_date').val(selectedDate);
            }
        });

        if ($('.hideField_s').is(':checked')) {
            $('#days_select_s').attr('disabled', false);
            $('#start_date').attr('disabled', true);
            $('#end_date').attr('disabled', true);
            $('#m_strat_month').attr('disabled', true);
            $('#m_strat_day').attr('disabled', true);
            $('#m_strat_year').attr('disabled', true);
            $('#m_end_month').attr('disabled', true);
            $('#m_end_day').attr('disabled', true);
            $('#m_end_year').attr('disabled', true);
        } else {

            $('#days_select_s').attr('disabled', true);
            $('#start_date').attr('disabled', false);
            $('#end_date').attr('disabled', false);
            $('#m_strat_month').attr('disabled', false);
            $('#m_strat_day').attr('disabled', false);
            $('#m_strat_year').attr('disabled', false);
            $('#m_end_month').attr('disabled', false);
            $('#m_end_day').attr('disabled', false);
            $('#m_end_year').attr('disabled', false);

        }

        $(".hideField_s").on('click', function () {
            if ($(this).is(':checked')) {
                $('#days_select_s').attr('disabled', false);
                $('#start_date').attr('disabled', true);
                $('#end_date').attr('disabled', true);
                $('#m_strat_month').attr('disabled', true);
                $('#m_strat_day').attr('disabled', true);
                $('#m_strat_year').attr('disabled', true);
                $('#m_end_month').attr('disabled', true);
                $('#m_end_day').attr('disabled', true);
                $('#m_end_year').attr('disabled', true);
            } else {

                $('#days_select_s').attr('disabled', true);
                $('#start_date').attr('disabled', false);
                $('#end_date').attr('disabled', false);
                $('#m_strat_month').attr('disabled', false);
                $('#m_strat_day').attr('disabled', false);
                $('#m_strat_year').attr('disabled', false);
                $('#m_end_month').attr('disabled', false);
                $('#m_end_day').attr('disabled', false);
                $('#m_end_year').attr('disabled', false);

            }
        });



        $(".m_date").change(function () {
            if ($(this).attr('id') == 'm_strat_month') {

                if ($(this).val() > 12) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }
            } else if ($(this).attr('id') == 'm_strat_day') {

                if ($(this).val() > daysInMonth($('#m_strat_month').val(), $('#m_strat_year').val())) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }

            } else if ($(this).attr('id') == 'm_strat_year') {

                if ($(this).val() > 2015) {
                    $(this).val('2015');
                } else if ($(this).val() < 2012) {
                    $(this).val('2015');
                }

            } else if ($(this).attr('id') == 'm_end_month') {

                if ($(this).val() > 12) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }

            } else if ($(this).attr('id') == 'm_end_day') {

                if ($(this).val() > daysInMonth($('#m_end_month').val(), $('#m_end_year').val())) {
                    $(this).val('');
                } else if ($(this).val() < 1) {
                    $(this).val('');
                }

            } else if ($(this).attr('id') == 'm_end_year') {

                if ($(this).val() > 2015) {
                    $(this).val('2015');
                } else if ($(this).val() < 2012) {
                    $(this).val('2015');
                }

            }

            var m_strat_date = ($('#m_strat_month').val() + '/' + $('#m_strat_day').val() + '/' + $('#m_strat_year').val());
            $('#start_date').val(m_strat_date);

            var m_end_date = ($('#m_end_month').val() + '/' + $('#m_end_day').val() + '/' + $('#m_end_year').val());
            $('#end_date').val(m_end_date);

        });

    });
    function daysInMonth(month, year) {
        return new Date(year, month, 0).getDate();
    }
</script>
<style>
    img.ui-datepicker-trigger {
        margin: -5px 0px 0px 0px;
    }
</style>
<section class="mainContent viewMyDashboard clearfix">
    <div class="filterBox clearfix">
        <h2>Select a Date Range</h2>    
        <form action="" name="ViewUnsavedForm" class="sales_rep_form" id="ViewUnsavedForm" method="POST">
            <?php if (!empty($success_m)) { ?>
                <p style="color:green; font-weight: bold;"><?php echo $success_m; ?></p>
            <?php } ?>
            <div class="grid_12 dateArea clearfix marginNone" style="width:100%">
<!--                <p class="dateIcon"><input type="text" name="start_date" id="start_date" class="datepicker" placeholder="Start Date"></p>
                <label>to</label>
                <p class="dateIcon"><input type="text" name="end_date" id="end_date" class="datepicker" placeholder="End Date"></p>-->

                <p class="dateIcon" style="width: 160px;">                   
                    <input type="number" name="m_strat_month" id="m_strat_month" class="m_date" placeholder="mm" value="<?php echo $_POST['m_strat_month']; ?>" style="width: 38px;">
                    <input type="number" name="m_strat_day" id="m_strat_day" class="m_date" placeholder="dd" value="<?php echo $_POST['m_strat_day']; ?>" style="width: 30px;">
                    <input type="number" name="m_strat_year" id="m_strat_year" class="m_date" placeholder="yy" value="<?php echo $_POST['m_strat_year'] != "" ? $_POST['m_strat_year'] : '2015'; ?>" style="width: 47px;">
                    <input type="hidden" name="start_date" id="start_date" value="<?php echo $_POST['start_date']; ?>">
                    <input type="hidden" id="calender_image_viewhistory1"  class="datepicker_view_my_history"/>
                    <?php //echo $totalHours;  ?>
                </p>
                <label>to</label>
                <p class="dateIcon" style="width: 160px;">
                    <input type="number" name="m_end_month" id="m_end_month" class="m_date" placeholder="mm" value="<?php echo $_POST['m_end_month']; ?>" style="width: 38px;">
                    <input type="number" name="m_end_day" id="m_end_day" class="m_date" placeholder="dd" value="<?php echo $_POST['m_end_day']; ?>" style="width: 30px;">
                    <input type="number" name="m_end_year" id="m_end_year" class="m_date" placeholder="yy" value="<?php echo $_POST['m_end_year'] != "" ? $_POST['m_end_year'] : '2015'; ?>" style="width: 47px;">                    
                    <input type="hidden" name="end_date" id="end_date" value="<?php echo $_POST['end_date']; ?>">
                    <input type="hidden" id="calender_image_viewhistory2"  class="datepicker_view_myhistory"/>
                </p>

                <p class="dateIcon">
                    <?php if (isset($_POST['days_select'])) { ?>
                        <input type="checkbox" name="hideField" value="true" id="" class="hideField_s" checked="checked">
                    <?php } else { ?>
                        <input type="checkbox" name="hideField" value="true" id="" class="hideField_s">
                    <?php } ?>
                </p>
                <div class="dateArea clearfix marginNone">
                    <select name="days_select" disabled="disabled" id="days_select_s" style="width: 190px;">
                        <option value="7" <?php echo ($_POST['days_select']) == 7 ? 'selected="selected"' : ''; ?>>Last 7 Days</option>
                        <option value="30" <?php echo ($_POST['days_select']) == '30' ? 'selected="selected"' : ''; ?>>Last 30 Days</option>
                        <option value="60" <?php echo ($_POST['days_select']) == '60' ? 'selected="selected"' : ''; ?>>Last 60 Days</option>
                        <option value="90" <?php echo ($_POST['days_select']) == '90' ? 'selected="selected"' : ''; ?>>Last 90 Days</option>
                        <option value="6m" <?php echo ($_POST['days_select']) == '6m' ? 'selected="selected"' : ''; ?>>Last 6 Months</option>
                        <option value="12m" <?php echo ($_POST['days_select']) == '12m' ? 'selected="selected"' : ''; ?>>Last 12 Months</option>
                    </select>                                
                </div>
                <?php
                $user_id = $_SESSION['user_id'];
                $id = isset($user_id) ? $user_id : '';
                $results = User::getAllSalesRepresentative($id);
                ?>
                <select name="manager_id[]" id="representative_id_box"  multiple="multiple" style="margin-bottom:10px;">
                    <option value="" disabled="disabled">Select Sales Representative </option>
                    <?php
// Get all manager by getAllManagers() class
//getAllManager function is in classes/User.php
//$level=isset(Session::get('level')) ? Session::get('level') : '';
                    foreach ($results as $result) {
                        ?>
                        <option <?php echo (isset($_POST['manager_id']) && in_array($result['id'], $_POST['manager_id'])) ? 'selected="selected"' : ''; ?> value="<?php echo $result['id']; ?>"><?php echo $result['first_name'] . ' ' . $result['last_name']; ?></option>  
                    <?php } ?>
                </select>
                <input type="submit" name="go" value="View" class="redBtn goBTN">
                <input type="submit" name="representative_export_manager" value="Export" id="button_export_report" class="redBtn goBTN">
            </div>
        </form>
        <div class="filterBtn">
            <?php if (isset($totalHours) && $totalHours != 0) { ?>
                <p style="background-color: red;padding: 10px;width: 240px;height: 16px;float: right;color: white;">Total Hours for date Range Selected: <?php echo $totalHours; ?></p>   
            <?php } ?>
            <!--            <div class="clearfix"><a href="#meetingHourManagerPopup1" name="ViewUnsaved" id="ViewUnsaved" class="darkBtn fancybox clearfix">Click Here To Export All Sales Representative Data Into CSV</a></div>
                        <div class="clearfix"><a href="#meetingHourManagerPopup2" name="ViewUnsaved" id="ViewUnsaved" class="darkBtn fancybox clearfix">Click Here To Export Individual Sales Representative Data Into CSV</a></div>-->
        </div>
        <!-- ( POPUP CONTENT BEGIN ) -->
        <div id="meetingHourManagerPopup1" class="popupBox width_240">
            <form action="export.php" method="POST">
                <h3>Select a Date Range </h3>
                <p class="dateIcon"><input type="text" name="start_date" class="datepicker" placeholder="Start Date"></p>
                <p class="dateIcon"><input type="text" name="end_date" class="datepicker" placeholder="End Date">
                    <input type="hidden" name="byLevel" value="yes">
                </p>
                <input type="submit" name="exportAllSalesRep" value="Export" >
            </form>
        </div>
        <div id="meetingHourManagerPopup2" class="popupBox width_240">
            <form action="export.php" method="POST">
                <h3>Select a Date Range</h3>
                <p class="dateIcon"><input type="text" name="start_date" class="datepicker" placeholder="Start Date"></p>
                <p class="dateIcon"><input type="text" name="end_date" class="datepicker" placeholder="End Date">
                    <input type="hidden" name="individual" value="yes1">
                </p>
                <div>
                    <?php
                    $user_id = $_SESSION['user_id'];
                    $id = isset($user_id) ? $user_id : '';
                    $results = User::getAllSalesRepresentative($id);
                    ?>
                    <select name="manager_id">
                        <option value="">Select Sales Representative </option>
                        <?php
// Get all manager by getAllManagers() class
//getAllManager function is in classes/User.php
//$level=isset(Session::get('level')) ? Session::get('level') : '';
                        foreach ($results as $result) {
                            ?>
                            <option <?php echo ($_POST['representative_id'] == $result['id']) ? 'selected="selected"' : ''; ?> value="<?php echo $result['id']; ?>"><?php echo $result['first_name'] . ' ' . $result['last_name']; ?></option>  
                        <?php } ?>
                    </select>
                </div>
                <input type="submit" name="exportIndSalesRep" value="Export" >
            </form>
        </div>
        <!-- ( POPUP CONTENT END ) -->
    </div>
    <!-- ( .filterBox end ) -->
    <div class="tableSec summryTable clearfix">

        <?php if ($meetingHistoryData) {
            ?>
            <div class="summryTableHead clearfix">
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr class="mainTitle">
                            <th width="20%" align="left" class="tdCol-1"><span>Activity and Type Summary</span></th>
                            S                            <th width="8%"  align="center" class="tdCol-2"><span>Hours</span></th> 

                            <th align="left" class="tdCol-3 tdNone"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        <?php } ?>  <!-- ( .tableHead end ) -->
        <?php
        if ($submitted) {
            foreach ($users as $user_id) {
                if (isset($_POST['days_select'])) {
                    $totalHours = Reporting::getTotalHoursByDaysAndUserId($_POST['days_select'], $user_id);
                    $meetingHistoryData = Reporting::getMeetingHistoryByDaysAndUserId($_POST['days_select'], $user_id);
                    $userData = User::getFullNameByUserId($user_id);
                } else {
                    $totalHours = Reporting::getTotalHoursByDateRangeAndUserId($start_date, $end_date, $user_id);
                    $meetingHistoryData = Reporting::getMeetingHistoryByDateRangeAndUserId($start_date, $end_date, $user_id);
                    $userData = User::getFullNameByUserId($user_id);
                }
                ?>
                <?php
//                echo '<pre>';
//                print_r($meetingHistoryData);
//                //exit;
                ?>
                <div class="tableSec summryTable clearfix">
                    <?php if ($meetingHistoryData) {
                        ?>
                        <div class="summryTableHead clearfix">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <thead>
                                    <tr class="mainTitle">

                                        <th width="20%" style="width: 254px!important;" align="left" class="tdCol-1"><span>Activity Summary - [<?php echo $userData; ?>] [Total-Hours & Minutes - <?php echo time_format($totalHours); ?>]</span></th>                                       
                                        <th width="8%" style="width: 138px!important;" align="center" class="tdCol-2"><span>Hours & Minutes</span></th>
                                        <th width="8%" style="width: 129px!important;" class="tdCol-3 noBorderTop " align="center"><span>% of Total Hours & Minutes for Selected Date Range</span></th>
                                        <th width="8%" style="width: 135px!important;" align="center" class="tdCol-2"><span>Difficulty Level</span></th>
<!--                                        <th width="8%" style="width: 135px!important;" align="center" class="tdCol-2"><span style="border: none;">Comments</span></th>-->
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    <?php } ?>
                    <!-- ( .tableHead end ) -->
                    <?php
                    if ($meetingHistoryData) {
                        $csvdata[] = $meetingHistoryData;
                        foreach ($meetingHistoryData as $meeting) {

                            if (isset($_POST['days_select'])) {
                                $day_selected = $_POST['days_select'];
                                $ActivityTypedatarating = Reporting::getActivityTypeHoursbyDays($day_selected, $user_id, $meeting['meeting_category']);
                            } else {
                                $ActivityTypedatarating = Reporting::getActivityTypeHours($start_date, $end_date, $user_id, $meeting['meeting_category']);
                            }
                            $rate_count_rating = 0;
                            foreach ($ActivityTypedatarating as $row_count_rating) {
                                $rate_count_rating += ($row_count_rating['Rate']);
                            }
                            ?>
                            <div class="percentageTable clearfix">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <thead>
                                        <tr class="subTitle">
                                            <th align="left" class="tdCol-1"><span><?php echo $meeting['name'] ?></span></th>                                           
                                            <th align="center" class="tdCol-2"><span><?php echo time_format($meeting['hours']); ?></span></th>
                                            <th align="center" class="tdCol-2"><span><?php echo round($meeting['hours'] / $totalHours * 100, 2) . '%'; ?></span></th>
                                            <th align="center" class="tdCol-2"><span><?php echo number_format($rate_count_rating / sizeof($ActivityTypedatarating), 2); ?></span></th>
<!--                                            <th align="center" class="tdCol-2"><span>&nbsp;</span></th>-->
                                        </tr>
                                    </thead>
                                </table>
                                <!-- ( .tableHead end ) -->
                                <div class="grid_4 leftTableInfo">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <?php
                                            if ($submitted) {
                                                if (isset($_POST['days_select'])) {
                                                    $ActivityTypedata = Reporting::getActivityTypeHoursbyDays($_POST['days_select'], $user_id, $meeting['meeting_category']);
                                                    $ActivityTypedatacomments = Reporting::getActivityTypeCommentsbyDays($_POST['days_select'], $user_id, $meeting['meeting_category']);
                                                } else {
                                                    $ActivityTypedata = Reporting::getActivityTypeHours($start_date, $end_date, $user_id, $meeting['meeting_category']);
                                                    $ActivityTypedatacomments = Reporting::getActivityTypeComments($start_date, $end_date, $user_id, $meeting['meeting_category']);
                                                }
                                            } else {
                                                $ActivityTypedata = Reporting::getActivityTypeHoursByLevelId($start_date, $end_date, $level_id, $meeting['meeting_category']);
                                            }
                                            $csvdata[] = $ActivityTypedata;
                                            $ActivityTypeNameData = Meeting::getActivityTypeByMeetingCategoryId($meeting['meeting_category']);
                                            $other_fill = '';
                                            $other_value_fill = '';
                                            foreach ($ActivityTypedata as $activity_type) {
                                                $activity_type_id[] = $activity_type['id'];
                                                $activeper = $activity_type['hours'] / $meeting['hours'] * 100;
                                                if ($activity_type['name'] == 'Other') {
                                                    $other_fill = $activity_type['name'];
                                                    $other_value_fill = round($activeper, 2) . '%';
                                                } else {
                                                    ?>
                                                    <tr>
                                                        <td class="tdCol-1 noBorderTop"><?php echo $activity_type['name']; ?>&nbsp;<?php //echo round($activeper, 2) . '%';                   ?></td>
                                                        <td class="tdCol-2 noBorderTop" align="center"><?php echo time_format($activity_type['hours']); ?></td>                                                        
                                                        <td class="tdCol-2 noBorderTop" align="center"><?php echo round($activeper, 2) . '%'; ?></td>                                                        
                                                        <td class="tdCol-2 noBorderTop" align="center"><?php echo number_format($activity_type['Rate'], 2); ?></td>                                                                       
<!--                                                        <td class="tdCol-2 noBorderTop" align="center">-->
                                                            <?php /*
                                                            foreach ($ActivityTypedatacomments as $ActivityTypedatacomment) :
                                                                if ($activity_type['id'] == $ActivityTypedatacomment['Activity_type']) {
                                                                    ?>
                                                                    <span class="comments_span"><?php echo $ActivityTypedatacomment['Comments']; ?></span>
                                                                <?php } ?>
                                                            <?php endforeach; */?>  
<!--                                                        </td>                                                                       -->
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                            <?php
                                            $ActivityTypeAllNamesData = Reporting::getActivityTypeNotInIDs(implode(',', $activity_type_id), $meeting['meeting_category']);
                                            $activity_type_id = '';
                                            $other_name = '';
                                            if ($ActivityTypeAllNamesData) {
                                                foreach ($ActivityTypeAllNamesData as $activity_name) {
                                                    if ($activity_name['name'] == 'Other') {
                                                        $other_name = $activity_name['name'];
                                                    } else {
                                                        ?>
                                                        <!--
                                                                                                                <tr>
                                                                                                                    <td class="tdCol-1"><?php echo $activity_name['name']; ?></td>                                                                                                                   
                                                                                                                    <td align="center" class="tdCol-2" ></td>
                                                                                                                    <td align="center" class="tdCol-2" ></td>
                                                                                                                    <td align="center" class="tdCol-2" ></td>
                                                                                                                                                <td align="center" class="tdCol-2" ></td>
                                                                                                                                                                            
                                                                                                                </tr>-->
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                            <?php if ($other_name != '' && $other_value_fill == '') {
                                                ?>
                                        <!--                                                <tr>
                                                                                            <td class="tdCol-1"><?php echo $other_name; ?></td>
                                                                                            <td align="center" class="tdCol-2" ></td>
                                                                                            <td class="tdCol-3 tdNone"></td>
                                                                                        </tr>-->
                                            <?php } ?>
                                            <?php if ($other_fill != '' && $other_value_fill != '') {
                                                ?>
                                        <!--                                                <tr>
                                                                                            <td class="tdCol-1 noBorderTop"><?php echo $other_fill; ?></td>
                                                                                            <td class="tdCol-2 noBorderTop" align="center"><?php echo $other_value_fill; ?></td>
                                                                                            <td class="tdCol-3 noBorderTop tdNone"></td>
                                                                                        </tr>-->
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- ( .leftTableInfo end ) -->
                                <div class="grid_8 percentageSec">
                                    <div class="mobilePercentage clearfix">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <thead>
                                                <tr class="subTitle">
                                                    <th align="left" class="tdCol-1"><span>Average for the Date Range: 11%</span></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <!-- ( .tableHead end ) -->
                                    <?php /* ?>
                                      <h3>Percent of Time for This Activity</h3>
                                      <div class="percentageLiquid">
                                      <div class="percentageRow clearfix">
                                      <table>
                                      <tbody>
                                      <?php
                                      if ($submitted) {
                                      $AccountTypeData = Reporting::getAccountTypeHours($start_date, $end_date, $user_id, $meeting['meeting_category']);
                                      } else {
                                      $AccountTypeData = Reporting::getAccountTypeHoursByLevelId($start_date, $end_date, $level_id, $meeting['meeting_category']);
                                      }
                                      $csvdata[] = $AccountTypeData;
                                      $AccountTypeNameData = Meeting::getAccountType();
                                      $other_fill = '';
                                      $other_value_fill = '';
                                      foreach ($AccountTypeData as $account_type) {
                                      $account_type_id[] = $account_type['id'];
                                      if ($account_type['name'] == 'Other') {
                                      $other_fill = $account_type['name'];
                                      $other_value_fill = round($account_type['hours'] / $meeting['hours'] * 100, 2) . '%';
                                      } else {
                                      ?>
                                      <tr>
                                      <td width="90%"><?php echo $account_type['name']; ?></td>
                                      <td width="10%"><?php echo round($account_type['hours'] / $meeting['hours'] * 100, 2) . '%'; ?></td>
                                      </tr>
                                      <?php
                                      }
                                      }
                                      ?>
                                      <?php
                                      $AccountTypeAllNameData = Reporting::getAccountTypeNotInIDs(implode(',', $account_type_id));
                                      $account_type_id = '';
                                      $other_name = '';
                                      if ($AccountTypeAllNameData) {
                                      foreach ($AccountTypeAllNameData as $accountName) {
                                      if ($accountName['name'] == 'Other') {
                                      $other_name = $accountName['name'];
                                      } else {
                                      ?>
                                      <tr>
                                      <td><?php echo $accountName['name']; ?></td>
                                      <td>&nbsp;</td>
                                      </tr>
                                      <?php } ?>
                                      <?php
                                      }
                                      }
                                      ?>
                                      <?php if ($other_name != '' && $other_value_fill == '') {
                                      ?>
                                      <tr>
                                      <td><?php echo $other_name; ?></td>
                                      <td>&nbsp;</td>
                                      </tr>
                                      <?php } ?>
                                      <?php
                                      if ($other_fill != '' && $other_value_fill != '') {
                                      ?>
                                      <tr>
                                      <td width="90%"><?php echo $other_fill; ?></td>
                                      <td width="10%"><?php echo $other_value_fill; ?></td>
                                      </tr>
                                      <?php }
                                      ?>
                                      </tbody>
                                      <tfoot>
                                      <tr>
                                      <td>Total</td>
                                      <td>100%</td>
                                      </tr>
                                      </tfoot>
                                      </table>
                                      <!-- ( .row end ) -->
                                      <table>
                                      <tbody>
                                      <?php
                                      if ($submitted) {
                                      $CustomerStatusData = Reporting::getCustomerStatusHours($start_date, $end_date, $user_id, $meeting['meeting_category']);
                                      } else {
                                      $CustomerStatusData = Reporting::getCustomerStatusHoursByLevelId($start_date, $end_date, $level_id, $meeting['meeting_category']);
                                      }
                                      $csvdata[] = $CustomerStatusData;
                                      $other_fill = '';
                                      $other_value_fill = '';
                                      foreach ($CustomerStatusData as $customer_status) {
                                      $customer_status_id[] = $customer_status['id'];
                                      if ($customer_status['name'] == "Other") {
                                      $other_fill = $customer_status['name'];
                                      $other_value_fill = round($customer_status['hours'] / $meeting['hours'] * 100, 2) . '%';
                                      } else {
                                      ?>
                                      <tr>
                                      <td width="90%"><?php echo $customer_status['name']; ?></td>
                                      <td width="10%"><?php echo round($customer_status['hours'] / $meeting['hours'] * 100, 2) . '%'; ?></td>
                                      </tr>
                                      <?php } ?>
                                      <?php } ?>
                                      <?php
                                      $CustomerStatusAllNameData = Reporting::getCustomerStatusNotInIDs(implode(',', $customer_status_id));
                                      $customer_status_id = '';
                                      //print_r($CustomerStatusAllNameData);
                                      $other_name = '';
                                      if (is_array($CustomerStatusAllNameData) and count($CustomerStatusAllNameData) > 0) {
                                      foreach ($CustomerStatusAllNameData as $CustomerStatusName) {
                                      if ($CustomerStatusName['name'] == 'Other') {
                                      $other_name = $CustomerStatusName['name'];
                                      //$other_value=round($account_type['hours'] / $meeting['hours'] * 100, 2) . '%';
                                      } else {
                                      ?>
                                      <tr>
                                      <td><?php echo $CustomerStatusName['name']; ?></td>
                                      <td>&nbsp;</td>
                                      </tr>
                                      <?php
                                      }
                                      }
                                      }
                                      if ($other_name != '' && $other_value_fill == '') {
                                      ?>
                                      <tr>
                                      <td><?php echo $other_name; ?></td>
                                      <td>&nbsp;</td>
                                      </tr>
                                      <?php } ?>
                                      <?php if ($other_fill != '' and $other_value_fill != '') {
                                      ?>
                                      <tr>
                                      <td width="90%"><?php echo $other_fill; ?></td>
                                      <td width="10%"><?php echo $other_value_fill; ?></td>
                                      </tr>
                                      <?php }
                                      ?>
                                      </tbody>
                                      <tfoot>
                                      <tr>
                                      <td>Total</td>
                                      <td>100%</td>
                                      </tr>
                                      </tfoot>
                                      </table>
                                      <!-- ( .row end ) -->
                                      <table>
                                      <tbody>
                                      <?php
                                      if ($submitted) {
                                      $ProductServiceData = Reporting::getProductServiceHours($start_date, $end_date, $user_id, $meeting['meeting_category']);
                                      } else {
                                      $ProductServiceData = Reporting::getProductServiceHoursByLevelId($start_date, $end_date, $level_id, $meeting['meeting_category']);
                                      }
                                      $csvdata[] = $ProductServiceData;
                                      $other = '';
                                      $other_value = '';
                                      foreach ($ProductServiceData as $product_service) {
                                      $product_service_id[] = $product_service['id'];
                                      ?>
                                      <tr>
                                      <td width="90%"><?php echo $product_service['name']; ?></td>
                                      <td width="10%"><?php echo round($product_service['hours'] / $meeting['hours'] * 100, 2) . '%'; ?></td>
                                      </tr>
                                      <?php
                                      }
                                      ?>
                                      <?php
                                      $ProductStatusAllNameData = Reporting::getProductServiceNotInIDs(implode(',', $product_service_id));
                                      $product_service_id = '';
                                      $other = '';
                                      foreach ($ProductStatusAllNameData as $ProductStatusName) {
                                      ?>
                                      <tr>
                                      <td><?php echo $ProductStatusName['name']; ?></td>
                                      <td>&nbsp;</td>
                                      </tr>
                                      <?php } ?>
                                      </tbody>
                                      <tfoot>
                                      <tr>
                                      <td>Total</td>
                                      <td>100%</td>
                                      </tr>
                                      </tfoot>
                                      </table>
                                      <!-- ( .row end ) -->
                                      <table>
                                      <tbody>
                                      <?php
                                      if ($submitted) {
                                      $ContactModeData = Reporting::getContactModeHours($start_date, $end_date, $user_id, $meeting['meeting_category']);
                                      } else {
                                      $ContactModeData = Reporting::getContactModeHoursByLevelId($start_date, $end_date, $level_id, $meeting['meeting_category']);
                                      }
                                      $csvdata[] = $ContactModeData;
                                      $other_fill = '';
                                      $other_value_fill = '';
                                      foreach ($ContactModeData as $contact_mode) {
                                      $contact_mode_id[] = $contact_mode['id'];
                                      if ($contact_mode['name'] == "Other") {
                                      $other_fill = $contact_mode['name'];
                                      $other_value_fill = round($contact_mode['hours'] / $meeting['hours'] * 100, 2) . '%';
                                      } else {
                                      ?>
                                      <tr>
                                      <td width="90%"><?php echo $contact_mode['name']; ?></td>
                                      <td width="10%"><?php echo round($contact_mode['hours'] / $meeting['hours'] * 100, 2) . '%'; ?></td>
                                      </tr>
                                      <?php } ?>
                                      <?php } ?>
                                      <?php
                                      $ContactModeAllNameData = Reporting::getContactModeNotInIDs(implode(',', $contact_mode_id));
                                      $contact_mode_id = '';
                                      $other_name = '';
                                      if ($ContactModeAllNameData) {
                                      foreach ($ContactModeAllNameData as $ContactModeName) {
                                      if ($ContactModeName['name'] == 'Other')
                                      $other_name = $ContactModeName['name'];
                                      else {
                                      ?>
                                      <tr>
                                      <td><?php echo $ContactModeName['name']; ?></td>
                                      <td>&nbsp;</td>
                                      </tr>
                                      <?php } ?>
                                      <?php
                                      }
                                      }
                                      ?>
                                      <?php if ($other_name != '' && $other_value_fill == '') {
                                      ?>
                                      <tr>
                                      <td><?php echo $other_name; ?></td>
                                      <td>&nbsp;</td>
                                      </tr>
                                      <?php } ?>
                                      <?php if ($other_fill != '' && $other_value_fill != '') {
                                      ?>
                                      <tr>
                                      <td width="90%"><?php echo $other_fill; ?></td>
                                      <td width="10%"><?php echo $other_value_fill; ?></td>
                                      </tr>
                                      <?php } ?>
                                      </tbody>
                                      <tfoot>
                                      <tr>
                                      <td>Total</td>
                                      <td>100%</td>
                                      </tr>
                                      </tfoot>
                                      </table>
                                      <!-- ( .row end ) -->
                                      </div>
                                      <!-- ( .percentageRow end ) -->
                                      </div>
                                      <?php */ ?>
                                    <!-- ( .percentageLiquid end ) --> 
                                </div>
                                <!-- ( .percentageSec end ) --> 
                            </div>
                            <?php
                        }
                    } else if (empty($meetingHistoryData) && $submitted) {
                        ?>
                        <table cellpadding="0" border="0" cellspacing="0">
                            <thead>
                                <tr class="subTitle">
                                    <th align="center" class="tdCol-3 tdNone"><span><?php echo User::getFullNameByUserId($user_id); ?>  doesn't have any records</span></th>
                                </tr>
                            </thead>
                        </table>
                    <?php } ?>
                </div>
            <?php } // end err condition
            ?>
        <?php } ?>
    </div>
    <!-- ( .summryTable end ) -->
</section>         
<?php include('includes/footer.php'); ?>                 
<script>
    $(document).ready(function () {
        $("#end_date").change(function () {
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            if (end_date < start_date) {
                fancyAlert('Please select valid date range');
                $('#end_date').val('');
            }
        });
<?php if (isset($_POST['start_date'])) {
    ?>
            $("#start_date").datepicker("setDate", "<?php echo $_POST['start_date']; ?>");
    <?php
} else {
    ?>
            $("#start_date").datepicker("setDate", "");
<?php } ?>
<?php if (isset($_POST['end_date'])) {
    ?>
            $("#end_date").datepicker("setDate", "<?php echo $_POST['end_date']; ?>");
    <?php
} else {
    ?>
            $("#end_date").datepicker("setDate", "");
<?php } ?>
    });
</script>