<?php

include('includes/top.php');


if ($_POST['meeting_detail_id']) {
    $result = Meeting::getMeetingDetailsById($_POST['meeting_detail_id']);

    if ($result) {
        $meeting_id = $result['meeting_id'];

        $totalHours = Meeting::getTotalHoursByMeetingId($meeting_id);

        if ($totalHours) {
            echo $totalHours;
        } else {
            echo "error";
        }
    }
} else {

    $meeting_date = $_POST['meeting_date'];
    $meeting_date = date("Y-m-d", strtotime($meeting_date));
    $user_id = $_POST['user_id'];

    echo Meeting::getTotalTimeByDateAndUserId($meeting_date, $user_id);
}
?>
